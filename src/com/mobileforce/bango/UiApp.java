package com.mobileforce.bango;

import com.mobileforce.bango.manager.ClassManager;
import com.mobileforce.bango.services.*;
import com.mobileforce.bango.utility.AppPermissions;
import com.mobileforce.bango.utility.BBNotification;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.Properties;

import net.rim.blackberry.api.homescreen.HomeScreen;
import net.rim.device.api.system.Application;
import net.rim.device.api.system.ApplicationDescriptor;
import net.rim.device.api.system.ApplicationManager;
import net.rim.device.api.system.ApplicationManagerException;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.GlobalEventListener;
import net.rim.device.api.ui.UiApplication;

/**
 * This class extends the UiApplication class, providing a
 * graphical user interface.
 */
public class UiApp extends UiApplication implements GlobalEventListener{
	static Bitmap icon_normal = Bitmap.getBitmapResource(Properties._ICON_NORMAL);
	//static Bitmap icon_focus = Bitmap.getBitmapResource(Properties._ICON_FOCUS);
	static Bitmap icon_update = Bitmap.getBitmapResource(Properties._ICON_UPDATE);
	
	public UiApp(){
		//permission
		new AppPermissions().setPermissions();
		
		//lock orientation
//		DeviceProperties.setDisplayPortraitDirection();
		
		//reset data : test only
//		UserInfoManager.deleteUserInfo();
//		UserProfileManager.deleteUserProfile();
		
		startingApp();
		
		//run notification
		Application.getApplication().addGlobalEventListener(this);
		runBackgroundApp();
	}
	
	public static void main(String[] args) {
		if(args.length>0 && args[0].equals("notification")) {
			System.out.println("[-START BANGO with NotificationApplication-]");
			
			NotificationApplicationManager notificationApplication = new NotificationApplicationManager();
			notificationApplication.enterEventDispatcher();
		}else {
			UiApp uiApp = new UiApp();
			uiApp.enterEventDispatcher();
		}
	}
	
	public void runBackgroundApp() {
		invokeLater(new Runnable() {
			public void run() {
				try {
					System.out.println("ApplicationManager Bango Notification");
					ApplicationManager.getApplicationManager()
							.runApplication(
									new ApplicationDescriptor(
											ApplicationDescriptor.currentApplicationDescriptor(),
											new String[] { "notification" }), false);
				} catch (ApplicationManagerException e) {
					System.out.println("ApplicationManagerException"+ e.getMessage());
				}
			}
		});
	}
	
	void startingApp(){
		System.out.println("[-START BANGO-]");
		
		// Push a screen onto the UI stack for rendering.
        pushScreen(ClassManager.getInstance().getScreenManager().getSplashScreen());
	}
	
	//default icon
    public static void setIconDefault(){
    	try {
        	HomeScreen.updateIcon(icon_normal, 0);
            HomeScreen.setRolloverIcon(icon_normal, 0);
        } catch(Exception e) { }
    }
    
    //update icon
    public static void setIconUpdate(){
    	try {
    		HomeScreen.updateIcon(icon_update, 0);
    		HomeScreen.setRolloverIcon(icon_update, 0);
        } catch(Exception e) { }
    }
    
    public void activate(){
    	setIconDefault();
        BBNotification.clearAllNotification();
        
//        startingApp(); //re-start apps
    }

	public void eventOccurred(long guid, int data0, int data1, Object object0, Object object1) {
		// TODO Auto-generated method stub
	}
}
