package com.mobileforce.bango.configuration;

import com.mobileforce.bango.utility.ImageScaler;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.DeviceInfo;
import net.rim.device.api.system.Display;

public abstract class ModelConfig {
	static ModelConfig modelConfig = null;
	static String bbType = DeviceInfo.getDeviceName();
	
	static int devHeight = Display.getHeight();
	static int devWidth = Display.getWidth();
	
	public static ModelConfig getConfig() {
		if((devWidth == 320 && devHeight == 240) || bbType.startsWith("83") || bbType.startsWith("85") || bbType.startsWith("92") || bbType.equalsIgnoreCase("9320")){
				// low resolution screen -> landscape
				// 83xx, 85xx, 9220, 9320
				modelConfig = new Resolution320x240();
		}
		else if((devWidth == 480 && devHeight == 360) || bbType.startsWith("89") || bbType.startsWith("97") || bbType.equalsIgnoreCase("9360")){
				// medium resolution screen -> landscape
				// 89xx, 9360, 97xx
				modelConfig = new Resolution480x360();
		}
		else if((devWidth == 360 && devHeight == 480) || bbType.startsWith("95") || bbType.equalsIgnoreCase("9380") || bbType.equalsIgnoreCase("9800")){
				// medium resolution screen -> portrait
				// 9380, 95xx, 9800
				modelConfig = new Resolution360x480();
		}
		else if((devWidth == 640 && devHeight == 480) || bbType.startsWith("99")){
				// high resolution screen -> landscape
				// 9900, 9810L
				modelConfig = new Resolution640x480();
		}
		else if((devWidth == 800 && devHeight == 480) || bbType.startsWith("98")){
				// high resolution screen -> landscape
				// 9810L
				modelConfig = new Resolution800x480();
		}
		else if((devWidth == 480 && devHeight == 640) || bbType.equalsIgnoreCase("9810")){
				// high resolution screen -> portrait
				// 9810P
				modelConfig = new Resolution480x640();
		}
		else if((devWidth == 480 && devHeight == 800) || bbType.equalsIgnoreCase("9860")){
				// high wide resolution -> landscape/portrait
				// 9860P
				modelConfig = new Resolution480x800();
		}
		else{
				//default medium resolution
				modelConfig = new Resolution480x360();
		}

		return modelConfig;
	}
	
	public boolean isOrientationPortrait(){
		return devWidth > devHeight ? false : true;
	}

	// Font
	public abstract int getFontSuperSmallHeight();
	public abstract int getFontSmallHeight();
	public abstract int getFontMediumHeight();
	public abstract int getFontLargeHeight();
	public abstract int getFontSuperLargeHeight();
	
	// Margins/Edge value in pixels (auto scale)
	public abstract int getValue5px();
	public abstract int getValue8px();
	public abstract int getValue10px();
	
	// proportional scale
	public abstract int getProportionalScale();
	
	// icons
	public abstract int getIconWidth();
	public abstract int getIconHeight();
	
	// preview
	public abstract int getPreviewWidth();
	public abstract int getPreviewHeight();
	
	// big preview
	public abstract int getBigPreviewWidth();
	public abstract int getBigPreviewHeight();
	
	// badges 
	public abstract int getBadgeWidth();
	public abstract int getBadgeHeight();
	
	// image resource path
	public abstract String getImageResourcePath();
	
	
	public final String getImageResourcePathBackground = getImageResourcePath() + "background/";
	public final String getImageResourcePathButton = getImageResourcePath() + "button/";
	public final String getImageResourcePathForm = getImageResourcePath() + "form/";
	public final String getImageResourcePathIcon = getImageResourcePath() + "icon/";
	public final String getImageResourcePathLoading = getImageResourcePath() + "loading/";
	
	
	// generic use
	public Bitmap getBackgroundSplash() {
		if(isOrientationPortrait()){
			if(bbType.equalsIgnoreCase("9860")){
				return ImageScaler.resizeBitmapARGB(Bitmap.getBitmapResource(getImageResourcePathBackground+"splash_P.jpg"), devWidth, devHeight);
			}
			return Bitmap.getBitmapResource(getImageResourcePathBackground+"splash_P.jpg");
		}
		else{
			if(bbType.equalsIgnoreCase("9860")){
				return ImageScaler.resizeBitmapARGB(Bitmap.getBitmapResource(getImageResourcePathBackground+"splash_L.jpg"), devWidth, devHeight);
			}
			return Bitmap.getBitmapResource(getImageResourcePathBackground+"splash_L.jpg");
		}
		
	}
	
	public Bitmap getBackgroundWelcome() {
		if(bbType.equalsIgnoreCase("9860")){
			return ImageScaler.resizeBitmapARGB(Bitmap.getBitmapResource(getImageResourcePathBackground+"welcome.jpg"), devWidth, devHeight);
		}
		return Bitmap.getBitmapResource(getImageResourcePathBackground+"welcome.jpg");
	}
	
	public Bitmap getBackgroundBlurry() {
		if(bbType.equalsIgnoreCase("9860")){
			return ImageScaler.resizeBitmapARGB(Bitmap.getBitmapResource(getImageResourcePathBackground+"blurry.jpg"), devWidth, devHeight);
		}
		return Bitmap.getBitmapResource(getImageResourcePathBackground+"blurry.jpg");
	}
	
	public Bitmap getSplashFooterRepeat() {
		return Bitmap.getBitmapResource(getImageResourcePathBackground+"splash_footer_repeat.png");
	}
	
	public Bitmap getWelcomeBoxTopP1() {
		return Bitmap.getBitmapResource(getImageResourcePathBackground+"welcome_box_top_p1.png");
	}
	
	public Bitmap getWelcomeBoxTopP2() {
		return Bitmap.getBitmapResource(getImageResourcePathBackground+"welcome_box_top_p2.png");
	}
	
	public Bitmap getWelcomeBoxTopP3() {
		return Bitmap.getBitmapResource(getImageResourcePathBackground+"welcome_box_top_p3.png");
	}
	
	public Bitmap getWelcomeBoxBottomP1() {
		return Bitmap.getBitmapResource(getImageResourcePathBackground+"welcome_box_bottom_p1.png");
	}
	
	public Bitmap getWelcomeBoxBottomP2() {
		return Bitmap.getBitmapResource(getImageResourcePathBackground+"welcome_box_bottom_p2.png");
	}
	
	public Bitmap getHeaderBgRepeat() {
		return Bitmap.getBitmapResource(getImageResourcePathBackground+"top_repeat.png");
	}
	
	public Bitmap getBgBaseDetail() {
		return Bitmap.getBitmapResource(getImageResourcePathBackground+"base-detail.png");
	}
	
	//button single
	public Bitmap getButtonLoginFB() {
		return Bitmap.getBitmapResource(getImageResourcePathButton+"btn_fb_login.png");
	}
	
	public Bitmap getButtonSignIn() {
		return Bitmap.getBitmapResource(getImageResourcePathButton+"btn_sign_in.png");
	}
	
	public Bitmap getButtonShare() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"share-btn.png");
	}
	
	//button partial
	public Bitmap getButtonGreen_l() {
		return Bitmap.getBitmapResource(getImageResourcePathButton+"green_button_left.png");
	}
	public Bitmap getButtonGreen_m() {
		return Bitmap.getBitmapResource(getImageResourcePathButton+"green_button_middle.png");
	}
	public Bitmap getButtonGreen_r() {
		return Bitmap.getBitmapResource(getImageResourcePathButton+"green_button_right.png");
	}
	
	//button partial
	public Bitmap getButtonSmallGreen_l_un() {
		return Bitmap.getBitmapResource(getImageResourcePathButton+"green_small_l_un.png");
	}
	public Bitmap getButtonSmallGreen_m_un() {
		return Bitmap.getBitmapResource(getImageResourcePathButton+"green_small_m_un.png");
	}
	public Bitmap getButtonSmallGreen_r_un() {
		return Bitmap.getBitmapResource(getImageResourcePathButton+"green_small_r_un.png");
	}
	public Bitmap getButtonSmallGreen_l_on() {
		return Bitmap.getBitmapResource(getImageResourcePathButton+"green_small_l_on.png");
	}
	public Bitmap getButtonSmallGreen_m_on() {
		return Bitmap.getBitmapResource(getImageResourcePathButton+"green_small_m_on.png");
	}
	public Bitmap getButtonSmallGreen_r_on() {
		return Bitmap.getBitmapResource(getImageResourcePathButton+"green_small_r_on.png");
	}
	
	
	// input: email, password
	public Bitmap getFormGrey_l() {
		return Bitmap.getBitmapResource(getImageResourcePathForm+"grey_form_left.png");
	}
	public Bitmap getFormGrey_m() {
		return Bitmap.getBitmapResource(getImageResourcePathForm+"grey_form_middle.png");
	}
	public Bitmap getFormGrey_r() {
		return Bitmap.getBitmapResource(getImageResourcePathForm+"grey_form_right.png");
	}
	public Bitmap getFormGrey_l_email() {
		return Bitmap.getBitmapResource(getImageResourcePathForm+"grey_form_left_email.png");
	}
	public Bitmap getFormGrey_l_password() {
		return Bitmap.getBitmapResource(getImageResourcePathForm+"grey_form_left_password.png");
	}
	
	//input: white
	public Bitmap getFormWhite_l() {
		return Bitmap.getBitmapResource(getImageResourcePathForm+"white_input_left.png");
	}
	public Bitmap getFormWhite_m() {
		return Bitmap.getBitmapResource(getImageResourcePathForm+"white_input_middle.png");
	}
	public Bitmap getFormWhite_r() {
		return Bitmap.getBitmapResource(getImageResourcePathForm+"white_input_right.png");
	}
	
	//input: green
	public Bitmap getFormGreen_m() {
		return Bitmap.getBitmapResource(getImageResourcePathForm+"green_form_middle.png");
	}
	
	//input: medium
	public Bitmap getFormMedium_l() {
		return Bitmap.getBitmapResource(getImageResourcePathForm+"input_medium_left.png");
	}
	public Bitmap getFormMedium_m() {
		return Bitmap.getBitmapResource(getImageResourcePathForm+"input_medium_middle.png");
	}
	public Bitmap getFormMedium_r() {
		return Bitmap.getBitmapResource(getImageResourcePathForm+"input_medium_right.png");
	}
	
	//input: big
	public Bitmap getFormBig_l() {
		return Bitmap.getBitmapResource(getImageResourcePathForm+"input_big_left.png");
	}
	public Bitmap getFormBig_m() {
		return Bitmap.getBitmapResource(getImageResourcePathForm+"input_big_middle.png");
	}
	public Bitmap getFormBig_r() {
		return Bitmap.getBitmapResource(getImageResourcePathForm+"input_big_right.png");
	}
	
	//icon, logo, etc
	public Bitmap getLogoBig() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"logo.png");
	}
	
	public Bitmap getIconBack() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"back-icon.png");
	}
	
	public Bitmap getIconMenu() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"menu-icon.png");
	}
	
	public Bitmap getIconMenuNotif() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"menu-icon-notif.png");
	}
	
	public Bitmap getIconRedStar() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"red_star.png");
	}
	
	public Bitmap getIconSearch() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"search-btn.png");
	}
	
	public Bitmap getIconHome() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"home-icon.png");
	}
	
	public Bitmap getIconShare() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"share-icon.png");
	}
	
	public Bitmap getIconFavorit() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"favorit-icon.png");
	}
	
	public Bitmap getIconResep() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"resep-icon.png");
	}
	
	public Bitmap getIconProfil() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"profil-icon.png");
	}
	
	public Bitmap getIconPromo() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"promo-icon.png");
	}
	
	public Bitmap getIconBigBerbagi() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"big-berbagikuliner.png");
	}
	
	public Bitmap getIconBigDaftarKuliner() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"big-daftarkuliner.png");
	}
	
	public Bitmap getIconBigJajananSpecial() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"big-jajanan-spesial.png");
	}
	
	public Bitmap getIconBigTulisReview() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"big-tulisreview.png");
	}
	
	public Bitmap getIconBigTunjukJalan() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"big-tunjukan-jalan.png");
	}
	
	public Bitmap getIconCamera() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"camera-icon.png");
	}
	
	public Bitmap getIconCommentLapor() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"commentlapor-icon.png");
	}
	
	public Bitmap getIconCommentLove() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"commentlove-icon.png");
	}
	
	public Bitmap getIconCommentShare() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"commentshare-icon.png");
	}
	
	public Bitmap getIconLeftArrow() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"leftarrow-btn.png");
	}
	
	public Bitmap getIconTemporerImage() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"temporer_image.png");
	}
	
	public Bitmap getAvatarBgNetral() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"avatar_bg_netral.png");
	}
	
	public Bitmap getAvatarBgBronze() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"avatar_bg_bronze.png");
	}
	
	public Bitmap getAvatarBgSilver() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"avatar_bg_silver.png");
	}
	
	public Bitmap getAvatarBgGold() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"avatar_bg_gold.png");
	}
	
	public Bitmap getAvatarDefault() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"avatar_default.png");
	}
	
	public Bitmap getIconSmallRating() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"small_rating.png");
	}
	
	public Bitmap getIconMasking() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"masking_icon.png");
	}
	
	public Bitmap getIconMaskingAvatar() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"masking_avatar.png");
	}
	
	public Bitmap getIconSetting() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"setting-icon.png");
	}
	
	public Bitmap getIconPOI() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"poi.png");
	}
	
	public Bitmap getIconPOIFocus() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"poi_focus.png");
	}
	
	public Bitmap getIconPointer() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"pointer.png");
	}
	
	public Bitmap getIconMyPosition() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"myposition.png");
	}
	
	public Bitmap getDivider() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"divider.png");
	}
	
	public Bitmap getSeparatorImage() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"separator_image.png");
	}
	
	public Bitmap getTemporerKuliner() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"temporer_kuliner.png");
	}
	
	public Bitmap getTemporerResep() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"temporer_resep.png");
	}
	
	public Bitmap getIconCloseGreen() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"close_green.png");
	}
	
	public Bitmap getIconCloseRed() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"close_red.png");
	}
	
	public Bitmap getIconCurrentLocation() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"current-location.png");
	}
	
	public Bitmap getIconLogout() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"logout-button.png");
	}
	
	public Bitmap getButtonTambahKuliner() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"tambah-kuliner-btn.png");
	}
	
	public Bitmap getButtonMasukkanLokasi() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"masukkan-lokasi.png");
	}
	
	public Bitmap getIconCommentBase() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"commentbase.png");
	}
	
	public Bitmap getIconBlank() {
		return Bitmap.getBitmapResource("images/blank.png");
	}
	
	// share resources
	public Bitmap getShareBG() {
		return Bitmap.getBitmapResource("images/share/share-bg.png");
	}
	public Bitmap getShareLogo() {
		return Bitmap.getBitmapResource("images/share/share-logo.png");
	}
	public Bitmap getShareStar() {
		return Bitmap.getBitmapResource("images/share/share-star.png");
	}
	
	
	//rating
	public Bitmap getIconRatingDot() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"rating_dot.png");
	}
	public Bitmap getIconRatingDotFocus() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"rating_dot_focus.png");
	}
	public Bitmap getIconRatingStar() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"rating_star.png");
	}
	public Bitmap getIconRatingStarFocus() {
		return Bitmap.getBitmapResource(getImageResourcePathIcon+"rating_star_focus.png");
	}
	
	// drop down
	public Bitmap getDropDown_l() {
		return Bitmap.getBitmapResource(getImageResourcePathForm+"select_form_l.png");
	}
	public Bitmap getDropDown_m() {
		return Bitmap.getBitmapResource(getImageResourcePathForm+"select_form_m.png");
	}
	public Bitmap getDropDown_r() {
		return Bitmap.getBitmapResource(getImageResourcePathForm+"select_form_r.png");
	}
	
	// drop down green
	public Bitmap getDropDown_l_green() {
		return Bitmap.getBitmapResource(getImageResourcePathForm
				+ "select_form_l_green.png");
	}

	public Bitmap getDropDown_m_green() {
		return Bitmap.getBitmapResource(getImageResourcePathForm
				+ "select_form_m_green.png");
	}

	public Bitmap getDropDown_r_green() {
		return Bitmap.getBitmapResource(getImageResourcePathForm
				+ "select_form_r_green.png");
	}
	
	//radio
	public Bitmap getRadio_un() {
		return Bitmap.getBitmapResource(getImageResourcePathForm+"radio_un.png");
	}
	public Bitmap getRadio_on() {
		return Bitmap.getBitmapResource(getImageResourcePathForm+"radio_on.png");
	}
	
	//checkbox
	public Bitmap getCheckbox_un() {
		return Bitmap.getBitmapResource(getImageResourcePathForm + "check_un.png");
	}

	public Bitmap getCheckbox_on() {
		return Bitmap.getBitmapResource(getImageResourcePathForm + "check_on.png");
	}
	
	//loading
	public Bitmap[] getLoadingImage() {
		Bitmap[] bmp = new Bitmap[8];
		for(int i=0;i<8;i++){
			bmp[i] = Bitmap.getBitmapResource(getImageResourcePathLoading+"loading_"+ (i+1) + ".png");
		}
		return bmp;
	}
	
//	public Bitmap getButtonGrayNormal(){
//		return GPATools.ResizeTransparentBitmapByPercentage(Bitmap.getBitmapResource("button_gray_unfocus.png"), getProportionalScale(), getProportionalScale(), Bitmap.FILTER_LANCZOS, Bitmap.SCALE_STRETCH);
//	}
	

//	public Bitmap getRadioOnButton(){
//		return GPATools.ResizeTransparentBitmapByPercentage(Bitmap.getBitmapResource("radio_on.png"), getProportionalScale(), getProportionalScale(), Bitmap.FILTER_LANCZOS, Bitmap.SCALE_STRETCH);
//	}
//	public Bitmap getRadioOnButtonFocus(){
//		return GPATools.ResizeTransparentBitmapByPercentage(Bitmap.getBitmapResource("radio_on_focus.png"), getProportionalScale(), getProportionalScale(), Bitmap.FILTER_LANCZOS, Bitmap.SCALE_STRETCH);
//	}
//	public Bitmap getRadioOffButton(){
//		return GPATools.ResizeTransparentBitmapByPercentage(Bitmap.getBitmapResource("radio_off.png"), getProportionalScale(), getProportionalScale(), Bitmap.FILTER_LANCZOS, Bitmap.SCALE_STRETCH);
//	}
//	public Bitmap getRadioOffButtonFocus(){
//		return GPATools.ResizeTransparentBitmapByPercentage(Bitmap.getBitmapResource("radio_off_focus.png"), getProportionalScale(), getProportionalScale(), Bitmap.FILTER_LANCZOS, Bitmap.SCALE_STRETCH);
//	}
//	public Bitmap getRadioAlt1Checked(){
//		return GPATools.ResizeTransparentBitmapByPercentage(Bitmap.getBitmapResource("radio_alt1_checked.png"), getProportionalScale(), getProportionalScale(), Bitmap.FILTER_LANCZOS, Bitmap.SCALE_STRETCH);
//	}
//	public Bitmap getRadioAlt1UnChecked(){
//		return GPATools.ResizeTransparentBitmapByPercentage(Bitmap.getBitmapResource("radio_alt1_unchecked.png"), getProportionalScale(), getProportionalScale(), Bitmap.FILTER_LANCZOS, Bitmap.SCALE_STRETCH);
//	}
//	public Bitmap getRadioAlt1CheckedHover(){
//		return GPATools.ResizeTransparentBitmapByPercentage(Bitmap.getBitmapResource("radio_alt1_checked_hover.png"), getProportionalScale(), getProportionalScale(), Bitmap.FILTER_LANCZOS, Bitmap.SCALE_STRETCH);
//	}
//	public Bitmap getRadioAlt1UnCheckedHover(){
//		return GPATools.ResizeTransparentBitmapByPercentage(Bitmap.getBitmapResource("radio_alt1_unchecked_hover.png"), getProportionalScale(), getProportionalScale(), Bitmap.FILTER_LANCZOS, Bitmap.SCALE_STRETCH);
//	}
//	public Bitmap getRegRadioOff(){
//		return GPATools.ResizeTransparentBitmapByPercentage(Bitmap.getBitmapResource("reg_radio_off.png"), getProportionalScale(), getProportionalScale(), Bitmap.FILTER_LANCZOS, Bitmap.SCALE_STRETCH);
//	}
//	public Bitmap getRegRadioOffFocus(){
//		return GPATools.ResizeTransparentBitmapByPercentage(Bitmap.getBitmapResource("reg_radio_off_focus.png"), getProportionalScale(), getProportionalScale(), Bitmap.FILTER_LANCZOS, Bitmap.SCALE_STRETCH);
//	}
}
