package com.mobileforce.bango.configuration;


/*
 * Common resolution 320x240 (width x height)
 */
public class Resolution320x240 extends ModelConfig {

	public int getFontSuperSmallHeight() {
		// TODO Auto-generated method stub
		return 9;
	}

	public int getFontSmallHeight() {
		// TODO Auto-generated method stub
		return 13;
	}

	public int getFontMediumHeight() {
		// TODO Auto-generated method stub
		return 15;
	}

	public int getFontLargeHeight() {
		// TODO Auto-generated method stub
		return 17;
	}

	public int getFontSuperLargeHeight() {
		// TODO Auto-generated method stub
		return 21;
	}

	public int getValue5px() {
		// TODO Auto-generated method stub
		return 3;
	}

	public int getValue10px() {
		// TODO Auto-generated method stub
		return 6;
	}

	public int getValue8px() {
		// TODO Auto-generated method stub
		return 5;
	}

	//67%W -> 0,67
	public int getProportionalScale() {
		// TODO Auto-generated method stub
		return 67;
	}

	public String getImageResourcePath() {
		// TODO Auto-generated method stub
		return "images/low/";
	}
	
	public int getIconWidth(){
		return 24;
	}
	
	public int getIconHeight(){
		return 24;
	}
	
	public int getPreviewWidth(){
		return 160;
	}
	
	public int getPreviewHeight(){
		return 100;
	}
	
	public int getBigPreviewWidth(){
		return 320;
	}
	
	public int getBigPreviewHeight(){
		return 120;
	}
	
	public int getBadgeWidth(){
		return 55;
	}
	
	public int getBadgeHeight(){
		return 55;
	}
}
