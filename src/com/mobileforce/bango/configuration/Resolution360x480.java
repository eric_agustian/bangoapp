package com.mobileforce.bango.configuration;

public class Resolution360x480 extends ModelConfig{

	public int getFontSuperSmallHeight() {
		// TODO Auto-generated method stub
		return 13;
	}

	public int getFontSmallHeight() {
		// TODO Auto-generated method stub
		return 16;
	}

	public int getFontMediumHeight() {
		// TODO Auto-generated method stub
		return 20;
	}

	public int getFontLargeHeight() {
		// TODO Auto-generated method stub
		return 23;
	}

	public int getFontSuperLargeHeight() {
		// TODO Auto-generated method stub
		return 26;
	}

	public int getValue5px() {
		// TODO Auto-generated method stub
		return 5;
	}

	public int getValue10px() {
		// TODO Auto-generated method stub
		return 10;
	}

	public int getValue8px() {
		// TODO Auto-generated method stub
		return 8;
	}

	//75%W -> 0,75 100%H
	public int getProportionalScale() {
		// TODO Auto-generated method stub
		return 100;
	}

	public String getImageResourcePath() {
		// TODO Auto-generated method stub
		return "images/medium/";
	}
	
	public int getIconWidth(){
		return 32;
	}
	
	public int getIconHeight(){
		return 32;
	}
	
	public int getPreviewWidth(){
		return 180;
	}
	
	public int getPreviewHeight(){
		return 158;
	}
	
	public int getBigPreviewWidth(){
		return 360;
	}
	
	public int getBigPreviewHeight(){
		return 170;
	}
	
	public int getBadgeWidth(){
		return 70;
	}
	
	public int getBadgeHeight(){
		return 70;
	}
}
