package com.mobileforce.bango.configuration;

public class Resolution480x360 extends ModelConfig{

	public int getFontSuperSmallHeight() {
		// TODO Auto-generated method stub
		return 13;
	}

	public int getFontSmallHeight() {
		// TODO Auto-generated method stub
		return 16;
	}

	public int getFontMediumHeight() {
		// TODO Auto-generated method stub
		return 20;
	}

	public int getFontLargeHeight() {
		// TODO Auto-generated method stub
		return 23;
	}

	public int getFontSuperLargeHeight() {
		// TODO Auto-generated method stub
		return 28;
	}

	public int getValue5px() {
		// TODO Auto-generated method stub
		return 5;
	}

	public int getValue10px() {
		// TODO Auto-generated method stub
		return 10;
	}

	public int getValue8px() {
		// TODO Auto-generated method stub
		return 8;
	}

	//100%W
	public int getProportionalScale() {
		// TODO Auto-generated method stub
		return 100;
	}

	public String getImageResourcePath() {
		// TODO Auto-generated method stub
		return "images/medium/";
	}
	
	public int getIconWidth(){
		return 32;
	}
	
	public int getIconHeight(){
		return 32;
	}
	
	public int getPreviewWidth(){
		return 240;
	}
	
	public int getPreviewHeight(){
		return 150;
	}
	
	public int getBigPreviewWidth(){
		return 480;
	}
	
	public int getBigPreviewHeight(){
		return 200;
	}
	
	public int getBadgeWidth(){
		return 70;
	}
	
	public int getBadgeHeight(){
		return 70;
	}
}
