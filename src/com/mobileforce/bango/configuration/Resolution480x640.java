package com.mobileforce.bango.configuration;
/*
 * Common resolution 640x480
 */

public class Resolution480x640 extends ModelConfig {

	public int getFontSuperSmallHeight() {
		// TODO Auto-generated method stub
		return 20;
	}

	public int getFontSmallHeight() {
		// TODO Auto-generated method stub
		return 24;
	}

	public int getFontMediumHeight() {
		// TODO Auto-generated method stub
		return 30;
	}

	public int getFontLargeHeight() {
		// TODO Auto-generated method stub
		return 34;
	}

	public int getFontSuperLargeHeight() {
		// TODO Auto-generated method stub
		return 38;
	}
	
	public int getValue5px() {
		// TODO Auto-generated method stub
		return 7;
	}

	public int getValue10px() {
		// TODO Auto-generated method stub
		return 13;
	}

	public int getValue8px() {
		// TODO Auto-generated method stub
		return 10;
	}
	
	//100%W, 133%H
	public int getProportionalScale() {
		// TODO Auto-generated method stub
		return 133;
	}

	public String getImageResourcePath() {
		// TODO Auto-generated method stub
		return "images/high/";
	}
	
	public int getIconWidth(){
		return 48;
	}
	
	public int getIconHeight(){
		return 48;
	}
	
	public int getPreviewWidth(){
		return 240;
	}
	
	public int getPreviewHeight(){
		return 158;
	}
	
	public int getBigPreviewWidth(){
		return 480;
	}
	
	public int getBigPreviewHeight(){
		return 225;
	}
	
	public int getBadgeWidth(){
		return 110;
	}
	
	public int getBadgeHeight(){
		return 110;
	}
}