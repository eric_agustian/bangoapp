package com.mobileforce.bango.manager;


public class ClassManager {
	private static ClassManager classManager;
	
	private ScreenManager screenManager;
	private ComponentManager componentManager;
//	private ControllerManager controllerManager;
//	private StorageManager storageManager;
	
	public static ClassManager getInstance(){
		if(classManager == null){
			classManager = new ClassManager();
		}
		return classManager;
	}
	
	public ScreenManager getScreenManager() {
		if(screenManager == null){
			screenManager = new ScreenManager();
		}
		return screenManager;
	}
	
	public ComponentManager getComponentManager() {
		if(componentManager == null){
			componentManager = new ComponentManager();
		}
		return componentManager;
	}
	
//	public ControllerManager getControllerManager() {
//		if(controllerManager == null){
//			controllerManager = new ControllerManager();
//		}
//		return controllerManager;
//	}
//	
//	public StorageManager getStorageManager() {
//		if(storageManager == null){
//			storageManager = new StorageManager();
//		}
//		return storageManager;
//	}
	
	public void resetScreenManager(){
		screenManager = null;
	}
}
