package com.mobileforce.bango.manager;

import com.mobileforce.bango.screen.component.ExtraMenu1;
import com.mobileforce.bango.screen.component.ExtraMenu2;
import com.mobileforce.bango.screen.component.ExtraMenu3;
import com.mobileforce.bango.screen.component.ExtraMenu4;
import com.mobileforce.bango.screen.component.ExtraMenu5;
import com.mobileforce.bango.screen.component.ExtraMenu6;
import com.mobileforce.bango.screen.component.HeaderExtra1;
import com.mobileforce.bango.screen.component.HeaderExtra2;
import com.mobileforce.bango.screen.component.HeaderExtra3;
import com.mobileforce.bango.screen.component.HeaderExtra4;
import com.mobileforce.bango.screen.component.HeaderMenu1;
import com.mobileforce.bango.screen.component.HeaderMenu2;
import com.mobileforce.bango.screen.component.HeaderMenu3;


public class ComponentManager {
	
	public ComponentManager() {
		// TODO Auto-generated constructor stub
	}
	
	public HeaderMenu1 getHeaderMenu1(){
		 return new HeaderMenu1();
	}
	
	public HeaderMenu2 getHeaderMenu2(){
		return new HeaderMenu2();
	}
	
	public HeaderMenu3 getHeaderMenu3(){
		return new HeaderMenu3();
	}
	
	public HeaderExtra1 getHeaderExtra1(){
		 return new HeaderExtra1();
	}
	
	public HeaderExtra2 getHeaderExtra2(){
		 return new HeaderExtra2();
	}
	
	public HeaderExtra3 getHeaderExtra3(){
		 return new HeaderExtra3();
	}
	
	public HeaderExtra4 getHeaderExtra4(){
		 return new HeaderExtra4();
	}
	
	public ExtraMenu1 getExtraMenu1() {
		return new ExtraMenu1();
	}
	
	public ExtraMenu2 getExtraMenu2() {
		return new ExtraMenu2();
	}
	
	public ExtraMenu3 getExtraMenu3() {
		return new ExtraMenu3();
	}
	
	public ExtraMenu4 getExtraMenu4() {
		return new ExtraMenu4();
	}
	
	public ExtraMenu5 getExtraMenu5() {
		return new ExtraMenu5();
	}
	
	public ExtraMenu6 getExtraMenu6() {
		return new ExtraMenu6();
	}
}

