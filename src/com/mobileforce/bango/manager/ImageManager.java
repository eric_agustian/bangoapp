package com.mobileforce.bango.manager;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Vector;

import javax.microedition.io.Connector;
import javax.microedition.io.file.FileConnection;
import javax.microedition.io.file.FileSystemRegistry;

import com.mobileforce.bango.storage.persistable.ImagePersistable;
import com.mobileforce.bango.storage.query.GenericStore;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.io.Base64InputStream;
import net.rim.device.api.io.Base64OutputStream;
import net.rim.device.api.util.StringUtilities;

/*
 * V0.0.1 Build 14-06-2011
 * 
 * Fungsi dari class ini adalah untuk mengelola semua content berupa image yang telah diterima oleh ODP dan kemudian menyimpannya kedalam memory internal atau memory external
 * Dengan sistem fetching content ini maka untuk menu yang sama tidak perlu membentuk koneksi HTTP dan melakukan request ke backend, cukup menampilkan data yang sudah disimpan
 * Sebelum disimpan, image dikonversi kedalam bentuk string terenkripsi dengan ekstensi format .bee dan juga di simpan dengan pengaturan hidden file. Proses penyimpanan secara otomatis akan menentukan apakah pada memory internal ataupun eksternal dengan prioritas memory eksternal
 * Untuk mengatasi kondisi image tidak ditemukan (memory dicabut, corrupt, image dihapus secara manual dsb), maka class ini secara otomatis akan me-load ulang image dan menyimpannya pada memory lain yang memungkinkan
 * Jika masih ada ruang kosong tersisa untuk menyimpan image, maka image akan disimpan jika tidak maka fungsi fething image di-nonaktifkan
 * Image akan dihapus dari memory jika dilakukan prosedur cleaning atau jika terjadi kondisi insufficient memory
 */

public class ImageManager {
	private String imagePath = "";
	private String imageExtention = ".bee";
	
	public ImageManager(){}
	
	//get vector
	private Vector getVector(long id){
		return new GenericStore(id).getVectorOfStoredData();
	}
	
	//save vector
	private void saveVector(long id, Vector v){
		new GenericStore(id).saveDataVector(v);
	}
	
	private void deleteVector(long id){
		try{
			new GenericStore(id).deleteStoredData(id);
			System.out.println("success deleting vector>>> "+id);
		}catch(Exception x){ System.out.println("Fail deleting vector>>> "+x.toString()+" ["+id+"]"); }
	}
	
	private void deleteSelectedVector(long fetchid){
		Vector s = getVector(Properties._IMAGE_MANAGER_ID);
		Vector n = new Vector();
		if(s.size()>0){
			for(int i=0; i<s.size(); i++){
				ImagePersistable data = (ImagePersistable) s.elementAt(i);
				if(data.get_imageID() != fetchid){
					n.addElement(data);
				}
			}
			
			saveVector(Properties._IMAGE_MANAGER_ID, n);
		}
	}
	
	public boolean isImageFetched(long id){
		boolean result = false;
		
		if(!isIDNeverSaved(id)) result = true;
		
		return result;
	}
	
	public void manageImageFetchingID(String id, byte[] image){
		//convert ID from string to long
		manageImageFetching(StringUtilities.stringHashToLong(id), image);
	}
	
	public void manageImageFetching(long fetchid, byte[] image){
		try{
			Vector s = getVector(Properties._IMAGE_MANAGER_ID);
			if(s.size()>0){
				if(isIDNeverSaved(fetchid)){
					//encode & save image
					encodeAndSaveImage(fetchid, image);
					
					//PHASE 1: manage ID
					ImagePersistable data = new ImagePersistable();
					data.set_imageID(fetchid);
					data.set_imagePath(this.imagePath);
					s.addElement(data);
					
					saveVector(Properties._IMAGE_MANAGER_ID, s);
				}
			}
			else{
				//encode & save image
				encodeAndSaveImage(fetchid, image);
				
				//PHASE 1: manage ID
				Vector v = new Vector();
				ImagePersistable data = new ImagePersistable();
				data.set_imageID(fetchid);
				data.set_imagePath(this.imagePath);
				v.addElement(data);
				
				saveVector(Properties._IMAGE_MANAGER_ID, v);
			}
			
			System.out.println("TOTAL IMAGE FETCHING>>>"+s.size());
		}catch(Exception x){ System.out.println("unable to fetch image using image manager >>> "+x.toString()); }
	}
	
	public void deleteAllFetchedImage(){
		//delete all image
		deleteAllImage();
		
		//delete persistent
		deleteVector(Properties._IMAGE_MANAGER_ID);
	}
	
	private boolean isIDNeverSaved(long fetchid){
		boolean result = true;
		
		Vector s = getVector(Properties._IMAGE_MANAGER_ID);
		if(s.size()>0){
			for(int i=0; i<s.size(); i++){
				ImagePersistable data = (ImagePersistable) s.elementAt(i);
				if(data.get_imageID() == fetchid){
					result = false;
					break;
				}
			}
		}
		
		return result;
	}
	
	private String getImagePath(long fetchid){
		String path = "";
		
		Vector s = getVector(Properties._IMAGE_MANAGER_ID);
		if(s.size()>0){
			for(int i=0; i<s.size(); i++){
				ImagePersistable data = (ImagePersistable) s.elementAt(i);
				if(data.get_imageID() == fetchid){
					path = data.get_imagePath();
					break;
				}
			}
		}
		
		return path;
	}
	
	//handling if memory card removed or file deleted
	public boolean isImageExistAndAvailable(String id){
		return isImageExistAndAvailable(StringUtilities.stringHashToLong(id));
	}
	
	public boolean isImageExistAndAvailable(long id){
		boolean result = false;
		
		if(!isIDNeverSaved(id)){
			if(decodeAndGetImage(id)!=null){
				result = true;
			}
		}
		
		return result;
	}
	
	
	void encodeAndSaveImage(long id, byte[] image){
		String filename = Long.toString(id)+this.imageExtention;
		
		//encode to string
		try{
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream( image.length );
			Base64OutputStream base64OutputStream = new Base64OutputStream( byteArrayOutputStream );
	    	base64OutputStream.write(image);
	    	base64OutputStream.flush();
	    	base64OutputStream.close();
	    	byteArrayOutputStream.flush();
	    	byteArrayOutputStream.close();
	    	
	    	saveImage(filename, byteArrayOutputStream.toString());
		}catch(Exception x){
			System.out.println("Error while encoding from byte into String >>> "+x.toString());
		}
	}
	
	public byte[] decodeAndGetImage(long fetchid){
		byte[] result = null;
		String path = getImagePath(fetchid);
		
		try{
			if(!path.equalsIgnoreCase("")){
				FileConnection file = null;
				InputStream in = null;
				
				byte[] data;
				try {
					//generate path
					if(path.startsWith("/")){
						path = "file://"+path;
					}
					else{
						path = "file:///"+path;
					}
					
					file = (FileConnection) Connector.open(path, Connector.READ);
			        in = file.openInputStream();
			        
			        int length = in.available();
			        if(length>0){
			        	data = new byte[length];
			        	in.read(data);
			        }
			        else{
			        	//NOTE: run on simulator
			        	//length == 0
			        	length = (int) file.fileSize();
			        	
			        	data = new byte[length];
			        	in.read(data);
			        }
			        //System.out.println("data length>>>> "+length);
					//data = new byte[(int) file.fileSize()];//in.available()];
					//System.out.println("byte available>>> "+in.available());
					//System.out.println("file size>>> "+file.fileSize());
			        
					result = Base64InputStream.decode(new String(data));
				} catch (IOException e) {
					e.printStackTrace();
					System.out.println("Error streaming "+e);
					
					deleteSelectedVector(fetchid);
				} finally{
					//24-08-2010
					try {
				        if (in != null) {
				            in.close();
				        }
				        if (file != null && file.isOpen()) {
				        	file.close();
				        }
					}catch (IOException ioe) {
						System.out.println("Error closing streaming "+ioe);
					}
				}
			}
		}catch(Exception x){
			System.out.println("Error while getting image >>> "+x.toString());
		}
		
		return result;
	}
	
	void saveImage(String filename, String image){
		//PHASE 1: select location
    	//check external memory
    	String path = "SDCard/BlackBerry/pictures/";
    	
    	FileConnection fc = null;
        try{
            fc = (FileConnection)Connector.open("file:///" + path, Connector.READ);
        } 
        catch (Exception ioex){
        	//use internal
        	path = "store/home/user/pictures/";
        } 
        finally{
            if (fc != null){   
                //external available
                //no action
            }
            else{
            	//external not available
            	//use internal
            	path = "store/home/user/pictures/";
            }
        }
        //close connection
        try {
            fc.close();
            fc = null;
        } 
        catch (Exception ioex){}
        
        path += filename;
        //System.out.println("PATH TO SAVE + FILE NAME>>> "+path);
        
        //write image
    	try{
	    	FileConnection fcOut = (FileConnection) Connector.open("file:///" + path, Connector.READ_WRITE);
	    	if(fcOut.exists()) {
	    		fcOut.delete(); // XXX: WARNING
	    	}
			fcOut.create();
	    	if(fcOut.canWrite()) {
	        	OutputStream os = fcOut.openOutputStream();
	        	os.write(image.getBytes());
	        	os.flush();
	        	os.close();
	        	
	        	System.out.println("Saving image complete >>> "+path);
	        	this.imagePath = path;
	    	}
	    	fcOut.close();
	    	
	    	setHiddenAttribute(this.imagePath);
    	}catch (IOException e) {
    		System.out.println("Error writing encoded image >>> "+e);
		}
	}
	
	//prosedur utk membuat image menjadi hidden
	void setHiddenAttribute(String path){
		if(path!=null){
			FileConnection file = null;
			InputStream in = null;
			
			try {
				if(path.startsWith("/")){ path = "file://"+path; }
				else{ path = "file:///"+path; }
				
				file = (FileConnection) Connector.open(path, Connector.READ_WRITE);
		        in = file.openInputStream();
		        
		        //change image attribute as HIDDEN
		        file.setHidden(true);
		        
			}catch (IOException e) {
				e.printStackTrace();
				System.out.println("Error streaming "+e);
			} finally{
				//24-08-2010
				try {
			        if (in != null) {
			            in.close();
			        }
			        if (file != null && file.isOpen()) {
			        	file.close();
			        }
				}catch (IOException ioe) {
					System.out.println("Error closing streaming "+ioe);
				}
			}
		}
	}
	
	
	//delete selected image
	public void deleteSelectedImage(String[] source){
		for(int f=0; f<source.length; f++){
			if(!source[f].equalsIgnoreCase("")){
				 FileConnection fc = null;
				 
			   try {
				   if(source[f].startsWith("/")){ source[f] = "file://"+source[f]; }
				   else{ source[f] = "file:///"+source[f]; }
					 
	               fc = (FileConnection)Connector.open(source[f]);
	               
	               //delete image
	               fc.delete();
	           }catch (Exception ex) {
	          	 System.out.println("Error deleting image "+source[f]+" "+ex);
	           }finally {
	               try{
	                   if(fc != null){
	                       fc.close();
	                       fc = null;
	                       
	                       System.out.println("Success deleting image: "+source[f]);
	                   }
	               }catch (Exception ioex){
	              	 System.out.println("Error deleting image "+source[f]+" "+ioex);
	               }
	           }
			}
		}
	}
	
	//delete all image
	private void deleteAllImage(){
		readRoots("SDCard/BlackBerry/pictures/");
		readRoots("store/home/user/pictures/");
	}
	
	private void readRoots(String root){
        FileConnection fc = null;
        Enumeration rootEnum = null;

        if (root != null){
            // Open the file system and get the list of directories/files.
            try{
                fc = (FileConnection)Connector.open("file:///" + root, Connector.READ);
                rootEnum = fc.list("*"+this.imageExtention, true); //hidden attribute = true //fc.list("*.jpg", true); //old version
            } 
            catch (Exception ioex){} 
            finally{
                if (fc != null){   
                    // Everything is read, make sure to close the connection.
                    try {
                        fc.close();
                        fc = null;
                    } 
                    catch (Exception ioex){}
                }
            }
        }

        // There was no root to read, so now we are reading the system roots.
        if (rootEnum == null){
            rootEnum = FileSystemRegistry.listRoots();
        }

        // Read through the list of directories/files.
        while (rootEnum.hasMoreElements()){
            String file = (String)rootEnum.nextElement();
            if (root != null){
                file = root + file;
            }
            
            readSubroots(file);
        }
    }
	
	private void readSubroots(String file){
        FileConnection fc = null;
        
        try{
            fc = (FileConnection)Connector.open("file:///" + file, Connector.READ_WRITE);
            
            //if(fc.isHidden()){ 
            	fc.delete();
            	System.out.println("DELETING PREVIOUS PHOTO: "+fc.getPath()+file);
            //}
        } 
        catch (Exception ioex){ System.out.println("Error accessing image "+file+" "+ioex); } 
        finally{
            if (fc != null){
                // Everything is read, make sure to close the connection.
                try{
                    fc.close();
                    fc = null;
                } 
                catch (Exception ioex){}
            }
        }
    }
	
}
