package com.mobileforce.bango.manager;

import com.mobileforce.bango.UiApp;
import com.mobileforce.bango.screen.*;
import com.mobileforce.bango.screen.component.ImageZoomPopupScreen;
import com.mobileforce.bango.screen.component.LocationDetailPopupScreen;
import com.mobileforce.bango.screen.component.NotificationHawkerScreen;
import com.mobileforce.bango.screen.component.SharePopupScreen;
import com.mobileforce.bango.screen.component.ShareSelectionDialog;
import com.mobileforce.bango.screen.component.SplashPromoPopupScreen;
import com.mobileforce.bango.utility.ImageUtil;

import net.rim.device.api.ui.Screen;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.PopupScreen;

public class ScreenManager {
	private SplashScreen splashScreen;
	private WelcomeScreen welcomeScreen;
	private LoginScreen loginScreen;
	private RegisterScreen registerScreen;
	private RegisterScreenFb registerScreenFb;
	private FirstMapScreen firstMapScreen;
	private SideBarScreen sideBarScreen;
	private DaftarKulinerScreen daftarKulinerScreen;
	private JajananSpecialScreen jajananSpecialScreen;
	private DaftarResepScreen daftarResepScreen;
	private BerbagiKulinerScreen berbagiKulinerScreen;
	private ProfilScreen profilScreen;
	private EditProfilScreen editProfilScreen;
	private StoreDetailScreen detailStoreScreen;
	private AddReviewStoreScreen addReviewScreen;
	private ResepDetailScreen resepDetailScreen;
	private AddReviewResepScreen addReviewResepScreen;
	private CariKulinerScreen cariKulinerScreen;
	private CariResepScreen cariResepScreen;
	private CariMapScreen cariMapScreen;
	private TambahLokasiScreen tambahLokasiScreen;
	private OthersProfilScreen profilScreenOthers;
	
	public ScreenManager() {
		// TODO Auto-generated constructor stub
	}
	
	public SplashScreen getSplashScreen(){
		splashScreen = new SplashScreen();
		return splashScreen;
	}
	
	public WelcomeScreen getWelcomeScreen() {
		welcomeScreen = new WelcomeScreen();
		return welcomeScreen;
	}
	
	public LoginScreen getLoginScreen() {
		loginScreen = new LoginScreen();
		return loginScreen;
	}
	
	public RegisterScreen getRegisterScreen() {
		registerScreen = new RegisterScreen();
		return registerScreen;
	}
	
	public RegisterScreenFb getRegisterScreenFb() {
		registerScreenFb = new RegisterScreenFb();
		return registerScreenFb;
	}
	
	public FirstMapScreen getFirstMapScreen() {
		firstMapScreen = new FirstMapScreen();
		return firstMapScreen;
	}
	
	public SideBarScreen getSideBarScreen() {
		sideBarScreen = new SideBarScreen();
		return sideBarScreen;
	}
	
	public DaftarKulinerScreen getDaftarKulinerScreen() {
		daftarKulinerScreen = new DaftarKulinerScreen();
		return daftarKulinerScreen;
	}
	
	public JajananSpecialScreen getJajananSpecialScreen() {
		jajananSpecialScreen = new JajananSpecialScreen();
		return jajananSpecialScreen;
	}
	
	public DaftarResepScreen getDaftarResepScreen() {
		daftarResepScreen = new DaftarResepScreen();
		return daftarResepScreen;
	}
	
	public BerbagiKulinerScreen getBerbagiKulinerScreen() {
		berbagiKulinerScreen = new BerbagiKulinerScreen();
		return berbagiKulinerScreen;
	}
	
	public ProfilScreen getProfilScreen() {
		profilScreen = new ProfilScreen();
		return profilScreen;
	}
	
	public EditProfilScreen getEditProfilScreen() {
		editProfilScreen = new EditProfilScreen();
		return editProfilScreen;
	}
	
	public StoreDetailScreen getStoreDetailScreen() {
		detailStoreScreen = new StoreDetailScreen();
		return detailStoreScreen;
	}
	
	public AddReviewStoreScreen getAddReviewStoreScreen() {
		addReviewScreen = new AddReviewStoreScreen();
		return addReviewScreen;
	}
	
	public ResepDetailScreen getResepDetailScreen() {
		resepDetailScreen = new ResepDetailScreen();
		return resepDetailScreen;
	}
	
	public AddReviewResepScreen getAddReviewResepScreen() {
		addReviewResepScreen = new AddReviewResepScreen();
		return addReviewResepScreen;
	}
	
	public CariKulinerScreen getCariKulinerScreen() {
		cariKulinerScreen = new CariKulinerScreen();
		return cariKulinerScreen;
	}
	
	public CariResepScreen getCariResepScreen() {
		cariResepScreen = new CariResepScreen();
		return cariResepScreen;
	}
	
	public CariMapScreen getCariMapScreen() {
		cariMapScreen = new CariMapScreen();
		return cariMapScreen;
	}
	
	public OthersProfilScreen getOthersProfilScreen() {
		profilScreenOthers = new OthersProfilScreen();
		return profilScreenOthers;
	}
	
	public TambahLokasiScreen getTambahLokasiScreen() {
		tambahLokasiScreen = new TambahLokasiScreen();
		return tambahLokasiScreen;
	}
	
	// general use
	public static void clearAllNonFirstMapScreen(){
		UiApp uiApp = ((UiApp) UiApplication.getUiApplication());
		int count = uiApp.getScreenCount();
		
		for(int x=0; x<count; x++){
			Screen screen = uiApp.getActiveScreen();
			if(screen instanceof FirstMapScreen){ 
				/*skip*/
			}
			else{
				uiApp.popScreen(screen);
			}
		}
	}
	
	// get UiAp
	public static UiApp getBangoApp(){
		return ((UiApp)UiApplication.getUiApplication());
	}
	
	public static void showSideBarScreen(){
		synchronized (UiApplication.getUiApplication().getAppEventLock()) {
			ImageUtil.captureCurrentScreen();
			getBangoApp().pushScreen(ClassManager.getInstance().getScreenManager().getSideBarScreen());
		}
	}
	
	public static void closeCurrentScreen(){
		synchronized (UiApplication.getUiApplication().getAppEventLock()) {
			int count = UiApplication.getUiApplication().getScreenCount();
			
			if(count > 1){
				Screen screen = getBangoApp().getActiveScreen();
				getBangoApp().popScreen(screen);
			}
			else{
				System.exit(0);
			}
		}
	}
	
	public static void clearAllScreen(){
		UiApp uiApp = getBangoApp();
		int count = uiApp.getScreenCount();
		
		for(int x=0; x<count; x++){
			Screen screen = uiApp.getActiveScreen();
			uiApp.popScreen(screen);
		}
	}
	
	public static void showLocationDetailPopup(final LocationDetailPopupScreen screen) {
		synchronized (UiApplication.getUiApplication().getAppEventLock()) {
			if(!(getBangoApp().getActiveScreen() instanceof LocationDetailPopupScreen)){
				Runnable runnable = new Runnable() {
		    		public void run() {
		    			try {
		    				// jika ini diaktifkan maka screen bisa membaur dng screen lainnya
	//	    				Ui.getUiEngine().pushGlobalScreen(screen, 4, false); 
		    				
		    				UiApp uiApp = getBangoApp();
		    				uiApp.pushScreen(screen);
		    			} catch (Exception e) {}	
		    		}
				};
				UiApplication.getUiApplication().invokeAndWait(runnable);	
			}	
		}
	} 
	
	public static void closeLocationDetailPopup(final LocationDetailPopupScreen screen) {
    	Runnable runnable = new Runnable() {
    		public void run() {
    			try {
    				// jika ini diaktifkan maka screen bisa membaur dng screen lainnya
//    				Ui.getUiEngine().dismissStatus(_tooltip);
    				
    				UiApp uiApp = getBangoApp();
    				uiApp.popScreen(screen);
    			} catch (Exception e) {}	
    		}
		};
		UiApplication.getUiApplication().invokeLater(runnable);	            	    		   						    			
    }
	
	public static void showPopupTypeScreen(final PopupScreen screen){
		getBangoApp().invokeLater(new Runnable() {
			public void run() {
				getBangoApp().pushScreen(screen);
			}
		});
	}
	
	public static void closePopupTypeScreen(final PopupScreen screen){
		if(screen != null){
			getBangoApp().invokeLater(new Runnable() {
				public void run() {
					try{
						getBangoApp().popScreen(screen);
					}catch(Exception x){}
				}
			});
		}
	}
	
	public static void showDialogTypeScreen(final String message) {
		getBangoApp().invokeLater(new Runnable() {
			public void run() {
				Dialog.alert(message);
			}
		});
	}
	
	public static void showSharePopup(final SharePopupScreen screen) {
//		Runnable runnable = new Runnable() {
//    		public void run() {
//    			try {
//    				// jika ini diaktifkan maka screen bisa membaur dng screen lainnya
//    				UiApp uiApp = getBangoApp();
//    				uiApp.pushScreen(screen);
//    			} catch (Exception e) {}	
//    		}
//		};
//		UiApplication.getUiApplication().invokeLater(runnable);
		getBangoApp().invokeLater(new Runnable() {
			public void run() {
				getBangoApp().pushScreen(screen);
			}
		});
	} 
	
	public static void closeSharePopup(final SharePopupScreen screen) {
		if(screen != null){
			getBangoApp().invokeLater(new Runnable() {
				public void run() {
					try{
						getBangoApp().popScreen(screen);
					}catch(Exception x){}
				}
			});
		}
    }
	
	public static void showShareSelectionDialog(final ShareSelectionDialog screen) {
		Runnable runnable = new Runnable() {
    		public void run() {
    			try {
    				UiApp uiApp = getBangoApp();
    				uiApp.pushScreen(screen);
    			} catch (Exception e) {}	
    		}
		};
		UiApplication.getUiApplication().invokeLater(runnable);	    	
	} 
	
	public static void closeShareSelectionDialog(final ShareSelectionDialog screen) {
    	Runnable runnable = new Runnable() {
    		public void run() {
    			try {
    				UiApp uiApp = getBangoApp();
    				uiApp.popScreen(screen);
    			} catch (Exception e) {}	
    		}
		};
		UiApplication.getUiApplication().invokeLater(runnable);	            	    		   						    			
    }
	
	public static void showImageZoomPopup(final ImageZoomPopupScreen screen) {
		Runnable runnable = new Runnable() {
    		public void run() {
    			try {
    				UiApp uiApp = getBangoApp();
    				uiApp.pushScreen(screen);
    			} catch (Exception e) {}	
    		}
		};
		UiApplication.getUiApplication().invokeLater(runnable);	    	
	} 
	
	public static void closeImageZoomPopup(final ImageZoomPopupScreen screen) {
    	Runnable runnable = new Runnable() {
    		public void run() {
    			try {
    				UiApp uiApp = getBangoApp();
    				uiApp.popScreen(screen);
    			} catch (Exception e) {}	
    		}
		};
		UiApplication.getUiApplication().invokeLater(runnable);	            	    		   						    			
    }
	
	public static void showHawkerNotification(final NotificationHawkerScreen screen) {
		Runnable runnable = new Runnable() {
    		public void run() {
    			try {
    				// jika ini diaktifkan maka screen bisa membaur dng screen lainnya
    				Ui.getUiEngine().pushGlobalScreen(screen, 4, false);
    			} catch (Exception e) {}	
    		}
		};
		UiApplication.getUiApplication().invokeLater(runnable);	    	
	} 
	
	public static void closeHawkerNotification(final NotificationHawkerScreen screen) {
    	Runnable runnable = new Runnable() {
    		public void run() {
    			try {
    				Ui.getUiEngine().dismissStatus(screen);
    			} catch (Exception e) {}	
    		}
		};
		UiApplication.getUiApplication().invokeLater(runnable);	            	    		   						    			
    }
	
	public static void showSplashPromoPopup(final SplashPromoPopupScreen screen) {
		Runnable runnable = new Runnable() {
    		public void run() {
    			try {
    				UiApp uiApp = getBangoApp();
    				uiApp.pushScreen(screen);
    			} catch (Exception e) {}	
    		}
		};
		UiApplication.getUiApplication().invokeLater(runnable);	    	
	} 
	
	public static void closeSplashPromoPopup(final SplashPromoPopupScreen screen) {
    	Runnable runnable = new Runnable() {
    		public void run() {
    			try {
    				UiApp uiApp = getBangoApp();
    				uiApp.popScreen(screen);
    			} catch (Exception e) {}	
    		}
		};
		UiApplication.getUiApplication().invokeLater(runnable);	            	    		   						    			
    }

}
