//#preprocess

package com.mobileforce.bango.screen;

import java.util.Vector;

import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.screen.component.CustomBitmapButton;
import com.mobileforce.bango.screen.component.CustomButton2;
import com.mobileforce.bango.screen.component.CustomEditField4;
import com.mobileforce.bango.screen.component.RatingField;
import com.mobileforce.bango.services.DataLoader;
import com.mobileforce.bango.services.ImageLoader;
import com.mobileforce.bango.services.callback.DataLoaderCallback;
import com.mobileforce.bango.services.callback.FilePickerCallback;
import com.mobileforce.bango.services.callback.OtherScreenCallback;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.FilePickerListener;
import com.mobileforce.bango.utility.GStringUtil;
import com.mobileforce.bango.utility.ImageUtil;
import com.mobileforce.bango.utility.Properties;
import com.mobileforce.bango.utility.network.cleveruapost.BinaryRequestParam;
import com.mobileforce.bango.utility.network.cleveruapost.PostPrameters;
import com.mobileforce.bango.utility.network.cleveruapost.RequestParam;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.system.JPEGEncodedImage;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.ScrollChangeListener;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.picker.FilePicker;


public class AddReviewResepScreen extends BaseScreen implements FieldChangeListener, DataLoaderCallback, FilePickerCallback{
	private VerticalFieldManager _bodyManager, _scrollManager;
	private HorizontalFieldManager _footerManager;
	
	private CustomEditField4 inputDetail;
	private RatingField ratingStar;
	
	private CustomBitmapButton btnCamera;
	private CustomButton2 btnSelesai;
	
	private Bitmap picture;
	private String resepID;
	
	private OtherScreenCallback callback;
	
	public AddReviewResepScreen(){
		super(NO_VERTICAL_SCROLL | NO_HORIZONTAL_SCROLL);
		
		initComponent();
		initSubLayout();
		initContent();
	}
	
	public void initComponent() {
		setHeaderType3();
		headerMenu3.setHeaderTitle1("");//(Properties._LBL_REVIEW + " : ");
		headerMenu3.setHeaderTitle2("");
		
		inputDetail = new CustomEditField4(Properties._LBL_HINT_TULIS_REVIEW, 500, FIELD_HCENTER, EditField.FILTER_DEFAULT);
		inputDetail.setWidthInPixel(Display.getWidth()-(config.getValue10px()*2));
		inputDetail.setMargin(config.getValue10px(), 0, 0, config.getValue10px());
		
		btnSelesai = new CustomButton2(Properties._LBL_SELESAI, FOCUSABLE | FIELD_RIGHT);
		btnSelesai.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontLargeHeight()));
		btnSelesai.setChangeListener(this);
		
		ratingStar = new RatingField(5, 0, FIELD_HCENTER);
		
		btnCamera = new CustomBitmapButton(config.getIconCamera(), FOCUSABLE | FIELD_LEFT);
		btnCamera.setChangeListener(this);
	}
	
	private void initSubLayout(){
		_bodyManager = contentWithExtraMenu();
		
		_footerManager = new HorizontalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void paintBackground(Graphics g){	
				//draw background
				g.setColor(Properties._COLOR_BG_WHITE);
				g.fillRect(0, 0, getWidth(), getHeight());
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				Field c1 = getField(0);
			    layoutChild(c1, c1.getPreferredWidth(), c1.getPreferredHeight());
			    
			    Field c2 = getField(1);
			    layoutChild(c2, c2.getPreferredWidth(), c2.getPreferredHeight());
			    
			    Field c3 = getField(2);
			    layoutChild(c3, c3.getPreferredWidth(), c3.getPreferredHeight());
			    
			    int aW = Display.getWidth();
			    int aH = c2.getHeight() + (config.getValue10px()*2);
				
			    setPositionChild(c1, config.getValue10px(), (aH-c1.getHeight())/2);
			    setPositionChild(c2, aW - (c2.getWidth()+c3.getWidth()+(config.getValue10px()*3)), (aH-c2.getHeight())/2);
			    setPositionChild(c3, aW - (c3.getWidth()+config.getValue10px()), (aH-c3.getHeight())/2);
				
			    setExtent(aW, aH);
			}
		};
		
		_scrollManager = new VerticalFieldManager(VERTICAL_SCROLL | VERTICAL_SCROLLBAR);
		_scrollManager.setScrollListener(
				new ScrollChangeListener(){
					public void scrollChanged(Manager manager, int newHorizontalScroll, int newVerticalScroll) {
						invalidate();
					}
				}
			);
	}
	
	private void initContent(){
		_scrollManager.add(inputDetail);
		
		_footerManager.add(btnCamera);
		_footerManager.add(ratingStar);
		_footerManager.add(btnSelesai);
		
		_bodyManager.add(_scrollManager);
		_bodyManager.add(_footerManager);
		
		_contentManager.add(_bodyManager);
	}
	
	protected void onUiEngineAttached(boolean attached) {
		super.onUiEngineAttached(attached);
	}

	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		if(field == btnCamera){
			//on OS 5 using this
			//#ifdef BlackBerrySDK5.0.0
			String[] filter = {".jpg", ".jpeg", ".png", ".gif" };
			new FilePickerListener(Properties._LBL_CHOOSE_PICTURE, -1, filter, System.getProperty("fileconn.dir.photos"), this);
			
			//on OS 6 or newer using this
			//#else
			new FilePickerListener(Properties._LBL_CHOOSE_PICTURE, FilePicker.VIEW_PICTURES, null, null, this);
			//#endif
		}
		else if(field == btnSelesai){
			String comment = inputDetail.getText().trim();
			int rating = ratingStar.getValue();
			boolean ispicture = picture != null ? true : false;
			
			if(comment.length() == 0){
				Dialog.alert(Properties._INF_KETIK_REVIEW);
			}
			else if(rating == 0){
				Dialog.alert(Properties._INF_ISI_RATING);
			}
//			else if(!ispicture){
//				Dialog.alert(Properties._INF_PILIH_FOTO);
//			}
			else{
				PostPrameters params = new PostPrameters();
				
				try{
					// sending using http multipart
					if(ispicture){
						BinaryRequestParam imagefile = new BinaryRequestParam(
								"imgfile",
								"imgfile_"
										+ GStringUtil
												.generateTimeStamp("yyyyMMddHHmmss")
										+ ".jpg", "image/jpeg",
								getPictureByte());
						params.addParameter(imagefile);
					}
					else{
						BinaryRequestParam imagefile = new BinaryRequestParam(
								"imgfile",
								"empty_"
										+ GStringUtil
												.generateTimeStamp("yyyyMMddHHmmss")
										+ ".txt", "text/plain",
								getEmptyPictureByte());
						params.addParameter(imagefile);
					}
				} catch (Exception e) {
					System.out
							.println("[Bango BinaryRequestParam Exception] "
									+ e);
				}
				
				RequestParam p1 = new RequestParam("recipeid", resepID);
				RequestParam p2 = new RequestParam("comment", comment);
				RequestParam p3 = new RequestParam("rating", Integer.toString(rating));
				
				params.addParameter(p1);
				params.addParameter(p2);
				params.addParameter(p3);
				
				
				new DataLoader(Properties._API_POST_NEW_RECIPE_REVIEW, params.getBody(), Properties._HTTP_METHOD_POST, Properties._CONN_TYPE_ADD_REVIEW, false, true, this);
			}
		}
	}

	public void onPicked(String path) {
		// TODO Auto-generated method stub
		try{
			picture = new ImageLoader().getExplorerBitmap(path, Properties._TEMPLATE_PHOTO_SIZE_W, Properties._TEMPLATE_PHOTO_SIZE_H, 1, false, true);
			
			// check rotation issue on torch
			picture = ImageUtil.getOrientationCorrectedImage(picture);
		}catch(Exception x){
			System.out.println("unable picked picture : " + x);
		}
	}

	private byte[] getPictureByte(){
		byte[] result = null;
		
		try{
			if(picture!=null){
				result = JPEGEncodedImage.encode(picture, 90).getData();
			}
		}
		catch(Exception f){ result = null; }
		
		return result;
	}
	
	private byte[] getEmptyPictureByte(){
		byte[] result = null;
		
		try{
			result = JPEGEncodedImage.encode(config.getIconBlank(), 90).getData();
		}
		catch(Exception f){ result = null; }
		
		return result;
	}

	public void onRequestCompleted(Vector vector, String type) {
		// TODO Auto-generated method stub
		
		// type is the result
		if (callback != null) {
			this.callback.onScreenCallback(type, Properties._CONN_TYPE_ADD_REVIEW);
			
			Vector temporer = new Vector();
			temporer.addElement(Integer.toString(ratingStar.getValue()));
			this.callback.onScreenCallbackValue(temporer, Properties._CONN_TYPE_ADD_REVIEW);
		} else {
			ScreenManager.showDialogTypeScreen(type);
		}

		close();
	}
	
	public void setResepId(String id){
		this.resepID = id;
	}
	
	public void setResepTitle(String title){
		headerMenu3.setHeaderTitle2(title);
	}
	
	public void setOtherScreenCallback(OtherScreenCallback callback){
		this.callback = callback;
	}
}
