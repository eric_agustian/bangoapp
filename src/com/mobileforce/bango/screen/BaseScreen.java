package com.mobileforce.bango.screen;

import com.mobileforce.bango.UiApp;
import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.manager.ClassManager;
import com.mobileforce.bango.screen.component.ExtraMenu1;
import com.mobileforce.bango.screen.component.ExtraMenu2;
import com.mobileforce.bango.screen.component.ExtraMenu3;
import com.mobileforce.bango.screen.component.ExtraMenu4;
import com.mobileforce.bango.screen.component.ExtraMenu5;
import com.mobileforce.bango.screen.component.ExtraMenu6;
import com.mobileforce.bango.screen.component.HeaderExtra1;
import com.mobileforce.bango.screen.component.HeaderExtra2;
import com.mobileforce.bango.screen.component.HeaderExtra3;
import com.mobileforce.bango.screen.component.HeaderExtra4;
import com.mobileforce.bango.screen.component.HeaderMenu1;
import com.mobileforce.bango.screen.component.HeaderMenu2;
import com.mobileforce.bango.screen.component.HeaderMenu3;
import com.mobileforce.bango.screen.component.ProgressAnimationField;
import com.mobileforce.bango.screen.component.ProgressDialog;
import com.mobileforce.bango.services.callback.LocationCompletedCallback;
import com.mobileforce.bango.storage.query.NotificationStatusStore;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.Properties;
import com.mobileforce.bango.utility.geolocator.GeoLocator;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;


public abstract class BaseScreen extends MainScreen{
	public VerticalFieldManager _bodyManager, _headerManager, _contentManager;
	public HeaderMenu1 headerMenu1;
	public HeaderMenu2 headerMenu2;
	public HeaderMenu3 headerMenu3;
	public HeaderExtra1 headerExtra1;
	public HeaderExtra2 headerExtra2;
	public HeaderExtra3 headerExtra3;
	public HeaderExtra4 headerExtra4;
	public ExtraMenu1 extraMenu1;
	public ExtraMenu2 extraMenu2;
	public ExtraMenu3 extraMenu3;
	public ExtraMenu4 extraMenu4;
	public ExtraMenu5 extraMenu5;
	public ExtraMenu6 extraMenu6;
	
	public ModelConfig config = ModelConfig.getConfig();
	
	private ProgressDialog dialog, dialog2;
	private ProgressAnimationField animField;
	
	public String latitude = "0";
	public String longitude = "0";
	
	private LocationCompletedCallback location;
	
	public BaseScreen(long style) {
		super(style);
		
		initDefaultComponent();
		initDefaultLayout();
	}
	
	public BaseScreen(){
		this(0);	
	}
	
	public void initDefaultComponent(){
		// TODO Auto-generated method stub
		DeviceProperties.hideVirtualKeyboard();
				
		headerMenu1 = ClassManager.getInstance().getComponentManager().getHeaderMenu1();
		headerMenu2 = ClassManager.getInstance().getComponentManager().getHeaderMenu2();
		headerMenu3 = ClassManager.getInstance().getComponentManager().getHeaderMenu3();
		
		headerExtra1 = ClassManager.getInstance().getComponentManager().getHeaderExtra1();
		headerExtra2 = ClassManager.getInstance().getComponentManager().getHeaderExtra2();
		headerExtra3 = ClassManager.getInstance().getComponentManager().getHeaderExtra3();
		headerExtra4 = ClassManager.getInstance().getComponentManager().getHeaderExtra4();
		
		extraMenu1 = ClassManager.getInstance().getComponentManager().getExtraMenu1();
		extraMenu2 = ClassManager.getInstance().getComponentManager().getExtraMenu2();
		extraMenu3 = ClassManager.getInstance().getComponentManager().getExtraMenu3();
		extraMenu4 = ClassManager.getInstance().getComponentManager().getExtraMenu4();
		extraMenu5 = ClassManager.getInstance().getComponentManager().getExtraMenu5();
		extraMenu6 = ClassManager.getInstance().getComponentManager().getExtraMenu6();
	}
	
	public void initDefaultLayout(){
		this.deleteAll();
		
		_bodyManager = new VerticalFieldManager(){
			protected void paintBackground(Graphics g) {
				g.setColor(Properties._COLOR_BG_CONTENT);
				g.fillRect(0, 0, Display.getWidth(), Display.getHeight());
				g.setColor(Properties._COLOR_FONT_LABEL_BLACK); //default all font color
			}
			
			protected void sublayout(int width, int height) {
				int cW = Display.getWidth();
				int cH = Display.getHeight();
				
				Field header = getField(0);
			    layoutChild(header, cW, header.getPreferredHeight());
			    setPositionChild(header, 0, 0);
			    
			    Field content = getField(1);
			    layoutChild(content, cW, cH-header.getHeight());
			    setPositionChild(content, 0, header.getHeight());
			    
			    setExtent(cW, cH);
			}
		};
		
		_headerManager = new VerticalFieldManager(NO_VERTICAL_SCROLL | NO_VERTICAL_SCROLLBAR);
		_contentManager = new VerticalFieldManager(USE_ALL_WIDTH | VERTICAL_SCROLL | VERTICAL_SCROLLBAR);
		
		_headerManager.add(headerMenu1);
//		_headerManager.add(navigationMenu);
//		_headerManager.add(getHorizontalLine());
//		_headerManager.add(userInfoMenu);
//		_headerManager.add(getHorizontalLine());
		
		_bodyManager.add(_headerManager);
		_bodyManager.add(_contentManager);
		this.add(_bodyManager);
	}
	
	public void setHeaderType2(){
		_headerManager.deleteAll();
		
		_headerManager.add(headerMenu2);
	}
	
	public void setHeaderType3(){
		_headerManager.deleteAll();
		
		_headerManager.add(headerMenu3);
	}
	
	private void checkNotificationStatus(){
		if(NotificationStatusStore.isNotificationUpdate()){
			headerMenu1.setMenuNotificationStatus(true);
			headerMenu2.setMenuNotificationStatus(true);
			headerMenu3.setMenuNotificationStatus(true);
		}
		else{
			headerMenu1.setMenuNotificationStatus(false);
			headerMenu2.setMenuNotificationStatus(false);
			headerMenu3.setMenuNotificationStatus(false);
		}
	}
	
	// init components
	public abstract void initComponent();
	
	// get UiAp
	public UiApp getBangoApp(){
		return ((UiApp)UiApplication.getUiApplication());
	}
	
	protected void onUiEngineAttached(boolean attached) {
		super.onUiEngineAttached(attached);
		checkNotificationStatus();
		
		System.out.println("<<<<<<<<Attached>>>>>>>>");
	}
	
	protected void onExposed() {
		super.onExposed();
		checkNotificationStatus();
		
		System.out.println("<<<<<<<<Exposed>>>>>>>>>");
	}
	
	public void showProgressDialog(){
		dialog = new ProgressDialog("Loading...");
		getBangoApp().invokeLater(new Runnable() {
			
			public void run() {
				getBangoApp().pushScreen(dialog);
			}
		});
	}
	
	public void closeProgressDialog(){
		if(dialog != null){
			getBangoApp().invokeLater(new Runnable() {
				
				public void run() {
					try{
						getBangoApp().popScreen(dialog);
					}catch(Exception x){}
				}
			});
		}
	}
	
	public void showProgressDialog2(String text){
		dialog2 = new ProgressDialog(text, config.getLoadingImage());
		getBangoApp().invokeLater(new Runnable() {
			
			public void run() {
				getBangoApp().pushScreen(dialog2);
			}
		});
	}
	
	public void closeProgressDialog2(){
		if(dialog2 != null){
			getBangoApp().invokeLater(new Runnable() {
				
				public void run() {
					try{
						getBangoApp().popScreen(dialog2);
					}catch(Exception x){}
				}
			});
		}
	}
	
	public void showErrorDialog(final String errorMessage){
		getBangoApp().invokeLater(new Runnable() {
			
			public void run() {
				Dialog.inform(errorMessage);
			}
		});
	}
	
	public void clearContentManger(){
		getBangoApp().invokeLater(new Runnable() {
			
			public void run() {
				_contentManager.deleteAll();
			}
		});
	}
	
	public void showAnimLoading(){
		animField = new ProgressAnimationField(ModelConfig.getConfig().getLoadingImage(), 
				ModelConfig.getConfig().getLoadingImage().length, FIELD_HCENTER);
		animField.setMargin(ModelConfig.getConfig().getValue10px(), 0, 0, 0);
		getBangoApp().invokeLater(new Runnable() {
			
			public void run() {
				_contentManager.add(animField);
			}
		});
	}
	
	public void closeAnimLoading(){
		getBangoApp().invokeLater(new Runnable() {
			
			public void run() {
				_contentManager.deleteAll();
			}
		});
	}
	
	protected boolean onSavePrompt() {
		return true;
	}
	
	/*
	 * vertical manager to add extra menu & content
	 */
	public VerticalFieldManager contentWithExtraMenu(){
		VerticalFieldManager V = new VerticalFieldManager(NO_VERTICAL_SCROLL){
			protected void sublayout(int width, int height) {
				int cW = Display.getWidth();
				int cH = Display.getHeight()-_headerManager.getHeight();
				
				Field p1 = getField(0);
			    layoutChild(p1, cW, p1.getPreferredHeight());
			    setPositionChild(p1, 0, 0);
			    
			    Field p2 = getField(1);
			    layoutChild(p2, cW, p2.getPreferredHeight());
			    setPositionChild(p2, 0, cH-p2.getHeight());
			    
			    setExtent(cW, cH);
			}
		};
		
		return V;
	}
	
	public void getLocationServices(LocationCompletedCallback location){
		this.location = location;
		new WaitingForLocation();
	}
	
	public void getLocationServicesSilently(){
		new WaitingForLocation(true);
	}
	
	class WaitingForLocation extends Thread {
		private int wait = 30; //timeout
		private GeoLocator geoLocator;
		private boolean silent = false;
		
		public WaitingForLocation(){
			this(false);
		}
		
		public WaitingForLocation(boolean silent){
			this.silent = silent;
			if(silent) wait = 15;
			
			start();
		}
		
		public void run() {
			try {
				geoLocator = new GeoLocator();
				geoLocator.startGeo();
				
				if(!silent) showProgressDialog2(Properties._LBL_MENGAMBIL_LOKASI);
				
				//processing
				int w = 0;
				while(w<wait){
					sleep(1000);
					
					if(geoLocator.retrievingLocation()) break;
					w++;
				}
			} catch (InterruptedException e) {
				System.out.println("Failed to running WaitingForLocation thread>>>"+e.getMessage());
			} finally {
				if(!silent) closeProgressDialog2();
				
				latitude = geoLocator.getLatitudeDefaultZero();
				longitude = geoLocator.getLongitudeDefaultZero();
				
				Properties._LATITUDE = latitude;
				Properties._LONGITUDE = longitude;
				
				if(!silent) location.onLocationCompleted();
			}
		}
	}
}
