package com.mobileforce.bango.screen;

import com.mobileforce.bango.UiApp;
import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.screen.component.ProgressDialog;
import com.mobileforce.bango.utility.Properties;
import com.mobileforce.bango.utility.geolocator.GeoLocator;

import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.MainScreen;

public abstract class BaseScreenX extends MainScreen {
	public ModelConfig config = ModelConfig.getConfig();
	private ProgressDialog dialog, dialog2;
	public String latitude = "0";
	public String longitude = "0";
	
	public BaseScreenX(long style) {
		super(style);
		dialog = new ProgressDialog("Loading...");
	}
	
	public BaseScreenX(){
		this(0);	
	}
	
	// init components
	public abstract void initComponent();
	
	// get INSTANT uiApp
	public UiApp getBangoApp(){
		return ((UiApp)UiApplication.getUiApplication());
	}
	
	protected boolean onSavePrompt() {
		return true;
	}
	
	public void showProgressDialog(){
		getBangoApp().invokeLater(new Runnable() {
			
			public void run() {
				getBangoApp().pushScreen(dialog);
			}
		});
	}
	
	public void closeProgressDialog(){
		if(dialog != null && getBangoApp().getActiveScreen() == dialog){
			getBangoApp().invokeLater(new Runnable() {
				
				public void run() {
					try{
						getBangoApp().popScreen(dialog);
					}catch(Exception x){}
				}
			});
		}
	}
	
	public void showErrorDialog(final String errorMessage){
		getBangoApp().invokeLater(new Runnable() {
			
			public void run() {
				Dialog.inform(errorMessage);
			}
		});
	}
	
	public void showProgressDialog2(String text){
		dialog2 = new ProgressDialog(text, config.getLoadingImage());
		getBangoApp().invokeLater(new Runnable() {
			
			public void run() {
				getBangoApp().pushScreen(dialog2);
			}
		});
	}
	
	public void closeProgressDialog2(){
		if(dialog2 != null){
			getBangoApp().invokeLater(new Runnable() {
				
				public void run() {
					try{
						getBangoApp().popScreen(dialog2);
					}catch(Exception x){}
				}
			});
		}
	}
	
	public void getLocationServices(){
		new WaitingForLocation();
	}
	
	public void getLocationServicesSilently(){
		new WaitingForLocation(true);
	}
	
	class WaitingForLocation extends Thread {
		private int wait = 25; //timeout
		private GeoLocator geoLocator;
		private boolean silent = false;
		
		public WaitingForLocation(){
			this(false);
		}
		
		public WaitingForLocation(boolean silent){
			this.silent = silent;
			if(silent) wait = 5;
			
			start();
		}
		
		public void run() {
			try {
				geoLocator = new GeoLocator();
				geoLocator.startGeo();
				
				if(!silent) showProgressDialog2(Properties._LBL_MENGAMBIL_LOKASI);
				
				//processing
				int w = 0;
				while(w<wait){
					sleep(1000);
					
					if(geoLocator.retrievingLocation()) break;
					w++;
				}
			} catch (InterruptedException e) {
				System.out.println("Failed to running WaitingForLocation thread>>>"+e.getMessage());
			} finally {
				if(!silent) closeProgressDialog2();
				
				latitude = geoLocator.getLatitudeDefaultZero();
				longitude = geoLocator.getLongitudeDefaultZero();
				
				Properties._LATITUDE = latitude;
				Properties._LONGITUDE = longitude;
			}
		}
	}
}


