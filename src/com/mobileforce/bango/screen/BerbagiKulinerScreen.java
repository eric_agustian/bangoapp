//#preprocess

package com.mobileforce.bango.screen;

import java.util.Vector;

import com.mobileforce.bango.manager.ClassManager;
import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.screen.component.CustomBitmapButton;
import com.mobileforce.bango.screen.component.CustomButton2;
import com.mobileforce.bango.screen.component.CustomEditField2;
import com.mobileforce.bango.screen.component.CustomEditField5;
import com.mobileforce.bango.screen.component.CustomLabelField;
import com.mobileforce.bango.screen.component.NotificationHawkerScreen;
import com.mobileforce.bango.screen.component.RatingField;
import com.mobileforce.bango.screen.component.SharePopupScreen;
import com.mobileforce.bango.services.DataLoader;
import com.mobileforce.bango.services.ImageLoader;
import com.mobileforce.bango.services.callback.DataLoaderCallback;
import com.mobileforce.bango.services.callback.FilePickerCallback;
import com.mobileforce.bango.services.callback.OtherScreenCallback;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.FilePickerListener;
import com.mobileforce.bango.utility.GStringUtil;
import com.mobileforce.bango.utility.ImageUtil;
import com.mobileforce.bango.utility.Properties;
import com.mobileforce.bango.utility.network.cleveruapost.BinaryRequestParam;
import com.mobileforce.bango.utility.network.cleveruapost.PostPrameters;
import com.mobileforce.bango.utility.network.cleveruapost.RequestParam;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.system.JPEGEncodedImage;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.ScrollChangeListener;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.picker.FilePicker;

public class BerbagiKulinerScreen extends BaseScreen implements
		FieldChangeListener, DataLoaderCallback, FilePickerCallback, OtherScreenCallback {
	private VerticalFieldManager _bodyManager, _scrollManager;
	private HorizontalFieldManager _footerManager;

	private CustomLabelField labelNama, labelDetail;

	private CustomEditField2 inputNama;
	private CustomEditField5 inputDetail;
	private RatingField ratingStar;

	private CustomBitmapButton btnCamera;
	private CustomButton2 btnTambahLokasi, btnSelesai;

	private Bitmap picture;
	private String latitudeAdd, longitudeAdd, address, city;

	public BerbagiKulinerScreen() {
		super(NO_VERTICAL_SCROLL | NO_HORIZONTAL_SCROLL);

		initComponent();
		initSubLayout();
		initContent();

		getLocationServicesSilently();
	}

	public void initComponent() {
		setHeaderType3();
		headerMenu3.setHeaderTitle1(Properties._LBL_BERBAGI_KULINER.substring(
				0, Properties._LBL_BERBAGI_KULINER.indexOf(" ")));
		headerMenu3.setHeaderTitle2(Properties._LBL_BERBAGI_KULINER
				.substring(Properties._LBL_BERBAGI_KULINER.indexOf(" ")));

		labelNama = new CustomLabelField(Properties._LBL_NAMA,
				Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelNama.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD,
				config.getFontMediumHeight()));
		labelNama.setMargin(config.getValue10px(), 0, 0, config.getValue10px());

		labelDetail = new CustomLabelField(Properties._LBL_DETAIL,
				Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelDetail.setFont(DeviceProperties.getDefaultApplicationFont(
				Font.BOLD, config.getFontMediumHeight()));
		labelDetail.setMargin(config.getValue10px(), 0, 0,
				config.getValue10px());

		inputNama = new CustomEditField2(Properties._LBL_HINT_NAMA_KULINER, 75,
				FIELD_HCENTER, EditField.FILTER_DEFAULT);
		inputNama.setWidthInPixel(Display.getWidth()
				- (config.getValue10px() * 2));
		inputNama.setMargin(0, 0, 0, config.getValue10px());

		inputDetail = new CustomEditField5(Properties._LBL_HINT_DETAIL_KULINER,
				500, FIELD_HCENTER, EditField.FILTER_DEFAULT);
		inputDetail.setWidthInPixel(Display.getWidth()
				- (config.getValue10px() * 2));
		inputDetail.setMargin(0, 0, 0, config.getValue10px());

		btnTambahLokasi = new CustomButton2(Properties._LBL_TAMBAH_LOKASI,
				FOCUSABLE | FIELD_HCENTER);
		btnTambahLokasi.setFont(DeviceProperties.getDefaultApplicationFont(
				Font.BOLD, config.getFontLargeHeight()));
		btnTambahLokasi.setButtonWidthInPercent(60);
		btnTambahLokasi.setMargin(config.getValue10px() * 2, 0,
				config.getValue10px(), 0);
		btnTambahLokasi.setChangeListener(this);

		btnSelesai = new CustomButton2(Properties._LBL_SELESAI, FOCUSABLE
				| FIELD_RIGHT);
		btnSelesai.setFont(DeviceProperties.getDefaultApplicationFont(
				Font.BOLD, config.getFontLargeHeight()));
		btnSelesai.setChangeListener(this);

		ratingStar = new RatingField(5, 0, FIELD_HCENTER);

		btnCamera = new CustomBitmapButton(config.getIconCamera(), FOCUSABLE
				| FIELD_LEFT);
		btnCamera.setChangeListener(this);
	}

	private void initSubLayout() {
		_bodyManager = contentWithExtraMenu();

		_footerManager = new HorizontalFieldManager(Manager.NO_VERTICAL_SCROLL
				| Manager.NO_HORIZONTAL_SCROLL) {
			protected void paintBackground(Graphics g) {
				// draw background
				g.setColor(Properties._COLOR_BG_WHITE);
				g.fillRect(0, 0, getWidth(), getHeight());
			}

			protected void sublayout(int maxWidth, int maxHeight) {
				Field c1 = getField(0);
				layoutChild(c1, c1.getPreferredWidth(), c1.getPreferredHeight());

				Field c2 = getField(1);
				layoutChild(c2, c2.getPreferredWidth(), c2.getPreferredHeight());

				Field c3 = getField(2);
				layoutChild(c3, c3.getPreferredWidth(), c3.getPreferredHeight());

				int aW = Display.getWidth();
				int aH = c2.getHeight() + (config.getValue10px() * 2);

				setPositionChild(c1, config.getValue10px(),
						(aH - c1.getHeight()) / 2);
				setPositionChild(
						c2,
						aW
								- (c2.getWidth() + c3.getWidth() + (config
										.getValue10px() * 3)),
						(aH - c2.getHeight()) / 2);
				setPositionChild(c3,
						aW - (c3.getWidth() + config.getValue10px()),
						(aH - c3.getHeight()) / 2);

				setExtent(aW, aH);
			}
		};

		_scrollManager = new VerticalFieldManager(VERTICAL_SCROLL
				| VERTICAL_SCROLLBAR);
		_scrollManager.setScrollListener(new ScrollChangeListener() {
			public void scrollChanged(Manager manager, int newHorizontalScroll,
					int newVerticalScroll) {
				invalidate();
			}
		});
	}

	private void initContent() {
		_scrollManager.add(labelNama);
		_scrollManager.add(inputNama);
		_scrollManager.add(labelDetail);
		_scrollManager.add(inputDetail);
		_scrollManager.add(btnTambahLokasi);

		_footerManager.add(btnCamera);
		_footerManager.add(ratingStar);
		_footerManager.add(btnSelesai);

		_bodyManager.add(_scrollManager);
		_bodyManager.add(_footerManager);

		_contentManager.add(_bodyManager);
	}

	protected void onUiEngineAttached(boolean attached) {
		super.onUiEngineAttached(attached);
	}

	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		if (field == btnTambahLokasi) {
			TambahLokasiScreen screen = ClassManager.getInstance().getScreenManager().getTambahLokasiScreen();
			screen.setOtherScreenCallback(this);
			getBangoApp().pushScreen(screen);
		} else if (field == btnCamera) {
			//on OS 5 using this
			//#ifdef BlackBerrySDK5.0.0
			String[] filter = {".jpg", ".jpeg", ".png", ".gif" };
			new FilePickerListener(Properties._LBL_CHOOSE_PICTURE, -1, filter, System.getProperty("fileconn.dir.photos"), this);
			
			//on OS 6 or newer using this
			//#else
			new FilePickerListener(Properties._LBL_CHOOSE_PICTURE, FilePicker.VIEW_PICTURES, null, null, this);
			//#endif
		} else if (field == btnSelesai) {
			String nama = inputNama.getText().trim();
			String detail = inputDetail.getText().trim();
			int rating = ratingStar.getValue();
			boolean ispicture = picture != null ? true : false;

			if (nama.length() == 0) {
				Dialog.alert(Properties._INF_KETIK_NAMA_KULINER);
			} else if (detail.length() == 0) {
				Dialog.alert(Properties._INF_KETIK_DETAIL_KULINER);
			} else if (rating == 0) {
				Dialog.alert(Properties._INF_ISI_RATING);
			} else if (!ispicture) {
				Dialog.alert(Properties._INF_PILIH_FOTO);
			} else if (latitudeAdd == null || longitudeAdd == null) {
				Dialog.alert(Properties._INF_LATLON_GAGAL);
			} else if (address == null) {
				Dialog.alert(Properties._INF_ALAMAT_KOSONG);
			} else if (city == null) {
				Dialog.alert(Properties._INF_KOTA_KOSONG);
			} else {
				PostPrameters params = new PostPrameters();

				try {
					// sending using http multipart
					BinaryRequestParam imagefile = new BinaryRequestParam(
							"imgfile",
							"imgfile_"
									+ GStringUtil
											.generateTimeStamp("yyyyMMddHHmmss")
									+ ".jpg", "image/jpeg", getPictureByte());
					params.addParameter(imagefile);
				} catch (Exception e) {
					System.out.println("[Bango BinaryRequestParam Exception] "
							+ e);
				}

				RequestParam p1 = new RequestParam("title", nama);
				RequestParam p2 = new RequestParam("desc", detail);
				RequestParam p3 = new RequestParam("lat", latitude);
				RequestParam p4 = new RequestParam("lon", longitude);
				RequestParam p5 = new RequestParam("rating",
						Integer.toString(rating));
				RequestParam p6 = new RequestParam("addr", address);
				RequestParam p7 = new RequestParam("city", city);

				params.addParameter(p1);
				params.addParameter(p2);
				params.addParameter(p3);
				params.addParameter(p4);
				params.addParameter(p5);
				params.addParameter(p6);
				params.addParameter(p7);

				new DataLoader(Properties._API_POST_NEW_STORE,
						params.getBody(), Properties._HTTP_METHOD_POST,
						Properties._CONN_TYPE_ADD_STORE, false, true, this);
			}
		}
	}

	public void onPicked(String path) {
		// TODO Auto-generated method stub
		try {
			picture = new ImageLoader().getExplorerBitmap(path,
					Properties._TEMPLATE_PHOTO_SIZE_W,
					Properties._TEMPLATE_PHOTO_SIZE_H, 1, false, true);

			// check rotation issue on torch
			picture = ImageUtil.getOrientationCorrectedImage(picture);
		} catch (Exception x) {
			System.out.println("unable picked picture : " + x);
		}
	}

	private byte[] getPictureByte() {
		byte[] result = null;

		try {
			if (picture != null) {
				result = JPEGEncodedImage.encode(picture, 90).getData();
			}
		} catch (Exception f) {
			result = null;
		}

		return result;
	}

	public void onRequestCompleted(Vector vector, String type) {
		// TODO Auto-generated method stub
		// show notification hawker
		ScreenManager.showHawkerNotification(NotificationHawkerScreen.showDetail(type, headerMenu3.getPreferredHeight()));
		
		// close me
		getBangoApp().popScreen(this);
	}

	public void onScreenCallback(String result, String type) {
		// TODO Auto-generated method stub
		
	}

	public void onScreenCallbackValue(Vector value, String type) {
		// TODO Auto-generated method stub
		if(type.equalsIgnoreCase(Properties._LBL_TAMBAH_LOKASI)){
			latitudeAdd = (String) value.elementAt(0);
			longitudeAdd = (String) value.elementAt(1);
			address = (String) value.elementAt(2);
			city = (String) value.elementAt(3);
		}
	}

}
