package com.mobileforce.bango.screen;

import java.util.Vector;

import com.mobileforce.bango.manager.ClassManager;
import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.screen.component.CustomLabelField;
import com.mobileforce.bango.screen.component.CustomListField2;
import com.mobileforce.bango.screen.component.FetchingAnimation;
import com.mobileforce.bango.services.DataLoader;
import com.mobileforce.bango.services.ImageFetching;
import com.mobileforce.bango.services.callback.ButtonPressedCallback;
import com.mobileforce.bango.services.callback.DataLoaderCallback;
import com.mobileforce.bango.storage.persistable.BodyPersistable;
import com.mobileforce.bango.storage.persistable.HeaderPersistable;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.GStringUtil;
import com.mobileforce.bango.utility.Properties;
import com.mobileforce.bango.utility.URLUTF8Encoder;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.ScrollChangeListener;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;


public class CariKulinerScreen extends BaseScreen implements DataLoaderCallback, ButtonPressedCallback{
	private VerticalFieldManager _kulinerManager, FIX, _scrollManager;
	
	public CustomListField2[] searchList;
	
	private boolean morePage = false;
	private String moreURI = "";
	
	public CariKulinerScreen(){
		super(NO_VERTICAL_SCROLL | NO_HORIZONTAL_SCROLL);
		
		initComponent();
		initSubLayout();
		initContent();
		
		//start location services
		getLocationServicesSilently();
	}
	
	public void initComponent() {
		setHeaderType3();
		headerMenu3.setHeaderTitle1(Properties._LBL_DAFTAR_KULINER.substring(0, Properties._LBL_DAFTAR_KULINER.indexOf(" ")));
		headerMenu3.setHeaderTitle2(Properties._LBL_DAFTAR_KULINER.substring(Properties._LBL_DAFTAR_KULINER.indexOf(" ")));
		
		headerExtra1.setSearchHint(Properties._LBL_HINT_CARI_KULINER);
		headerExtra1.setButtonPressedCallback(this);
		headerExtra2.setTerdekatClicked();
		headerExtra2.setButtonPressedCallback(this);
	}
	
	private void initSubLayout(){
		_kulinerManager = contentWithExtraMenu();
		
		
		FIX = new VerticalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void paintBackground(Graphics g){	
				//draw background
				g.setColor(Properties._COLOR_BG_CONTENT);
				g.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aH = Display.getHeight()-(headerMenu3.getPreferredHeight()+headerExtra1.getPreferredHeight()+headerExtra2.getPreferredHeight()+extraMenu2.getPreferredHeight());
				int aW = Display.getWidth();
				
				Field content = getField(0);
			    layoutChild(content, content.getPreferredWidth(), aH);
			    setPositionChild(content, (aW-content.getWidth())/2, 0);
				
			    setExtent(aW, aH);
			}
		};
		
		_scrollManager = new VerticalFieldManager(VERTICAL_SCROLL | VERTICAL_SCROLLBAR);
		_scrollManager.setScrollListener(
				new ScrollChangeListener(){
					public void scrollChanged(Manager manager, int newHorizontalScroll, int newVerticalScroll) {
						invalidate();
					}
				}
			);
	}
	
	private void initContent(){
		_headerManager.add(headerExtra1);
		_headerManager.add(headerExtra2);
		
		FIX.add(_scrollManager);
		
		
		if(config.isOrientationPortrait()){
			_kulinerManager.add(FIX);
			_kulinerManager.add(extraMenu2);
		}
		else{
			_kulinerManager.add(extraMenu2);
			_kulinerManager.add(FIX);
		}
		
		_contentManager.add(_kulinerManager);
	}
	
	private void getContent(String type, String keyword){
		String url = Properties._API_SEARCH_STORE;
		
		if(type.equals(Properties._CONN_TYPE_HOME_DISTANCE)){
			String params = "q=" + URLUTF8Encoder.encode(keyword) + "&order=distance";
			new DataLoader(url, params, Properties._HTTP_METHOD_GET, Properties._CONN_TYPE_SEARCH_HOME_DISTANCE, false, true, this);
		}
		else if(type.equals(Properties._CONN_TYPE_HOME_POPULAR)){
			String params = "q=" + URLUTF8Encoder.encode(keyword) + "&order=popular";
			new DataLoader(url, params, Properties._HTTP_METHOD_GET, Properties._CONN_TYPE_SEARCH_HOME_POPULAR, false, true, this);
		}
		else if(type.equals(Properties._CONN_TYPE_HOME_NEW)){
			String params = "q=" + URLUTF8Encoder.encode(keyword) + "&order=new";
			new DataLoader(url, params, Properties._HTTP_METHOD_GET, Properties._CONN_TYPE_SEARCH_HOME_NEW, false, true, this);
		}
	}
	
	private void setContent(String type, Vector vector){
		Vector header = (Vector) vector.elementAt(0);
		Vector body = (Vector) vector.elementAt(1);
		
		if(searchList == null && body.size() > 0){
			_scrollManager.deleteAll();
			
			// add search label
			_scrollManager.add(searchLabel(headerExtra1.getSearchText()));
			
			searchList = new CustomListField2[body.size()];
			String[] imgurl = new String[searchList.length];
			for(int r=0; r<searchList.length; r++){
				final BodyPersistable bodyData = (BodyPersistable) body.elementAt(r);
				
				String[] data = {
						bodyData.get_title(),
						bodyData.get_distance() + " KM",
						Integer.toString(bodyData.get_rating())
				};
				
				searchList[r] = new CustomListField2(data);
				searchList[r].setWidthInPercent(100);
				searchList[r].setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						StoreDetailScreen screen = ClassManager.getInstance().getScreenManager().getStoreDetailScreen();
						screen.setDetailData(bodyData);
						ScreenManager.getBangoApp().pushScreen(screen);
					}
				});
				
				imgurl[r] = bodyData.get_urlimg();
				
				_scrollManager.add(searchList[r]);
			}
			
			new ImageFetching(this, imgurl, Properties._CONN_TYPE_SEARCH_KULINER_ICON);
		}
		else if(searchList != null && body.size() > 0){
			int previous = searchList.length;
			
			CustomListField2[] oJS = searchList;
			searchList = new CustomListField2[previous + body.size()];
			String[] imgurl = new String[searchList.length];
			
			for(int o=0; o<previous; o++){
				searchList[o] = oJS[o];
				imgurl[o] = "";
			}
			oJS = null;
			
			
			for(int n=0; n<body.size(); n++){
				final BodyPersistable bodyData = (BodyPersistable) body.elementAt(n);
				
				String[] data = {
						bodyData.get_title(),
						bodyData.get_distance() + " KM",
						Integer.toString(bodyData.get_rating())
				};
						
				searchList[previous+n] = new CustomListField2(data);
				searchList[previous+n].setWidthInPercent(100);
				searchList[previous+n].setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						StoreDetailScreen screen = ClassManager.getInstance().getScreenManager().getStoreDetailScreen();
						screen.setDetailData(bodyData);
						ScreenManager.getBangoApp().pushScreen(screen);
					}
				});
						
				imgurl[previous+n] = bodyData.get_urlimg();
					
				_scrollManager.add(searchList[previous+n]);
			}
			
			new ImageFetching(this, imgurl, Properties._CONN_TYPE_SEARCH_KULINER_ICON);
		}
		
		
		checkPaging(type, header);
	}
	
	private void checkPaging(String type, Vector header){
		if(header.size()> 0){
			HeaderPersistable head = (HeaderPersistable) header.elementAt(0);
			if(GStringUtil.checkHTTP(head.get_nexturl())){
				morePage = true;
				moreURI = head.get_nexturl();
				
				FetchingAnimation page = new FetchingAnimation(Properties._LBL_LOAD_MORE, FIELD_HCENTER);
				page.setMargin(config.getValue10px(), 0, config.getValue10px(), 0);
				page.setButtonPressedCallback(this, type + Properties._CONN_TYPE_PAGING);
				
				_scrollManager.add(page);
			}
		}
	}
	
	protected void onUiEngineAttached(boolean attached) {
		super.onUiEngineAttached(attached);
	}

	public void onRequestCompleted(Vector vector, String type) {
		// TODO Auto-generated method stub
		if(morePage){
			// clear fetching animation
			try{
				_scrollManager.delete(_scrollManager.getField(_scrollManager.getFieldCount()-1));
			}catch(Exception d){ System.out.println("unable delete fething animation"); }
		}
		
		setContent(type, vector);
	}

	public void onPressed(String type) {
		// TODO Auto-generated method stub
		if(morePage && type.indexOf(Properties._CONN_TYPE_PAGING)!= -1){
			new DataLoader(moreURI, "", Properties._HTTP_METHOD_GET, type.substring(0, type.indexOf(Properties._CONN_TYPE_PAGING)), true, false, this);
		}
		else if(type.equalsIgnoreCase(Properties._CONN_TYPE_SEARCH_CLICKED)){
			String keyword = headerExtra1.getSearchText();
			
			// do search
			morePage = false;
			moreURI = "";
			searchList = null;
			
			_scrollManager.deleteAll();
			getContent(Properties._CONN_TYPE_HOME_DISTANCE, keyword);
		}
		else{
			String keyword = headerExtra1.getSearchText();
			
			if(keyword.trim().length() == 0){
				Dialog.alert(Properties._INF_KETIK_KEYWORD);
			}
			else{
				// reset
				morePage = false;
				moreURI = "";
				searchList = null;
				
				_scrollManager.deleteAll();
				getContent(type, keyword);
			}
		}
	}
	
	public void searchKeyword(String keyword){
		headerExtra1.setSearchText(keyword);
		
		// do search default by distance
		_scrollManager.deleteAll();
		getContent(Properties._CONN_TYPE_HOME_DISTANCE, keyword);
	}
	
	private HorizontalFieldManager searchLabel(String text){
		HorizontalFieldManager H = new HorizontalFieldManager(){
			protected void paintBackground(Graphics g){	
				//draw background
				g.setColor(Properties._COLOR_BG_PALE_GREEN);
				g.fillRect(0, 0, Display.getWidth(), getHeight());
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aH = 0;
				int aW = Display.getWidth();
				
				Field content = getField(0);
			    layoutChild(content, content.getPreferredWidth(), content.getPreferredHeight());
			    setPositionChild(content, (aW-content.getWidth())/2, aH);
			    
			    aH += content.getHeight();
				
			    setExtent(aW, aH);
			}
		};
		
		CustomLabelField label = new CustomLabelField(Properties._LBL_HASIL_PENCARIAN + " \""+ text + "\"", Properties._COLOR_FONT_LABEL_WHITE, FIELD_HCENTER | NON_FOCUSABLE);
		label.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, config.getFontSmallHeight()));
		H.add(label);
		
		
		return H;
	}

	public void onPressedValue(Vector value, String type) {
		// TODO Auto-generated method stub
		
	}
}
