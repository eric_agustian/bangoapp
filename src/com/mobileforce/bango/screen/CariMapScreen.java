package com.mobileforce.bango.screen;

import java.util.Vector;

import javax.microedition.location.Coordinates;

import com.mobileforce.bango.manager.ClassManager;
import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.screen.component.maps.CustomPOIMap;
import com.mobileforce.bango.screen.component.maps.Marker;
import com.mobileforce.bango.services.DataLoader;
import com.mobileforce.bango.services.callback.ButtonPressedCallback;
import com.mobileforce.bango.services.callback.DataLoaderCallback;
import com.mobileforce.bango.services.callback.LocationCompletedCallback;
import com.mobileforce.bango.storage.persistable.BodyPersistable;
import com.mobileforce.bango.utility.Properties;
import com.mobileforce.bango.utility.URLUTF8Encoder;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.Menu;
import net.rim.device.api.ui.container.VerticalFieldManager;


public class CariMapScreen extends BaseScreen implements FieldChangeListener, LocationCompletedCallback, DataLoaderCallback, ButtonPressedCallback{
	private VerticalFieldManager _mapManager, MAP;
	private CustomPOIMap mapsField;
	
	public CariMapScreen(){
		super(NO_VERTICAL_SCROLL | NO_HORIZONTAL_SCROLL);
		
		initComponent();
		initSubLayout();
		initContent();
		
		// start location services
		getLocationServicesSilently();
	}
	
	public void initComponent() {
		setHeaderType2();
		headerMenu2.setSearchHint(Properties._LBL_HINT_CARI_KULINER);
		extraMenu5.setButtonPressedCallback(this);
		
		mapsField = new CustomPOIMap(config.getIconPointer());
		mapsField.setZoom((mapsField.getMinZoom() * 3 + mapsField.getMaxZoom()) / 7);
		if(config.isOrientationPortrait()){
			mapsField.setPosYCorrection(headerMenu2.getPreferredHeight());
		}
		else{
			mapsField.setPosYCorrection(headerMenu2.getPreferredHeight()+extraMenu5.getPreferredHeight());
		}
	}
	
	private void initSubLayout(){
		_mapManager = contentWithExtraMenu();
		
		MAP = new VerticalFieldManager(){
			protected void paintBackground(Graphics g){	
				//draw background
				g.setColor(Properties._COLOR_BG_CONTENT);
				g.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aH = Display.getHeight()-(headerMenu2.getPreferredHeight()+extraMenu1.getPreferredHeight());
				int aW = Display.getWidth();
				
				try{
					Field content = getField(0);
				    layoutChild(content, aW, aH);
				    setPositionChild(content, 0, 0);
				}catch(Exception x){}
				
			    setExtent(aW, aH);
			}
		};
	}
	
	private void initContent(){
		if(config.isOrientationPortrait()){
			_mapManager.add(MAP);
			_mapManager.add(extraMenu5);
		}
		else{
			_mapManager.add(extraMenu5);
			_mapManager.add(MAP);
		}
		
		MAP.add(mapsField);
		_contentManager.add(_mapManager);
	}
	
	protected void onUiEngineAttached(boolean attached) {
		super.onUiEngineAttached(attached);
	}
	
	protected void onExposed() {
		// TODO Auto-generated method stub
		super.onExposed();
	}
	
	private void getContent(String keyword){
		String url = Properties._API_SEARCH_STORE;
		
		String params = "q=" + URLUTF8Encoder.encode(keyword) + "&order=distance";
		new DataLoader(url, params, Properties._HTTP_METHOD_GET, Properties._CONN_TYPE_SEARCH_HOME_DISTANCE, false, true, this);
	}

	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
	}
	
	public void onLocationCompleted() {
		// TODO Auto-generated method stub
	}

	public void onRequestCompleted(Vector vector, String type) {
		// TODO Auto-generated method stub
		// add multiple marker
		setMultiplePOI(vector);
	}
	
	private void setMultiplePOI(Vector vector){
		if(vector.size() > 0){
			try{
				Vector body = (Vector) vector.elementAt(1);
				
				for(int p=0; p<body.size(); p++){
					BodyPersistable data = (BodyPersistable) body.elementAt(p);
					
					// move map poi position
					if(p==0){
						mapsField.moveTo(new Coordinates(Double.parseDouble(data.get_lat()), Double.parseDouble(data.get_lon()), (float) 0.0));
					}
					
					Marker poi = new Marker(data, new Coordinates(Double.parseDouble(data.get_lat()), Double.parseDouble(data.get_lon()), (float) 0.0), config.getIconPOI(), config.getIconPOIFocus());
					mapsField.addMarker(poi);
				}
			}catch(Exception x){
				System.out.println("Unable set poi on map " + x);
			}
		}
	}
	
	public void searchKeyword(String keyword){
		headerMenu2.setSearchText(keyword);
		
		// set default my location
		mapsField.moveTo(new Coordinates(Double.parseDouble(Properties._LATITUDE), Double.parseDouble(Properties._LONGITUDE), (float) 0.0));
		Marker markerMe = new Marker(new Coordinates(Double.parseDouble(Properties._LATITUDE), Double.parseDouble(Properties._LONGITUDE), (float) 0.0), config.getIconMyPosition(), config.getIconMyPosition());
		mapsField.addMarker(markerMe);
		
		// do search default by distance
		getContent(keyword);
	}

	public void onPressed(String path) {
		// TODO Auto-generated method stub
		BerbagiKulinerScreen screen = ClassManager.getInstance().getScreenManager().getBerbagiKulinerScreen();
		screen.onPicked(path);
		ScreenManager.getBangoApp().pushScreen(screen);
	}

	public void onPressedValue(Vector value, String type) {
		// TODO Auto-generated method stub
		
	}
	
	
	//**********************CUSTOM MENU ITEM*************************************************************	
	private MenuItem _zoomIN = new MenuItem("Zoom In", 1000, 1) {
		public void run() {
			int min = mapsField.getMinZoom();

			int current = mapsField.getZoom();
			if (current - 1 >= min) {
				mapsField.setZoom(current - 1);
			} else {
				Dialog.alert("Maximum Zoom");
			}
		}
	};

	private MenuItem _zoomOUT = new MenuItem("Zoom Out", 1000, 2) {
		public void run() {
			int max = mapsField.getMaxZoom();

			int current = mapsField.getZoom();
			if (current + 1 <= max) {
				mapsField.setZoom(current + 1);
			} else {
				Dialog.alert("Minimum Zoom");
			}
		}
	};

	private MenuItem _mylocation = new MenuItem("Show My Location", 1000, 3) {
		public void run() {
			mapsField.moveTo(new Coordinates(Double.parseDouble(latitude),
					Double.parseDouble(longitude), (float) 0.0));
		}
	};

	private MenuItem _exit = new MenuItem("Exit Application", 1000000, 1) {
		public void run() {
			System.exit(0);
		}
	};

	protected void makeMenu(Menu menu, int instance) {
		// menu.deleteAll();
		menu.add(_zoomIN);
		menu.add(_zoomOUT);
		menu.add(_mylocation);
		menu.add(_exit);
	}
}
