package com.mobileforce.bango.screen;

import java.util.Vector;

import com.mobileforce.bango.screen.component.FetchingAnimation;
import com.mobileforce.bango.screen.component.PreviewHalf1;
import com.mobileforce.bango.services.DataLoader;
import com.mobileforce.bango.services.ImageFetching;
import com.mobileforce.bango.services.callback.ButtonPressedCallback;
import com.mobileforce.bango.services.callback.DataLoaderCallback;
import com.mobileforce.bango.storage.persistable.BodyPersistable;
import com.mobileforce.bango.storage.persistable.HeaderPersistable;
import com.mobileforce.bango.storage.query.TemporerStore;
import com.mobileforce.bango.utility.GStringUtil;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.ScrollChangeListener;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;


public class DaftarKulinerScreen extends BaseScreen implements DataLoaderCallback, ButtonPressedCallback{
	private VerticalFieldManager _kulinerManager, FIX, _scrollManager;
	
	public PreviewHalf1[] P1, P2, P3;
	
	private boolean morePage = false;
	private String moreURI = "";
	
	public DaftarKulinerScreen(){
		super(NO_VERTICAL_SCROLL | NO_HORIZONTAL_SCROLL);
		
		initComponent();
		initSubLayout();
		initContent();
		
		//start location services
		getLocationServicesSilently();
		
		//default content
		getContent(Properties._CONN_TYPE_HOME_DISTANCE);
	}
	
	public void initComponent() {
		setHeaderType2();
		headerMenu2.setSearchHint(Properties._LBL_HINT_CARI_KULINER);
		headerExtra2.setTerdekatClicked();
		headerExtra2.setButtonPressedCallback(this);
	}
	
	private void initSubLayout(){
		_kulinerManager = contentWithExtraMenu();
		
		FIX = new VerticalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void paintBackground(Graphics g){	
				//draw background
				g.setColor(Properties._COLOR_BG_CONTENT);
				g.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aH = Display.getHeight()-(headerMenu2.getPreferredHeight()+headerExtra2.getPreferredHeight()+extraMenu2.getPreferredHeight());
				int aW = Display.getWidth();
				
				Field content = getField(0);
			    layoutChild(content, content.getPreferredWidth(), aH);
			    setPositionChild(content, (aW-content.getWidth())/2, 0);
				
			    setExtent(aW, aH);
			}
		};
		
		_scrollManager = new VerticalFieldManager(VERTICAL_SCROLL | VERTICAL_SCROLLBAR);
		_scrollManager.setScrollListener(
				new ScrollChangeListener(){
					public void scrollChanged(Manager manager, int newHorizontalScroll, int newVerticalScroll) {
						invalidate();
					}
				}
			);
	}
	
	private void initContent(){
		_headerManager.add(headerExtra2);
		FIX.add(_scrollManager);
		
		
		if(config.isOrientationPortrait()){
			_kulinerManager.add(FIX);
			_kulinerManager.add(extraMenu2);
		}
		else{
			_kulinerManager.add(extraMenu2);
			_kulinerManager.add(FIX);
		}
		
		_contentManager.add(_kulinerManager);
	}
	
	private void getContent(String type){
		String url = "";
		
		if(type.equals(Properties._CONN_TYPE_HOME_DISTANCE)){
			if(TemporerStore.getHomebyDistance().size() == 0){
				url = Properties._API_HOME_DISTANCE + "/1";
				new DataLoader(url, "", Properties._HTTP_METHOD_GET, type, false, true, this);
			}
			else{
				setContent(Properties._CONN_TYPE_HOME_DISTANCE, TemporerStore.getHomebyDistance());
			}
		}
		else if(type.equals(Properties._CONN_TYPE_HOME_POPULAR)){
			if(TemporerStore.getHomebyPopular().size() == 0){
				url = Properties._API_HOME_POPULAR + "/1";
				new DataLoader(url, "", Properties._HTTP_METHOD_GET, type, false, true, this);
			}
			else{
				setContent(Properties._CONN_TYPE_HOME_POPULAR, TemporerStore.getHomebyPopular());
			}
		}
		else if(type.equals(Properties._CONN_TYPE_HOME_NEW)){
			if(TemporerStore.getHomebyNew().size() == 0){
				url = Properties._API_HOME_NEW + "/1";
				new DataLoader(url, "", Properties._HTTP_METHOD_GET, type, false, true, this);
			}
			else{
				setContent(Properties._CONN_TYPE_HOME_NEW, TemporerStore.getHomebyNew());
			}
		}
	}
	
	private void setContent(String type, Vector vector){
		Vector header = (Vector) vector.elementAt(0);
		Vector body = (Vector) vector.elementAt(1);
		
		
		if(type.equals(Properties._CONN_TYPE_HOME_DISTANCE)){
			if(P1 == null && body.size() > 0){
				P1 = new PreviewHalf1[body.size()];
				String[] imgurl = new String[P1.length];
				
				int loop = (body.size()/2) + (body.size() % 2);
				int n = 0;
				for(int a=0; a<loop; a++){
					HorizontalFieldManager ROW = new HorizontalFieldManager(FIELD_LEFT);
					_scrollManager.add(ROW);
					
					for(int r=0; r<2; r++){
						if(n < body.size()){
							BodyPersistable data = (BodyPersistable) body.elementAt(n);
							
							P1[n] = new PreviewHalf1(data);
							ROW.add(P1[n]);
							
							imgurl[n] = data.get_urlimg();
						}
						
						n++;
					}
				}
				
				new ImageFetching(this, imgurl, Properties._CONN_TYPE_HOME_DISTANCE);
			}
			else{
				int previous = P1.length;
				
				PreviewHalf1[] oP1 = P1;
				P1 = new PreviewHalf1[previous + body.size()];
				String[] imgurl = new String[P1.length];
				
				for(int o=0; o<previous; o++){
					P1[o] = oP1[o];
					imgurl[o] = ""; //hanya fetching image utk list terbaru saja
				}
				oP1 = null;
				
				
				int loop = (body.size()/2) + (body.size() % 2);
				int n = 0;
				for(int a=0; a<loop; a++){
					HorizontalFieldManager ROW = new HorizontalFieldManager(FIELD_LEFT);
					_scrollManager.add(ROW);
					
					for(int r=0; r<2; r++){
						if(n < body.size()){
							BodyPersistable data = (BodyPersistable) body.elementAt(n);
							
							P1[previous+n] = new PreviewHalf1(data);
							ROW.add(P1[previous+n]);
							
							imgurl[previous+n] = data.get_urlimg();
						}
						
						n++;
					}
				}
				
				new ImageFetching(this, imgurl, Properties._CONN_TYPE_HOME_DISTANCE);
			}
		}
		else if(type.equals(Properties._CONN_TYPE_HOME_POPULAR)){
			if(P2 == null && body.size() > 0){
				P2 = new PreviewHalf1[body.size()];
				String[] imgurl = new String[P2.length];
				
				int loop = (body.size()/2) + (body.size() % 2);
				int n = 0;
				for(int a=0; a<loop; a++){
					HorizontalFieldManager ROW = new HorizontalFieldManager(FIELD_LEFT);
					_scrollManager.add(ROW);
					
					for(int r=0; r<2; r++){
						if(n < body.size()){
							BodyPersistable data = (BodyPersistable) body.elementAt(n);
							
							P2[n] = new PreviewHalf1(data);
							ROW.add(P2[n]);
							
							imgurl[n] = data.get_urlimg();
						}
						
						n++;
					}
				}
				
				new ImageFetching(this, imgurl, Properties._CONN_TYPE_HOME_POPULAR);
			}
			else{
				int previous = P2.length;
				
				PreviewHalf1[] oP2 = P2;
				P2 = new PreviewHalf1[previous + body.size()];
				String[] imgurl = new String[P2.length];
				
				for(int o=0; o<previous; o++){
					P2[o] = oP2[o];
					imgurl[o] = "";
				}
				oP2 = null;
				
				
				int loop = (body.size()/2) + (body.size() % 2);
				int n = 0;
				for(int a=0; a<loop; a++){
					HorizontalFieldManager ROW = new HorizontalFieldManager(FIELD_LEFT);
					_scrollManager.add(ROW);
					
					for(int r=0; r<2; r++){
						if(n < body.size()){
							BodyPersistable data = (BodyPersistable) body.elementAt(n);
							
							P2[previous+n] = new PreviewHalf1(data);
							ROW.add(P2[previous+n]);
							
							imgurl[previous+n] = data.get_urlimg();
						}
						
						n++;
					}
				}
				
				new ImageFetching(this, imgurl, Properties._CONN_TYPE_HOME_POPULAR);
			}
		}
		else if(type.equals(Properties._CONN_TYPE_HOME_NEW)){
			if(P3 == null && body.size() > 0){
				P3 = new PreviewHalf1[body.size()];
				String[] imgurl = new String[P3.length];
				
				int loop = (body.size()/2) + (body.size() % 2);
				int n = 0;
				for(int a=0; a<loop; a++){
					HorizontalFieldManager ROW = new HorizontalFieldManager(FIELD_LEFT);
					_scrollManager.add(ROW);
					
					for(int r=0; r<2; r++){
						if(n < body.size()){
							BodyPersistable data = (BodyPersistable) body.elementAt(n);
							
							P3[n] = new PreviewHalf1(data);
							ROW.add(P3[n]);
							
							imgurl[n] = data.get_urlimg();
						}
						
						n++;
					}
				}
				
				new ImageFetching(this, imgurl, Properties._CONN_TYPE_HOME_NEW);
			}
			else{
				int previous = P3.length;
				
				PreviewHalf1[] oP3 = P3;
				P3 = new PreviewHalf1[previous + body.size()];
				String[] imgurl = new String[P3.length];
				
				for(int o=0; o<previous; o++){
					P3[o] = oP3[o];
					imgurl[o] = "";
				}
				oP3 = null;
				
				
				int loop = (body.size()/2) + (body.size() % 2);
				int n = 0;
				for(int a=0; a<loop; a++){
					HorizontalFieldManager ROW = new HorizontalFieldManager(FIELD_LEFT);
					_scrollManager.add(ROW);
					
					for(int r=0; r<2; r++){
						if(n < body.size()){
							BodyPersistable data = (BodyPersistable) body.elementAt(n);
							
							P3[previous+n] = new PreviewHalf1(data);
							ROW.add(P3[previous+n]);
							
							imgurl[previous+n] = data.get_urlimg();
						}
						
						n++;
					}
				}
				
				new ImageFetching(this, imgurl, Properties._CONN_TYPE_HOME_NEW);
			}
		}
		
		
		//check paging
		checkPaging(type, header);
	}
	
	private void checkPaging(String type, Vector header){
		if(header.size()> 0){
			HeaderPersistable head = (HeaderPersistable) header.elementAt(0);
			if(GStringUtil.checkHTTP(head.get_nexturl())){
				morePage = true;
				moreURI = head.get_nexturl();
				
				FetchingAnimation page = new FetchingAnimation(Properties._LBL_LOAD_MORE, FIELD_HCENTER);
				page.setMargin(config.getValue10px(), 0, config.getValue10px(), 0);
				page.setButtonPressedCallback(this, type + Properties._CONN_TYPE_PAGING);
				
				_scrollManager.add(page);
			}
		}
	}
	
	protected void onUiEngineAttached(boolean attached) {
		super.onUiEngineAttached(attached);
	}

	public void onRequestCompleted(Vector vector, String type) {
		// TODO Auto-generated method stub
		if(morePage){
			// clear fetching animation
			try{
				_scrollManager.delete(_scrollManager.getField(_scrollManager.getFieldCount()-1));
			}catch(Exception d){ System.out.println("unable delete fething animation"); }
		}
		
		setContent(type, vector);
	}

	public void onPressed(String type) {
		// TODO Auto-generated method stub
		if(morePage && type.indexOf(Properties._CONN_TYPE_PAGING)!= -1){
			new DataLoader(moreURI, "", Properties._HTTP_METHOD_GET, type.substring(0, type.indexOf(Properties._CONN_TYPE_PAGING)), true, false, this);
		}
		else{
			// reset
			morePage = false;
			moreURI = "";
			
			_scrollManager.deleteAll();
			getContent(type);
		}
	}

	public void onPressedValue(Vector value, String type) {
		// TODO Auto-generated method stub
		
	}
}
