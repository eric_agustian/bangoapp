package com.mobileforce.bango.screen;

import java.util.Vector;

import com.mobileforce.bango.manager.ClassManager;
import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.screen.component.FetchingAnimation;
import com.mobileforce.bango.screen.component.PreviewHalf2;
import com.mobileforce.bango.services.DataLoader;
import com.mobileforce.bango.services.ImageFetching;
import com.mobileforce.bango.services.callback.ButtonPressedCallback;
import com.mobileforce.bango.services.callback.DataLoaderCallback;
import com.mobileforce.bango.storage.persistable.BodyPersistable2;
import com.mobileforce.bango.storage.persistable.HeaderPersistable;
import com.mobileforce.bango.storage.query.TemporerStore;
import com.mobileforce.bango.utility.GStringUtil;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.ScrollChangeListener;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;


public class DaftarResepScreen extends BaseScreen implements DataLoaderCallback, ButtonPressedCallback{
	private VerticalFieldManager _resepManager, FIX, _scrollManager;
	
	public PreviewHalf2[] P1, P2;
	
	private boolean morePage = false;
	private String moreURI = "";
	
	public DaftarResepScreen(){
		super(NO_VERTICAL_SCROLL | NO_HORIZONTAL_SCROLL);
		
		initComponent();
		initSubLayout();
		initContent();
		
		//default content
		getContent(Properties._CONN_TYPE_RECIPE_NEW);
	}
	
	public void initComponent() {
		setHeaderType3();
		headerMenu3.setHeaderTitle1(Properties._LBL_RESEP.substring(0, Properties._LBL_RESEP.indexOf(" ")));
		headerMenu3.setHeaderTitle2(Properties._LBL_RESEP.substring(Properties._LBL_RESEP.indexOf(" ")));
		
		headerExtra1.setSearchHint(Properties._LBL_HINT_CARI_RESEP);
		headerExtra1.setButtonPressedCallback(this);
		headerExtra3.setTerbaruClicked();
		headerExtra3.setButtonPressedCallback(this);
	}
	
	private void initSubLayout(){
		_resepManager = contentWithExtraMenu();
		
		FIX = new VerticalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void paintBackground(Graphics g){	
				//draw background
				g.setColor(Properties._COLOR_BG_CONTENT);
				g.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aH = Display.getHeight()-(headerMenu3.getPreferredHeight()+headerExtra1.getPreferredHeight()+headerExtra3.getPreferredHeight()+extraMenu2.getPreferredHeight());
				int aW = Display.getWidth();
				
				Field content = getField(0);
			    layoutChild(content, content.getPreferredWidth(), aH);
			    setPositionChild(content, (aW-content.getWidth())/2, 0);
				
			    setExtent(aW, aH);
			}
		};
		
		_scrollManager = new VerticalFieldManager(VERTICAL_SCROLL | VERTICAL_SCROLLBAR);
		_scrollManager.setScrollListener(
				new ScrollChangeListener(){
					public void scrollChanged(Manager manager, int newHorizontalScroll, int newVerticalScroll) {
						invalidate();
					}
				}
			);
	}
	
	private void initContent(){
		_headerManager.add(headerExtra1);
		_headerManager.add(headerExtra3);
		FIX.add(_scrollManager);
		
		
		if(config.isOrientationPortrait()){
			_resepManager.add(FIX);
			_resepManager.add(extraMenu2);
		}
		else{
			_resepManager.add(extraMenu2);
			_resepManager.add(FIX);
		}
		
		_contentManager.add(_resepManager);
	}
	
	private void getContent(String type){
		String url = "";
		
		if(type.equals(Properties._CONN_TYPE_RECIPE_NEW)){
			if(TemporerStore.getRecipebyNew().size() == 0){
				url = Properties._API_LIST_RECIPE_NEW + "/1";
				new DataLoader(url, "", Properties._HTTP_METHOD_GET, type, false, true, this);
			}
			else{
				setContent(Properties._CONN_TYPE_RECIPE_NEW, TemporerStore.getRecipebyNew());
			}
		}
		else if(type.equals(Properties._CONN_TYPE_RECIPE_POPULAR)){
			if(TemporerStore.getRecipebyPopular().size() == 0){
				url = Properties._API_LIST_RECIPE_POPULAR + "/1";
				new DataLoader(url, "", Properties._HTTP_METHOD_GET, type, false, true, this);
			}
			else{
				setContent(Properties._CONN_TYPE_RECIPE_POPULAR, TemporerStore.getRecipebyPopular());
			}
		}
	}
	
	private void setContent(String type, Vector vector){
		Vector header = (Vector) vector.elementAt(0);
		Vector body = (Vector) vector.elementAt(1);
		
		
		if(type.equals(Properties._CONN_TYPE_RECIPE_NEW)){
			if(P1 == null && body.size() > 0){
				P1 = new PreviewHalf2[body.size()];
				String[] imgurl = new String[P1.length];
				
				int loop = (body.size()/2) + (body.size() % 2);
				int n = 0;
				for(int a=0; a<loop; a++){
					HorizontalFieldManager ROW = new HorizontalFieldManager(FIELD_LEFT);
					_scrollManager.add(ROW);
					
					for(int r=0; r<2; r++){
						if(n < body.size()){
							BodyPersistable2 data = (BodyPersistable2) body.elementAt(n);
							
							P1[n] = new PreviewHalf2(data);
							ROW.add(P1[n]);
							
							imgurl[n] = data.get_urlimg();
						}
						
						n++;
					}
				}
				
				new ImageFetching(this, imgurl, Properties._CONN_TYPE_RECIPE_NEW);
			}
			else{
				int previous = P1.length;
				
				PreviewHalf2[] oP1 = P1;
				P1 = new PreviewHalf2[previous + body.size()];
				String[] imgurl = new String[P1.length];
				
				for(int o=0; o<previous; o++){
					P1[o] = oP1[o];
					imgurl[o] = ""; //hanya fetching image utk list terbaru saja
				}
				oP1 = null;
				
				
				int loop = (body.size()/2) + (body.size() % 2);
				int n = 0;
				for(int a=0; a<loop; a++){
					HorizontalFieldManager ROW = new HorizontalFieldManager(FIELD_LEFT);
					_scrollManager.add(ROW);
					
					for(int r=0; r<2; r++){
						if(n < body.size()){
							BodyPersistable2 data = (BodyPersistable2) body.elementAt(n);
							
							P1[previous+n] = new PreviewHalf2(data);
							ROW.add(P1[previous+n]);
							
							imgurl[previous+n] = data.get_urlimg();
						}
						
						n++;
					}
				}
				
				new ImageFetching(this, imgurl, Properties._CONN_TYPE_RECIPE_NEW);
			}
		}
		else if(type.equals(Properties._CONN_TYPE_RECIPE_POPULAR)){
			if(P2 == null && body.size() > 0){
				P2 = new PreviewHalf2[body.size()];
				String[] imgurl = new String[P2.length];
				
				int loop = (body.size()/2) + (body.size() % 2);
				int n = 0;
				for(int a=0; a<loop; a++){
					HorizontalFieldManager ROW = new HorizontalFieldManager(FIELD_LEFT);
					_scrollManager.add(ROW);
					
					for(int r=0; r<2; r++){
						if(n < body.size()){
							BodyPersistable2 data = (BodyPersistable2) body.elementAt(n);
							
							P2[n] = new PreviewHalf2(data);
							ROW.add(P2[n]);
							
							imgurl[n] = data.get_urlimg();
						}
						
						n++;
					}
				}
				
				new ImageFetching(this, imgurl, Properties._CONN_TYPE_RECIPE_POPULAR);
			}
			else{
				int previous = P2.length;
				
				PreviewHalf2[] oP2 = P2;
				P2 = new PreviewHalf2[previous + body.size()];
				String[] imgurl = new String[P2.length];
				
				for(int o=0; o<previous; o++){
					P2[o] = oP2[o];
					imgurl[o] = "";
				}
				oP2 = null;
				
				
				int loop = (body.size()/2) + (body.size() % 2);
				int n = 0;
				for(int a=0; a<loop; a++){
					HorizontalFieldManager ROW = new HorizontalFieldManager(FIELD_LEFT);
					_scrollManager.add(ROW);
					
					for(int r=0; r<2; r++){
						if(n < body.size()){
							BodyPersistable2 data = (BodyPersistable2) body.elementAt(n);
							
							P2[previous+n] = new PreviewHalf2(data);
							ROW.add(P2[previous+n]);
							
							imgurl[previous+n] = data.get_urlimg();
						}
						
						n++;
					}
				}
				
				new ImageFetching(this, imgurl, Properties._CONN_TYPE_RECIPE_POPULAR);
			}
		}
		
		//check paging
		checkPaging(type, header);
	}
	
	private void checkPaging(String type, Vector header){
		if(header.size()> 0){
			HeaderPersistable head = (HeaderPersistable) header.elementAt(0);
			if(GStringUtil.checkHTTP(head.get_nexturl())){
				morePage = true;
				moreURI = head.get_nexturl();
				
				FetchingAnimation page = new FetchingAnimation(Properties._LBL_LOAD_MORE, FIELD_HCENTER);
				page.setMargin(config.getValue10px(), 0, config.getValue10px(), 0);
				page.setButtonPressedCallback(this, type + Properties._CONN_TYPE_PAGING);
				
				_scrollManager.add(page);
			}
		}
	}
	
	protected void onUiEngineAttached(boolean attached) {
		super.onUiEngineAttached(attached);
	}

	public void onRequestCompleted(Vector vector, String type) {
		// TODO Auto-generated method stub
		if(morePage){
			// clear fetching animation
			try{
				_scrollManager.delete(_scrollManager.getField(_scrollManager.getFieldCount()-1));
			}catch(Exception d){ System.out.println("unable delete fething animation"); }
		}
		
		setContent(type, vector);
	}

	public void onPressed(String type) {
		// TODO Auto-generated method stub
		if(morePage && type.indexOf(Properties._CONN_TYPE_PAGING)!= -1){
			new DataLoader(moreURI, "", Properties._HTTP_METHOD_GET, type.substring(0, type.indexOf(Properties._CONN_TYPE_PAGING)), true, false, this);
		}
		else if(type.equalsIgnoreCase(Properties._CONN_TYPE_SEARCH_CLICKED)){
			CariResepScreen screen = ClassManager.getInstance().getScreenManager().getCariResepScreen();
			screen.searchKeyword(headerExtra1.getSearchText());
			ScreenManager.getBangoApp().pushScreen(screen);
		}
		else{
			// reset
			morePage = false;
			moreURI = "";
			
			_scrollManager.deleteAll();
			getContent(type);
		}
	}

	public void onPressedValue(Vector value, String type) {
		// TODO Auto-generated method stub
		
	}
}
