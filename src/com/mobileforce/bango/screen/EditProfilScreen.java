//#preprocess

package com.mobileforce.bango.screen;

import java.util.Vector;

import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.screen.component.AvatarField;
import com.mobileforce.bango.screen.component.CustomBitmapButton;
import com.mobileforce.bango.screen.component.CustomButton2;
import com.mobileforce.bango.screen.component.CustomChoiceField;
import com.mobileforce.bango.screen.component.CustomDropDownList;
import com.mobileforce.bango.screen.component.CustomEditField2;
import com.mobileforce.bango.screen.component.CustomLabelField;
import com.mobileforce.bango.services.DataLoader;
import com.mobileforce.bango.services.ImageFetching;
import com.mobileforce.bango.services.ImageLoader;
import com.mobileforce.bango.services.UserInfoManager;
import com.mobileforce.bango.services.UserProfileManager;
import com.mobileforce.bango.services.callback.DataLoaderCallback;
import com.mobileforce.bango.services.callback.FilePickerCallback;
import com.mobileforce.bango.storage.persistable.ProfilePersistable;
import com.mobileforce.bango.storage.query.TemporerStore;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.FilePickerListener;
import com.mobileforce.bango.utility.GStringUtil;
import com.mobileforce.bango.utility.ImageUtil;
import com.mobileforce.bango.utility.Properties;
import com.mobileforce.bango.utility.network.cleveruapost.BinaryRequestParam;
import com.mobileforce.bango.utility.network.cleveruapost.PostPrameters;
import com.mobileforce.bango.utility.network.cleveruapost.RequestParam;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.system.JPEGEncodedImage;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.ScrollChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.XYRect;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.picker.FilePicker;
//#ifdef TouchEnabled
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.TouchGesture;
//#endif


public class EditProfilScreen extends BaseScreen implements FieldChangeListener, FilePickerCallback, DataLoaderCallback{
	private VerticalFieldManager _formManager, _scrollManager;
	private HorizontalFieldManager _genderManager, _logoutManager;
	private Bitmap _background;
	
	private CustomLabelField labelEditFoto, labelNama, labelEmail, labelTelepon, labelAlamat, labelKota, labelPOS, labelTglLahir, labelIdentitas, labelNoIdentitas, labelGender, labelLogout;
	private CustomEditField2 inputNama, inputAlamat, inputTelepon, inputKota, inputPOS, inputNoIdentitas;
	private CustomLabelField inputEmail, inputTglLahir;
	private CustomDropDownList inputIdentitas;
//	private CustomDateField inputTglLahir;
	
	private CustomChoiceField gender1, gender2;
	private CustomButton2 btnEdit;
	private CustomBitmapButton btnLogout;
	private AvatarField avatar;
	private Bitmap picture, loaded;
	
	private String tgl_lahir, userRank;
	
	public EditProfilScreen(){
		super(NO_VERTICAL_SCROLL | NO_HORIZONTAL_SCROLL);
		
		initComponent();
		initSubLayout();
		initContent();
	}
	
	public void initComponent() {
		// TODO Auto-generated method stub
		_background = config.getBackgroundBlurry();
		
		setHeaderType3();
		headerMenu3.setHeaderTitle1(Properties._LBL_EDIT_PROFIL.substring(0, Properties._LBL_EDIT_PROFIL.indexOf(" ")));
		headerMenu3.setHeaderTitle2(Properties._LBL_EDIT_PROFIL.substring(Properties._LBL_EDIT_PROFIL.indexOf(" ")));
		
		labelEditFoto = new CustomLabelField(Properties._LBL_EDIT_FOTO, Properties._COLOR_FONT_LABEL_BLACK, FIELD_HCENTER | NON_FOCUSABLE);
		labelEditFoto.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelEditFoto.setMargin(config.getValue5px(), 0, config.getValue10px()*2, 0);
		
		labelNama = new CustomLabelField(Properties._LBL_NAMA, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelNama.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelNama.setMargin(config.getValue10px()+config.getValue5px(), 0, 0, 0);
		
		labelEmail = new CustomLabelField(Properties._LBL_EMAIL, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelEmail.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelEmail.setMargin(config.getValue10px()+config.getValue5px(), 0, 0, 0);
		
		labelTelepon = new CustomLabelField(Properties._LBL_TELEPON, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelTelepon.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelTelepon.setMargin(config.getValue10px()+config.getValue5px(), 0, 0, 0);

		labelAlamat = new CustomLabelField(Properties._LBL_ALAMAT, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelAlamat.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelAlamat.setMargin(config.getValue10px()+config.getValue5px(), 0, 0, 0);

		labelKota = new CustomLabelField(Properties._LBL_KOTA, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelKota.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelKota.setMargin(config.getValue10px()+config.getValue5px(), 0, 0, 0);

		labelPOS = new CustomLabelField(Properties._LBL_POS, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelPOS.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelPOS.setMargin(config.getValue10px()+config.getValue5px(), 0, 0, 0);

		labelTglLahir = new CustomLabelField(Properties._LBL_TGL_LAHIR, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelTglLahir.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelTglLahir.setMargin(config.getValue10px()+config.getValue5px(), 0, 0, 0);
		
		labelIdentitas = new CustomLabelField(Properties._LBL_IDENTITAS, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelIdentitas.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelIdentitas.setMargin(config.getValue10px()+config.getValue5px(), 0, 0, 0);
		
		labelNoIdentitas = new CustomLabelField(Properties._LBL_NOMOR_IDENTITAS, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelNoIdentitas.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelNoIdentitas.setMargin(config.getValue10px()+config.getValue5px(), 0, 0, 0);
		
		labelGender = new CustomLabelField(Properties._LBL_GENDER, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelGender.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelGender.setMargin(config.getValue10px()+config.getValue5px(), 0, 0, 0);
		
		labelLogout = new CustomLabelField(Properties._LBL_LOGOUT, FIELD_HCENTER | FIELD_VCENTER | FOCUSABLE, Properties._COLOR_FONT_LABEL_GREEN, Properties._COLOR_FONT_LABEL_GREEN);
		labelLogout.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, config.getFontMediumHeight()));
		labelLogout.setChangeListener(this);
		
		inputNama = new CustomEditField2("", 55, FIELD_HCENTER, EditField.FILTER_DEFAULT);
		inputNama.setWidthInPercent(85);
		
		inputAlamat = new CustomEditField2("", 125, FIELD_HCENTER, EditField.FILTER_DEFAULT);
		inputAlamat.setWidthInPercent(85);
		
		inputTelepon = new CustomEditField2("", 35, FIELD_HCENTER, EditField.FILTER_PHONE);
		inputTelepon.setWidthInPercent(85);
		
		inputEmail = new CustomLabelField("-", Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		inputEmail.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, config.getFontMediumHeight()));
		inputEmail.setMargin(config.getValue5px(), 0, 0, 0);
		
//		inputTglLahir = new CustomDateField(Properties._LBL_HINT_TGL_LAHIR, 0, "dd-MM-yyyy", FOCUSABLE);
//		inputTglLahir.setWidthInPercent(85);
		
		inputTglLahir = new CustomLabelField("", Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		inputTglLahir.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, config.getFontMediumHeight()));
		inputTglLahir.setMargin(config.getValue5px(), 0, 0, 0);
		
		inputKota = new CustomEditField2("", 25, FIELD_HCENTER, EditField.FILTER_DEFAULT);
		inputKota.setWidthInPercent(85);
		
		inputPOS = new CustomEditField2("", 6, FIELD_HCENTER, EditField.FILTER_NUMERIC);
		inputPOS.setWidthInPercent(85);
		
		inputIdentitas = new CustomDropDownList(Properties._LBL_HINT_IDENTITAS, (Display.getWidth()*85)/100, Properties._ARRAY_IDENTITAS, FOCUSABLE | FIELD_HCENTER);
		
		inputNoIdentitas = new CustomEditField2("", 35, FIELD_HCENTER, EditField.FILTER_DEFAULT);
		inputNoIdentitas.setWidthInPercent(85);
		
		gender1 = new CustomChoiceField(Properties._LBL_GENDER_L, DeviceProperties.getDefaultApplicationFont(Font.PLAIN, config.getFontMediumHeight()), Properties._COLOR_FONT_LABEL_GREEN, Properties._COLOR_FONT_LABEL_BLACK, true);
		gender1.setMargin(0, config.getValue10px(), 0, 0);
		gender1.setChangeListener(this);
		
		gender2 = new CustomChoiceField(Properties._LBL_GENDER_P, DeviceProperties.getDefaultApplicationFont(Font.PLAIN, config.getFontMediumHeight()), Properties._COLOR_FONT_LABEL_GREEN, Properties._COLOR_FONT_LABEL_BLACK, true);
		gender2.setMargin(0, 0, 0, config.getValue10px());
		gender2.setChangeListener(this);
		
		avatar = new AvatarField(FOCUSABLE | FIELD_HCENTER);
		avatar.setChangeListener(this);
		avatar.setMargin(config.getValue10px(), config.getValue10px(), config.getValue10px(), config.getValue10px());
		
		btnEdit = new CustomButton2(Properties._LBL_EDIT_PROFIL, FOCUSABLE | FIELD_HCENTER);
		btnEdit.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontLargeHeight()));
		btnEdit.setButtonWidthInPercent(60);
		btnEdit.setMargin(config.getValue10px()*2, 0, config.getValue10px(), 0);
		btnEdit.setChangeListener(this);
		
		btnLogout = new CustomBitmapButton(config.getIconLogout(), FOCUSABLE | FIELD_HCENTER);
		btnLogout.setMargin(config.getValue10px(), 0, config.getValue10px()*2, 0);
		btnLogout.setChangeListener(this);
	}
	
	private void initSubLayout(){
		_formManager = new VerticalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void paintBackground(Graphics g){	
				//draw background
				g.drawBitmap((Display.getWidth()-_background.getWidth())/2, (Display.getHeight()-_background.getHeight())/2, _background.getWidth(), _background.getHeight(), _background, 0, 0);
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aH = Display.getHeight()-headerMenu1.getPreferredHeight();
				int aW = Display.getWidth();
				
				Field content = getField(0);
			    layoutChild(content, content.getPreferredWidth(), aH-(config.getValue10px()));
			    setPositionChild(content, (aW-content.getWidth())/2, config.getValue5px());
				
			    setExtent(aW, aH);
			}
		};
		
		_scrollManager = new VerticalFieldManager(VERTICAL_SCROLL | VERTICAL_SCROLLBAR);
		_scrollManager.setScrollListener(
				new ScrollChangeListener(){
					public void scrollChanged(Manager manager, int newHorizontalScroll, int newVerticalScroll) {
						invalidate();
					}
				}
			);
		
		_genderManager = new HorizontalFieldManager(NO_HORIZONTAL_SCROLL);
		_logoutManager = new HorizontalFieldManager(FIELD_HCENTER){
			protected void sublayout(int maxWidth, int maxHeight){
				int aW =0;
				int aH = 0;
				
				Field btn = getField(0);
			    layoutChild(btn, btn.getPreferredWidth(), btn.getPreferredHeight());
			    
			    Field lbl = getField(1);
			    layoutChild(lbl, lbl.getPreferredWidth(), lbl.getPreferredHeight());
			    
			    aW = btn.getWidth() +config.getValue10px() + lbl.getWidth();
			    aH = btn.getHeight() > lbl.getHeight() ? btn.getHeight() : lbl.getHeight();
			    
			    setPositionChild(btn, 0, (aH - btn.getHeight()) / 2);
			    setPositionChild(lbl, config.getValue10px() + btn.getWidth(), (aH - lbl.getHeight()) / 2);
				
			    setExtent(aW, aH);
			}
		};
	}
	
	private void initContent(){
		_genderManager.add(gender1);
		_genderManager.add(gender2);
		
		_logoutManager.add(btnLogout);
		_logoutManager.add(labelLogout);
		
		_scrollManager.add(avatar);
		_scrollManager.add(labelEditFoto);
		
		_scrollManager.add(labelNama);
		_scrollManager.add(inputNama);
		_scrollManager.add(labelEmail);
		_scrollManager.add(inputEmail);
		_scrollManager.add(labelTelepon);
		_scrollManager.add(inputTelepon);
		_scrollManager.add(labelAlamat);
		_scrollManager.add(inputAlamat);
		_scrollManager.add(labelKota);
		_scrollManager.add(inputKota);
		_scrollManager.add(labelPOS);
		_scrollManager.add(inputPOS);
		_scrollManager.add(labelTglLahir);
		_scrollManager.add(inputTglLahir);
		_scrollManager.add(labelIdentitas);
		_scrollManager.add(inputIdentitas);
		_scrollManager.add(labelNoIdentitas);
		_scrollManager.add(inputNoIdentitas);
		_scrollManager.add(labelGender);
		_scrollManager.add(_genderManager);
		
		_scrollManager.add(btnEdit);
		_scrollManager.add(_logoutManager);
		
		_formManager.add(_scrollManager);
		
		_contentManager.add(_formManager);
	}
	
	public void initProfileValue(ProfilePersistable data){
		inputNama.setText(data.get_fullname());
		inputEmail.setText(data.get_email());
		inputTelepon.setText(data.get_phone());
		inputAlamat.setText(data.get_address());
		inputKota.setText(data.get_city());
		inputPOS.setText(data.get_zip());
		inputNoIdentitas.setText(data.get_id_number());
		
		if(data.get_gender().equalsIgnoreCase("1")){
			gender1.setSelectedValue(true);
		}
		else{
			gender2.setSelectedValue(true);
		}
		
		// set bday
		tgl_lahir = data.get_bdate();
		if(data.get_bdate().length() > 0){
			String bday = data.get_bdate().substring(8);
			switch(Integer.parseInt(data.get_bdate().substring(5, 7))){
			case 1:
				bday += " " + "Januari" + " ";
				break;
			case 2:
				bday += " " + "Februari" + " ";
				break;
			case 3:
				bday += " " + "Maret" + " ";
				break;
			case 4:
				bday += " " + "April" + " ";
				break;
			case 5:
				bday += " " + "Mei" + " ";
				break;
			case 6:
				bday += " " + "Juni" + " ";
				break;
			case 7:
				bday += " " + "Juli" + " ";
				break;
			case 8:
				bday += " " + "Agustus" + " ";
				break;
			case 9:
				bday += " " + "September" + " ";
				break;
			case 10:
				bday += " " + "Oktober" + " ";
				break;
			case 11:
				bday += " " + "November" + " ";
				break;
			default:
				bday += " " + "Desember" + " ";
				break;
			}
			bday += data.get_bdate().substring(0, 4);
			inputTglLahir.setText(bday);
		}
		
		if(data.get_id_type().length() > 0){
			switch(Integer.parseInt(data.get_id_type())){
			case 1:
				inputIdentitas.setSelectedIndex(0);
				break;
			case 2:
				inputIdentitas.setSelectedIndex(1);
				break;
			case 3:
				inputIdentitas.setSelectedIndex(2);
				break;
			}
		}
		
		// other
		userRank = data.get_userrank();
		
		// get avatar
		String[] avatarUrl = { data.get_avaimg() };
		new ImageFetching(this, avatarUrl, Properties._CONN_TYPE_EDIT_AVATAR_PROFILE);
	}
	
	protected void onUiEngineAttached(boolean attached) {
		super.onUiEngineAttached(attached);
	}
	
	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		if(field == avatar){
			//on OS 5 using this
			//#ifdef BlackBerrySDK5.0.0
			String[] filter = {".jpg", ".jpeg", ".png", ".gif" };
			new FilePickerListener(Properties._LBL_CHOOSE_PICTURE, -1, filter, System.getProperty("fileconn.dir.photos"), this);
			
			//on OS 6 or newer using this
			//#else
			new FilePickerListener(Properties._LBL_CHOOSE_PICTURE, FilePicker.VIEW_PICTURES, null, null, this);
			//#endif
		}
		else if(field == btnEdit){
			String name = inputNama.getText().trim();
			String email = inputEmail.getText().trim();
			String gender = gender1.getSelectedValue() ? "1" : "2";
			String telepon = inputTelepon.getText().trim();
			String alamat = inputAlamat.getText().trim();
			String kota = inputKota.getText().trim();
			String kodePOS = inputPOS.getText().trim();
			String noIdentitas = inputNoIdentitas.getText().trim();
			int identitas = inputIdentitas.getSelectedIndex();
			boolean ispicture = picture != null ? true : false;
			
			if(name.length() == 0){
				Dialog.alert(Properties._INF_KETIK_NAMA);
			}
			else if(email.length() == 0){
				Dialog.alert(Properties._INF_KETIK_EMAIL);
			}
			else if(!gender1.getSelectedValue() && !gender2.getSelectedValue()){
				Dialog.alert(Properties._INF_PILIH_GENDER);
			}
			else if(telepon.length() == 0){
				Dialog.alert(Properties._INF_KETIK_TELEPON);
			}
			else if(alamat.length() == 0){
				Dialog.alert(Properties._INF_KETIK_ALAMAT);
			}
			else if(kota.length() == 0){
				Dialog.alert(Properties._INF_KETIK_KOTA);
			}
			else if(kodePOS.length() == 0){
				Dialog.alert(Properties._INF_KETIK_KODE_POS);
			}
			else if(noIdentitas.length() == 0){
				Dialog.alert(Properties._INF_KETIK_NOMOR_IDENTITAS);
			}
			else if(identitas == -1){
				Dialog.alert(Properties._INF_PILIH_IDENTITAS);
			}
			else if(telepon.length() == 0){
				Dialog.alert(Properties._INF_KETIK_LOKASI);
			}
			else if(!ispicture && loaded == null){
				Dialog.alert(Properties._INF_PILIH_FOTO_PROFIL);
			}
			else{
				PostPrameters params = new PostPrameters();
				
				try{
					// sending using http multipart
					BinaryRequestParam imagefile = new BinaryRequestParam(
							"imgfile",
							"imgfile_"
									+ GStringUtil
											.generateTimeStamp("yyyyMMddHHmmss")
									+ ".jpg", "image/jpeg",
									ispicture ? getPictureByte()
											: getLoadedPictureByte());
					params.addParameter(imagefile);
				} catch (Exception e) {
					System.out
							.println("[Bango BinaryRequestParam Exception] "
									+ e);
				}
				
				RequestParam p1 = new RequestParam("fullname", name);
				RequestParam p2 = new RequestParam("addr", alamat);
				RequestParam p3 = new RequestParam("city", kota);
				RequestParam p4 = new RequestParam("zip", kodePOS);
				RequestParam p5 = new RequestParam("gender", gender);
				RequestParam p6 = new RequestParam("bdate", tgl_lahir);
				RequestParam p7 = new RequestParam("id_type", Integer.toString((identitas+1)));
				RequestParam p8 = new RequestParam("id_number", noIdentitas);
				RequestParam p9 = new RequestParam("email", email);
				RequestParam p10 = new RequestParam("phone", telepon);
//				RequestParam p11 = new RequestParam("passwd", "123456");
				
				params.addParameter(p1);
				params.addParameter(p2);
				params.addParameter(p3);
				params.addParameter(p4);
				params.addParameter(p5);
				params.addParameter(p6);
				params.addParameter(p7);
				params.addParameter(p8);
				params.addParameter(p9);
				params.addParameter(p10);
//				params.addParameter(p11);
				
				new DataLoader(Properties._API_EDIT_USER_PROFILE, params.getBody(), Properties._HTTP_METHOD_POST, Properties._CONN_TYPE_EDIT_USER_PROFILE, false, true, this);
			}
		}
		else if(field == gender1){
			gender2.setSelectedValue(!gender1.getSelectedValue());
		}
		else if(field == gender2){
			gender1.setSelectedValue(!gender2.getSelectedValue());
		}
		else if(field == btnLogout || field == labelLogout){
			// send logout
			new DataLoader(Properties._API_LOGOUT, "", Properties._HTTP_METHOD_GET, Properties._CONN_TYPE_LOGOUT, false, false);
		}
	}

	public void onPicked(String path) {
		// TODO Auto-generated method stub
		try {
			picture = new ImageLoader().getExplorerBitmap(path,
					Properties._TEMPLATE_PHOTO_SIZE_W,
					Properties._TEMPLATE_PHOTO_SIZE_H, 1, false, true);
			
			// check rotation issue on torch
			picture = ImageUtil.getOrientationCorrectedImage(picture);
			
			// update avatar
			setAvatarBitmap(picture);
		} catch (Exception x) {
			System.out.println("unable picked picture : " + x);
		}
	}
	
	public void setAvatarBitmap(Bitmap bitmap){
		synchronized (UiApplication.getUiApplication().getAppEventLock()) {
			loaded = bitmap;
			
			if(userRank.equalsIgnoreCase(Properties._LBL_BANGO_BRONZE)){
				avatar.setBronzeAvatar(bitmap);
			}
			else if(userRank.equalsIgnoreCase(Properties._LBL_BANGO_SILVER)){
				avatar.setSilverAvatar(bitmap);
			}
			else if(userRank.equalsIgnoreCase(Properties._LBL_BANGO_GOLD)){
				avatar.setGoldAvatar(bitmap);
			}
			else{
				// netral
				avatar.setNetralAvatar(bitmap);
			}
		}
	}

	private byte[] getPictureByte(){
		byte[] result = null;
		
		try{
			if(picture!=null){
				result = JPEGEncodedImage.encode(picture, 90).getData();
			}
		}
		catch(Exception f){ result = null; }
		
		return result;
	}
	
	private byte[] getLoadedPictureByte(){
		byte[] result = null;
		
		try{
			result = JPEGEncodedImage.encode(loaded, 100).getData();
		}
		catch(Exception f){ result = null; }
		
		return result;
	}

	public void onRequestCompleted(Vector vector, String type) {
		// TODO Auto-generated method stub
		TemporerStore.resetUserProfile();
		UserInfoManager.deleteUserInfo();
		UserProfileManager.deleteUserProfile();
		ScreenManager.clearAllNonFirstMapScreen();
		
		// type is the result
		ScreenManager.showDialogTypeScreen(type);
	}
	
	// mengatasi issue touch
	// #ifdef TouchEnabled
	protected boolean touchEvent(TouchEvent message) {
		switch (message.getEvent()) {
		case TouchEvent.GESTURE:
			TouchGesture gesture = message.getGesture();
			if (gesture.getEvent() == TouchGesture.TAP) {
				XYRect startRect = new XYRect();
				XYRect activeField = new XYRect();
				UiApplication.getUiApplication().getActiveScreen()
						.getFocusRect(startRect);
				activeField.set(startRect);

				int posX = activeField.x;
				int posY = activeField.y;

				int touchX = message.getX(1);
				int touchY = message.getY(1);

				if ((touchX >= posX && touchX <= posX + activeField.width)
						&& (touchY >= posY && touchY <= posY
								+ activeField.height)) {
					return super.touchEvent(message);
				}
			}
			return true;
		}

		return super.touchEvent(message);
	}
	// #endif
}
