package com.mobileforce.bango.screen;

import java.util.Vector;

import javax.microedition.location.Coordinates;

import com.mobileforce.bango.screen.component.maps.CustomPOIMap;
import com.mobileforce.bango.screen.component.maps.Marker;
import com.mobileforce.bango.services.DataLoader;
import com.mobileforce.bango.services.SettingManager;
import com.mobileforce.bango.services.UserProfileManager;
import com.mobileforce.bango.services.callback.DataLoaderCallback;
import com.mobileforce.bango.services.callback.LocationCompletedCallback;
import com.mobileforce.bango.storage.persistable.BodyPersistable;
import com.mobileforce.bango.storage.query.TemporerStore;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.Menu;
import net.rim.device.api.ui.container.VerticalFieldManager;


public class FirstMapScreen extends BaseScreen implements FieldChangeListener, LocationCompletedCallback, DataLoaderCallback{
	private FirstMapScreen _self;
	private VerticalFieldManager _mapManager, MAP;
	private CustomPOIMap mapsField;
	
	public FirstMapScreen(){
		super(NO_VERTICAL_SCROLL | NO_HORIZONTAL_SCROLL);
		_self = this;
		
		initComponent();
		initSubLayout();
		initContent();
		
		// start location services
		getLocationServices(this);
		
		// get menu special
		getMenuSpecial();
		
		// check notification state for first time
		getNotificationSetting();
		
		// check user profile for first time
		getUserProfile();
	}
	
	public void initComponent() {
		setHeaderType2();
		headerMenu2.setSearchHint(Properties._LBL_HINT_CARI_KULINER);
		headerMenu2.removeLeftMenu(); // dihilangkan
		headerMenu2.increaseSearchBarSize();
		
		mapsField = new CustomPOIMap(config.getIconPointer());
		mapsField.setZoom((mapsField.getMinZoom() * 3 + mapsField.getMaxZoom()) / 7);
		
		if(config.isOrientationPortrait()){
			mapsField.setPosYCorrection(headerMenu2.getPreferredHeight());
		}
		else{
			mapsField.setPosYCorrection(headerMenu2.getPreferredHeight()+extraMenu1.getPreferredHeight());
		}
//		mapsField.setPreferredSize(Display.getWidth(), Display.getHeight()-(headerMenu2.getPreferredHeight()+extraMenu1.getPreferredHeight()));
	}
	
	private void initSubLayout(){
		_mapManager = contentWithExtraMenu();
		
		MAP = new VerticalFieldManager(){
			protected void paintBackground(Graphics g){	
				//draw background
				g.setColor(Properties._COLOR_BG_CONTENT);
				g.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aW = Display.getWidth();
				int aH = Display.getHeight()-(headerMenu2.getPreferredHeight()+extraMenu1.getPreferredHeight());
				
				Field content = getField(0);
				layoutChild(content, aW, aH);
				setPositionChild(content, 0, 0);
				
			    setExtent(aW, aH);
			}
		};
	}
	
	private void initContent(){
		if(config.isOrientationPortrait()){
			_mapManager.add(MAP);
			_mapManager.add(extraMenu1);
		}
		else{
			_mapManager.add(extraMenu1);
			_mapManager.add(MAP);
		}
		
		MAP.add(mapsField);
		_contentManager.add(_mapManager);
	}
	
	protected void onExposed() {
		// TODO Auto-generated method stub
		super.onExposed();
		
		if(TemporerStore.getHomebyDistance().size() > 0 ){
//			setContent(Properties._CONN_TYPE_HOME_DISTANCE, TemporerStore.getHomebyDistance());
		}
	}
	
	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		
	}
	
	private void getMenuSpecial(){
		new DataLoader(Properties._API_MENU_SPECIAL, "", Properties._HTTP_METHOD_GET, Properties._CONN_TYPE_MENU_SPECIAL, true, false, this);
	}
	
	private void getNotificationSetting(){
		if(SettingManager.getSetting() == null){
			new DataLoader(Properties._API_STATE_NOTIF, "", Properties._HTTP_METHOD_GET, Properties._CONN_TYPE_STATE_NOTIF, false, false);
		}
	}
	
	private void getUserProfile(){
		if(UserProfileManager.getUserProfile() == null){
			new DataLoader(Properties._API_GET_PROFILE, "", Properties._HTTP_METHOD_GET, Properties._CONN_TYPE_GET_PROFILE, false, false);
		}
	}

	public void onLocationCompleted() {
		// TODO Auto-generated method stub
		if(TemporerStore.getHomebyDistance().size() == 0){
			new DataLoader(Properties._API_HOME_DISTANCE + "/1", "", Properties._HTTP_METHOD_GET, Properties._CONN_TYPE_HOME_DISTANCE, false, true, this);
			
			// add default
			mapsField.moveTo(new Coordinates(Double.parseDouble(latitude), Double.parseDouble(longitude), (float) 0.0));
			Marker markerMe = new Marker(new Coordinates(Double.parseDouble(latitude), Double.parseDouble(longitude), (float) 0.0), config.getIconMyPosition(), config.getIconMyPosition());
			mapsField.addMarker(markerMe);
		}
		else{
			
		}
	}

	public void onRequestCompleted(Vector vector, String type) {
		// TODO Auto-generated method stub
		if(type.equalsIgnoreCase(Properties._CONN_TYPE_HOME_DISTANCE)){
			// add multiple marker
			setMultiplePOI(vector);
		}
	}
	
	private void setMultiplePOI(Vector vector){
		if(vector.size() > 0){
			try{
				Vector body = (Vector) vector.elementAt(1);
				
				for(int p=0; p<body.size(); p++){
					BodyPersistable data = (BodyPersistable) body.elementAt(p);
					
					Marker poi = new Marker(data, new Coordinates(Double.parseDouble(data.get_lat()), Double.parseDouble(data.get_lon()), (float) 0.0), config.getIconPOI(), config.getIconPOIFocus());
					mapsField.addMarker(poi);
				}
			}catch(Exception x){
				System.out.println("Unable set poi on map " + x);
			}
		}
	}
	
//**********************CUSTOM MENU ITEM*************************************************************	
	private MenuItem _zoomIN = new MenuItem("Zoom In", 1000, 1){
        public void run(){
        	int min = mapsField.getMinZoom();
        	
        	int current = mapsField.getZoom();
        	if(current - 1 >= min){
        		mapsField.setZoom(current - 1);
        	}
        	else{
        		Dialog.alert("Maximum Zoom");
        	}
        }
    };
    
    private MenuItem _zoomOUT = new MenuItem("Zoom Out", 1000, 2){
        public void run(){
        	int max = mapsField.getMaxZoom();
        	
        	int current = mapsField.getZoom();
        	if(current + 1 <= max){
        		mapsField.setZoom(current + 1);
        	}
        	else{
        		Dialog.alert("Minimum Zoom");
        	}
        }
    };
	
	private MenuItem _mylocation = new MenuItem("Show My Location", 1000, 3){
        public void run(){
        	mapsField.moveTo(new Coordinates(Double.parseDouble(latitude), Double.parseDouble(longitude), (float) 0.0));
        }
    };
	
	private MenuItem _refresh = new MenuItem("Refresh Location", 1000, 4){
        public void run(){
        	mapsField.removeAllMarkers();
        	TemporerStore.resetHomeByDistance();
        	getLocationServices(_self);
        }
    };
	
	private MenuItem _exit = new MenuItem("Exit Application", 1000000, 1){
        public void run(){
        	System.exit(0);
        }
    };
    
	protected void makeMenu(Menu menu, int instance){ 
//	    menu.deleteAll();
		menu.add(_zoomIN);
		menu.add(_zoomOUT);
		menu.add(_mylocation);
		menu.add(_refresh);
	    menu.add(_exit);
	}
}
