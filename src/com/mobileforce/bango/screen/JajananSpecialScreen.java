package com.mobileforce.bango.screen;

import java.util.Vector;

import com.mobileforce.bango.screen.component.FetchingAnimation;
import com.mobileforce.bango.screen.component.PreviewHalf1;
import com.mobileforce.bango.services.DataLoader;
import com.mobileforce.bango.services.ImageFetching;
import com.mobileforce.bango.services.callback.ButtonPressedCallback;
import com.mobileforce.bango.services.callback.DataLoaderCallback;
import com.mobileforce.bango.storage.persistable.BodyPersistable;
import com.mobileforce.bango.storage.persistable.HeaderPersistable;
import com.mobileforce.bango.storage.query.TemporerStore;
import com.mobileforce.bango.utility.GStringUtil;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.ScrollChangeListener;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;


public class JajananSpecialScreen extends BaseScreen implements DataLoaderCallback, ButtonPressedCallback{
	private VerticalFieldManager _scrollManager;
	public PreviewHalf1[] JS;
	
	private boolean morePage = false;
	private String moreURI = "";
	
	public JajananSpecialScreen(){
		super(NO_VERTICAL_SCROLL | NO_HORIZONTAL_SCROLL);
		
		initComponent();
		initSubLayout();
		initContent();
	}
	
	public void initComponent() {
		setHeaderType3();
		headerMenu3.setHeaderTitle1(Properties._LBL_JAJANAN + " : ");
	}
	
	private void initSubLayout(){
		_scrollManager = new VerticalFieldManager(VERTICAL_SCROLL | VERTICAL_SCROLLBAR);
		_scrollManager.setScrollListener(
				new ScrollChangeListener(){
					public void scrollChanged(Manager manager, int newHorizontalScroll, int newVerticalScroll) {
						invalidate();
					}
				}
			);
	}
	
	private void initContent(){
		_contentManager.add(_scrollManager);
	}
	
	public void getContent(int index){
		headerMenu3.setHeaderTitle2(TemporerStore.getMenuSpecialData(index).get_title());
		
		String url = TemporerStore.getMenuSpecialData(index).get_nexturl();
		new DataLoader(url, "", Properties._HTTP_METHOD_GET, Properties._CONN_TYPE_JAJANAN_SPECIAL, false, true, this);
	}
	
	private void setContent(Vector vector){
		Vector header = (Vector) vector.elementAt(0);
		Vector body = (Vector) vector.elementAt(1);
		
		if(JS == null && body.size() > 0){
			JS = new PreviewHalf1[body.size()];
			String[] imgurl = new String[JS.length];

			int loop = (body.size() / 2) + (body.size() % 2);
			int n = 0;
			for (int a = 0; a < loop; a++) {
				HorizontalFieldManager ROW = new HorizontalFieldManager(
						FIELD_LEFT);
				_scrollManager.add(ROW);

				for (int r = 0; r < 2; r++) {
					if (n < body.size()) {
						BodyPersistable data = (BodyPersistable) body
								.elementAt(n);

						JS[n] = new PreviewHalf1(data);
						ROW.add(JS[n]);

						imgurl[n] = data.get_urlimg();
					}

					n++;
				}
			}

			new ImageFetching(this, imgurl, Properties._CONN_TYPE_JAJANAN_SPECIAL);
		}
		else{
			int previous = JS.length;
			
			PreviewHalf1[] oJS = JS;
			JS = new PreviewHalf1[previous + body.size()];
			String[] imgurl = new String[JS.length];
			
			for(int o=0; o<previous; o++){
				JS[o] = oJS[o];
				imgurl[o] = "";
			}
			oJS = null;
			
			
			int loop = (body.size()/2) + (body.size() % 2);
			int n = 0;
			for(int a=0; a<loop; a++){
				HorizontalFieldManager ROW = new HorizontalFieldManager(FIELD_LEFT);
				_scrollManager.add(ROW);
				
				for(int r=0; r<2; r++){
					if(n < body.size()){
						BodyPersistable data = (BodyPersistable) body.elementAt(n);
						
						JS[previous+n] = new PreviewHalf1(data);
						ROW.add(JS[previous+n]);
						
						imgurl[previous+n] = data.get_urlimg();
					}
					
					n++;
				}
			}
			
			new ImageFetching(this, imgurl, Properties._CONN_TYPE_JAJANAN_SPECIAL);
		}
		
		
		//check paging
		checkPaging(header);
	}
	
	private void checkPaging(Vector header){
		if(header.size()> 0){
			HeaderPersistable head = (HeaderPersistable) header.elementAt(0);
			if(GStringUtil.checkHTTP(head.get_nexturl())){
				morePage = true;
				moreURI = head.get_nexturl();
				
				FetchingAnimation page = new FetchingAnimation(Properties._LBL_LOAD_MORE, FIELD_HCENTER);
				page.setMargin(config.getValue10px(), 0, config.getValue10px(), 0);
				page.setButtonPressedCallback(this, Properties._CONN_TYPE_JAJANAN_SPECIAL + Properties._CONN_TYPE_PAGING);
				
				_scrollManager.add(page);
			}
		}
	}
	
	protected void onUiEngineAttached(boolean attached) {
		super.onUiEngineAttached(attached);
	}

	public void onRequestCompleted(Vector vector, String type) {
		// TODO Auto-generated method stub
		if(morePage){
			// clear fetching animation
			try{
				_scrollManager.delete(_scrollManager.getField(_scrollManager.getFieldCount()-1));
			}catch(Exception d){ System.out.println("unable delete fething animation"); }
		}
		
		setContent(vector);
	}
	
	public void onPressed(String type) {
		// TODO Auto-generated method stub
		if(morePage && type.indexOf(Properties._CONN_TYPE_PAGING)!= -1){
			new DataLoader(moreURI, "", Properties._HTTP_METHOD_GET, type.substring(0, type.indexOf(Properties._CONN_TYPE_PAGING)), true, false, this);
		}
	}
	
	public void setTitleCustom(String title){
		headerMenu3.setHeaderTitle1(title);
		headerMenu3.setHeaderTitle2("");
	}

	public void onPressedValue(Vector value, String type) {
		// TODO Auto-generated method stub
		
	}
}
