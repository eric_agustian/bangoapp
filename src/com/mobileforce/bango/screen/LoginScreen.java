package com.mobileforce.bango.screen;

import com.mobileforce.bango.screen.component.CustomBitmapButton;
import com.mobileforce.bango.screen.component.CustomEditField1;
import com.mobileforce.bango.screen.component.CustomLabelField;
import com.mobileforce.bango.screen.component.CustomPasswordField1;
import com.mobileforce.bango.services.DataLoader;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.ImageUtil;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.container.VerticalFieldManager;

public final class LoginScreen extends BaseScreenX implements FieldChangeListener{
	private VerticalFieldManager _contentManager, _box, _boxTop;
	private Bitmap _background, _box_p1, _box_p2, _box_p3;
	
	private CustomEditField1 inputEmail;
	private CustomPasswordField1 inputPassword;
	
	private CustomLabelField label1;
	private CustomBitmapButton loginBiasa;
	
	public LoginScreen(){
		super(NO_VERTICAL_SCROLL | NO_HORIZONTAL_SCROLL);
		
		initComponent();
		initSubLayout();
	}
	
	public void initSubLayout(){
		_contentManager = new VerticalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void paintBackground(Graphics g){	
				//draw background
				g.drawBitmap((Display.getWidth()-_background.getWidth())/2, (Display.getHeight()-_background.getHeight())/2, _background.getWidth(), _background.getHeight(), _background, 0, 0);
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aH = Display.getHeight();
				int aW = Display.getWidth();
				
				Field content = getField(0);
			    layoutChild(content, content.getPreferredWidth(), content.getPreferredHeight());
			    setPositionChild(content, (aW-content.getWidth())/2, (aH-content.getHeight())/2);
				
			    setExtent(aW, aH);
			}
		};
		
		_box = new VerticalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void paintBackground(Graphics g){	
				//draw background
				g.drawBitmap(0, 0, _box_p1.getWidth(), _box_p1.getHeight(), _box_p1, 0, 0);
				
				ImageUtil.drawSpriteTexture(_box_p2, g, 0, _box_p1.getWidth(), _box_p1.getWidth(), 0, _box_p1.getHeight(), _box_p1.getHeight(), ((Display.getHeight()*7)/10)-_box_p3.getHeight(), ((Display.getHeight()*7)/10)-_box_p3.getHeight());
				
				g.drawBitmap(0, ((Display.getHeight()*7)/10)-_box_p3.getHeight(), _box_p3.getWidth(), _box_p3.getHeight(), _box_p3, 0, 0);
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aH = (Display.getHeight()*7)/10;
				int aW = _box_p1.getWidth();
				
				Field top = getField(0);
			    layoutChild(top, top.getPreferredWidth(), top.getPreferredHeight());
			    setPositionChild(top, (aW-top.getWidth())/2, (aH-top.getHeight())/2);
				
			    setExtent(aW, aH);
			}
		};
		
		_boxTop = new VerticalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL | FIELD_HCENTER);
		
		_boxTop.add(label1);
		_boxTop.add(inputEmail);
		_boxTop.add(inputPassword);
		_boxTop.add(loginBiasa);
		
		_box.add(_boxTop);
		_contentManager.add(_box);
		
		this.add(_contentManager);
	}
	
	//*****************************************************************************	

	public void initComponent() {
		DeviceProperties.hideVirtualKeyboard();
		
		_background = config.getBackgroundWelcome();
		_box_p1 = config.getWelcomeBoxTopP1();
		_box_p2 = config.getWelcomeBoxTopP2();
		_box_p3 = config.getWelcomeBoxTopP3();
		
		label1 = new CustomLabelField(Properties._LBL_SIGNIN, Properties._COLOR_FONT_LABEL_WHITE, FIELD_HCENTER | NON_FOCUSABLE);
		label1.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, config.getFontSuperLargeHeight()));
		
		inputEmail = new CustomEditField1(Properties._LBL_HINT_EMAIL, 55, FIELD_HCENTER, EditField.FILTER_EMAIL, config.getFormGrey_l_email());
		inputEmail.setWidthInPixel(config.getButtonSignIn().getWidth()+config.getValue10px());
		inputEmail.setFontColor(Properties._COLOR_FONT_LABEL_WHITE);
		
		inputPassword = new CustomPasswordField1(Properties._LBL_HINT_PASSWORD, 35, FIELD_HCENTER, EditField.FILTER_DEFAULT, config.getFormGrey_l_password());
		inputPassword.setWidthInPixel(config.getButtonSignIn().getWidth()+config.getValue10px());
		inputPassword.setFontColor(Properties._COLOR_FONT_LABEL_WHITE);
		
		loginBiasa = new CustomBitmapButton(config.getButtonSignIn(), FOCUSABLE | FIELD_HCENTER);
		loginBiasa.setChangeListener(this);
		
		if(config.isOrientationPortrait()){
			label1.setMargin(config.getValue10px()*4, 0, config.getValue10px()*3, 0);
			inputEmail.setMargin(config.getValue10px()*3, 0, config.getValue10px()*3, 0);
			inputPassword.setMargin(config.getValue10px()*3, 0, config.getValue10px()*3, 0);
			loginBiasa.setMargin(config.getValue10px()*5, 0, config.getValue10px()*2, 0);
		}
		else{
			label1.setMargin(config.getValue10px()*2, 0, config.getValue10px()*2, 0);
			inputEmail.setMargin(config.getValue10px()*2, 0, config.getValue10px()*2, 0);
			inputPassword.setMargin(config.getValue10px()*2, 0, config.getValue10px()*2, 0);
			loginBiasa.setMargin(config.getValue10px()*3, 0, config.getValue10px(), 0);
		}
	}
	
	public void fieldChanged(Field field, int context) {
		if(field == loginBiasa){
			String email = inputEmail.getText().trim();
			String password = inputPassword.getText().trim();
			
			if(email.length() == 0){
				Dialog.alert(Properties._INF_KETIK_EMAIL);
			}
			else if(password.length() == 0){
				Dialog.alert(Properties._INF_KETIK_PASSWORD);
			}
			else{
				inputEmail.setHintText(Properties._LBL_HINT_EMAIL);
				inputEmail.setText("");
				inputPassword.setHintText(Properties._LBL_HINT_PASSWORD);
				inputPassword.setText("");
				
				StringBuffer parameter = new StringBuffer();
				parameter.append("email="); parameter.append(email);
				parameter.append("&passwd="); parameter.append(password);
				
				new DataLoader(Properties._API_LOGIN_APPLICATION, parameter.toString(), Properties._HTTP_METHOD_POST, Properties._CONN_TYPE_LOGIN_1, false, true);
			}
		}
	}
}
