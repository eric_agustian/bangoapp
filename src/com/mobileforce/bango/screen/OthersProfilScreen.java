package com.mobileforce.bango.screen;

import java.util.Vector;

import com.mobileforce.bango.manager.ClassManager;
import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.screen.component.AvatarField;
import com.mobileforce.bango.screen.component.CustomBitmapButton;
import com.mobileforce.bango.screen.component.CustomLabelField;
import com.mobileforce.bango.screen.component.CustomListField2;
import com.mobileforce.bango.screen.component.FetchingAnimation;
import com.mobileforce.bango.services.DataLoader;
import com.mobileforce.bango.services.ImageFetching;
import com.mobileforce.bango.services.callback.ButtonPressedCallback;
import com.mobileforce.bango.services.callback.DataLoaderCallback;
import com.mobileforce.bango.storage.persistable.BadgesPersistable;
import com.mobileforce.bango.storage.persistable.BodyPersistable;
import com.mobileforce.bango.storage.persistable.BodyPersistable2;
import com.mobileforce.bango.storage.persistable.HeaderPersistable;
import com.mobileforce.bango.storage.persistable.ProfilePersistable;
import com.mobileforce.bango.storage.persistable.ReviewsPersistable;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.GStringUtil;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.ScrollChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;


public class OthersProfilScreen extends BaseScreen implements DataLoaderCallback, ButtonPressedCallback{
	private VerticalFieldManager _formManager, _ver1, _reviewsManager, _scrollManager;
	private HorizontalFieldManager _row0, _row1, _row2, _badgesManager;
	private Bitmap _background;
	
	private CustomLabelField labelNama, labelRangking, labelBadges, labelReviews, labelNoBadges, labelNoReviews;
	private AvatarField avatar;
	
	private CustomBitmapButton[] badgesBtn;
	public CustomListField2[] reviewsList;
	private boolean morePage = false;
	private String moreURI = "";
	
	private ProfilePersistable profileData;
	
	public OthersProfilScreen(){
		super(NO_VERTICAL_SCROLL | NO_HORIZONTAL_SCROLL);
		
		initComponent();
		initSubLayout();
		initContent();
	}
	
	public void initComponent() {
		// TODO Auto-generated method stub
		_background = config.getBackgroundBlurry();
		
		headerMenu1.setHeaderTitle(Properties._LBL_PROFIL);
		
		labelNama = new CustomLabelField("", Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelNama.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, config.getFontSuperLargeHeight()));
		labelNama.setMargin(config.getValue10px(), 0, 0, config.getValue10px());
		
		labelRangking = new CustomLabelField("-", Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelRangking.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, config.getFontMediumHeight()));
//		labelRangking.setMargin(0, 0, 0, 0);
		
		labelBadges = new CustomLabelField(Properties._LBL_BADGES, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelBadges.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelBadges.setMargin(config.getValue10px()*2, 0, 0, config.getValue10px());
		
		labelReviews = new CustomLabelField(Properties._LBL_REVIEWS, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelReviews.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelReviews.setMargin(config.getValue10px()*2, 0, 0, config.getValue10px());
		
		labelNoBadges = new CustomLabelField(Properties._LBL_NO_BADGES, Properties._COLOR_FONT_LABEL_BLACK, FIELD_HCENTER | NON_FOCUSABLE);
		labelNoBadges.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, config.getFontSmallHeight()));
		labelNoBadges.setMargin(config.getValue10px()+config.getValue5px(), 0, 0, config.getValue10px());
		
		labelNoReviews = new CustomLabelField(Properties._LBL_NO_REVIEWS, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelNoReviews.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, config.getFontSmallHeight()));
		labelNoReviews.setMargin(config.getValue10px()+config.getValue5px(), 0, 0, config.getValue10px()*2);
		
		avatar = new AvatarField();
		avatar.setMargin(config.getValue10px(), config.getValue10px(), config.getValue10px(), config.getValue10px());
	}
	
	private void initSubLayout(){
		_formManager = new VerticalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void paintBackground(Graphics g){	
				//draw background
				g.drawBitmap((Display.getWidth()-_background.getWidth())/2, (Display.getHeight()-_background.getHeight())/2, _background.getWidth(), _background.getHeight(), _background, 0, 0);
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aH = Display.getHeight()-headerMenu1.getPreferredHeight();
				int aW = Display.getWidth();
				
				Field content = getField(0);
			    layoutChild(content, content.getPreferredWidth(), aH-(config.getValue10px()));
			    setPositionChild(content, 0, config.getValue5px());
				
			    setExtent(aW, aH);
			}
		};
		
		_row0 = new HorizontalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void sublayout(int maxWidth, int maxHeight){
				int aW = Display.getWidth()-(avatar.getPreferredWidth() + (config.getValue10px()*5));
				int aH = 0;
				
				Field content = getField(0);
			    layoutChild(content, aW, content.getPreferredHeight());
			    setPositionChild(content, 0, 0);
			    
			    aH += content.getHeight();
				
			    setExtent(aW, aH);
			}
		};
		
		_row2 = new HorizontalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void sublayout(int maxWidth, int maxHeight){
				int aW = Display.getWidth();
				int aH = config.getValue10px();
				
				Field content = getField(0);
			    layoutChild(content, aW-(config.getValue10px()*2), content.getPreferredHeight());
			    setPositionChild(content, config.getValue10px(), config.getValue10px());
			    
			    aH += content.getHeight() + config.getValue10px();
				
			    setExtent(aW, aH);
			}
		};
		
		_ver1 = new VerticalFieldManager(FIELD_VCENTER);
		_row1 = new HorizontalFieldManager(USE_ALL_WIDTH | FIELD_VCENTER);
		
		_scrollManager = new VerticalFieldManager(VERTICAL_SCROLL | VERTICAL_SCROLLBAR);
		_scrollManager.setScrollListener(
				new ScrollChangeListener(){
					public void scrollChanged(Manager manager, int newHorizontalScroll, int newVerticalScroll) {
						invalidate();
					}
				}
			);
		
		_badgesManager = new HorizontalFieldManager(HORIZONTAL_SCROLL | HORIZONTAL_SCROLLBAR);
		_reviewsManager = new VerticalFieldManager(NO_VERTICAL_SCROLL | NO_VERTICAL_SCROLLBAR);
	}
	
	private void initContent(){
		_row0.add(labelNama);
		
		_ver1.add(_row0);
		_ver1.add(labelRangking);
		
		_row1.add(avatar);
		_row1.add(_ver1);
		
		_row2.add(_badgesManager);
		
		_scrollManager.add(_row1);
		_scrollManager.add(labelBadges);
		_scrollManager.add(_row2);
		_scrollManager.add(labelReviews);
		_scrollManager.add(_reviewsManager);
		
		_formManager.add(_scrollManager);
		
		_contentManager.add(_formManager);
	}
	
	protected void onUiEngineAttached(boolean attached) {
		super.onUiEngineAttached(attached);
	}
	
	public void getUserProfile(String userid){
		String url = Properties._API_GET_PROFILE + "/" + userid;
		
		new DataLoader(url, "", Properties._HTTP_METHOD_GET, Properties._CONN_TYPE_GET_PROFILE_OTHER, false, true, this);
	}
	
	public void getUserProfileByUri(String uri){
		new DataLoader(uri, "", Properties._HTTP_METHOD_GET, Properties._CONN_TYPE_GET_PROFILE_OTHER, false, true, this);
	}
	
	private void setUserProfile(Vector v){
		if(v.size() > 0){
			// set profile
			Vector profile = (Vector) v.elementAt(0);
			if(profile.size() > 0){
				profileData = (ProfilePersistable) profile.elementAt(0);
				
				labelNama.setText(profileData.get_fullname());
				
				String rank = profileData.get_userrank();
				if(rank.equalsIgnoreCase("R0")){
					labelRangking.setText(Properties._LBL_BANGO_NONE);
				}
				else if(rank.equalsIgnoreCase("R1")){
					labelRangking.setText(Properties._LBL_BANGO_BRONZE);
				}
				else if(rank.equalsIgnoreCase("R2")){
					labelRangking.setText(Properties._LBL_BANGO_SILVER);
				}
				else if(rank.equalsIgnoreCase("R3")){
					labelRangking.setText(Properties._LBL_BANGO_GOLD);
				}
				
				// get avatar
				String[] avatarUrl = { profileData.get_avaimg() };
				new ImageFetching(this, avatarUrl, Properties._CONN_TYPE_AVATAR_PROFILE_OTHER);
			}
			
			// set badges
			Vector badges = (Vector) v.elementAt(1);
			if(badges.size() > 0){
				_badgesManager.deleteAll();
				
				badgesBtn = new CustomBitmapButton[badges.size()];
				
				String[] badgeUrl = new String[badgesBtn.length];
				for(int b=0; b<badgeUrl.length; b++){
					BadgesPersistable badgeData = (BadgesPersistable) badges.elementAt(b);
					badgeUrl[b] = badgeData.get_urlbadge();
				}
				
				new ImageFetching(this, badgeUrl, Properties._CONN_TYPE_BADGES_PROFILE_OTHER);
			}
			else{
				_badgesManager.deleteAll();
				_badgesManager.add(labelNoBadges);
			}
			
			// set reviews
			Vector header = (Vector) v.elementAt(2);
			Vector reviews = (Vector) v.elementAt(3);
			if(reviewsList == null && reviews.size() > 0){
				_reviewsManager.deleteAll();
				
				reviewsList = new CustomListField2[reviews.size()];
				String[] imgurl = new String[reviewsList.length];
				for(int r=0; r<reviewsList.length; r++){
					final ReviewsPersistable reviewData = (ReviewsPersistable) reviews.elementAt(r);
					
					String[] data = {
							reviewData.get_title(),
							"",
							Integer.toString(reviewData.get_rating())
					};
					
					final String type = reviewData.get_pagetype();
					
					reviewsList[r] = new CustomListField2(data);
					reviewsList[r].setWidthInPercent(100);
					reviewsList[r].setChangeListener(new FieldChangeListener() {
						public void fieldChanged(Field field, int context) {
							if(type.equalsIgnoreCase("P01")){
								BodyPersistable store = new BodyPersistable();
								store.set_title(reviewData.get_title());
								store.set_distance(0);
								store.set_address("");
								store.set_city("");
								store.set_rating(reviewData.get_rating());
								store.set_urlimg(reviewData.get_urlimg());
								store.set_urldetail(reviewData.get_nexturl());
								
								StoreDetailScreen screen = ClassManager.getInstance().getScreenManager().getStoreDetailScreen();
								screen.setDetailData(store);
								ScreenManager.getBangoApp().pushScreen(screen);
							}
							else if(type.equalsIgnoreCase("P02")){
								BodyPersistable2 recipe = new BodyPersistable2();
								recipe.set_title(reviewData.get_title());
								recipe.set_fullname("-");
								recipe.set_rating(reviewData.get_rating());
								recipe.set_urlimg(reviewData.get_urlimg());
								recipe.set_urldetail(reviewData.get_nexturl());
								
								ResepDetailScreen screen = ClassManager.getInstance().getScreenManager().getResepDetailScreen();
								screen.setDetailData(recipe);
								ScreenManager.getBangoApp().pushScreen(screen);
							}
						}
					});
					
					imgurl[r] = reviewData.get_urlimg();
					
					_reviewsManager.add(reviewsList[r]);
				}
				
				new ImageFetching(this, imgurl, Properties._CONN_TYPE_REVIEWS_ICON_OTHER);
			}
			else if(reviewsList != null && reviews.size() > 0){
				int previous = reviewsList.length;
				
				CustomListField2[] oJS = reviewsList;
				reviewsList = new CustomListField2[previous + reviews.size()];
				String[] imgurl = new String[reviewsList.length];
				
				for(int o=0; o<previous; o++){
					reviewsList[o] = oJS[o];
					imgurl[o] = "";
				}
				oJS = null;
				
				
				for(int n=0; n<reviews.size(); n++){
					final ReviewsPersistable reviewData = (ReviewsPersistable) reviews.elementAt(n);
					
					String[] data = {
							reviewData.get_title(),
							"",
							Integer.toString(reviewData.get_rating())
					};
					
					final String type = reviewData.get_pagetype();
							
					reviewsList[previous+n] = new CustomListField2(data);
					reviewsList[previous+n].setWidthInPercent(100);
					reviewsList[previous+n].setChangeListener(new FieldChangeListener() {
						public void fieldChanged(Field field, int context) {
							if(type.equalsIgnoreCase("P01")){
								BodyPersistable store = new BodyPersistable();
								store.set_title(reviewData.get_title());
								store.set_distance(0);
								store.set_address("");
								store.set_city("");
								store.set_rating(reviewData.get_rating());
								store.set_urlimg(reviewData.get_urlimg());
								store.set_urldetail(reviewData.get_nexturl());
								
								StoreDetailScreen screen = ClassManager.getInstance().getScreenManager().getStoreDetailScreen();
								screen.setDetailData(store);
								ScreenManager.getBangoApp().pushScreen(screen);
							}
							else if(type.equalsIgnoreCase("P02")){
								BodyPersistable2 recipe = new BodyPersistable2();
								recipe.set_title(reviewData.get_title());
								recipe.set_fullname("-");
								recipe.set_rating(reviewData.get_rating());
								recipe.set_urlimg(reviewData.get_urlimg());
								recipe.set_urldetail(reviewData.get_nexturl());
								
								ResepDetailScreen screen = ClassManager.getInstance().getScreenManager().getResepDetailScreen();
								screen.setDetailData(recipe);
								ScreenManager.getBangoApp().pushScreen(screen);
							}
						}
					});
							
					imgurl[previous+n] = reviewData.get_urlimg();
						
					_reviewsManager.add(reviewsList[previous+n]);
				}
				
				new ImageFetching(this, imgurl, Properties._CONN_TYPE_REVIEWS_ICON_OTHER);
			}
			else{
				_reviewsManager.deleteAll();
				_reviewsManager.add(labelNoReviews);
			}
			
			
			checkPaging(header);
		}
	}
	
	private void setUserReviewMore(Vector v){
		if(v.size() > 0){
			// set reviews
			Vector header = (Vector) v.elementAt(0);
			Vector reviews = (Vector) v.elementAt(1);
			if(reviewsList == null && reviews.size() > 0){
				_reviewsManager.deleteAll();
				
				reviewsList = new CustomListField2[reviews.size()];
				String[] imgurl = new String[reviewsList.length];
				for(int r=0; r<reviewsList.length; r++){
					final ReviewsPersistable reviewData = (ReviewsPersistable) reviews.elementAt(r);
					
					String[] data = {
							reviewData.get_title(),
							"",
							Integer.toString(reviewData.get_rating())
					};
					
					final String type = reviewData.get_pagetype();
					
					reviewsList[r] = new CustomListField2(data);
					reviewsList[r].setWidthInPercent(100);
					reviewsList[r].setChangeListener(new FieldChangeListener() {
						public void fieldChanged(Field field, int context) {
							if(type.equalsIgnoreCase("P01")){
								BodyPersistable store = new BodyPersistable();
								store.set_title(reviewData.get_title());
								store.set_distance(0);
								store.set_address("");
								store.set_city("");
								store.set_rating(reviewData.get_rating());
								store.set_urlimg(reviewData.get_urlimg());
								store.set_urldetail(reviewData.get_nexturl());
								
								StoreDetailScreen screen = ClassManager.getInstance().getScreenManager().getStoreDetailScreen();
								screen.setDetailData(store);
								ScreenManager.getBangoApp().pushScreen(screen);
							}
							else if(type.equalsIgnoreCase("P02")){
								BodyPersistable2 recipe = new BodyPersistable2();
								recipe.set_title(reviewData.get_title());
								recipe.set_fullname("-");
								recipe.set_rating(reviewData.get_rating());
								recipe.set_urlimg(reviewData.get_urlimg());
								recipe.set_urldetail(reviewData.get_nexturl());
								
								ResepDetailScreen screen = ClassManager.getInstance().getScreenManager().getResepDetailScreen();
								screen.setDetailData(recipe);
								ScreenManager.getBangoApp().pushScreen(screen);
							}
						}
					});
					
					imgurl[r] = reviewData.get_urlimg();
					
					_reviewsManager.add(reviewsList[r]);
				}
				
				new ImageFetching(this, imgurl, Properties._CONN_TYPE_REVIEWS_ICON_OTHER);
			}
			else if(reviewsList != null && reviews.size() > 0){
				int previous = reviewsList.length;
				
				CustomListField2[] oJS = reviewsList;
				reviewsList = new CustomListField2[previous + reviews.size()];
				String[] imgurl = new String[reviewsList.length];
				
				for(int o=0; o<previous; o++){
					reviewsList[o] = oJS[o];
					imgurl[o] = "";
				}
				oJS = null;
				
				
				for(int n=0; n<reviews.size(); n++){
					final ReviewsPersistable reviewData = (ReviewsPersistable) reviews.elementAt(n);
					
					String[] data = {
							reviewData.get_title(),
							"",
							Integer.toString(reviewData.get_rating())
					};
					
					final String type = reviewData.get_pagetype();
							
					reviewsList[previous+n] = new CustomListField2(data);
					reviewsList[previous+n].setWidthInPercent(100);
					reviewsList[previous+n].setChangeListener(new FieldChangeListener() {
						public void fieldChanged(Field field, int context) {
							if(type.equalsIgnoreCase("P01")){
								BodyPersistable store = new BodyPersistable();
								store.set_title(reviewData.get_title());
								store.set_distance(0);
								store.set_address("");
								store.set_city("");
								store.set_rating(reviewData.get_rating());
								store.set_urlimg(reviewData.get_urlimg());
								store.set_urldetail(reviewData.get_nexturl());
								
								StoreDetailScreen screen = ClassManager.getInstance().getScreenManager().getStoreDetailScreen();
								screen.setDetailData(store);
								ScreenManager.getBangoApp().pushScreen(screen);
							}
							else if(type.equalsIgnoreCase("P02")){
								BodyPersistable2 recipe = new BodyPersistable2();
								recipe.set_title(reviewData.get_title());
								recipe.set_fullname("-");
								recipe.set_rating(reviewData.get_rating());
								recipe.set_urlimg(reviewData.get_urlimg());
								recipe.set_urldetail(reviewData.get_nexturl());
								
								ResepDetailScreen screen = ClassManager.getInstance().getScreenManager().getResepDetailScreen();
								screen.setDetailData(recipe);
								ScreenManager.getBangoApp().pushScreen(screen);
							}
						}
					});
							
					imgurl[previous+n] = reviewData.get_urlimg();
						
					_reviewsManager.add(reviewsList[previous+n]);
				}
				
				new ImageFetching(this, imgurl, Properties._CONN_TYPE_REVIEWS_ICON_OTHER);
			}
			else{
				_reviewsManager.deleteAll();
				_reviewsManager.add(labelNoReviews);
			}
			
			
			checkPaging(header);
		}
	}
	
	private void checkPaging(Vector header){
		if(header.size()> 0){
			HeaderPersistable head = (HeaderPersistable) header.elementAt(0);
			if(GStringUtil.checkHTTP(head.get_nexturl())){
				morePage = true;
				moreURI = head.get_nexturl();
				
				FetchingAnimation page = new FetchingAnimation(Properties._LBL_LOAD_MORE, FIELD_HCENTER);
				page.setMargin(config.getValue10px(), 0, config.getValue10px(), 0);
				page.setButtonPressedCallback(this, Properties._CONN_TYPE_USER_REVIEW_PAGING_OTHER);
				
				_reviewsManager.add(page);
			}
		}
	}
	
	public void setAvatarBitmap(Bitmap bitmap){
		synchronized (UiApplication.getUiApplication().getAppEventLock()) {
			if(labelRangking.getText().equalsIgnoreCase(Properties._LBL_BANGO_BRONZE)){
				avatar.setBronzeAvatar(bitmap);
			}
			else if(labelRangking.getText().equalsIgnoreCase(Properties._LBL_BANGO_SILVER)){
				avatar.setSilverAvatar(bitmap);
			}
			else if(labelRangking.getText().equalsIgnoreCase(Properties._LBL_BANGO_GOLD)){
				avatar.setGoldAvatar(bitmap);
			}
			else{
				// netral
				avatar.setNetralAvatar(bitmap);
			}
		}
	}
	
	public void setBadgesBitmap(Bitmap bitmap, int index){
		synchronized (UiApplication.getUiApplication().getAppEventLock()) {
			badgesBtn[index] = new CustomBitmapButton(bitmap);
			badgesBtn[index].setMargin(config.getValue5px(), config.getValue5px(), config.getValue5px(), config.getValue5px());
			
			_badgesManager.add(badgesBtn[index]);
		}
	}

	public void onRequestCompleted(Vector vector, String type) {
		// TODO Auto-generated method stub
		if(morePage){
			// clear fetching animation
			try{
				_reviewsManager.delete(_reviewsManager.getField(_reviewsManager.getFieldCount()-1));
			}catch(Exception d){ System.out.println("unable delete fething animation"); }
		}
		
		if(type.equalsIgnoreCase(Properties._CONN_TYPE_USER_REVIEW_PAGING_OTHER)){
			setUserReviewMore(vector);
		}
		else{
			setUserProfile(vector);
		}
	}
	
	public void onPressed(String type) {
		// TODO Auto-generated method stub
		if(morePage){
			new DataLoader(moreURI, "", Properties._HTTP_METHOD_GET, type, true, false, this);
		}
	}

	public void onPressedValue(Vector value, String type) {
		// TODO Auto-generated method stub
		
	}
}
