package com.mobileforce.bango.screen;

import com.mobileforce.bango.screen.component.CustomButton2;
import com.mobileforce.bango.screen.component.CustomChoiceField;
import com.mobileforce.bango.screen.component.CustomDateField;
import com.mobileforce.bango.screen.component.CustomEditField2;
import com.mobileforce.bango.screen.component.CustomLabelField;
import com.mobileforce.bango.screen.component.CustomPasswordField2;
import com.mobileforce.bango.services.DataLoader;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.ScrollChangeListener;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;


public class RegisterScreen extends BaseScreen implements FieldChangeListener{
	private VerticalFieldManager _formManager, _scrollManager;
	private HorizontalFieldManager _genderManager;
	private Bitmap _background;
	
	private CustomLabelField labelNama, labelPassword, labelPasswordUlang, labelEmail, labelGender, labelTglLahir;
	private CustomEditField2 inputNama, inputEmail;
	private CustomPasswordField2 inputPassword, inputPasswordUlang;
	private CustomDateField inputTglLahir;
	
	private CustomChoiceField gender1, gender2;
	
	private CustomButton2 btnDaftar;
	
	public RegisterScreen(){
		super(NO_VERTICAL_SCROLL | NO_HORIZONTAL_SCROLL);
		
		initComponent();
		initSubLayout();
		initContent();
	}
	
	public void initComponent() {
		// TODO Auto-generated method stub
		_background = config.getBackgroundBlurry();
		
		headerMenu1.setHeaderTitle(Properties._LBL_DAFTAR);
		headerMenu1.removeRightMenu();
		
		labelNama = new CustomLabelField(Properties._LBL_NAMA, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelNama.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelNama.setMargin(config.getValue10px()+config.getValue5px(), 0, 0, 0);
		
		labelPassword = new CustomLabelField(Properties._LBL_PASSWORD, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelPassword.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelPassword.setMargin(config.getValue10px()+config.getValue5px(), 0, 0, 0);
		
		labelPasswordUlang = new CustomLabelField(Properties._LBL_PASSWORD_ULANG, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelPasswordUlang.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelPasswordUlang.setMargin(config.getValue10px()+config.getValue5px(), 0, 0, 0);
		
		labelEmail = new CustomLabelField(Properties._LBL_EMAIL, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelEmail.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelEmail.setMargin(config.getValue10px()+config.getValue5px(), 0, 0, 0);
		
		labelGender = new CustomLabelField(Properties._LBL_GENDER, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelGender.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelGender.setMargin(config.getValue10px()+config.getValue5px(), 0, 0, 0);
		
		labelTglLahir = new CustomLabelField(Properties._LBL_TGL_LAHIR, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelTglLahir.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelTglLahir.setMargin(config.getValue10px()+config.getValue5px(), 0, 0, 0);
		
		inputNama = new CustomEditField2(Properties._LBL_HINT_NAMA, 35, FIELD_HCENTER, EditField.FILTER_DEFAULT);
		inputNama.setWidthInPercent(85);
		
		inputPassword = new CustomPasswordField2(Properties._LBL_HINT_PASSWORD2, 55, FIELD_HCENTER, EditField.FILTER_DEFAULT);
		inputPassword.setWidthInPercent(85);
		
		inputPasswordUlang = new CustomPasswordField2(Properties._LBL_HINT_PASSWORD_ULANG, 55, FIELD_HCENTER, EditField.FILTER_DEFAULT);
		inputPasswordUlang.setWidthInPercent(85);
		
		inputEmail = new CustomEditField2(Properties._LBL_HINT_EMAIL2, 55, FIELD_HCENTER, EditField.FILTER_EMAIL);
		inputEmail.setWidthInPercent(85);
		
		inputTglLahir = new CustomDateField(Properties._LBL_HINT_TGL_LAHIR, 0, "dd-MM-yyyy", FOCUSABLE);
		inputTglLahir.setWidthInPercent(85);
		
		
		gender1 = new CustomChoiceField(Properties._LBL_GENDER_L, DeviceProperties.getDefaultApplicationFont(Font.PLAIN, config.getFontMediumHeight()), Properties._COLOR_FONT_LABEL_GREEN, Properties._COLOR_FONT_LABEL_BLACK, true);
		gender1.setMargin(0, config.getValue10px(), 0, 0);
		gender1.setChangeListener(this);
		
		gender2 = new CustomChoiceField(Properties._LBL_GENDER_P, DeviceProperties.getDefaultApplicationFont(Font.PLAIN, config.getFontMediumHeight()), Properties._COLOR_FONT_LABEL_GREEN, Properties._COLOR_FONT_LABEL_BLACK, true);
		gender2.setMargin(0, 0, 0, config.getValue10px());
		gender2.setChangeListener(this);
		
		btnDaftar = new CustomButton2(Properties._LBL_DAFTAR, FOCUSABLE | FIELD_HCENTER);
		btnDaftar.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontLargeHeight()));
		btnDaftar.setButtonWidthInPercent(60);
		btnDaftar.setMargin(config.getValue10px()*2, 0, config.getValue10px(), 0);
		btnDaftar.setChangeListener(this);
	}
	
	private void initSubLayout(){
		_formManager = new VerticalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void paintBackground(Graphics g){	
				//draw background
				g.drawBitmap((Display.getWidth()-_background.getWidth())/2, (Display.getHeight()-_background.getHeight())/2, _background.getWidth(), _background.getHeight(), _background, 0, 0);
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aH = Display.getHeight()-headerMenu1.getPreferredHeight();
				int aW = Display.getWidth();
				
				Field content = getField(0);
			    layoutChild(content, content.getPreferredWidth(), aH-(config.getValue10px()));
			    setPositionChild(content, (aW-content.getWidth())/2, config.getValue5px());
				
			    setExtent(aW, aH);
			}
		};
		
		_scrollManager = new VerticalFieldManager(VERTICAL_SCROLL | VERTICAL_SCROLLBAR);
		_scrollManager.setScrollListener(
				new ScrollChangeListener(){
					public void scrollChanged(Manager manager, int newHorizontalScroll, int newVerticalScroll) {
						invalidate();
					}
				}
			);
		
		_genderManager = new HorizontalFieldManager(NO_HORIZONTAL_SCROLL);
	}
	
	private void initContent(){
		_genderManager.add(gender1);
		_genderManager.add(gender2);
		
		_scrollManager.add(labelNama);
		_scrollManager.add(inputNama);
		_scrollManager.add(labelPassword);
		_scrollManager.add(inputPassword);
		_scrollManager.add(labelPasswordUlang);
		_scrollManager.add(inputPasswordUlang);
		_scrollManager.add(labelEmail);
		_scrollManager.add(inputEmail);
		_scrollManager.add(labelGender);
		_scrollManager.add(_genderManager);
		_scrollManager.add(labelTglLahir);
		_scrollManager.add(inputTglLahir);
		_scrollManager.add(btnDaftar);
		
		_formManager.add(_scrollManager);
		
		_contentManager.add(_formManager);
	}
	
	protected void onUiEngineAttached(boolean attached) {
		super.onUiEngineAttached(attached);
	}
	
	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		if(field == btnDaftar){
			String fname = inputNama.getText().trim();
			String gender = gender1.getSelectedValue() ? "1" : "2";
			String email = inputEmail.getText().trim();
			String passwd = inputPassword.getText().trim();
			String repasswd = inputPasswordUlang.getText().trim();
			String bdate = inputTglLahir.getDate() == null ? null : inputTglLahir.getDate("yyyy-MM-dd");
			
			if(fname.length() == 0){
				Dialog.alert(Properties._INF_KETIK_NAMA);
			}
			else if(email.length() == 0){
				Dialog.alert(Properties._INF_KETIK_EMAIL);
			}
			else if(passwd.length() == 0){
				Dialog.alert(Properties._INF_KETIK_PASSWORD);
			}
			else if(!passwd.equals(repasswd)){
				Dialog.alert(Properties._INF_PASSWORD_BEDA);
			}
			else if(!gender1.getSelectedValue() && !gender2.getSelectedValue()){
				Dialog.alert(Properties._INF_PILIH_GENDER);
			}
			else if(bdate == null){
				Dialog.alert(Properties._INF_PILIH_TANGGAL_LAHIR);
			}
			else{
				StringBuffer parameter = new StringBuffer();
				parameter.append("fname="); parameter.append(fname);
				parameter.append("&gender="); parameter.append(gender);
				parameter.append("&email="); parameter.append(email);
				parameter.append("&passwd="); parameter.append(passwd);
				parameter.append("&bdate="); parameter.append(bdate);
				
				new DataLoader(Properties._API_REGISTER_USER, parameter.toString(), Properties._HTTP_METHOD_POST, Properties. _CONN_TYPE_REGISTER_1, false, true);
			}
		}
		else if(field == gender1){
			gender2.setSelectedValue(!gender1.getSelectedValue());
		}
		else if(field == gender2){
			gender1.setSelectedValue(!gender2.getSelectedValue());
		}
	}

}
