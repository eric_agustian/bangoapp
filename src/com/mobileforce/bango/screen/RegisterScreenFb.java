package com.mobileforce.bango.screen;

import java.util.Calendar;

import com.mobileforce.bango.screen.component.CustomButton2;
import com.mobileforce.bango.screen.component.CustomChoiceField;
import com.mobileforce.bango.screen.component.CustomDateField;
import com.mobileforce.bango.screen.component.CustomEditField2;
import com.mobileforce.bango.screen.component.CustomLabelField;
import com.mobileforce.bango.services.DataLoader;
import com.mobileforce.bango.services.FacebookConnector;
import com.mobileforce.bango.storage.persistable.FacebookPersistable;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.ScrollChangeListener;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;


public class RegisterScreenFb extends BaseScreen implements FieldChangeListener{
	private VerticalFieldManager _formManager, _scrollManager;
	private HorizontalFieldManager _genderManager;
	private Bitmap _background;
	
	private CustomLabelField labelNama1, labelNama2, labelEmail, labelGender, labelTglLahir, labelLocation;
	private CustomEditField2 inputNama1, inputNama2, inputEmail, inputLocation;
	private CustomDateField inputTglLahir;
	
	private CustomChoiceField gender1, gender2;
	
	private CustomButton2 btnDaftar;
	
	public RegisterScreenFb(){
		super(NO_VERTICAL_SCROLL | NO_HORIZONTAL_SCROLL);
		
		initComponent();
		initSubLayout();
		initContent();
		initDefaultValue();
	}
	
	public void initComponent() {
		// TODO Auto-generated method stub
		_background = config.getBackgroundBlurry();
		
		headerMenu1.setHeaderTitle(Properties._LBL_DAFTAR);
		headerMenu1.removeRightMenu();
		
		labelNama1 = new CustomLabelField(Properties._LBL_FNAMA, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelNama1.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelNama1.setMargin(config.getValue10px()+config.getValue5px(), 0, 0, 0);
		
		labelNama2 = new CustomLabelField(Properties._LBL_LNAMA, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelNama2.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelNama2.setMargin(config.getValue10px()+config.getValue5px(), 0, 0, 0);
		
		labelEmail = new CustomLabelField(Properties._LBL_EMAIL, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelEmail.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelEmail.setMargin(config.getValue10px()+config.getValue5px(), 0, 0, 0);
		
		labelGender = new CustomLabelField(Properties._LBL_GENDER, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelGender.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelGender.setMargin(config.getValue10px()+config.getValue5px(), 0, 0, 0);
		
		labelTglLahir = new CustomLabelField(Properties._LBL_TGL_LAHIR, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelTglLahir.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelTglLahir.setMargin(config.getValue10px()+config.getValue5px(), 0, 0, 0);
		
		labelLocation = new CustomLabelField(Properties._LBL_LOKASI, Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelLocation.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontMediumHeight()));
		labelLocation.setMargin(config.getValue10px()+config.getValue5px(), 0, 0, 0);
		
		inputNama1 = new CustomEditField2(Properties._LBL_HINT_FNAMA, 25, FIELD_HCENTER, EditField.FILTER_DEFAULT);
		inputNama1.setWidthInPercent(85);
		
		inputNama2 = new CustomEditField2(Properties._LBL_HINT_LNAMA, 25, FIELD_HCENTER, EditField.FILTER_DEFAULT);
		inputNama2.setWidthInPercent(85);
		
		inputEmail = new CustomEditField2(Properties._LBL_HINT_EMAIL2, 55, FIELD_HCENTER, EditField.FILTER_EMAIL);
		inputEmail.setWidthInPercent(85);
		
		inputTglLahir = new CustomDateField(Properties._LBL_HINT_TGL_LAHIR, 0, "dd-MM-yyyy", FOCUSABLE);
		inputTglLahir.setWidthInPercent(85);
		
		gender1 = new CustomChoiceField(Properties._LBL_GENDER_L, DeviceProperties.getDefaultApplicationFont(Font.PLAIN, config.getFontMediumHeight()), Properties._COLOR_FONT_LABEL_GREEN, Properties._COLOR_FONT_LABEL_BLACK, true);
		gender1.setMargin(0, config.getValue10px(), 0, 0);
		gender1.setChangeListener(this);
		
		gender2 = new CustomChoiceField(Properties._LBL_GENDER_P, DeviceProperties.getDefaultApplicationFont(Font.PLAIN, config.getFontMediumHeight()), Properties._COLOR_FONT_LABEL_GREEN, Properties._COLOR_FONT_LABEL_BLACK, true);
		gender2.setMargin(0, 0, 0, config.getValue10px());
		gender2.setChangeListener(this);
		
		inputLocation = new CustomEditField2(Properties._LBL_HINT_LOKASI, 35, FIELD_HCENTER, EditField.FILTER_DEFAULT);
		inputLocation.setWidthInPercent(85);
		
		btnDaftar = new CustomButton2(Properties._LBL_DAFTAR, FOCUSABLE | FIELD_HCENTER);
		btnDaftar.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontLargeHeight()));
		btnDaftar.setButtonWidthInPercent(60);
		btnDaftar.setMargin(config.getValue10px()*2, 0, config.getValue10px(), 0);
		btnDaftar.setChangeListener(this);
	}
	
	private void initSubLayout(){
		_formManager = new VerticalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void paintBackground(Graphics g){	
				//draw background
				g.drawBitmap((Display.getWidth()-_background.getWidth())/2, (Display.getHeight()-_background.getHeight())/2, _background.getWidth(), _background.getHeight(), _background, 0, 0);
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aH = Display.getHeight()-headerMenu1.getPreferredHeight();
				int aW = Display.getWidth();
				
				Field content = getField(0);
			    layoutChild(content, content.getPreferredWidth(), aH-(config.getValue10px()));
			    setPositionChild(content, (aW-content.getWidth())/2, config.getValue5px());
				
			    setExtent(aW, aH);
			}
		};
		
		_scrollManager = new VerticalFieldManager(VERTICAL_SCROLL | VERTICAL_SCROLLBAR);
		_scrollManager.setScrollListener(
				new ScrollChangeListener(){
					public void scrollChanged(Manager manager, int newHorizontalScroll, int newVerticalScroll) {
						invalidate();
					}
				}
			);
		
		_genderManager = new HorizontalFieldManager(NO_HORIZONTAL_SCROLL);
	}
	
	private void initContent(){
		_genderManager.add(gender1);
		_genderManager.add(gender2);
		
		_scrollManager.add(labelNama1);
		_scrollManager.add(inputNama1);
		_scrollManager.add(labelNama2);
		_scrollManager.add(inputNama2);
		
		_scrollManager.add(labelEmail);
		try{
			if(FacebookConnector.loadData().getFacebook_email().trim().length() <= 5){
				_scrollManager.add(inputEmail);
			}
			else{
				CustomLabelField email = new CustomLabelField(FacebookConnector.loadData().getFacebook_email(), Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
				email.setFont(DeviceProperties.getDefaultApplicationFont(Font.ITALIC, config.getFontMediumHeight()));
				email.setMargin(config.getValue5px(), 0, +config.getValue5px(), 0);
				_scrollManager.add(email);
			}
		}catch(Exception x){
			_scrollManager.add(inputEmail);
		}
		
		
		_scrollManager.add(labelGender);
		_scrollManager.add(_genderManager);
		_scrollManager.add(labelTglLahir);
		_scrollManager.add(inputTglLahir);
		
		_scrollManager.add(labelLocation);
		try{
			if(FacebookConnector.loadData().getFacebook_location().trim().length() <= 5){
				_scrollManager.add(inputLocation);
			}
			else{
				CustomLabelField location = new CustomLabelField(FacebookConnector.loadData().getFacebook_location(), Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
				location.setFont(DeviceProperties.getDefaultApplicationFont(Font.ITALIC, config.getFontMediumHeight()));
				location.setMargin(config.getValue5px(), 0, +config.getValue5px(), 0);
				_scrollManager.add(location);
			}
		}catch(Exception x){
			_scrollManager.add(inputLocation);
		}
		
		_scrollManager.add(btnDaftar);
		
		_formManager.add(_scrollManager);
		
		_contentManager.add(_formManager);
	}
	
	private void initDefaultValue(){
		FacebookPersistable data = FacebookConnector.loadData();
		
		inputNama1.setText(data.getFacebook_firstname());
		inputNama2.setText(data.getFacebook_lastname());
		
		try{
			if(data.getFacebook_email().trim().length() > 5){
				inputEmail.setText(data.getFacebook_email());
			}
		}catch(Exception x){}
		
		try{
			if(data.getFacebook_location().trim().length() <= 5){
				inputLocation.setText(data.getFacebook_location());
			}
		}catch(Exception x){}
		
		if(data.getFacebook_gender().equalsIgnoreCase("1")){
			gender1.setSelectedValue(true);
		}
		else{
			gender2.setSelectedValue(true);
		}
		
		Calendar initDate = Calendar.getInstance();
		try{
			System.out.println("day >" + data.getFacebook_birthday().substring(8));
			System.out.println("month >" + data.getFacebook_birthday().substring(5, 7));
			System.out.println("year >" + data.getFacebook_birthday().substring(0, 4));
			
			initDate.set(Calendar.DAY_OF_MONTH, Integer.parseInt(data.getFacebook_birthday().substring(8)));
			switch(Integer.parseInt(data.getFacebook_birthday().substring(5, 7))){
			case 1:
				initDate.set(Calendar.MONTH, Calendar.JANUARY);
				break;
			case 2:
				initDate.set(Calendar.MONTH, Calendar.FEBRUARY);
				break;
			case 3:
				initDate.set(Calendar.MONTH, Calendar.MARCH);
				break;
			case 4:
				initDate.set(Calendar.MONTH, Calendar.APRIL);
				break;
			case 5:
				initDate.set(Calendar.MONTH, Calendar.MAY);
				break;
			case 6:
				initDate.set(Calendar.MONTH, Calendar.JUNE);
				break;
			case 7:
				initDate.set(Calendar.MONTH, Calendar.JULY);
				break;
			case 8:
				initDate.set(Calendar.MONTH, Calendar.AUGUST);
				break;
			case 9:
				initDate.set(Calendar.MONTH, Calendar.SEPTEMBER);
				break;
			case 10:
				initDate.set(Calendar.MONTH, Calendar.OCTOBER);
				break;
			case 11:
				initDate.set(Calendar.MONTH, Calendar.NOVEMBER);
				break;
			default:
				initDate.set(Calendar.MONTH, Calendar.DECEMBER);
				break;
			}
			
			initDate.set(Calendar.YEAR, Integer.parseInt(data.getFacebook_birthday().substring(0, 4)));
			inputTglLahir.setDateAndInitialCalendar(data.getFacebook_birthday(), initDate);
		}catch(Exception x){
			System.out.println("unable set birthdate > " + x);
		}
	}
	
	protected void onUiEngineAttached(boolean attached) {
		super.onUiEngineAttached(attached);
	}
	
	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		if(field == btnDaftar){
			String fname = inputNama1.getText().trim();
			String lname = inputNama2.getText().trim();
			String gender = gender1.getSelectedValue() ? "1" : "2";
			String email = inputEmail.getText().trim();
			String bdate = inputTglLahir.getDate() == null ? null : inputTglLahir.getDate("yyyy-MM-dd");
			String location = inputLocation.getText().trim();
			
			if(fname.length() == 0){
				Dialog.alert(Properties._INF_KETIK_FNAMA);
			}
			else if(lname.length() == 0){
				Dialog.alert(Properties._INF_KETIK_LNAMA);
			}
			else if(email.length() == 0){
				Dialog.alert(Properties._INF_KETIK_EMAIL);
			}
			else if(!gender1.getSelectedValue() && !gender2.getSelectedValue()){
				Dialog.alert(Properties._INF_PILIH_GENDER);
			}
			else if(bdate == null){
				Dialog.alert(Properties._INF_PILIH_TANGGAL_LAHIR);
			}
			else if(location.length() == 0){
				Dialog.alert(Properties._INF_KETIK_LOKASI);
			}
			else{
				StringBuffer parameter = new StringBuffer();
				parameter.append("fbid="); parameter.append(FacebookConnector.loadData().getFacebook_userid());
				parameter.append("&fname="); parameter.append(fname);
				parameter.append("&lname="); parameter.append(lname);
				parameter.append("&gender="); parameter.append(gender);
				parameter.append("&email="); parameter.append(email);
				parameter.append("&bdate="); parameter.append(bdate);
				parameter.append("&location="); parameter.append(location);
				
				new DataLoader(Properties._API_REGISTER_FACEBOOK, parameter.toString(), Properties._HTTP_METHOD_POST, Properties. _CONN_TYPE_REGISTER_2, false, true);
			}
		}
		else if(field == gender1){
			gender2.setSelectedValue(!gender1.getSelectedValue());
		}
		else if(field == gender2){
			gender1.setSelectedValue(!gender2.getSelectedValue());
		}
	}

}
