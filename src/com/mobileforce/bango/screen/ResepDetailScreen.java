//#preprocess

package com.mobileforce.bango.screen;

import java.util.Vector;

import com.mobileforce.bango.manager.ClassManager;
import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.screen.component.CustomBitmapField;
import com.mobileforce.bango.screen.component.CustomLabelField;
import com.mobileforce.bango.screen.component.FetchingAnimation;
import com.mobileforce.bango.screen.component.ReviewListField;
import com.mobileforce.bango.screen.component.SharePopupScreen;
import com.mobileforce.bango.screen.component.ShareSelectionDialog;
import com.mobileforce.bango.screen.component.SmallRatingField;
import com.mobileforce.bango.services.DataLoader;
import com.mobileforce.bango.services.FacebookConnector;
import com.mobileforce.bango.services.ImageFetching;
import com.mobileforce.bango.services.ShareCreator;
import com.mobileforce.bango.services.TwitterConnector;
import com.mobileforce.bango.services.UserProfileManager;
import com.mobileforce.bango.services.callback.ButtonPressedCallback;
import com.mobileforce.bango.services.callback.DataLoaderCallback;
import com.mobileforce.bango.services.callback.OtherScreenCallback;
import com.mobileforce.bango.storage.persistable.BodyPersistable2;
import com.mobileforce.bango.storage.persistable.BodyPersistable4;
import com.mobileforce.bango.storage.persistable.DetailResepPersistable;
import com.mobileforce.bango.storage.persistable.HeaderPersistable;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.GStringUtil;
import com.mobileforce.bango.utility.ImageScaler;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.system.PNGEncodedImage;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.ScrollChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.XYRect;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;
//#ifdef TouchEnabled
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.TouchGesture;
//#endif


public class ResepDetailScreen extends BaseScreen implements FieldChangeListener, DataLoaderCallback, ButtonPressedCallback, OtherScreenCallback{
	private VerticalFieldManager FIX, _bodyManager, _reviewsManager, _scrollManager;
	
	private HorizontalFieldManager _row1, _row2, _row3, _row4, _row5, _row6;
	
	private CustomLabelField labelNama, labelOleh, labelReviews, labelDetail, labelBahan, labelLangkah;
	private CustomBitmapField previewBig;
	private SmallRatingField starRating;
	
	public ReviewListField[] reviewList;
	
	private boolean morePage = false;
	private String moreURI = "";
	
	private DetailResepPersistable detailData;
	private String urlDetail = "";
	private Bitmap previewBitmap;
	private int ownRating = 0;
	private int otherRating = 0;
	private String otherName = "-";
	
	public ResepDetailScreen(){
		super(NO_VERTICAL_SCROLL | NO_HORIZONTAL_SCROLL);
		
		initComponent();
		initSubLayout();
		initContent();
	}
	
	public void initComponent() {
		setHeaderType3();
		headerMenu3.setHeaderTitle1(Properties._LBL_RESEP.substring(0, Properties._LBL_RESEP.indexOf(" ")));
		headerMenu3.setHeaderTitle2(Properties._LBL_RESEP.substring(Properties._LBL_RESEP.indexOf(" ")));
		
		extraMenu4.setButtonPressedCallback(this);
		
		labelNama = new CustomLabelField("-", Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelNama.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontSuperLargeHeight()));
		labelNama.setMargin(config.getValue10px(), 0, 0, config.getValue10px());
		
		labelReviews = new CustomLabelField("-", Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelReviews.setFont(DeviceProperties.getDefaultApplicationFont(Font.ITALIC, config.getFontMediumHeight()));
		labelReviews.setMargin(0, 0, 0, 0);
		
		labelOleh = new CustomLabelField("-", Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | NON_FOCUSABLE);
		labelOleh.setFont(DeviceProperties.getDefaultApplicationFont(Font.ITALIC, config.getFontMediumHeight()));
		labelOleh.setMargin(0, 0, 0, 0);
		
		labelDetail = new CustomLabelField("-", Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | FOCUSABLE);
		labelDetail.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, config.getFontMediumHeight()));
//		labelDetail.setMargin(config.getValue10px(), 0, config.getValue10px(), 0);
		
		labelBahan = new CustomLabelField("-", Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | FOCUSABLE);
		labelBahan.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, config.getFontMediumHeight()));
//		labelBahan.setMargin(config.getValue10px(), 0, config.getValue10px(), 0);
		
		labelLangkah = new CustomLabelField("-", Properties._COLOR_FONT_LABEL_BLACK, FIELD_LEFT | FOCUSABLE);
		labelLangkah.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, config.getFontMediumHeight()));
//		labelLangkah.setMargin(config.getValue10px(), 0, config.getValue10px(), 0);
		
		previewBig = new CustomBitmapField(ImageScaler.resizeBitmapARGB(config.getTemporerResep(), config.getBigPreviewWidth(), config.getBigPreviewHeight()), FOCUSABLE);
		
		starRating = new SmallRatingField(0, NON_FOCUSABLE);
		starRating.setMaximumRate(5);
		starRating.setMargin(0, config.getValue5px(), 0, config.getValue10px());
		
	}
	
	private void initSubLayout(){
		FIX = contentWithExtraMenu();
		
		_bodyManager = new VerticalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void sublayout(int maxWidth, int maxHeight){
				int aH = Display.getHeight()-(headerMenu3.getPreferredHeight()+extraMenu4.getPreferredHeight());
				int aW = Display.getWidth();
				
				Field content = getField(0);
			    layoutChild(content, content.getPreferredWidth(), aH);
			    setPositionChild(content, 0, 0);
				
			    setExtent(aW, aH);
			}
		};
		
		_row1 = new HorizontalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void sublayout(int maxWidth, int maxHeight){
				int aW = Display.getWidth();
				int aH = config.getValue10px();
				
				Field content = getField(0);
			    layoutChild(content, aW-(config.getValue10px()*2), content.getPreferredHeight());
			    setPositionChild(content, config.getValue10px(), config.getValue10px());
			    
			    aH += content.getHeight() + config.getValue10px();
				
			    setExtent(aW, aH);
			}
		};
		
		_row2 = new HorizontalFieldManager(USE_ALL_WIDTH);
		
		_row3 = new HorizontalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void sublayout(int maxWidth, int maxHeight){
				int aW = Display.getWidth();
				int aH = config.getValue10px();
				
				Field content = getField(0);
			    layoutChild(content, aW-(config.getValue10px()*2), content.getPreferredHeight());
			    setPositionChild(content, config.getValue10px(), config.getValue10px());
			    
			    aH += content.getHeight() + config.getValue10px();
				
			    setExtent(aW, aH);
			}
		};
		
		_row4 = new HorizontalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void sublayout(int maxWidth, int maxHeight){
				int aW = Display.getWidth();
				int aH = config.getValue10px();
				
				Field content = getField(0);
			    layoutChild(content, aW-(config.getValue10px()*2), content.getPreferredHeight());
			    setPositionChild(content, config.getValue10px(), config.getValue10px());
			    
			    aH += content.getHeight() + config.getValue10px();
				
			    setExtent(aW, aH);
			}
		};
		
		_row5 = new HorizontalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void sublayout(int maxWidth, int maxHeight){
				int aW = Display.getWidth();
				int aH = config.getValue10px();
				
				Field content = getField(0);
			    layoutChild(content, aW-(config.getValue10px()*2), content.getPreferredHeight());
			    setPositionChild(content, config.getValue10px(), config.getValue10px());
			    
			    aH += content.getHeight() + config.getValue10px();
				
			    setExtent(aW, aH);
			}
		};
		
		_row6 = new HorizontalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void sublayout(int maxWidth, int maxHeight){
				int aW = Display.getWidth();
				int aH = config.getValue10px();
				
				Field content = getField(0);
			    layoutChild(content, aW-(config.getValue10px()*2), content.getPreferredHeight());
			    setPositionChild(content, config.getValue10px(), config.getValue10px());
			    
			    aH += content.getHeight() + config.getValue10px();
				
			    setExtent(aW, aH);
			}
		};
		
		_scrollManager = new VerticalFieldManager(VERTICAL_SCROLL | VERTICAL_SCROLLBAR);
		_scrollManager.setScrollListener(
				new ScrollChangeListener(){
					public void scrollChanged(Manager manager, int newHorizontalScroll, int newVerticalScroll) {
						invalidate();
					}
				}
			);
		
		_reviewsManager = new VerticalFieldManager(NO_VERTICAL_SCROLL | NO_VERTICAL_SCROLLBAR);
	}
	
	private void initContent(){
		if(config.isOrientationPortrait()){
			FIX.add(_bodyManager);
			FIX.add(extraMenu4);
		}
		else{
			FIX.add(extraMenu4);
			FIX.add(_bodyManager);
		}
		
		
		_row1.add(labelNama);
		
		_row2.add(starRating);
		_row2.add(labelReviews);
		
		_row3.add(labelOleh);
		_row4.add(labelDetail);
		_row5.add(labelBahan);
		_row6.add(labelLangkah);
		
		_scrollManager.add(previewBig);
		_scrollManager.add(_row1);
		_scrollManager.add(_row2);
		_scrollManager.add(_row3);
		_scrollManager.add(_row4);
		_scrollManager.add(_row5);
		_scrollManager.add(_row6);
		_scrollManager.add(_reviewsManager);
		
		_bodyManager.add(_scrollManager);
		
		_contentManager.add(FIX);
	}
	
	protected void onUiEngineAttached(boolean attached) {
		super.onUiEngineAttached(attached);
	}
	
	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		
	}
	
	private void setDetailResep(Vector v){
		if(v.size() > 0){
			// set detail
			Vector detail = (Vector) v.elementAt(0);
			if(detail.size() > 0){
				detailData = (DetailResepPersistable) detail.elementAt(0);
				
				labelNama.setText(detailData.get_title());
				labelOleh.setText("Oleh " + detailData.get_fullname());
				starRating.setCurrentRate(detailData.get_rating());
				labelReviews.setText("Dari " + detailData.get_num_rating() + " review");
				labelDetail.setText(detailData.get_desc());
				labelBahan.setText(detailData.get_ingredients());
				labelLangkah.setText(detailData.get_steps());
			}
			
			// set reviews
			Vector header = (Vector) v.elementAt(1);
			Vector reviews = (Vector) v.elementAt(2);
			if(reviewList == null && reviews.size() > 0){
				_reviewsManager.deleteAll();
				
				reviewList = new ReviewListField[reviews.size()];
				String[] imgurl = new String[reviewList.length];
				String[] imgurl2 = new String[reviewList.length];
				
				for(int r=0; r<reviewList.length; r++){
					BodyPersistable4 reviewData = (BodyPersistable4) reviews.elementAt(r);
					
					reviewList[r] = new ReviewListField(reviewData);
					reviewList[r].setMasterTitle(labelNama.getText());
					reviewList[r].setMasterImage(detailData.get_urlimg());
					reviewList[r].setMargin(0, 0, config.getValue10px(), 0);
					reviewList[r].setButtonPressedCallback(this);
					
					imgurl[r] = reviewData.get_avaimg();
					imgurl2[r] = reviewData.get_urlimg();
					
					_reviewsManager.add(reviewList[r]);
				}
				
				new ImageFetching(this, imgurl, Properties._CONN_TYPE_RECIPE_REVIEW_AVATAR);
				new ImageFetching(this, imgurl2, Properties._CONN_TYPE_RECIPE_REVIEW_IMAGE);
			}
			else if(reviewList != null && reviews.size() > 0){
				int previous = reviewList.length;
				
				ReviewListField[] oJS = reviewList;
				reviewList = new ReviewListField[previous + reviews.size()];
				String[] imgurl = new String[reviewList.length];
				String[] imgurl2 = new String[reviewList.length];
				
				for(int o=0; o<previous; o++){
					reviewList[o] = oJS[o];
					imgurl[o] = "";
					imgurl2[o] = "";
				}
				oJS = null;
				
				
				for(int n=0; n<reviews.size(); n++){
					BodyPersistable4 reviewData = (BodyPersistable4) reviews.elementAt(n);
					
					reviewList[previous+n] = new ReviewListField(reviewData);
					reviewList[previous+n].setMasterTitle(labelNama.getText());
					reviewList[previous+n].setMasterImage(detailData.get_urlimg());
					reviewList[previous+n].setMargin(0, 0, config.getValue10px(), 0);
					reviewList[previous+n].setButtonPressedCallback(this);
							
					imgurl[previous+n] = reviewData.get_avaimg();
					imgurl2[previous+n] = reviewData.get_urlimg();
						
					_reviewsManager.add(reviewList[previous+n]);
				}
				
				new ImageFetching(this, imgurl, Properties._CONN_TYPE_RECIPE_REVIEW_AVATAR);
				new ImageFetching(this, imgurl2, Properties._CONN_TYPE_RECIPE_REVIEW_IMAGE);
			}
			
			
			checkPaging(header);
		}
	}
	
	private void setUserReviewMore(Vector v){
		if(v.size() > 0){
			// set reviews
			Vector header = (Vector) v.elementAt(0);
			Vector reviews = (Vector) v.elementAt(1);
			
			if(reviewList == null && reviews.size() > 0){
				_reviewsManager.deleteAll();
				
				reviewList = new ReviewListField[reviews.size()];
				String[] imgurl = new String[reviewList.length];
				String[] imgurl2 = new String[reviewList.length];
				
				for(int r=0; r<reviewList.length; r++){
					BodyPersistable4 reviewData = (BodyPersistable4) reviews.elementAt(r);
					
					reviewList[r] = new ReviewListField(reviewData);
					reviewList[r].setMasterTitle(labelNama.getText());
					reviewList[r].setMasterImage(detailData.get_urlimg());
					reviewList[r].setMargin(0, 0, config.getValue10px(), 0);
					reviewList[r].setButtonPressedCallback(this);
					
					imgurl[r] = reviewData.get_avaimg();
					imgurl2[r] = reviewData.get_urlimg();
					
					_reviewsManager.add(reviewList[r]);
				}
				
				new ImageFetching(this, imgurl, Properties._CONN_TYPE_RECIPE_REVIEW_AVATAR);
				new ImageFetching(this, imgurl2, Properties._CONN_TYPE_RECIPE_REVIEW_IMAGE);
			}
			else if(reviewList != null && reviews.size() > 0){
				int previous = reviewList.length;
				
				ReviewListField[] oJS = reviewList;
				reviewList = new ReviewListField[previous + reviews.size()];
				String[] imgurl = new String[reviewList.length];
				String[] imgurl2 = new String[reviewList.length];
				
				for(int o=0; o<previous; o++){
					reviewList[o] = oJS[o];
					imgurl[o] = "";
					imgurl2[o] = "";
				}
				oJS = null;
				
				
				for(int n=0; n<reviews.size(); n++){
					BodyPersistable4 reviewData = (BodyPersistable4) reviews.elementAt(n);
					
					reviewList[previous+n] = new ReviewListField(reviewData);
					reviewList[previous+n].setMasterTitle(labelNama.getText());
					reviewList[previous+n].setMasterImage(detailData.get_urlimg());
					reviewList[previous+n].setMargin(0, 0, config.getValue10px(), 0);
					reviewList[previous+n].setButtonPressedCallback(this);
							
					imgurl[previous+n] = reviewData.get_avaimg();
					imgurl2[previous+n] = reviewData.get_urlimg();
						
					_reviewsManager.add(reviewList[previous+n]);
				}
				
				new ImageFetching(this, imgurl, Properties._CONN_TYPE_RECIPE_REVIEW_AVATAR);
				new ImageFetching(this, imgurl2, Properties._CONN_TYPE_RECIPE_REVIEW_IMAGE);
			}
			
			
			checkPaging(header);
		}
	}
	
	private void checkPaging(Vector header){
		if(header.size()> 0){
			HeaderPersistable head = (HeaderPersistable) header.elementAt(0);
			if(GStringUtil.checkHTTP(head.get_nexturl())){
				morePage = true;
				moreURI = head.get_nexturl();
				
				FetchingAnimation page = new FetchingAnimation(Properties._LBL_LOAD_MORE, FIELD_HCENTER);
				page.setMargin(config.getValue10px(), 0, config.getValue10px(), 0);
				page.setButtonPressedCallback(this, Properties._CONN_TYPE_DETAIL_RECIPE_REVIEW_PAGING);
				
				_reviewsManager.add(page);
			}
		}
	}
	
	public void onRequestCompleted(Vector vector, String type) {
		// TODO Auto-generated method stub
		if(morePage){
			// clear fetching animation
			try{
				_reviewsManager.delete(_reviewsManager.getField(_reviewsManager.getFieldCount()-1));
			}catch(Exception d){ System.out.println("unable delete fething animation"); }
		}
		
		if(type.equalsIgnoreCase(Properties._CONN_TYPE_DETAIL_RECIPE_REVIEW_PAGING)){
			setUserReviewMore(vector);
		}
		else{
			setDetailResep(vector);
		}
	}
	
	public void onPressed(String type) {
		// TODO Auto-generated method stub
		if(morePage && type.equalsIgnoreCase(Properties._CONN_TYPE_DETAIL_RECIPE_REVIEW_PAGING)){
			new DataLoader(moreURI, "", Properties._HTTP_METHOD_GET, type, true, false, this);
		}
		else if(type.equalsIgnoreCase(Properties._LBL_TULIS_REVIEW)){
			AddReviewResepScreen screen = ClassManager.getInstance().getScreenManager().getAddReviewResepScreen();
			screen.setResepId(detailData.get_id());
			screen.setResepTitle(detailData.get_title());
			screen.setOtherScreenCallback(this);
			
			ScreenManager.getBangoApp().pushScreen(screen);
		}
		else if(type.equalsIgnoreCase(Properties._CONN_TYPE_REFRESH)){
			// update
			morePage = false;
			moreURI = "";
			reviewList = null;
			_reviewsManager.deleteAll();
			new DataLoader(urlDetail, "", Properties._HTTP_METHOD_GET, Properties._CONN_TYPE_DETAIL_RESEP, false, false, this);
		}
		else if(type.indexOf(Properties._CONN_TYPE_LIKE_REVIEW) != -1){
			// update
			morePage = false;
			moreURI = "";
			reviewList = null;
			_reviewsManager.deleteAll();
			new DataLoader(urlDetail, "", Properties._HTTP_METHOD_GET, Properties._CONN_TYPE_DETAIL_RESEP, false, false, this);

			// show popup share
			ScreenManager.showSharePopup(SharePopupScreen.showDetail(type.substring(0, type.indexOf(Properties._CONN_TYPE_LIKE_REVIEW)), headerMenu1.getPreferredHeight(), this, Properties._CONN_TYPE_LIKE_REVIEW));
		}
	}
	
	public void setDetailData(BodyPersistable2 data){
		// set temporary data
		labelNama.setText(data.get_title());
		labelOleh.setText("Oleh " + data.get_fullname());
		starRating.setCurrentRate(data.get_rating());
		
		String[] imgUrl = { data.get_urlimg() };
		new ImageFetching(this, imgUrl, Properties._CONN_TYPE_RESEP_BIG_PREVIEW);
		
		urlDetail = data.get_urldetail();
		
		// fetching real detail
		new DataLoader(data.get_urldetail(), "", Properties._HTTP_METHOD_GET, Properties._CONN_TYPE_DETAIL_RESEP, false, true, this);
	}
	
	public void getDetailData(String uri){
		new DataLoader(uri, "", Properties._HTTP_METHOD_GET, Properties._CONN_TYPE_DETAIL_RESEP, false, true, this);
	}
	
	public void setBigPreview(Bitmap bitmap){
		synchronized (UiApplication.getUiApplication().getAppEventLock()) {
			previewBitmap = bitmap;
			previewBig.setBitmap(bitmap);
		}
	}

	public void onScreenCallback(String result, String type) {
		// TODO Auto-generated method stub
		if(type.equalsIgnoreCase(Properties._CONN_TYPE_ADD_REVIEW)){
			// update
			morePage = false;
			moreURI = "";
			reviewList = null;
			_reviewsManager.deleteAll();
			new DataLoader(urlDetail, "", Properties._HTTP_METHOD_GET, Properties._CONN_TYPE_DETAIL_RESEP, false, false, this);
			
			// show popup share
			ScreenManager.showSharePopup(SharePopupScreen.showDetail(result, headerMenu3.getPreferredHeight(), this, Properties._CONN_TYPE_ADD_REVIEW));
		}
		else if(type.equalsIgnoreCase(Properties._CONN_TYPE_SHARE_REVIEW)){
			// show pilihan share
			ScreenManager.showShareSelectionDialog(new ShareSelectionDialog(this, result));
		}
		else if(type.equalsIgnoreCase(Properties._LBL_SHARE_FACEBOOK)){
			// share to facebook
			String text_1 = UserProfileManager.getUserProfile() == null ? "Anda" : UserProfileManager.getUserProfile().get_fullname();
			String text_2 = "";
			String title = detailData.get_title();
			int rating = 0;
			
			if(result.equalsIgnoreCase(Properties._CONN_TYPE_ADD_REVIEW)){
				text_1 += " baru saja menulis review";
				rating = ownRating;
			}
			else if(result.equalsIgnoreCase(Properties._CONN_TYPE_LIKE_REVIEW)){
				text_1 += " baru saja me-Like review";
				text_2 += "oleh " + otherName;
				rating = otherRating;
			}
			
			Bitmap share = ShareCreator.getShareImage(previewBitmap, text_1, title, text_2, rating);
			PNGEncodedImage encoded = PNGEncodedImage.encode(share);
			
			new FacebookConnector(Properties._CONN_TYPE_SHARE_FACEBOOK, Properties._LBL_SHARE_TEXT_FACEBOOK, encoded);
		}
		else if(type.equalsIgnoreCase(Properties._LBL_SHARE_TWITTER)){
			// share to twitter
			String text_1 = UserProfileManager.getUserProfile() == null ? "Anda" : UserProfileManager.getUserProfile().get_fullname();
			String text_2 = "";
			String title = detailData.get_title();
			int rating = 0;
			
			if(result.equalsIgnoreCase(Properties._CONN_TYPE_ADD_REVIEW)){
				text_1 += " baru saja menulis review";
				rating = ownRating;
			}
			else if(result.equalsIgnoreCase(Properties._CONN_TYPE_LIKE_REVIEW)){
				text_1 += " baru saja me-Like review";
				text_2 += "oleh " + otherName;
				rating = otherRating;
			}
			
			Bitmap share = ShareCreator.getShareImage(previewBitmap, text_1, title, text_2, rating);
			
			ScreenManager.getBangoApp().pushScreen(new TwitterConnector(Properties._CONN_TYPE_SHARE_TWITTER, Properties._LBL_SHARE_TEXT_TWITTER, share));
		}

	}

	public void onScreenCallbackValue(Vector value, String type) {
		// TODO Auto-generated method stub
		if (type.equalsIgnoreCase(Properties._CONN_TYPE_ADD_REVIEW)) {
			String rating = (String) value.elementAt(0);
			ownRating = Integer.parseInt(rating);
		}
	}

	public void onPressedValue(Vector value, String type) {
		// TODO Auto-generated method stub
		if(type.equalsIgnoreCase(Properties._CONN_TYPE_LIKE_REVIEW)){
			String name = (String) value.elementAt(1);
			otherName = name;
			
			String rating = (String) value.elementAt(1);
			otherRating = Integer.parseInt(rating);
		}
	}
	
	// mengatasi issue touch
	// #ifdef TouchEnabled
	protected boolean touchEvent(TouchEvent message) {
		switch (message.getEvent()) {
		case TouchEvent.GESTURE:
			TouchGesture gesture = message.getGesture();
			if (gesture.getEvent() == TouchGesture.TAP) {
				XYRect startRect = new XYRect();
				XYRect activeField = new XYRect();
				UiApplication.getUiApplication().getActiveScreen()
						.getFocusRect(startRect);
				activeField.set(startRect);

				int posX = activeField.x;
				int posY = activeField.y;

				int touchX = message.getX(1);
				int touchY = message.getY(1);

				if ((touchX >= posX && touchX <= posX + activeField.width)
						&& (touchY >= posY && touchY <= posY
								+ activeField.height)) {
					return super.touchEvent(message);
				}
			}
			return true;
		}

		return super.touchEvent(message);
	}
	// #endif
}
