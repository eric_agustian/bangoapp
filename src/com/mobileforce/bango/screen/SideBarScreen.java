//#preprocess

package com.mobileforce.bango.screen;

import java.util.Vector;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.manager.ClassManager;
import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.screen.component.CustomBitmapButton;
import com.mobileforce.bango.screen.component.CustomDropDownList;
import com.mobileforce.bango.screen.component.CustomLabelField;
import com.mobileforce.bango.screen.component.CustomLineField;
import com.mobileforce.bango.screen.component.CustomListField1;
import com.mobileforce.bango.screen.component.CustomListField3;
import com.mobileforce.bango.screen.component.FetchingAnimation;
import com.mobileforce.bango.services.DataLoader;
import com.mobileforce.bango.services.SettingManager;
import com.mobileforce.bango.services.ShareCreator;
import com.mobileforce.bango.services.callback.ButtonPressedCallback;
import com.mobileforce.bango.services.callback.DataLoaderCallback;
import com.mobileforce.bango.services.callback.FilePickerCallback;
import com.mobileforce.bango.services.callback.SelectionCallback;
import com.mobileforce.bango.storage.persistable.BodyPersistable3;
import com.mobileforce.bango.storage.persistable.HeaderPersistable;
import com.mobileforce.bango.storage.query.NotificationStatusStore;
import com.mobileforce.bango.storage.query.TemporerStore;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.FilePickerListener;
import com.mobileforce.bango.utility.GStringUtil;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.ScrollChangeListener;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.picker.FilePicker;


public class SideBarScreen extends BaseScreenX implements FieldChangeListener, DataLoaderCallback, ButtonPressedCallback, SelectionCallback, FilePickerCallback{
	private HorizontalFieldManager _bodyManager, _headerManager, _notifBarManager;
	private VerticalFieldManager _contentManager, _scrollManager, _favoritManager, _notifikasiManager;
	private CustomBitmapButton _sliderBar, buttonHome;
	
	private Bitmap top;
	private CustomLabelField labelTitle, labelNotification;
	private CustomListField1 menu1, menu2, menu3, menu4, promo;
	private CustomDropDownList buttonNotification;
	
	private int xSlide = 0;
	private int leftWidth = ModelConfig.getConfig().getIconMenu().getWidth() + (config.getValue10px() *2);
	
	private CustomLabelField[] jajananSpecial;
	private CustomListField3[] notifList;
	
	private Vector menuSpecial = new Vector();
	
	private boolean morePage = false;
	private String moreURI = "";
	
	public SideBarScreen(){
		super(NO_VERTICAL_SCROLL | NO_HORIZONTAL_SCROLL);
		
		initComponent();
		initSubLayout();
		initContent();
		
		// get menu special
		getMenuSpecial();
		
		// get notification list
		if(SettingManager.isNotificationEnabled()){
			getNotificationList();
		}
	}
	
	public void initComponent() {
		// TODO Auto-generated method stub
		_sliderBar = new CustomBitmapButton(ShareCreator.getModifiedSlide(Properties._SCREEN_CAPTURE));
		_sliderBar.setChangeListener(this);
		
		top = config.getHeaderBgRepeat();
		
		labelTitle = new CustomLabelField(Properties._LBL_HOME, Properties._COLOR_FONT_LABEL_WHITE, FIELD_HCENTER | NON_FOCUSABLE);
		labelTitle.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontLargeHeight()));
		
		labelNotification = new CustomLabelField(Properties._LBL_NOTIFIKASI, Properties._COLOR_FONT_LABEL_WHITE, FIELD_HCENTER | NON_FOCUSABLE);
		labelNotification.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, config.getFontLargeHeight()));
		
		buttonHome = new CustomBitmapButton(config.getIconHome());
		buttonHome.setChangeListener(this);
		
		//init menu
		menu1 = new CustomListField1(Properties._LBL_SHARE, config.getIconShare());
		menu1.setWidthInPixel((Display.getWidth()-leftWidth)+config.getValue5px());
		menu1.setFontSize(config.getFontLargeHeight());
		menu1.setChangeListener(this);
		
		menu2 = new CustomListField1(Properties._LBL_JAJANAN, config.getIconFavorit());
		menu2.setWidthInPixel((Display.getWidth()-leftWidth)+config.getValue5px());
		menu2.setFontSize(config.getFontLargeHeight());
		menu2.setChangeListener(this);
		
		menu3 = new CustomListField1(Properties._LBL_RESEP, config.getIconResep());
		menu3.setWidthInPixel((Display.getWidth()-leftWidth)+config.getValue5px());
		menu3.setFontSize(config.getFontLargeHeight());
		menu3.setChangeListener(this);
		
		menu4 = new CustomListField1(Properties._LBL_PROFIL, config.getIconProfil());
		menu4.setWidthInPixel((Display.getWidth()-leftWidth)+config.getValue5px());
		menu4.setFontSize(config.getFontLargeHeight());
		menu4.setChangeListener(this);
		
		promo = new CustomListField1(Properties._LBL_PROMO, config.getIconPromo());
		promo.setWidthInPixel((Display.getWidth()-leftWidth)+config.getValue5px());
		promo.setFontSize(config.getFontLargeHeight());
		promo.setChangeListener(this);
		
		buttonNotification = new CustomDropDownList("", (Display.getWidth()*40)/100, Properties._ARRAY_NOTIFIKASI, FOCUSABLE | FIELD_HCENTER);
		if(SettingManager.isNotificationEnabled()){
			buttonNotification.setSelectedIndex(0);
		}
		else{
			buttonNotification.setSelectedIndex(1);
		}
		buttonNotification.setGreenMode();
		buttonNotification.selectionCallback(this, "");
	}
	
	private void initSubLayout(){
		_bodyManager = new HorizontalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void paintBackground(Graphics g){	
				//draw background
				g.setColor(Properties._COLOR_BG_SIDEBAR_MENU);
				g.fillRect(0, 0, getWidth(), getHeight());
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aH = Display.getHeight();
				int aW = Display.getWidth();
				
				Field slide = getField(0);
			    layoutChild(slide, slide.getPreferredWidth(), slide.getPreferredHeight());
			    setPositionChild(slide, xSlide, 0);
			    
			    Field content = getField(1);
			    layoutChild(content, content.getPreferredWidth(), content.getPreferredHeight());
			    setPositionChild(content, (xSlide + slide.getWidth()) - config.getValue5px(), 0);
				
			    setExtent(aW, aH);
			}
		};
		
		_contentManager = new VerticalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void paintBackground(Graphics g){
				//draw background
				g.setColor(Properties._COLOR_BG_SIDEBAR_MENU);
				g.fillRect(0, 0, getPreferredWidth(), getHeight());
				
				g.setColor(Properties._COLOR_SEMI_BLACK);
				g.drawLine(0, 0, 0, getHeight());
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aW = (Display.getWidth()-leftWidth)+config.getValue5px();
				int aH = Display.getHeight();
				
				Field header = getField(0);
			    layoutChild(header, aW, header.getPreferredHeight());
			    setPositionChild(header, 0, 0);
			    
			    Field scroll = getField(1);
			    layoutChild(scroll, aW, aH - header.getHeight());
			    setPositionChild(scroll, 0, header.getHeight());
				
			    setExtent(aW, aH);
			}
		};
		
		_headerManager = new HorizontalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void paintBackground(Graphics g){	
				//draw background
//				ImageUtil.drawSpriteTexture(top, g, 0, getWidth(), getWidth(), 0, 0, 0, getHeight(), getHeight());
				g.setColor(Properties._COLOR_BG_SIDEBAR_MENU);
				g.fillRect(0, 0, getWidth(), getHeight());
				
				//draw line
				g.setColor(Properties._COLOR_BG_LINE_SEPARATOR);
				g.drawLine(0, getHeight()-1, getWidth(), getHeight()-1);
				
				g.setColor(Properties._COLOR_SEMI_BLACK);
				g.drawLine(0, 0, 0, getHeight());
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aW = (Display.getWidth()-leftWidth)+config.getValue5px();
				int aH = top.getHeight();
				
				Field lbl1 = getField(0);
			    layoutChild(lbl1, lbl1.getPreferredWidth(), lbl1.getPreferredHeight());
			    setPositionChild(lbl1, config.getValue10px(), (aH - lbl1.getHeight())/2);
			    
			    Field icon = getField(1);
			    layoutChild(icon, icon.getPreferredWidth(), icon.getPreferredHeight());
			    setPositionChild(icon, aW-(icon.getWidth()+config.getValue10px()), (aH - icon.getHeight())/2);
				
			    setExtent(aW, aH);
			}
		};
		
		_notifBarManager = new HorizontalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void paintBackground(Graphics g){	
				//draw background
				g.setColor(Properties._COLOR_BG_SIDEBAR_MENU);
				g.fillRect(0, 0, getWidth(), getHeight());
				
				//draw line
				g.setColor(Properties._COLOR_BG_LINE_SEPARATOR);
				g.drawLine(0, getHeight()-1, getWidth(), getHeight()-1);
				
				g.setColor(Properties._COLOR_SEMI_BLACK);
				g.drawLine(0, 0, 0, getHeight());
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aW = (Display.getWidth()-leftWidth)+config.getValue5px();
				int aH = top.getHeight();
				
				Field lbl1 = getField(0);
			    layoutChild(lbl1, lbl1.getPreferredWidth(), lbl1.getPreferredHeight());
			    setPositionChild(lbl1, config.getValue10px(), (aH - lbl1.getHeight())/2);
			    
			    Field icon = getField(1);
			    layoutChild(icon, icon.getPreferredWidth(), icon.getPreferredHeight());
			    setPositionChild(icon, aW-(icon.getWidth()+config.getValue10px()), (aH - icon.getHeight())/2);
				
			    setExtent(aW, aH);
			}
		};
		
		_scrollManager = new VerticalFieldManager(VERTICAL_SCROLL | VERTICAL_SCROLLBAR);
		_scrollManager.setScrollListener(
				new ScrollChangeListener(){
					public void scrollChanged(Manager manager, int newHorizontalScroll, int newVerticalScroll) {
						invalidate();
					}
				}
			);
		
		_favoritManager = new VerticalFieldManager(NO_VERTICAL_SCROLL | NO_VERTICAL_SCROLLBAR);
		_notifikasiManager = new VerticalFieldManager(NO_VERTICAL_SCROLL | NO_VERTICAL_SCROLLBAR);
	}
	
	private void initContent(){
		_scrollManager.add(menu1);
		_scrollManager.add(getLineSeparator());
		_scrollManager.add(menu2);
		_scrollManager.add(_favoritManager);
		_scrollManager.add(getLineSeparator());
		_scrollManager.add(menu3);
		_scrollManager.add(getLineSeparator());
		_scrollManager.add(promo);
		_scrollManager.add(getLineSeparator());
		_scrollManager.add(menu4);
		_scrollManager.add(getLineSeparator());
		_scrollManager.add(_notifBarManager);
		_scrollManager.add(_notifikasiManager);
		
		
		_headerManager.add(labelTitle);
		_headerManager.add(buttonHome);
		
		_notifBarManager.add(labelNotification);
		_notifBarManager.add(buttonNotification);
		
		_contentManager.add(_headerManager);
		_contentManager.add(_scrollManager);
		
		_bodyManager.add(_sliderBar);
		_bodyManager.add(_contentManager);
		
		add(_bodyManager);
		
		buttonHome.setFocus();
	}
	
	protected void onUiEngineAttached(boolean attached) {
		super.onUiEngineAttached(attached);
		
		if(attached){
			new AnimateToLeft(xSlide, xSlide - (Display.getWidth()-leftWidth)).run();
		}
	}
	
	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		if(field == buttonHome){
			new AnimateToRight(xSlide, 0).run();
			
			ScreenManager.clearAllNonFirstMapScreen();
		}
		else if(field == _sliderBar){
			new AnimateToRight(xSlide, 0).run();
		}
		else if(field == menu1){
			//on OS 5 using this
			//#ifdef BlackBerrySDK5.0.0
			String[] filter = {".jpg", ".jpeg", ".png", ".gif" };
			new FilePickerListener(Properties._LBL_CHOOSE_PICTURE, -1, filter, System.getProperty("fileconn.dir.photos"), this);
			
			//on OS 6 or newer using this
			//#else
			new FilePickerListener(Properties._LBL_CHOOSE_PICTURE, FilePicker.VIEW_PICTURES, null, null, this);
			//#endif
		}
		else{
			closeSideBar();
			ScreenManager.clearAllNonFirstMapScreen();
			
			if(field == menu2){
				if(menuSpecial.size() > 0){
					// tampilkan jajanan special index pertama
					JajananSpecialScreen screen = ClassManager.getInstance().getScreenManager().getJajananSpecialScreen();
					screen.getContent(0);
					getBangoApp().pushScreen(screen);
				}
			}
			else if(field == menu3){
				getBangoApp().pushScreen(ClassManager.getInstance().getScreenManager().getDaftarResepScreen());
			}
			else if(field == menu4){
				getBangoApp().pushScreen(ClassManager.getInstance().getScreenManager().getProfilScreen());
			}
			else if(menuSpecial.size() > 0){
				for(int s=0; s<menuSpecial.size(); s++){
					if(field == jajananSpecial[s]){
						JajananSpecialScreen screen = ClassManager.getInstance().getScreenManager().getJajananSpecialScreen();
						screen.getContent(s);
						getBangoApp().pushScreen(screen);
						
						break;
					}
				}
			}
			else if(field == promo){
				
			}
		}
	}
	
	protected boolean keyChar(char character, int keyCode, int time) {
		if(character == Keypad.KEY_ESCAPE){
			new AnimateToRight(xSlide, 0).run();
			
			return true;
		}
		return super.keyChar(character, keyCode, time);
	}
	
	private void closeSideBar(){
		getBangoApp().popScreen(this);
	}
	
	//ANIMATION
	private class AnimateToLeft implements Runnable{
		private int target = 0;
		private int actual = 0;
		private int movement = 1;
		
		public AnimateToLeft(int start, int target){
			this.target = target;
			this.actual = start;
		}
		
		public void run() {
			// TODO Auto-generated method stub
			int multiply = 1;
			while(true) {
			    actual -= (movement * multiply);
			    multiply++;
				
			    xSlide = actual;
			    
			    try {
					Thread.sleep(10);
				} catch (InterruptedException e) { e.printStackTrace(); }
			    
			    if(actual <= target){
			    	xSlide = target;
			    	invalidate();
				    updateLayout();
				    updateDisplay();
				    
			    	break;
			    }
			    else{
			    	invalidate();
				    updateLayout();
				    updateDisplay();
			    }
			}
		}
	}

	//ANIMATION
	private class AnimateToRight implements Runnable{
		private int target = 0;
		private int actual = 0;
		private int movement = 1;
			
		public AnimateToRight(int start, int target){
			this.target = target;
			this.actual = start;
		}
			
		public void run() {
			// TODO Auto-generated method stub
			int multiply = 1;
			while(true) {
				actual += (movement * multiply);
				multiply++;
					
				xSlide = actual;
				    
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) { e.printStackTrace(); }
				    
				if(actual >= target){
				    xSlide = target;
				    invalidate();
					updateLayout();
					updateDisplay();
					
					closeSideBar();
					break;
				}
				else{
					invalidate();
					updateLayout();
					updateDisplay();
				}
			}
		}
	}
	
	
// ---------------------------------------------------------------	
	private CustomLineField getLineSeparator(){
		CustomLineField line = new CustomLineField(NON_FOCUSABLE);
		line.setHorizontalLine((Display.getWidth()-leftWidth)+config.getValue5px(), 1, Properties._COLOR_BG_LINE_SEPARATOR);
		
		return line;
	}
	
	private void getMenuSpecial(){
		if(TemporerStore.getMenuSpecial().size() == 0){
			new DataLoader(Properties._API_MENU_SPECIAL, "", Properties._HTTP_METHOD_GET, Properties._CONN_TYPE_MENU_SPECIAL, true, false, this);
		}
		else{
			addMenuSpecial(TemporerStore.getMenuSpecial());
		}
	}
	
	private void addMenuSpecial(Vector vector){
		_favoritManager.deleteAll();
		menuSpecial = vector;
		
		jajananSpecial = new CustomLabelField[menuSpecial.size()];
		for(int s=0; s<jajananSpecial.length; s++){
			jajananSpecial[s] = new CustomLabelField(TemporerStore.getMenuSpecialData(s).get_title(), FOCUSABLE, Properties._COLOR_FONT_LABEL_BLACK, Properties._COLOR_FONT_LABEL_WHITE);
			jajananSpecial[s].setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, config.getFontMediumHeight()));
			jajananSpecial[s].setMargin(0, config.getValue10px(), config.getValue5px(), config.getValue10px()*3);
			jajananSpecial[s].setChangeListener(this);
			
			_favoritManager.add(jajananSpecial[s]);
		}
	}
	
	private void getNotificationList(){
		if(TemporerStore.getNotificationList().size() == 0){
			CustomLabelField load = new CustomLabelField(Properties._LBL_LOAD_NOTIFICATION, Properties._COLOR_FONT_LABEL_WHITE, FIELD_HCENTER | NON_FOCUSABLE);
			load.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, config.getFontSmallHeight()));
			load.setMargin(config.getValue10px(), 0, config.getValue10px(), (((Display.getWidth()-leftWidth))-load.getFont().getAdvance(Properties._LBL_LOAD_NOTIFICATION))/2);
			
			_notifikasiManager.add(load);
			
			new DataLoader(Properties._API_LIST_NOTIFICATION, "", Properties._HTTP_METHOD_GET, Properties._CONN_TYPE_NOTIFICATION, true, false, this);
		}
		else{
			addNotificationList(TemporerStore.getNotificationList());
			
			TemporerStore.resetNotificationList();
			new DataLoader(Properties._API_LIST_NOTIFICATION, "", Properties._HTTP_METHOD_GET, Properties._CONN_TYPE_NOTIFICATION, true, false, this);
		}
	}
	
	private void addNotificationList(Vector vector){
		_notifikasiManager.deleteAll();
			
		Vector header = (Vector) vector.elementAt(0);
		Vector body = (Vector) vector.elementAt(1);
		
//		// add label notification
//		if(body.size() > 0){
//			_notifikasiManager.add(menu5);
//		}
		
		if(body.size() == 0){
			TemporerStore.resetNotificationList();
		}
		
		notifList = new CustomListField3[body.size()];
		for(int s=0; s<notifList.length; s++){
			final BodyPersistable3 persistable = (BodyPersistable3) body.elementAt(s);
			String[] data = {
				persistable.get_notiftxt(),
				persistable.get_reltime()
			};
			
			final int x = s;
			notifList[s] = new CustomListField3(data);
			notifList[s].setWidthInPixel((Display.getWidth()-leftWidth)+config.getValue5px());
			notifList[s].setChangeListener(new FieldChangeListener() {
				public void fieldChanged(Field field, int context) {
					notifList[x].setUnread(false);
					NotificationStatusStore.saveNotifRead(persistable.get_notifid());
					
					showNextScreen(persistable.get_pagetype(), persistable.get_urldetail());
				}
			});
			
			if(NotificationStatusStore.isNotifRead(persistable.get_notifid())){
				notifList[s].setUnread(false);
			}
			
			_notifikasiManager.add(notifList[s]);
		}
		
		//check paging
		checkPaging(header);
	}
	
	private void addMoreNotificationList(Vector vector){
		try{
			Vector header = (Vector) vector.elementAt(0);
			Vector body = (Vector) vector.elementAt(1);
			
			if(notifList == null && body.size() > 0){
				for(int s=0; s<notifList.length; s++){
					final BodyPersistable3 persistable = (BodyPersistable3) body.elementAt(s);
					String[] data = {
						persistable.get_notiftxt(),
						persistable.get_reltime()
					};
					
					final int x = s;
					notifList[s] = new CustomListField3(data);
					notifList[s].setWidthInPixel((Display.getWidth()-leftWidth)+config.getValue5px());
					notifList[s].setChangeListener(new FieldChangeListener() {
						public void fieldChanged(Field field, int context) {
							notifList[x].setUnread(false);
							NotificationStatusStore.saveNotifRead(persistable.get_notifid());
							
							showNextScreen(persistable.get_pagetype(), persistable.get_urldetail());
						}
					});
					
					if(NotificationStatusStore.isNotifRead(persistable.get_notifid())){
						notifList[s].setUnread(false);
					}
					
					_notifikasiManager.add(notifList[s]);
				}
			}
			else{
				int previous = notifList.length;
				
				CustomListField3[] oJS = notifList;
				notifList = new CustomListField3[previous + body.size()];
				
				for(int o=0; o<previous; o++){
					notifList[o] = oJS[o];
				}
				oJS = null;
				
				
				for(int s=0; s<body.size(); s++){
					final BodyPersistable3 persistable = (BodyPersistable3) body.elementAt(s);
					String[] data = {
						persistable.get_notiftxt(),
						persistable.get_reltime()
					};
					
					final int x = previous+s;
					notifList[previous+s] = new CustomListField3(data);
					notifList[previous+s].setWidthInPixel((Display.getWidth()-leftWidth)+config.getValue5px());
					notifList[previous+s].setChangeListener(new FieldChangeListener() {
						public void fieldChanged(Field field, int context) {
							notifList[x].setUnread(false);
							NotificationStatusStore.saveNotifRead(persistable.get_notifid());
							
							showNextScreen(persistable.get_pagetype(), persistable.get_urldetail());
						}
					});
					
					if(NotificationStatusStore.isNotifRead(persistable.get_notifid())){
						notifList[previous+s].setUnread(false);
					}
					
					_notifikasiManager.add(notifList[previous+s]);
				}
			}
			
			//check paging
			checkPaging(header);
		}catch(Exception x){ System.out.println("Exception SideBarScreen : " + x.getMessage()); }
	}
	
	private void checkPaging(Vector header){
		if(header.size()> 0){
			HeaderPersistable head = (HeaderPersistable) header.elementAt(0);
			if(GStringUtil.checkHTTP(head.get_nexturl())){
				morePage = true;
				moreURI = head.get_nexturl();
				
				FetchingAnimation page = new FetchingAnimation(Properties._LBL_LOAD_MORE, FIELD_HCENTER);
				page.setMargin(config.getValue10px(), 0, config.getValue10px(), 0);
				page.setButtonPressedCallback(this, Properties._CONN_TYPE_NOTIFICATION + Properties._CONN_TYPE_PAGING);
				
				_notifikasiManager.add(page);
			}
		}
	}
	
	public void onRequestCompleted(Vector vector, String type) {
		// TODO Auto-generated method stub
		if(type.equals(Properties._CONN_TYPE_MENU_SPECIAL)){
			addMenuSpecial(vector);
		}
		else if(type.equals(Properties._CONN_TYPE_NOTIFICATION)){
			// clear fetching animation
			try{
				_notifikasiManager.delete(_notifikasiManager.getField(_notifikasiManager.getFieldCount()-1));
			}catch(Exception d){ System.out.println("unable delete fething animation"); }
			
			if(morePage){
				addMoreNotificationList(vector);
			}
			else{
				addNotificationList(vector);
			}
		}
		else if(type.indexOf(Properties._CONN_TYPE_CHANGE_NOTIF) != -1){
			ScreenManager.showDialogTypeScreen(type.substring(0, type.indexOf(Properties._CONN_TYPE_CHANGE_NOTIF)));
		}
	}

	public void onPressed(String type) {
		// TODO Auto-generated method stub
		if(morePage && type.indexOf(Properties._CONN_TYPE_PAGING)!= -1){
			new DataLoader(moreURI, "", Properties._HTTP_METHOD_GET, type.substring(0, type.indexOf(Properties._CONN_TYPE_PAGING)), true, false, this);
		}
	}

	public void onSelected(String type) {
		// TODO Auto-generated method stub
		String state = "";
		if(buttonNotification.getSelectedIndex() == 0){
			SettingManager.setNotificationSetting(true);
			state = "1";
			getNotificationList();
		}
		else{
			SettingManager.setNotificationSetting(false);
			state = "0";
			_notifikasiManager.deleteAll();
		}
		
		String param = "state=" + state;
		new DataLoader(Properties._API_CHANGE_NOTIF, param, Properties._HTTP_METHOD_POST, Properties._CONN_TYPE_CHANGE_NOTIF, false, true, this);
	}

	public void onPressedValue(Vector value, String type) {
		// TODO Auto-generated method stub
		
	}
	
	private void showNextScreen(String type, String uri){
		if(type.equalsIgnoreCase("P01")){
			// detail store
			StoreDetailScreen screen = ClassManager.getInstance().getScreenManager().getStoreDetailScreen();
			screen.getDetailData(uri);
			ScreenManager.getBangoApp().pushScreen(screen);
		}
		else if(type.equalsIgnoreCase("P02")){
			// detail recipe
			ResepDetailScreen screen = ClassManager.getInstance().getScreenManager().getResepDetailScreen();
			screen.getDetailData(uri);
			ScreenManager.getBangoApp().pushScreen(screen);
		}
		else if(type.equalsIgnoreCase("P03")){
			// detail profil
			OthersProfilScreen screen = ClassManager.getInstance().getScreenManager().getOthersProfilScreen();
			screen.getUserProfileByUri(uri);
			ScreenManager.getBangoApp().pushScreen(screen);
		}
	}
	
	public void onPicked(String path) {
		// TODO Auto-generated method stub
		try{
			closeSideBar();
			ScreenManager.clearAllNonFirstMapScreen();
			
			BerbagiKulinerScreen screen = ClassManager.getInstance().getScreenManager().getBerbagiKulinerScreen();
			screen.onPicked(path);
			getBangoApp().pushScreen(screen);
		}catch(Exception x){
			System.out.println("unable picked picture : " + x);
		}
	}
}
