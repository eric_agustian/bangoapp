package com.mobileforce.bango.screen;

import com.mobileforce.bango.manager.ClassManager;
import com.mobileforce.bango.services.SplashPromoManager;
import com.mobileforce.bango.services.UserInfoManager;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.ImageUtil;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Menu;
import net.rim.device.api.ui.container.VerticalFieldManager;

public final class SplashScreen extends BaseScreenX {
	private VerticalFieldManager _contentManager;
	private WaitingThread _wait;
	private Bitmap _background, _logo, _footer;
	
	public SplashScreen(){
		super(NO_VERTICAL_SCROLL | NO_HORIZONTAL_SCROLL);
		
		_wait = new WaitingThread();
		
		initComponent();
		paintScreen();
	}
	
	public void paintScreen(){
		_contentManager = new VerticalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void paintBackground(Graphics g){	
				//draw background
				g.drawBitmap((Display.getWidth()-_background.getWidth())/2, (Display.getHeight()-_background.getHeight())/2, _background.getWidth(), _background.getHeight(), _background, 0, 0);
				
				//draw footer
				ImageUtil.drawSpriteTexture(_footer, g, 0, Display.getWidth(), Display.getWidth(), 0, Display.getHeight()-_footer.getHeight(), Display.getHeight()-_footer.getHeight(), Display.getHeight(), Display.getHeight());
				
				//draw logo
				g.drawBitmap((Display.getWidth()-_logo.getWidth())/2, Display.getHeight()-(_logo.getHeight()+((_footer.getHeight()*3)/10)), _logo.getWidth(), _logo.getHeight(), _logo, 0, 0);
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aH = Display.getHeight();
				int aW = Display.getWidth();
				
			    setExtent(aW, aH);
			}
		};
		
		this.add(_contentManager);
		
		//start thread
		_wait.start();
	}
	
	void showNextScreen(){
		synchronized (UiApplication.getUiApplication().getAppEventLock()) {
			if(!UserInfoManager.getUserID().equalsIgnoreCase("")){
				getBangoApp().pushScreen(ClassManager.getInstance().getScreenManager().getFirstMapScreen());
				
				SplashPromoManager.fetchAndShownPromoRandomly();
				SplashPromoManager.loadSplashPromo();
			}
			else{
				getBangoApp().pushScreen(ClassManager.getInstance().getScreenManager().getWelcomeScreen());
			}
	    		
	    	getBangoApp().popScreen(this);
		}
	}
	
	class WaitingThread extends Thread {
		public WaitingThread(){}
		
		public void run() {
			try{
	           sleep(Properties._SPLASH_INTERVAL); //3 second
	        }catch (InterruptedException iex) {}
	        
	        showNextScreen();
		}
	}
	
	protected boolean keyChar(char character, int keyCode, int time) {
		if(character == Keypad.KEY_ESCAPE){
			return true;
		}
		return super.keyChar(character, keyCode, time);
	}
	
	//*****************************************************************************	
	protected void makeMenu(Menu menu, int instance){ 
	    menu.deleteAll();
	}

	public void initComponent() {
		DeviceProperties.hideVirtualKeyboard();
		
		_background = config.getBackgroundSplash();
		_logo = config.getLogoBig();
		_footer = config.getSplashFooterRepeat();
	}
}
