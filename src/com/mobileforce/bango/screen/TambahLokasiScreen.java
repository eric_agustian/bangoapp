package com.mobileforce.bango.screen;

import java.util.Vector;

import javax.microedition.location.Coordinates;

import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.screen.component.maps.CustomPOIMap;
import com.mobileforce.bango.services.DataLoader;
import com.mobileforce.bango.services.callback.ButtonPressedCallback;
import com.mobileforce.bango.services.callback.DataLoaderCallback;
import com.mobileforce.bango.services.callback.LocationCompletedCallback;
import com.mobileforce.bango.services.callback.OtherScreenCallback;
import com.mobileforce.bango.storage.persistable.GeocodingPersistable;
import com.mobileforce.bango.storage.query.TemporerStore;
import com.mobileforce.bango.utility.GStringUtil;
import com.mobileforce.bango.utility.Properties;
import com.mobileforce.bango.utility.URLUTF8Encoder;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.Menu;
import net.rim.device.api.ui.container.VerticalFieldManager;


public class TambahLokasiScreen extends BaseScreen implements FieldChangeListener, LocationCompletedCallback, DataLoaderCallback, ButtonPressedCallback{
	private TambahLokasiScreen _self;
	private VerticalFieldManager _mapManager, MAP;
	private CustomPOIMap mapsField;
	
	private String lat = "0";
	private String lon = "0";
	private String address = "-";
	private String city = "-";
	
	private OtherScreenCallback callback;
	
	public TambahLokasiScreen(){
		super(NO_VERTICAL_SCROLL | NO_HORIZONTAL_SCROLL);
		_self = this;
		
		initComponent();
		initSubLayout();
		initContent();
		
		// start location services
		getLocationServices(this);
	}
	
	public void initComponent() {
		setHeaderType2();
		headerMenu2.setSearchHint(Properties._LBL_HINT_CARI_LOKASI);
		extraMenu6.setButtonPressedCallback(this);
		
		mapsField = new CustomPOIMap(config.getIconPOI());
		mapsField.setZoom((mapsField.getMinZoom() * 3 + mapsField.getMaxZoom()) / 7);
		if(config.isOrientationPortrait()){
			mapsField.setPosYCorrection(headerMenu2.getPreferredHeight());
		}
		else{
			mapsField.setPosYCorrection(headerMenu2.getPreferredHeight()+extraMenu6.getPreferredHeight());
		}
		
		// set default location
		mapsField.moveTo(new Coordinates(Double.parseDouble(latitude), Double.parseDouble(longitude), (float) 0.0));
//		Marker poi = new Marker(new Coordinates(Double.parseDouble(latitude), Double.parseDouble(longitude), (float) 0.0), config.getIconPOI(), config.getIconPOIFocus());
//		mapsField.addMarker(poi);
	}
	
	private void initSubLayout(){
		_mapManager = contentWithExtraMenu();
		
		MAP = new VerticalFieldManager(){
			protected void paintBackground(Graphics g){	
				//draw background
				g.setColor(Properties._COLOR_BG_CONTENT);
				g.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aH = Display.getHeight()-(headerMenu2.getPreferredHeight()+extraMenu1.getPreferredHeight());
				int aW = Display.getWidth();
				
				try{
					Field content = getField(0);
				    layoutChild(content, aW, aH);
				    setPositionChild(content, 0, 0);
				}catch(Exception x){}
				
			    setExtent(aW, aH);
			}
		};
	}
	
	private void initContent(){
		if(config.isOrientationPortrait()){
			_mapManager.add(MAP);
			_mapManager.add(extraMenu6);
		}
		else{
			_mapManager.add(extraMenu6);
			_mapManager.add(MAP);
		}
		
		MAP.add(mapsField);
		_contentManager.add(_mapManager);
	}
	
	protected void onUiEngineAttached(boolean attached) {
		super.onUiEngineAttached(attached);
	}
	
	protected void onExposed() {
		// TODO Auto-generated method stub
		super.onExposed();
	}
	
	private void getLocationInformation(String lat, String lon){
		String url = Properties._API_GOOGLE_GEOCODING;
		String params = "address=" + lat + "," + lon + "&sensor=false";
		
		new DataLoader(url, params, Properties._HTTP_METHOD_GET, Properties._CONN_TYPE_GOOGLE_GEOCODING, false, true, this);
	}
	
	public void searchLocationByName(String keyword){
		String url = Properties._API_GOOGLE_GEOCODING;
		String params = "address=" + URLUTF8Encoder.encode(keyword.trim()) + "&sensor=false";
		
		new DataLoader(url, params, Properties._HTTP_METHOD_GET, Properties._CONN_TYPE_GOOGLE_GEOCODING, false, true, this);
	}

	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
	}
	
	public void onLocationCompleted() {
		// TODO Auto-generated method stub
		// get default location information
		getLocationInformation(latitude, longitude);
	}

	public void onRequestCompleted(Vector vector, String type) {
		if(type.equalsIgnoreCase(Properties._CONN_TYPE_GOOGLE_GEOCODING)){
			mapsField.removeAllMarkers();
			
			// set new
			if(vector.size() > 0){
				GeocodingPersistable data = (GeocodingPersistable) vector.elementAt(0);
				this.lat = data.get_latitude();
				this.lon = data.get_longitude();
				this.address = data.get_formatted_address();
				
				String[] parseCity = GStringUtil.split(data.get_formatted_address(), " ");
				String city = parseCity.length > 0 ? parseCity[parseCity.length-1] : address;
				this.city = city;
			
				mapsField.moveTo(new Coordinates(Double.parseDouble(lat), Double.parseDouble(lon), (float) 0.0));
//				Marker poi = new Marker(new Coordinates(Double.parseDouble(lat), Double.parseDouble(lon), (float) 0.0), config.getIconPOI(), config.getIconPOIFocus());
//				mapsField.addMarker(poi);
			}
			else{
				ScreenManager.showDialogTypeScreen(Properties._INF_LOKASI_TIDAK_DITEMUKAN);
			}
		}
	}

	public void onPressed(String type) {
		// TODO Auto-generated method stub
		if(callback != null){
			boolean different = false;
			String latM = "";
			String lonM = "";
			
			if(lat.indexOf(".") != -1 && lon.indexOf(".") != -1){
				double x = (double)mapsField.getLatitude() / (int)100000;
				double y = (double)mapsField.getLongitude() / (int)100000;
				latM = String.valueOf(x);
				lonM = String.valueOf(y);
				
				String commalatM = latM.substring(latM.indexOf("."));
				String commalonM = lonM.substring(lonM.indexOf("."));
				String commaLat = lat.substring(latM.indexOf("."));
				String commaLon = lon.substring(lonM.indexOf("."));
				
				if(commaLat.length() >= 3 && commalatM.length() >= 3){
					if(commaLat.indexOf(commalatM.substring(0, 3)) == -1 || commaLon.indexOf(commalonM.substring(0, 3)) == -1){
						different = true;
					}
				}
				else{
					if(commaLat.indexOf(commalatM.substring(0, 1)) == -1 || commaLon.indexOf(commalonM.substring(0, 1)) == -1){
						different = true;
					}
				}
			}
			
			if(different){
				getLocationInformation(latM, lonM);
			}
			else{
				Vector value = new Vector();
				value.addElement(lat);
				value.addElement(lon);
				value.addElement(address);
				value.addElement(city);
				
				callback.onScreenCallbackValue(value, Properties._LBL_TAMBAH_LOKASI);
				getBangoApp().popScreen(this);
			}
		}
	}

	public void onPressedValue(Vector value, String type) {
		// TODO Auto-generated method stub
		
	}
	
	public void setOtherScreenCallback(OtherScreenCallback callback){
		this.callback = callback;
	}
	
	
	//**********************CUSTOM MENU ITEM*************************************************************	
	private MenuItem _zoomIN = new MenuItem("Zoom In", 1000, 1) {
		public void run() {
			int min = mapsField.getMinZoom();

			int current = mapsField.getZoom();
			if (current - 1 >= min) {
				mapsField.setZoom(current - 1);
			} else {
				Dialog.alert("Maximum Zoom");
			}
		}
	};

	private MenuItem _zoomOUT = new MenuItem("Zoom Out", 1000, 2) {
		public void run() {
			int max = mapsField.getMaxZoom();

			int current = mapsField.getZoom();
			if (current + 1 <= max) {
				mapsField.setZoom(current + 1);
			} else {
				Dialog.alert("Minimum Zoom");
			}
		}
	};

	private MenuItem _mylocation = new MenuItem("Show My Location", 1000, 3) {
		public void run() {
			mapsField.moveTo(new Coordinates(Double.parseDouble(latitude),
					Double.parseDouble(longitude), (float) 0.0));
		}
	};

	private MenuItem _refresh = new MenuItem("Refresh Location", 1000, 4) {
		public void run() {
			mapsField.removeAllMarkers();
			TemporerStore.resetHomeByDistance();
			getLocationServices(_self);
		}
	};

	private MenuItem _exit = new MenuItem("Exit Application", 1000000, 1) {
		public void run() {
			System.exit(0);
		}
	};

	protected void makeMenu(Menu menu, int instance) {
		// menu.deleteAll();
		menu.add(_zoomIN);
		menu.add(_zoomOUT);
		menu.add(_mylocation);
		menu.add(_refresh);
		menu.add(_exit);
	}
}
