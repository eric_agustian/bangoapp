package com.mobileforce.bango.screen;

import com.mobileforce.bango.manager.ClassManager;
import com.mobileforce.bango.screen.component.CustomBitmapButton;
import com.mobileforce.bango.screen.component.CustomLabelField;
import com.mobileforce.bango.services.FacebookConnector;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.ImageUtil;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;

public final class WelcomeScreen extends BaseScreenX implements FieldChangeListener{
	private VerticalFieldManager _contentManager, _box, _boxTop;
	private HorizontalFieldManager _boxBottom;
	private Bitmap _background, _box_p1, _box_p2, _box_p3, _bottom_p1, _bottom_p2;
	
	private CustomLabelField label1, label2, label3, labelDaftar;
	private CustomBitmapButton loginFb, loginBiasa;
	
	public WelcomeScreen(){
		super(NO_VERTICAL_SCROLL | NO_HORIZONTAL_SCROLL);
		
		initComponent();
		initSubLayout();
		
		getLocationServicesSilently();
	}
	
	public void initSubLayout(){
		_contentManager = new VerticalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void paintBackground(Graphics g){	
				//draw background
				g.drawBitmap((Display.getWidth()-_background.getWidth())/2, (Display.getHeight()-_background.getHeight())/2, _background.getWidth(), _background.getHeight(), _background, 0, 0);
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aH = Display.getHeight();
				int aW = Display.getWidth();
				
				Field content = getField(0);
			    layoutChild(content, content.getPreferredWidth(), content.getPreferredHeight());
			    setPositionChild(content, (aW-content.getWidth())/2, (aH-content.getHeight())/2);
				
			    setExtent(aW, aH);
			}
		};
		
		_box = new VerticalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void paintBackground(Graphics g){	
				//draw background
				g.drawBitmap(0, 0, _box_p1.getWidth(), _box_p1.getHeight(), _box_p1, 0, 0);
				
				ImageUtil.drawSpriteTexture(_box_p2, g, 0, _box_p1.getWidth(), _box_p1.getWidth(), 0, _box_p1.getHeight(), _box_p1.getHeight(), ((Display.getHeight()*7)/10)-_box_p3.getHeight(), ((Display.getHeight()*7)/10)-_box_p3.getHeight());
				
				g.drawBitmap(0, ((Display.getHeight()*7)/10)-_box_p3.getHeight(), _box_p3.getWidth(), _box_p3.getHeight(), _box_p3, 0, 0);
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aH = (Display.getHeight()*7)/10;
				int aW = _box_p1.getWidth();
				
				Field top = getField(0);
			    layoutChild(top, top.getPreferredWidth(), top.getPreferredHeight());
			    setPositionChild(top, (aW-top.getWidth())/2, 0);
			    
			    Field bottom = getField(1);
			    layoutChild(bottom, bottom.getPreferredWidth(), bottom.getPreferredHeight());
			    setPositionChild(bottom, (aW-bottom.getWidth())/2, aH-bottom.getHeight());
				
			    setExtent(aW, aH);
			}
		};
		
		_boxTop = new VerticalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL | FIELD_HCENTER);
		
		_boxBottom = new HorizontalFieldManager(Manager.NO_VERTICAL_SCROLL | Manager.NO_HORIZONTAL_SCROLL){
			protected void paintBackground(Graphics g){	
				//draw background
				g.drawBitmap(0, getHeight()-_bottom_p1.getHeight(), _bottom_p1.getWidth(), _bottom_p1.getHeight(), _bottom_p1, 0, 0);
				
				ImageUtil.drawSpriteTexture(_bottom_p2, g, 0, _bottom_p1.getWidth(), _bottom_p1.getWidth(), 0, 0, 0, getHeight()-_bottom_p1.getHeight(), getHeight()-_bottom_p1.getHeight());
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aW = _bottom_p1.getWidth();
				int aH = config.getValue5px()*2;
				
				Field left = getField(0);
			    layoutChild(left, left.getPreferredWidth(), left.getPreferredHeight());
			    
			    Field right = getField(1);
			    layoutChild(right, right.getPreferredWidth(), right.getPreferredHeight());
			    
			    int pX = (aW - (left.getWidth()+right.getWidth()))/2;
			    aH += right.getHeight();
			    
			    setPositionChild(left, pX, (aH-left.getHeight())/2);
			    setPositionChild(right, pX+left.getWidth()+config.getValue5px(), (aH-right.getHeight())/2);
				
			    setExtent(aW, aH);
			}
		};
		
		_boxTop.add(label1);
		_boxTop.add(loginFb);
		_boxTop.add(label2);
		_boxTop.add(loginBiasa);
		
		_boxBottom.add(label3);
		_boxBottom.add(labelDaftar);
		
		_box.add(_boxTop);
		_box.add(_boxBottom);
		_contentManager.add(_box);
		
		this.add(_contentManager);
	}
	
	public boolean onClose() {
		System.exit(0);
		
		return true;
	}
	
	//*****************************************************************************	

	public void initComponent() {
		DeviceProperties.hideVirtualKeyboard();
		
		_background = config.getBackgroundWelcome();
		_box_p1 = config.getWelcomeBoxTopP1();
		_box_p2 = config.getWelcomeBoxTopP2();
		_box_p3 = config.getWelcomeBoxTopP3();
		_bottom_p1 = config.getWelcomeBoxBottomP1();
		_bottom_p2 = config.getWelcomeBoxBottomP2();
		
		label1 = new CustomLabelField(Properties._LBL_SELAMAT_DATANG, Properties._COLOR_FONT_LABEL_WHITE, FIELD_HCENTER | NON_FOCUSABLE);
		label1.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, config.getFontSuperLargeHeight()));
		
		label2 = new CustomLabelField(Properties._LBL_ATAU, Properties._COLOR_FONT_LABEL_WHITE, FIELD_HCENTER | NON_FOCUSABLE);
		label2.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, config.getFontMediumHeight()));
		
		label3 = new CustomLabelField(Properties._LBL_BELUM_PUNYA_AKUN, Properties._COLOR_FONT_LABEL_WHITE, FIELD_HCENTER | NON_FOCUSABLE);
		label3.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, config.getFontSmallHeight()-2));
		
		labelDaftar = new CustomLabelField(Properties._LBL_DAFTAR_DISINI, FOCUSABLE | FIELD_LEFT, Properties._COLOR_FONT_LABEL_BLACK, Properties._COLOR_FONT_LABEL_WHITE);
		labelDaftar.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, config.getFontSmallHeight()-2));
		labelDaftar.setChangeListener(this);
		
		loginFb = new CustomBitmapButton(config.getButtonLoginFB());
		loginFb.setChangeListener(this);
		
		loginBiasa = new CustomBitmapButton(config.getButtonSignIn());
		loginBiasa.setChangeListener(this);
		
		if(config.isOrientationPortrait()){
			label1.setMargin(config.getValue10px()*4, 0, config.getValue10px()*3, 0);
			label2.setMargin(config.getValue10px(), 0, config.getValue10px(), 0);
			
			loginFb.setMargin(config.getValue10px()*2, 0, config.getValue10px()*2, 0);
			loginBiasa.setMargin(config.getValue10px()*2, 0, config.getValue10px()*2, 0);
		}
		else{
			label1.setMargin(config.getValue10px()*3, 0, config.getValue10px()*2, 0);
			label2.setMargin(config.getValue10px(), 0, config.getValue10px(), 0);
			
			loginFb.setMargin(config.getValue10px(), 0, config.getValue10px(), 0);
			loginBiasa.setMargin(config.getValue10px(), 0, config.getValue10px(), 0);
		}
	}
	
	public void fieldChanged(Field field, int context) {
		if(field == labelDaftar){
			synchronized (UiApplication.getUiApplication().getAppEventLock()) {
				getBangoApp().pushScreen(ClassManager.getInstance().getScreenManager().getRegisterScreen());
			}
		}
		else if(field == loginFb){
			new FacebookConnector(Properties._CONN_TYPE_LOGIN_2);
		}
		else if(field == loginBiasa){
			synchronized (UiApplication.getUiApplication().getAppEventLock()) {
				getBangoApp().pushScreen(ClassManager.getInstance().getScreenManager().getLoginScreen());
			}
		}
	}
}
