package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.BasicEditField;


public class AdvEditField extends BasicEditField {
	private Font _font;
	private int _color;
	
	public AdvEditField(Font font, long style, int maxchar, int color){
		super("", "", maxchar, style | JUMP_FOCUS_AT_END);
		
		_font = font;
		_color = color;
		setFont(_font);
	}
	
	public void paint(Graphics g) { 
		g.setColor(_color);
		
		super.paint(g); 
	}
	
	//this is to remove scroll to left side issue on OS6
	protected void fieldChangeNotify(int context) {
		if(Properties._BB_OS.startsWith("6") || Properties._BB_OS.startsWith("7")){
	        try{
	            super.fieldChangeNotify(context);
	            setExtent(this.getFont().getAdvance(this.getText()+"X"), this.getHeight());
	            super.updateLayout();
	        }
	        catch(Exception e){
	            //don't recall why I needed this, but it works so I'm hesitant to remove it
	        }
		}
    }
	
	//this is to remove scroll to left side issue on OS6
    protected void layout(int width, int height) {
        super.layout(width, height);
        
        if(Properties._BB_OS.startsWith("6") || Properties._BB_OS.startsWith("7")){
        	setExtent(this.getFont().getAdvance(this.getText()+"X"), this.getHeight());
        }
    }
	
	public void setFont(Font font) {
		super.setFont(font);
	}
	
	public void setColor(int color){
		_color = color;
		invalidate();
	}
}
