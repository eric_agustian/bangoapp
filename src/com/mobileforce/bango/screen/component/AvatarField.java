package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.utility.ImageScaler;
import com.mobileforce.bango.utility.ImageUtil;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;

public class AvatarField extends BaseButtonField {
	private Bitmap _background, _avatar, _masking;
	
	private boolean isfocus;
	private int mode = 0; //0 -> top align, 1 -> center align
	
	public AvatarField(){
		this(Field.FOCUSABLE);
	}
	
	public AvatarField(long style){
		super(style);
		
		prepareElement();
	}
	
	public int getPreferredWidth() {
		return _background.getWidth();
	}
	
	public int getPreferredHeight() {
		return _background.getHeight();
	}
	
	public void prepareElement(){
		_background = ModelConfig.getConfig().getAvatarBgNetral();
		_avatar = ModelConfig.getConfig().getAvatarDefault();
		_masking = ModelConfig.getConfig().getIconMaskingAvatar();
	}
	
	protected void layout(int width, int height){
		super.setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	//Invoked when this field receives the focus.
	protected void onFocus(int direction){
    	isfocus = true;
        super.onFocus(direction);
    }

    //Invoked when a field loses the focus.
    protected void onUnfocus(){
        isfocus = false;
        super.onUnfocus();
    }
    
	protected void paint(Graphics g){
		if(isfocus){
			g.setGlobalAlpha(155);
		}
		else{
			g.setGlobalAlpha(255);
		}
		
		// draw bg
		g.drawBitmap((getPreferredWidth()-_background.getWidth())/2, (getPreferredHeight()-_background.getHeight())/2, _background.getWidth(), _background.getHeight(), _background, 0, 0);
		
		// draw avatar
		if(mode == 0){
			g.drawBitmap((getPreferredWidth()-_avatar.getWidth())/2, (getPreferredHeight()*4)/100, _avatar.getWidth(), _avatar.getHeight(), _avatar, 0, 0);
		}
		else if(mode == 1){
			g.drawBitmap((getPreferredWidth()-_avatar.getWidth())/2, (getPreferredHeight()-_avatar.getHeight())/2, _avatar.getWidth(), _avatar.getHeight(), _avatar, 0, 0);
		}
	}
	
	private void setRescalledAvatar(int width, int height){
		int scaleX = ((width * 100) / _background.getWidth());
		int scaleY = (height * 100) / _background.getHeight();
		
		this._background = ImageScaler.resizeBitmapARGB(_background, width, height);
		this._avatar = ImageScaler.resizeBitmapARGB(_avatar, (_avatar.getWidth() * scaleX)/100, (_avatar.getHeight() * scaleY)/100);
	}
	
	public void setNetralAvatar(Bitmap avatar){
		avatar = ImageUtil.getMaskedCircledIcon(_masking, avatar, _masking.getWidth(), _masking.getHeight());
		
		this._background = ModelConfig.getConfig().getAvatarBgNetral();
		this._avatar = avatar;
		
		invalidate();
	}
	
	public void setBronzeAvatar(Bitmap avatar){
		avatar = ImageUtil.getMaskedCircledIcon(_masking, avatar, _masking.getWidth(), _masking.getHeight());
		
		this._background = ModelConfig.getConfig().getAvatarBgBronze();
		this._avatar = avatar;
		
		invalidate();
	}
	
	public void setSilverAvatar(Bitmap avatar){
		avatar = ImageUtil.getMaskedCircledIcon(_masking, avatar, _masking.getWidth(), _masking.getHeight());
		
		this._background = ModelConfig.getConfig().getAvatarBgSilver();
		this._avatar = avatar;
		
		invalidate();
	}
	
	public void setGoldAvatar(Bitmap avatar){
		avatar = ImageUtil.getMaskedCircledIcon(_masking, avatar, _masking.getWidth(), _masking.getHeight());
		
		this._background = ModelConfig.getConfig().getAvatarBgGold();
		this._avatar = avatar;
		
		invalidate();
	}
	
	public void setDefaultAvatar(){
		this._background = ModelConfig.getConfig().getAvatarBgNetral();
		this._avatar = ImageUtil.getMaskedCircledIcon(_masking, ModelConfig.getConfig().getAvatarDefault(), _masking.getWidth(), _masking.getHeight());
		
		invalidate();
	}
	
	public void setNetralAvatar(Bitmap avatar, int width, int height){
		setNetralAvatar(avatar);
		
		setRescalledAvatar(width, height);
		
		invalidate();
	}
	
	public void setBronzeAvatar(Bitmap avatar, int width, int height){
		setBronzeAvatar(avatar);
		
		setRescalledAvatar(width, height);
		
		invalidate();
	}
	
	public void setSilverAvatar(Bitmap avatar, int width, int height){
		setSilverAvatar(avatar);
		
		setRescalledAvatar(width, height);
		
		invalidate();
	}
	
	public void setGoldAvatar(Bitmap avatar, int width, int height){
		setGoldAvatar(avatar);
		
		setRescalledAvatar(width, height);
		
		invalidate();
	}
	
	public void setDefaultAvatar(int width, int height){
		setDefaultAvatar();
		
		setRescalledAvatar(width, height);
		
		invalidate();
	}
	
	public void setAvatarAlignment(int mode){
		this.mode = mode;
		invalidate();
	}
}
