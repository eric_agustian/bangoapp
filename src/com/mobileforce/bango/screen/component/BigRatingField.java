package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.utility.ImageScaler;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;

public class BigRatingField extends BaseButtonField {
	private Bitmap _bitmap, _unfocus;
	private int rate = 1;
	private int maximumRate = 4;
	
	private boolean isfocus = false;;
	private boolean unfocusMode = false;
	
	public BigRatingField(int rate){
		this(rate, Field.FOCUSABLE);
	}
	
	public BigRatingField(int rate, long style){
		super(style);
		
		this.rate = rate;
		
		prepareElement();
	}
	
	public int getPreferredWidth() {
		return _bitmap.getWidth() * maximumRate;
	}
	
	public int getPreferredHeight() {
		return _bitmap.getHeight();
	}
	
	public void prepareElement(){
		_bitmap = ModelConfig.getConfig().getIconRatingStar();
		_unfocus = ModelConfig.getConfig().getIconRatingDot();
	}
	
	protected void layout(int width, int height){
		super.setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	//Invoked when this field receives the focus.
	protected void onFocus(int direction){
    	isfocus = true;
        super.onFocus(direction);
    }

    //Invoked when a field loses the focus.
    protected void onUnfocus(){
        isfocus = false;
        super.onUnfocus();
    }
    
	protected void paint(Graphics g){
		if(isfocus){
			g.setGlobalAlpha(155);
		}
		else{
			g.setGlobalAlpha(255);
		}
		
		// draw bitmap
		if(unfocusMode){
			for(int n = 0; n < maximumRate; n++){
				g.drawBitmap(n * _unfocus.getWidth(), (getPreferredHeight()-_unfocus.getHeight())/2, _unfocus.getWidth(), _unfocus.getHeight(), _unfocus, 0, 0);
			}
		}
		
		for(int n = 0; n < rate; n++){
			g.drawBitmap(n * _bitmap.getWidth(), (getPreferredHeight()-_bitmap.getHeight())/2, _bitmap.getWidth(), _bitmap.getHeight(), _bitmap, 0, 0);
		}
	}
	
	public void resizeStarSize(int percent){
		this._bitmap = ImageScaler.resizeBitmapARGB(_bitmap, (_bitmap.getWidth()*percent)/100, (_bitmap.getHeight()*percent)/100);
		this._unfocus = ImageScaler.resizeBitmapARGB(_unfocus, (_unfocus.getWidth()*percent)/100, (_unfocus.getHeight()*percent)/100);
	}
	
	public void setMaximumRate(int rate){
		this.maximumRate = rate;
		this.updateLayout();
	}
	
	public void setUnfocusMode(){
		this.unfocusMode = true;
	}
}
