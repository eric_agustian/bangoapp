package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.configuration.ModelConfig;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;

public class CustomBitmapField extends BaseButtonField {
	private int _width = 0;
	private int _height = 0;
	private int _widthPercent = -1;
	private int _widthPixels = -1;
	private int _heightPercent = -1;
	private int _heightPixels = -1;
	
	private Bitmap _bitmap;
	
	private boolean isfocus = false;
	private boolean isborder = false;
	
	public CustomBitmapField(Bitmap bitmap){
		this(bitmap, Field.FOCUSABLE);
	}
	
	public CustomBitmapField(Bitmap bitmap, long style){
		super(style);
		
		this._bitmap = bitmap;
		
		prepareElement();
	}
	
	public void setButtonWidthInPercent(int width){
		this._widthPercent = width;
	}
	
	public void setButtonWidthInPixel(int width){
		this._widthPixels = Display.getWidth() - width;
	}
	
	public void setButtonHeightInPercent(int height){
		this._heightPercent = height;
	}
	
	public void setButtonHeightInPixel(int height){
		this._heightPixels = Display.getHeight() - height;
	}
	
	public int getPreferredWidth() {
		if(_widthPixels != -1){
			return Display.getWidth()-_widthPixels;
		}
		else if(_widthPercent != -1){
			return (Display.getWidth() * _widthPercent) / 100;
		}
		else{
			return _width;
		}
	}
	
	public int getPreferredHeight() {
		if(_heightPixels != -1){
			return Display.getHeight()-_heightPixels;
		}
		else if(_heightPercent != -1){
			return (Display.getHeight() * _heightPercent) / 100;
		}
		else{
			return _height;
		}
	}
	
	public void prepareElement(){
		_width = _bitmap.getWidth();
		_height = _bitmap.getHeight();
	}
	
	protected void layout(int width, int height){
		super.setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	//Invoked when this field receives the focus.
	protected void onFocus(int direction){
    	isfocus = true;
        super.onFocus(direction);
    }

    //Invoked when a field loses the focus.
    protected void onUnfocus(){
        isfocus = false;
        super.onUnfocus();
    }
    
	protected void paint(Graphics g){
//		if(isfocus){
//			g.setGlobalAlpha(125);
//		}
//		else{
//			g.setGlobalAlpha(255);
//		}
		
		// draw bitmap
		g.drawBitmap((getPreferredWidth()-_bitmap.getWidth())/2, (getPreferredHeight()-_bitmap.getHeight())/2, _bitmap.getWidth(), _bitmap.getHeight(), _bitmap, 0, 0);
		
		if(isfocus && isborder){
			g.setColor(Color.RED);
			g.drawRoundRect(0, 0, getPreferredWidth(), getPreferredHeight(), ModelConfig.getConfig().getValue10px(), ModelConfig.getConfig().getValue10px());
		}
		else{
		}
	}
	
	public void setBitmap(Bitmap bitmap){
		this._bitmap = bitmap;
//		prepareElement();
		invalidate();
	}
	
	public void setBorderFocus(boolean border){
		this.isborder = border;
		invalidate();
	}
}
