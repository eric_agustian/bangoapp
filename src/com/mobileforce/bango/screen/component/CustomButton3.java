package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.ImageUtil;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;

public class CustomButton3 extends BaseButtonField {
	private int _width = 0;
	private int _height = 0;
	private int _widthPercent = -1;
	private int _widthPixels = -1;
	
	private Bitmap bg_part_l_un, bg_part_m_un, bg_part_r_un;
	private Bitmap bg_part_l_on, bg_part_m_on, bg_part_r_on;
	
	private int fontSize = ModelConfig.getConfig().getFontMediumHeight();
	private int fontColorFocus = 0xDDDDDD;
	private int fontColorUnfocus = 0xFFFFFF;
	
	private Font fontDefault = DeviceProperties.getDefaultApplicationFont(fontSize);
	
	private String _label;
	
	private boolean isfocus = false;
	private boolean singlemode = false;
	private boolean moreRow = false;
	private boolean clickedmode = false;
	
	public CustomButton3(String label){
		this(label, Field.FOCUSABLE);
	}
	
	public CustomButton3(String label, long style){
		super(Field.FOCUSABLE | style);
		
		this._label = label;
		
		prepareElement();
		checkMoreThanSingleRow();
	}
	
	public void setFontSize(int newSize){
		this.fontSize = newSize;
		fontDefault = DeviceProperties.getDefaultApplicationFont(fontSize);
		invalidate();
		checkMoreThanSingleRow();
	}
	
	public void setFontColor(int newFocusColor, int newUnfocusColor){
		this.fontColorFocus = newFocusColor;
		this.fontColorUnfocus = newUnfocusColor;
	}
	
	public void setFontFamily(Font fontFamily){
		this.fontDefault = fontFamily;
		invalidate();
		checkMoreThanSingleRow();
	}
	
	public void setButtonWidthInPercent(int width){
		this._widthPercent = width;
		checkMoreThanSingleRow();
	}
	
	public void setButtonWidthInPixel(int width){
		this._widthPixels = Display.getWidth() - width;
		checkMoreThanSingleRow();
	}
	
	public int getPreferredWidth() {
		if(_widthPixels != -1){
			return Display.getWidth()-_widthPixels;
		}
		else if(_widthPercent != -1){
			return (Display.getWidth() * _widthPercent) / 100;
		}
		else{
			return _width;
		}
	}
	
	public void setButtonBackgroundMode(boolean mode){
		this.singlemode = mode;
	}
	
	public int getPreferredHeight() {
		return _height;
	}
	
	public void prepareElement(){
		bg_part_l_un = ModelConfig.getConfig().getButtonSmallGreen_l_un();
		bg_part_m_un = ModelConfig.getConfig().getButtonSmallGreen_m_un();
		bg_part_r_un = ModelConfig.getConfig().getButtonSmallGreen_r_un();
		bg_part_l_on = ModelConfig.getConfig().getButtonSmallGreen_l_on();
		bg_part_m_on = ModelConfig.getConfig().getButtonSmallGreen_m_on();
		bg_part_r_on = ModelConfig.getConfig().getButtonSmallGreen_r_on();
		
		_width = bg_part_l_un.getWidth() + fontDefault.getAdvance(_label) + bg_part_r_un.getWidth();
		_height = bg_part_l_un.getHeight();
	}
	
	protected void layout(int width, int height){
		super.setExtent(getPreferredWidth(), getPreferredHeight());
		checkMoreThanSingleRow();
	}
	
	//Invoked when this field receives the focus.
	protected void onFocus(int direction){
    	isfocus = true;
        super.onFocus(direction);
    }

    //Invoked when a field loses the focus.
    protected void onUnfocus(){
        isfocus = false;
        super.onUnfocus();
    }
    
	protected void paint(Graphics g){
		if(singlemode){
			if(isfocus){
				g.setGlobalAlpha(175);
				
				// font color
				g.setColor(fontColorFocus);
			}
			else{
				g.setGlobalAlpha(255);
						
				// font color
				g.setColor(fontColorUnfocus);
			}
			
			// left
			g.drawBitmap(0, 0, bg_part_l_on.getWidth(), bg_part_l_on.getHeight(), bg_part_l_on, 0, 0);
									
			// middle
			ImageUtil.drawSpriteTexture(bg_part_m_on, g, bg_part_l_on.getWidth(), getPreferredWidth()-bg_part_r_on.getWidth(), getPreferredWidth()-bg_part_r_on.getWidth(), bg_part_l_on.getWidth(), 0, 0, getPreferredHeight(), getPreferredHeight());
									
			// right
			g.drawBitmap(getPreferredWidth()-bg_part_r_on.getWidth(), 0, bg_part_r_on.getWidth(), bg_part_r_on.getHeight(), bg_part_r_on, 0, 0);
			
		}
		else{
			if(isfocus){
				if(clickedmode){
					g.setGlobalAlpha(125);
				}
				else{
					g.setGlobalAlpha(255);
				}
				
				// left
				g.drawBitmap(0, 0, bg_part_l_on.getWidth(), bg_part_l_on.getHeight(), bg_part_l_on, 0, 0);
										
				// middle
				ImageUtil.drawSpriteTexture(bg_part_m_on, g, bg_part_l_on.getWidth(), getPreferredWidth()-bg_part_r_on.getWidth(), getPreferredWidth()-bg_part_r_on.getWidth(), bg_part_l_on.getWidth(), 0, 0, getPreferredHeight(), getPreferredHeight());
										
				// right
				g.drawBitmap(getPreferredWidth()-bg_part_r_on.getWidth(), 0, bg_part_r_on.getWidth(), bg_part_r_on.getHeight(), bg_part_r_on, 0, 0);
				
				// font color
				g.setColor(fontColorFocus);
			}
			else{
				if(clickedmode){
					// left
					g.drawBitmap(0, 0, bg_part_l_on.getWidth(), bg_part_l_on.getHeight(), bg_part_l_on, 0, 0);
											
					// middle
					ImageUtil.drawSpriteTexture(bg_part_m_on, g, bg_part_l_on.getWidth(), getPreferredWidth()-bg_part_r_on.getWidth(), getPreferredWidth()-bg_part_r_on.getWidth(), bg_part_l_on.getWidth(), 0, 0, getPreferredHeight(), getPreferredHeight());
											
					// right
					g.drawBitmap(getPreferredWidth()-bg_part_r_on.getWidth(), 0, bg_part_r_on.getWidth(), bg_part_r_on.getHeight(), bg_part_r_on, 0, 0);
				}
				else{
					// left
					g.drawBitmap(0, 0, bg_part_l_un.getWidth(), bg_part_l_un.getHeight(), bg_part_l_un, 0, 0);
											
					// middle
					ImageUtil.drawSpriteTexture(bg_part_m_un, g, bg_part_l_un.getWidth(), getPreferredWidth()-bg_part_r_un.getWidth(), getPreferredWidth()-bg_part_r_un.getWidth(), bg_part_l_un.getWidth(), 0, 0, getPreferredHeight(), getPreferredHeight());
											
					// right
					g.drawBitmap(getPreferredWidth()-bg_part_r_un.getWidth(), 0, bg_part_r_un.getWidth(), bg_part_r_un.getHeight(), bg_part_r_un, 0, 0);
				}
				
				// font color
				g.setColor(fontColorUnfocus);
			}
		}
		
		
		// set font
		g.setFont(fontDefault);
		
		// draw text
		if(moreRow){
			String[] label = separateLabel();
			int y = 0;
			
			if(label.length == 2){
				// 2 rows
				y = (getPreferredHeight() - (fontDefault.getHeight() * 2)) / 2;
				g.drawText(label[0], (getPreferredWidth() - fontDefault.getAdvance(label[0])) / 2, y);
				y += fontDefault.getHeight();
				g.drawText(label[1], (getPreferredWidth() - fontDefault.getAdvance(label[1])) / 2, y);
			}
			else{
				// 3 rows
				y = (getPreferredHeight() - (fontDefault.getHeight() * 3)) / 2;
				g.drawText(label[0], (getPreferredWidth() - fontDefault.getAdvance(label[0])) / 2, y);
				y += fontDefault.getHeight();
				g.drawText(label[1], (getPreferredWidth() - fontDefault.getAdvance(label[1])) / 2, y);
				y += fontDefault.getHeight();
				g.drawText(label[2], (getPreferredWidth() - fontDefault.getAdvance(label[2])) / 2, y);
			}
		}
		else{
			if(fontDefault.getAdvance(_label) <= getPreferredWidth()){
				g.drawText(_label, (getPreferredWidth() - fontDefault.getAdvance(_label)) / 2, (getPreferredHeight() - fontDefault.getHeight()) / 2);
			}
			else{
				g.drawText(_label, bg_part_l_un.getWidth(), (getPreferredHeight() - fontDefault.getHeight()) / 2, DrawStyle.ELLIPSIS, getPreferredWidth() - (bg_part_l_un.getWidth() + bg_part_r_un.getWidth()));
			}
		}
	}
	
	private void checkMoreThanSingleRow(){
//		if(fontDefault.getAdvance(_label) > getPreferredWidth() - (bg_part_l_un.getWidth())){
//			moreRow = true;
//		}
//		else{
//			moreRow = false;
//		}
	}
	
	private String[] separateLabel(){
		String[] label = new String[2];
		label[0] = _label.substring(0, _label.indexOf(" ")).trim();
		label[1] = _label.substring(_label.indexOf(" ") + 1).trim();
		
		if(label[1].indexOf(" ") != -1 && fontDefault.getAdvance(label[1]) > getPreferredWidth()){
			String[] nLabel = new String[3];
			nLabel[0] = label[0];
			nLabel[1] = label[1].substring(0, label[1].indexOf(" "));
			nLabel[2] = label[1].substring(label[1].indexOf(" ") + 1, label[1].length()).trim();
			
			// change font size
			fontSize = ModelConfig.getConfig().getFontSmallHeight();
			fontDefault = DeviceProperties.getDefaultApplicationFont(fontSize);
			
			return nLabel;
		}
		else if(fontDefault.getAdvance(label[1]) > getPreferredWidth()){
			// change font size
			fontSize = ModelConfig.getConfig().getFontSmallHeight();
			fontDefault = DeviceProperties.getDefaultApplicationFont(fontSize);
		}
		
		return label;
	}
	
	public void setClickedMode(boolean click){
		this.clickedmode = click;
		invalidate();
	}
	
	public void enableMultiRowMode(){
		this.moreRow = true;
		invalidate();
	}
}
