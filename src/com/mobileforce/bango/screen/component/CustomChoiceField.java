package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.configuration.ModelConfig;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;

public class CustomChoiceField extends Field {
	private int _widthPercent = -1;
	private int _widthPixels = -1;
	
	private boolean isfocus = false;
	private boolean isselected = false;
	private boolean _isfocusable = false;
	private String _label;
	private Font _font;
	private int _fcolor, _ucolor;
	
	private Bitmap choice_un, choice_on;
	
	
	public CustomChoiceField(String label, Font font, int fcolor, int ucolor, boolean isfocusable){
		this(label, font, fcolor, ucolor, isfocusable, Field.FOCUSABLE);
	}
	
	public CustomChoiceField(String label, Font font, int fcolor, int ucolor, boolean isfocusable, long style){
		super(style);
		
		_label = label;
		_font = font;
		_fcolor = fcolor;
		_ucolor = ucolor;
		_isfocusable = isfocusable;
		
		setContent();
	}
	
	void setContent(){
		choice_un = ModelConfig.getConfig().getRadio_un();
		choice_on = ModelConfig.getConfig().getRadio_on();
	}
	
	public void setButtonWidthInPercent(int width){
		this._widthPercent = width;
	}
	
	public void setButtonWidthInPixel(int width){
		this._widthPixels = Display.getWidth() - width;
	}
	
	protected void layout(int width, int height){
		super.setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	//Invoked when this field receives the focus.
	protected void onFocus(int direction){
		isfocus = true;
		super.onFocus(direction);
	}

	//Invoked when a field loses the focus.
	protected void onUnfocus(){
		isfocus = false;
		super.onUnfocus();
	}
	
	protected void drawFocus(Graphics g, boolean on){
    	invalidate();
    }
	
	
	protected void paintBackground(Graphics g){
		//gambar background, border
		if(_isfocusable){
			g.setGlobalAlpha(255);
		}
		else{
			g.setGlobalAlpha(200);
		}
	}
	
	public int getPreferredWidth() {
		if(_widthPixels != -1){
			return Display.getWidth()-_widthPixels;
		}
		else if(_widthPercent != -1){
			return (Display.getWidth() * _widthPercent) / 100;
		}
		else{
			return choice_un.getWidth()+_font.getAdvance(_label)+ModelConfig.getConfig().getValue10px();
		}
	}
	
	public int getPreferredHeight(){
		return _font.getHeight() > choice_on.getHeight() ? _font.getHeight() : choice_on.getHeight();
	}
	
	protected void paint(Graphics g){
		if(_isfocusable){
			if(isfocus){
				g.setGlobalAlpha(200);
				g.setColor(_fcolor);
			}
			else{
				g.setGlobalAlpha(255);
				g.setColor(_ucolor);
			}
		}
		else{
			g.setGlobalAlpha(120);
			g.setColor(_ucolor);
		}
		
		g.setFont(_font);
		
		if(getSelectedValue()){
			g.drawBitmap(0, (getPreferredHeight()-choice_on.getHeight())/2, choice_on.getWidth(), choice_on.getHeight(), choice_on, 0, 0);
		}
		else{
			g.drawBitmap(0, (getPreferredHeight()-choice_un.getHeight())/2, choice_un.getWidth(), choice_un.getHeight(), choice_un, 0, 0);
		}
		
		g.drawText(_label, choice_on.getWidth()+ModelConfig.getConfig().getValue5px(), (getPreferredHeight()-_font.getHeight())/2);
	}
	
	/* (non-Javadoc)
     * @see net.rim.device.api.ui.Field#keyChar(char, int, int)
     */
    protected boolean keyChar( char character, int status, int time ) 
    {
        if( character == Characters.ENTER ) {
            clickButton();
            return true;
        }
        return super.keyChar( character, status, time );
    }
    
    /* (non-Javadoc)
     * @see net.rim.device.api.ui.Field#navigationClick(int, int)
     */
    protected boolean navigationClick( int status, int time ) 
    {
        clickButton(); 
        return true;    
    }
    
    /* (non-Javadoc)
     * @see net.rim.device.api.ui.Field#trackwheelClick(int, int)
     */
    protected boolean trackwheelClick( int status, int time )
    {        
        clickButton();    
        return true;
    }
	
	public boolean getSelectedValue(){
		return this.isselected;
	}
	
	public void setSelectedValue(boolean value){
		this.isselected = value;
		this.invalidate();
	}
	
	/**
     * A public way to click this button.
     */
    public void clickButton() 
    {
    	if(getSelectedValue()){
    		setSelectedValue(false);
    	}
    	else{
    		setSelectedValue(true);
    	}
    	
        fieldChangeNotify( 0 );
    }
}
