package com.mobileforce.bango.screen.component;

import java.util.Calendar;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.ImageUtil;

import net.rim.device.api.i18n.DateFormat;
import net.rim.device.api.i18n.SimpleDateFormat;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.picker.DateTimePicker;


public class CustomDateField extends Field{
	private ModelConfig config;
	private Bitmap _sideL, _sideM, _sideR;
	
	private int fontSize = ModelConfig.getConfig().getFontSmallHeight();
	private Font _font = DeviceProperties.getDefaultApplicationFont(fontSize);
	private int _fontColor = 0x000000;
	
	private String _label, _drawableDate, _format;
	
	private int _differential;
	private int _widthPercent = -1;
	private int _maxH;
	
	private boolean _isfocus;
	private boolean _disableDTP = false;
	
	private DateFormat formatter;// = new SimpleDateFormat("yyyy-MM-dd"+"T"+"HH:mm:ss");
	private Calendar initial, min, max, selected;
	
	public CustomDateField(String label, int maxW, String format, long style){
		this(label, maxW, format, style, 0x000000);
	}
	
	public CustomDateField(String label, int maxW, String format, long style, int fontColor){
		super(style);
		
		_label = label;
		_drawableDate = _label;
		
		_differential = Display.getWidth() - maxW;
		_format = format;
		
		formatter = new SimpleDateFormat(format);
		
		_fontColor = fontColor;
		
		initComponent();
	}
	
	void initComponent(){
		config = ModelConfig.getConfig();
		_font = DeviceProperties.getDefaultApplicationFont(config.getFontMediumHeight());
		
		_sideL = config.getDropDown_l();
		_sideM = config.getDropDown_m();
		_sideR = config.getDropDown_r();
		
		_maxH = _sideM.getHeight();
	}
	
	protected void layout(int width, int height) {
		super.setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	public void paint(Graphics g) {
		if(_isfocus){
			g.setGlobalAlpha(200);
		}
		else{
			g.setGlobalAlpha(255);
		}
		
		// background
		g.drawBitmap(0, 0, _sideL.getWidth(), _sideL.getHeight(), _sideL, 0, 0);
		ImageUtil.drawSpriteTexture(_sideM, g, _sideL.getWidth(), getPreferredWidth()-_sideR.getWidth(), getPreferredWidth()-_sideR.getWidth(), _sideL.getWidth(), 0, 0, _sideM.getHeight(), _sideM.getHeight());
		g.drawBitmap(getPreferredWidth()-_sideR.getWidth(), 0, _sideR.getWidth(), _sideR.getHeight(), _sideR, 0, 0);
		
		g.setColor(_fontColor);
		g.setFont(_font);
		int aWidth =  _font.getAdvance(_drawableDate) > getPreferredWidth() - (_sideL.getWidth() + _sideR.getWidth()) ? getPreferredWidth() - (_sideL.getWidth() + _sideR.getWidth()) : _font.getAdvance(_drawableDate);
		g.drawText(_drawableDate, _sideL.getWidth(), (getPreferredHeight()-_font.getHeight())/2, DrawStyle.LEFT | DrawStyle.ELLIPSIS, aWidth);
	}
	
	protected boolean keyChar(char character, int status, int time){
		if (character == Keypad.KEY_ENTER) {
			//show popup list
			if(!_disableDTP) showDatePicker();
			fieldChangeNotify( 0 );
		}
		return super.keyChar(character, status, time);
	}
	
	protected boolean navigationClick(int status, int time){
		//show popup list
		if(!_disableDTP) showDatePicker();
		fieldChangeNotify( 0 );
		return true;
	}
	
	public void showDatePicker(){
		DateTimePicker picker = initial != null ? DateTimePicker.createInstance(initial, _format, null) : DateTimePicker.createInstance(Calendar.getInstance(), _format, null);
		if(min != null) picker.setMinimumDate(min);
		if(max != null) picker.setMaximumDate(max);
		
		if (picker.doModal()) {
			selected = picker.getDateTime();
			String date = formatter.format(selected);
			if (date != null) {
				_drawableDate = date;
				invalidate();
			}
		}
	}
	
	public int getPreferredWidth() {
		if(_widthPercent != -1){
			return (Display.getWidth() * _widthPercent) / 100;
		}
		else{
			return Display.getWidth()-_differential;
		}
	}
	
	public int getPreferredHeight() {
		return _maxH;
	}
	
	public void setWidthInPercent(int width){
		this._widthPercent = width;
	}
	
	public void setMinimumDate(Calendar min){
		this.min = min;
	}
	
	public void setMaximumDate(Calendar max){
		this.max = max;
	}
	
	public void setInitialDate(Calendar initial){
		this.initial = initial;
	}
	
	//Invoked when this field receives the focus.
	protected void onFocus(int direction){
    	_isfocus = true;
        super.onFocus(direction);
    }

    //Invoked when a field loses the focus.
    protected void onUnfocus(){
        _isfocus = false;
        super.onUnfocus();
    }
    
    protected void drawFocus(Graphics g, boolean on){
    	invalidate();
    }
    
    public String getDate(){
    	if(_label.equalsIgnoreCase(_drawableDate)){
    		return null;
    	}
    	else{
    		return _drawableDate;
    	}
    }
    
    public String getDate(String format){
    	SimpleDateFormat sdf = new SimpleDateFormat(format);
		
		return sdf.format(selected);
    }
    
    public Calendar getSelectedCalendar(){
    	return this.selected;
    }
    
    public String getLabel(){
    	return _label;
    }
    
    public void setDate(String date){
    	this._drawableDate = date;
    	this.invalidate();
    }
    
    public void setDateAndInitialCalendar(String date, Calendar initial){
    	setDate(date);
    	this.selected = initial;
    }
    
    public void resetField(){
    	_drawableDate = _label;
    	invalidate();
    }
    
    public void disableInternalDateTimePicker(){
    	this._disableDTP = true;
    }
    
    public void refreshAndInvalidate(){
    	this.invalidate();
    }
}