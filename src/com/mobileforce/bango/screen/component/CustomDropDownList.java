package com.mobileforce.bango.screen.component;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.services.callback.SelectionCallback;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.ImageUtil;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.XYRect;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;


public class CustomDropDownList extends Field{
	private ModelConfig config;
	
	private Bitmap _sideL, _sideM, _sideR;
	private ChoiceListField[] _data;
	private AdvPopupList _list;
	
	private int _maxH, _index, _popW, _popH; 
	private Font _font;
	private String _label;
	private String[] _all_data;
	
	private int _differential;
	private int _widthPercent = -1;
	
	private boolean _isfocus;
	
	private RollingText rolling;
	private boolean isrolling;
	private int posX;
	
	private boolean greenMode = false;
	private SelectionCallback callback;
	private String callbackType;
	
	public CustomDropDownList(String label, int maxW, String[] all_data, long style){
		super(style);
		
		_label = label;
		_all_data = all_data;
		_differential = Display.getWidth() - maxW;
		
		initComponent();
	}
	
	void initComponent(){
		config = ModelConfig.getConfig();
		_font = DeviceProperties.getDefaultApplicationFont(config.getFontMediumHeight());
		_index = -1;
		
		_sideL = config.getDropDown_l();
		_sideM = config.getDropDown_m();
		_sideR = config.getDropDown_r();
		_maxH = _sideM.getHeight();
		
		initializeRolling();
	}
	
	protected void layout(int width, int height) {
		super.setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	void setAction(int index){
		//set selected index & label
		setSelectedIndex(index);
		
		//close popup
		_list.closeMe();
		
		fieldChangeNotify(1);
		
		if(callback != null){
			callback.onSelected(callbackType);
		}
	}
	
	public void paint(Graphics g) {
		if(_isfocus){
			g.setGlobalAlpha(200);
		}
		else{
			g.setGlobalAlpha(255);
		}
		
			// background
			g.drawBitmap(0, 0, _sideL.getWidth(), _sideL.getHeight(), _sideL, 0, 0);
			ImageUtil.drawSpriteTexture(_sideM, g, _sideL.getWidth(), getPreferredWidth()-_sideR.getWidth(), getPreferredWidth()-_sideR.getWidth(), _sideL.getWidth(), 0, 0, _sideM.getHeight(), _sideM.getHeight());
			g.drawBitmap(getPreferredWidth()-_sideR.getWidth(), 0, _sideR.getWidth(), _sideR.getHeight(), _sideR, 0, 0);
			
			g.setFont(_font);
			if(_isfocus){
				if(greenMode){
					g.setColor(0xFFFFFF);
				}
				else{
					g.setColor(0x000000);
				}
				
				if(_index==-1){
					int aWidth = posX + _font.getAdvance(_label) > getPreferredWidth() - _sideR.getWidth() ? getPreferredWidth() - _sideR.getWidth() : posX + _font.getAdvance(_label);
					
					g.drawText(_label, posX, (getPreferredHeight()-_font.getHeight())/2, DrawStyle.LEFT, aWidth);
				}
				else{
					int aWidth = posX + _font.getAdvance(_all_data[_index]) > getPreferredWidth() - _sideR.getWidth() ? getPreferredWidth() - _sideR.getWidth() : _font.getAdvance(_all_data[_index]);
					
					g.drawText(_all_data[_index], posX, (getPreferredHeight()-_font.getHeight())/2, DrawStyle.LEFT, aWidth);
				}
			}
			else{
				if(greenMode){
					g.setColor(0xFFFFFF);
				}
				else{
					g.setColor(0x000000);
				}
				
				if(_index==-1){
					g.drawText(_label, _sideL.getWidth(), (getPreferredHeight()-_font.getHeight())/2, DrawStyle.ELLIPSIS, getPreferredWidth()-((_sideL.getWidth()/2)+_sideR.getWidth()));
				}
				else{
					g.drawText(_all_data[_index], _sideL.getWidth(), (getPreferredHeight()-_font.getHeight())/2, DrawStyle.ELLIPSIS, getPreferredWidth()-((_sideL.getWidth()/2)+_sideR.getWidth()));
				}
			}
	}
	
	protected boolean keyChar(char character, int status, int time){
		if (character == Keypad.KEY_ENTER) {
			//show popup list
			if(_all_data != null) showPopupList();
		}
		return super.keyChar(character, status, time);
	}
	
	protected boolean navigationClick(int status, int time){
		//show popup list
		if(_all_data != null) showPopupList();
		return true;
	}
	
	public int getPreferredWidth() {
		if(_widthPercent != -1){
			return (Display.getWidth() * _widthPercent) / 100;
		}
		else{
			return Display.getWidth()-_differential;
		}
	}
	
	public int getPreferredHeight() {
		return _maxH;
	}
	
	public void setWidthInPercent(int width){
		this._widthPercent = width;
	}
	
	public int getSelectedIndex(){
		return _index;
	}
	
	public void setSelectedIndex(int index){
		if(index!=-1){
			_index = index;
			
			initializeRolling();
		}
		else{
			initComponent();
		}
	}
	
	public void setData(String[] data){
		_all_data = data;
//		setSelectedIndex(0);
	}
	
	//Invoked when this field receives the focus.
	protected void onFocus(int direction){
    	_isfocus = true;
//    	startRolling();
        super.onFocus(direction);
    }

    //Invoked when a field loses the focus.
    protected void onUnfocus(){
        _isfocus = false;
        stopRolling();
        super.onUnfocus();
    }
    
    protected void drawFocus(Graphics g, boolean on){
    	invalidate();
    }
	
    
	void showPopupList(){
		if(_all_data.length>0){
			int tmpW = _font.getAdvance(_all_data[0])+(config.getValue5px());
			int tmpH = config.getValue5px()*4;
			
			for(int d=0; d<_all_data.length; d++){
				//update width
				if(_font.getAdvance(_all_data[d])+(config.getValue5px()*4)>tmpW) tmpW = _font.getAdvance(_all_data[d])+(config.getValue5px()*4);
				
				//update height
				tmpH += _font.getHeight();
			}
			
			//seleksi akhir
			if(tmpW>Display.getWidth()-(config.getValue5px()*4)) tmpW = Display.getWidth()-(config.getValue5px()*4);
			if(tmpH>Display.getHeight()-(config.getValue5px()*4)) tmpH = Display.getHeight()-(config.getValue5px()*4);
			
			_popW = tmpW;//final int maxW = tmpW;
			_popH = tmpH;//final int maxH = tmpH;
			
			//siapkan manager vertical
			VerticalFieldManager v = new VerticalFieldManager(){
				public void sublayout(int maxWidth, int maxHeight){
					int mW = _popW; //maxW;
					int mH = _popH; //maxH;
					
					//maxW tidak perlu dirubah, karena utk mencegah lebar jadi pendek saat limit width dilampaui
					//sebagai gantinya posisi X akan diupdate terhadap batas sisi kanan
					if(mW>Display.getWidth()-(config.getValue5px()*4)) mW = Display.getWidth()-(config.getValue5px()*4); _popW = mW;//if(_popW+((Display.getWidth()*_posX)/100)>Display.getWidth()) mW = Display.getWidth()-((Display.getWidth()*_posX)/100); _popW = mW;
					if(mH>Display.getHeight()-(config.getValue5px()*4)) mH = Display.getHeight()-(config.getValue5px()*4); _popH = mH;//if(_popH+((Display.getHeight()*_posY)/100)>Display.getHeight()) mH = Display.getHeight()-((Display.getHeight()*_posY)/100); _popH = mH; //if(maxH+_posY>Display.getHeight()) mH = Display.getHeight()-_posY;
					
					Field cnt = getField(0); //content
				    layoutChild(cnt, mW-(config.getValue5px()*2), mH-(config.getValue5px()*2));
				    setPositionChild(cnt, config.getValue5px(), config.getValue5px());
				        
			        setExtent(mW, mH);
				}
			};
			
			VerticalFieldManager scroll = new VerticalFieldManager(Manager.VERTICAL_SCROLL | Manager.VERTICAL_SCROLLBAR);
			//isi scroll
			_data = new ChoiceListField[_all_data.length];
			for(int s=0; s<_all_data.length; s++){
				_data[s] = new ChoiceListField(_all_data[s], FOCUSABLE, s);
				
				//add
				scroll.add(_data[s]);
			}
			v.add(scroll);
			
			
			//definisikan popup list
			_list = new AdvPopupList(v); //new AdvPopupList(v, maxW, maxH, _posX, _posY);
			//tampilkan
			_list.showMe();
			
			//set focus list
			try{
				if(_index==-1){
					_data[0].setFocus();
				}
				else{
					_data[_index].setFocus();
				}
			}catch(Exception f){}
		}
	}
	
	class ChoiceListField extends Field {
		boolean isfocus = false;
		private String _label;
		private int _action;
		
		public ChoiceListField(String label, long style, int action){
			super(style);
			
			_label = label;
			_action = action;
		}
		
		protected void layout(int width, int height){
			super.setExtent(getPreferredWidth(), getPreferredHeight());
		}
		
		public int getPreferredWidth() {
			return _popW;
		}
		
		public int getPreferredHeight() {
			return _font.getHeight();
		}
		
		//Invoked when this field receives the focus.
		protected void onFocus(int direction){
	    	isfocus = true;
	        super.onFocus(direction);
	    }

	    //Invoked when a field loses the focus.
	    protected void onUnfocus(){
	        isfocus = false;
	        super.onUnfocus();
	    }
	    
	    protected void drawFocus(Graphics g, boolean on){
	    	invalidate(); //this.updateLayout(); //dinonaktifkan untuk mempercepat display pada real BB devices, namun efek yg ditampilkan adalah scroll indicator selalu berkedip
	    }

		protected void paint(Graphics g){
			if(isfocus){
				//draw background
				g.setColor(0x555555);
				g.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
				
				//draw label
				g.setFont(_font);
				g.setColor(0xffffff);
				g.drawText(_label, config.getValue5px(), (getPreferredHeight()-_font.getHeight())/2, DrawStyle.ELLIPSIS, getPreferredWidth());
			}
			else{
				//draw label
				g.setFont(_font);
				g.setColor(0xffffff);
				g.drawText(_label, config.getValue5px(), (getPreferredHeight()-_font.getHeight())/2, DrawStyle.ELLIPSIS, getPreferredWidth());
			}
		}
		
		protected boolean keyChar(char character, int status, int time){
			if (character == Keypad.KEY_ENTER) {
				//set action
				setAction(_action);
			}
			return super.keyChar(character, status, time);
		}
		
		protected boolean navigationClick(int status, int time){
			//set action
			setAction(_action);
			return true;
		}
	}
	
	
	class AdvPopupList extends PopupScreen {
		private int maxW;
	    private int maxH;
	    private int _posX;
	    private int _posY;
	    private int _oriX;
	    private int _oriY;
	    private AdvPopupList _popup;
	    
	    public AdvPopupList(VerticalFieldManager v){
			super(v, PopupScreen.FOCUSABLE | PopupScreen.LEAVE_BLANK_SPACE);
			this.maxW = 0; if(Properties._BB_OS.startsWith("6") || Properties._BB_OS.startsWith("7")) this.maxW+=48; //fix issue left & bottom blank field
			this.maxH = 0; if(Properties._BB_OS.startsWith("6") || Properties._BB_OS.startsWith("7")) this.maxH+=48; //fix issue left & bottom blank field
			
			_popup = this;
		};
		
		public void paintBackground(Graphics g) {
			g.setColor(0x000000);
			g.fillRect(0, 0, _popW+maxW, _popH+maxH);
		}
		
		public void sublayout(int width, int height) {
			super.sublayout(_popW+maxW, _popH+maxH);
			
			if(_posX==-1 && _posY==-1){
				//center of the screen
				setPosition((Display.getWidth()-_popW)/2, (Display.getHeight()-_popH)/2);	
			}
			else{
				//given coordinate position
				if(getPosX()+_popW>Display.getWidth() && getPosY()+_popH>Display.getHeight()){
					//melebihi batas kanan & bawah
					setPosition(Display.getWidth()-_popW, Display.getHeight()-_popH);
				}
				else if(getPosY()+_popH>Display.getHeight()){
					//melebihi batas bawah
					setPosition(getPosX(), Display.getHeight()-_popH);
				}
				else if(getPosX()+_popW>Display.getWidth()){
					//melebihi batas kanan
					setPosition(Display.getWidth()-_popW, getPosY());
				}
				else{
					setPosition(getPosX(), getPosY());
				}
			}
		}
		
		int getPosX(){
			return (Display.getWidth()*_posX)/100;
		}
		
		int getPosY(){
			if(_oriY+_popH<Display.getHeight()){
				//asumsi posisi Y dropdown cenderung statis, maka posY gunakan statis, namun jika melebihi screen height baru pergunakan persentase
				return _oriY;
			}
			else{
				return (Display.getHeight()*_posY)/100;
			}
		}
		
	    protected void applyTheme() {
	    	///menghilangkan border
	    }
	    
	    public boolean keyChar(char key, int status, int time) {      
			if(key == Keypad.KEY_ESCAPE){
				closeMe();
			}
			return super.keyChar(key, status, time);        
	    }
	    
		void showMe() {
			Runnable runnable = new Runnable() {
	    		public void run() {
	    			try {
	    				UiApplication.getUiApplication().pushScreen(_popup);
	    			} catch (Exception e) {}	
	    		}
			};
			UiApplication.getUiApplication().invokeLater(runnable);
			
			
			//mengambil informasi koordinat & size field yg saat ini sedang menerima focus
			XYRect startRect = new XYRect();
			XYRect activeField = new XYRect();
			UiApplication.getUiApplication().getActiveScreen().getFocusRect(startRect);
			activeField.set(startRect);
			//****************************************************************************
			
			//tooltip positioning
			_oriX = activeField.x;
			_oriY = activeField.y;
			_posX = activeField.x;
			_posY = activeField.y; 
			//jadikan _posX & _posY sebagai persentasi, berguna pada device dng accelerometer
			if(_posX!=-1 && _posY!=-1){ //ini jika center
				//posisi ditentukan
				_posX = (_posX*100)/Display.getWidth(); //persentasi posX
				_posY = (_posY*100)/Display.getHeight(); //persentasi posY
			}
			//****************************************************************************
		}    			    			
	    void closeMe() {
	    	try {
	    		UiApplication.getUiApplication().popScreen(_popup);
	    	} catch (Exception e) {}	
	    }
	}
	
	
//***************************************************************************************	
	private void initializeRolling(){
		isrolling = false;
		
			if(_index==-1){
				if(_font.getAdvance(_label)>getPreferredWidth()-(_sideL.getWidth()+_sideR.getWidth())) isrolling = true;
			}
			else{
				if(_font.getAdvance(_all_data[_index])>getPreferredWidth()-(_sideL.getWidth()+_sideR.getWidth())) isrolling = true;
			}
			posX = _sideL.getWidth();
	}
	
	private void startRolling(){
		initializeRolling(); //untuk memastikan selalu ter-update perhitungan width teks
		
		if(isrolling){
			rolling = new RollingText();
			
			//define a schedule
			Calendar cal = Calendar.getInstance();
	    	Date dt =  cal.getTime();    
	    	Timer time = new Timer();
	    	time.schedule(rolling, dt, 100);
		}
	}
	
	private void stopRolling(){
		if(rolling!=null){
			rolling.stopRolling();
		}
		
			posX = _sideL.getWidth();
	}
	
	//rolling text animation
	class RollingText extends TimerTask {
	    public RollingText(){} 
	        
	    public void run(){
	    	int sideL = _sideL.getWidth();
	    	
	        try{
	        	//System.out.println("[running text] "+posX);
	        	if(_index==-1){
	        		if(posX<-(sideL+_font.getAdvance(_label))){
		        		//reset
		        		posX = sideL;
		        	}
		        	else{
		        		posX -= _font.getAdvance(" ");
		        	}
	        	}
	        	else{
		        	if(posX<-(sideL+_font.getAdvance(_all_data[_index]))){
		        		//reset
		        		posX = sideL;
		        	}
		        	else{
		        		posX -= _font.getAdvance(" ");
		        	}
	        	}
	        }catch(Exception x){
	        	posX = sideL;
	        	System.out.println("error while executing rolling text "+x);
	        }
	    }
	        
	    public void stopRolling(){
	        try{
	        	cancel();
	        }catch(Exception x){System.out.println("error while stop running animation "+x);}
	    }
	}
	
	public void setGreenMode(){
		greenMode = true;
		
		_sideL = config.getDropDown_l_green();
		_sideM = config.getDropDown_m_green();
		_sideR = config.getDropDown_r_green();
		_maxH = _sideM.getHeight();
		
		invalidate();
	}
	
	public void selectionCallback(SelectionCallback callback, String type){
		this.callbackType = type;
		this.callback = callback;
	}
}