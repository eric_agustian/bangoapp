package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.ImageUtil;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

public class CustomEditField2 extends Manager{
	private CustomEditField2 _self;
	private HorizontalFieldManager _inputM;
	private AdvEditField _editField;
	
	private int _width = 0;
	private int _height = 0;
	private int _widthPercent = -1;
	private int _widthPixels = -1;
	
	private Bitmap _sideL, _sideM, _sideR;
	
	private int fontSize = ModelConfig.getConfig().getFontMediumHeight();
	private int fontColor = 0x000000;
	
	private Font fontDefault = DeviceProperties.getDefaultApplicationFont(fontSize);
	
	private String _hint = "";
	private String _printHint = "";
	private int _maxChar = 10;
	private long _filter = EditField.FILTER_DEFAULT;
	
	public CustomEditField2(String hint, int maxChar, long style, long filter){
		super(style);
		
		_self = this;
		
		_hint = hint;
		_maxChar = maxChar;
		_filter = filter;
		
		
		//PHASE 1
		prepareElement();
		
		//PHASE 2
		defineField();
	}
	
	public void setFontSize(int newSize){
		this.fontSize = newSize;
		fontDefault = DeviceProperties.getDefaultApplicationFont(fontSize);
	}
	
	public void setFontColor(int newFontColor){
		this.fontColor = newFontColor;
		this._editField.setColor(newFontColor);
	}
	
	public void setFontFamily(Font fontFamily){
		this.fontDefault = fontFamily;
	}
	
	public void setWidthInPercent(int width){
		this._widthPercent = width;
	}
	
	public void setWidthInPixel(int width){
		this._widthPixels = Display.getWidth() - width;
	}
	
	public int getPreferredWidth() {
		if(_widthPixels != -1){
			return Display.getWidth()-_widthPixels;
		}
		else if(_widthPercent != -1){
			return (Display.getWidth() * _widthPercent) / 100;
		}
		else{
			return _width;
		}
	}
	
	public int getPreferredHeight() {
		return _height;
	}
	
	public void prepareElement(){
		_sideL = ModelConfig.getConfig().getFormWhite_l();
		_sideR = ModelConfig.getConfig().getFormWhite_r();
		_sideM = ModelConfig.getConfig().getFormWhite_m();
		
		_width = _sideL.getWidth() + fontDefault.getAdvance(_hint) + _sideR.getWidth();
		_height = _sideL.getHeight();
	}
	
	public void defineField(){
		_inputM = new HorizontalFieldManager(Manager.HORIZONTAL_SCROLL);
		
		_printHint = _hint;
		_editField = new AdvEditField(fontDefault, _filter, _maxChar, fontColor){
				protected void onFocus(int direction){
					_printHint = "";
					
					super.onFocus(direction);
					_self.invalidate();
				}
				protected void onUnfocus(){
					if(getText().equalsIgnoreCase("")){
						_printHint = _hint;
					}
					
					super.onUnfocus();
					_self.invalidate();
				}
		};
		
		_inputM.add(_editField);
		this.add(_inputM);
	}
	
	public void paintBackground(Graphics g) {
		// left
		g.drawBitmap(0, 0, _sideL.getWidth(), _sideL.getHeight(), _sideL, 0, 0);
		
		// middle
		ImageUtil.drawSpriteTexture(_sideM, g, _sideL.getWidth(), getPreferredWidth()-_sideR.getWidth(), getPreferredWidth()-_sideR.getWidth(), _sideL.getWidth(), 0, 0, _sideM.getHeight(), _sideM.getHeight());
		
		// right
		g.drawBitmap(getPreferredWidth()-_sideR.getWidth(), 0, _sideR.getWidth(), _sideR.getHeight(), _sideR, 0, 0);
		
		
		if(_printHint.equalsIgnoreCase("")){
			//g.clear();
		}
		else{
			g.setFont(fontDefault);
			g.setColor(fontColor);
			g.drawText(_printHint, _sideL.getWidth(), (getPreferredHeight()-fontDefault.getHeight())/2, DrawStyle.ELLIPSIS, getPreferredWidth()-(_sideL.getWidth()+_sideR.getWidth()));
		}
	}
	
	protected void sublayout(int width, int height) {
        Field inp = getField(0); //input
        layoutChild(inp, getPreferredWidth()-(_sideL.getWidth()+_sideR.getWidth()), inp.getPreferredHeight());
        setPositionChild(inp, _sideL.getWidth(), (getPreferredHeight()-inp.getHeight())/2);
        
        setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	
	public String getText(){
		return _editField.getText();
	}
	
	public void setText(String txt){
		_editField.setText(txt);
	}
}
