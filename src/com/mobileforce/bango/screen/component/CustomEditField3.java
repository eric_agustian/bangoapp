package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.ImageUtil;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

public class CustomEditField3 extends Manager{
	private CustomEditField3 _self;
	private HorizontalFieldManager _inputM;
	public AdvEditField editField;
	
	private int _width = 0;
	private int _height = 0;
	private int _widthPercent = -1;
	private int _widthPixels = -1;
	private int _space = 0;
	
	private Bitmap _sideM, _iconL;
	
	private int fontSize = ModelConfig.getConfig().getFontMediumHeight();
	private int fontColor = 0x000000;
	
	private Font fontDefault = DeviceProperties.getDefaultApplicationFont(fontSize);
	
	private String _hint = "";
	private String _printHint = "";
	private int _maxChar = 10;
	private long _filter = EditField.FILTER_DEFAULT;
	
	public CustomEditField3(String hint, int maxChar, long style, long filter){
		this(hint, maxChar, style, filter, null);
	}
	
	public CustomEditField3(String hint, int maxChar, long style, long filter, Bitmap icon){
		super(style);
		
		_self = this;
		
		_hint = hint;
		_maxChar = maxChar;
		_filter = filter;
		
		if(icon!=null){
			_iconL = icon;
			_space = ModelConfig.getConfig().getValue5px();
		}
		
		//PHASE 1
		prepareElement();
		
		//PHASE 2
		defineField();
	}
	
	public void setFontSize(int newSize){
		this.fontSize = newSize;
		fontDefault = DeviceProperties.getDefaultApplicationFont(fontSize);
	}
	
	public void setFontColor(int newFontColor){
		this.fontColor = newFontColor;
		this.editField.setColor(newFontColor);
	}
	
	public void setFontFamily(Font fontFamily){
		this.fontDefault = fontFamily;
	}
	
	public void setWidthInPercent(int width){
		this._widthPercent = width;
	}
	
	public void setWidthInPixel(int width){
		this._widthPixels = Display.getWidth() - width;
	}
	
	public int getPreferredWidth() {
		if(_widthPixels != -1){
			return Display.getWidth()-_widthPixels;
		}
		else if(_widthPercent != -1){
			return (Display.getWidth() * _widthPercent) / 100;
		}
		else{
			return _width;
		}
	}
	
	public int getPreferredHeight() {
		return _height;
	}
	
	public void prepareElement(){
		_sideM = ModelConfig.getConfig().getFormGreen_m();
		
		_width = fontDefault.getAdvance(_hint) + ModelConfig.getConfig().getValue10px();
		_width += _iconL != null ? _iconL.getWidth() : 0;
		
		_height = _sideM.getHeight();
	}
	
	public void defineField(){
		_inputM = new HorizontalFieldManager(Manager.HORIZONTAL_SCROLL);
		
		_printHint = _hint;
		editField = new AdvEditField(fontDefault, _filter, _maxChar, fontColor){
				protected void onFocus(int direction){
					_printHint = "";
					
					super.onFocus(direction);
					_self.invalidate();
				}
				protected void onUnfocus(){
					if(getText().equalsIgnoreCase("")){
						_printHint = _hint;
					}
					
					super.onUnfocus();
					_self.invalidate();
				}
				
				/* (non-Javadoc)
			     * @see net.rim.device.api.ui.Field#keyChar(char, int, int)
			     */
			    protected boolean keyChar( char character, int status, int time ) 
			    {
			        if( character == Characters.ENTER ) {
			            clickButton();
			            return true;
			        }
			        return super.keyChar( character, status, time );
			    }
			    
			    /* (non-Javadoc)
			     * @see net.rim.device.api.ui.Field#navigationClick(int, int)
			     */
			    protected boolean navigationClick( int status, int time ) 
			    {
			        clickButton(); 
			        return true;    
			    }
			    
			    /* (non-Javadoc)
			     * @see net.rim.device.api.ui.Field#trackwheelClick(int, int)
			     */
			    protected boolean trackwheelClick( int status, int time )
			    {        
			        clickButton();    
			        return true;
			    }
			    
			    /**
			     * A public way to click this button.
			     */
			    public void clickButton() 
			    {
			        fieldChangeNotify( 2 );
			    }
		};
		
		_inputM.add(editField);
		this.add(_inputM);
	}
	
	public void paintBackground(Graphics g) {
		//  repeat
		ImageUtil.drawSpriteTexture(_sideM, g, 0, getPreferredWidth(), getPreferredWidth(), 0, 0, 0, _sideM.getHeight(), _sideM.getHeight());
			
				
		// left
		int left = _space;
		if(_iconL != null){
			g.drawBitmap(left, (_sideM.getHeight() - _iconL.getHeight())/2, _iconL.getWidth(), _iconL.getHeight(), _iconL, 0, 0);
			
			left += _iconL.getWidth() + _space;
		}
		else{ }
		
		if(_printHint.equalsIgnoreCase("")){
			//g.clear();
		}
		else{
			g.setFont(fontDefault);
			g.setColor(fontColor);
			g.drawText(_printHint, left, (getPreferredHeight()-fontDefault.getHeight())/2, DrawStyle.ELLIPSIS, getPreferredWidth()-(left+_space));
		}
	}
	
	protected void sublayout(int width, int height) {
		int left = _space;
		if(_iconL != null){
			left += _iconL.getWidth() + _space;
		}
		
        Field inp = getField(0); //input
        layoutChild(inp, getPreferredWidth()-(left+_space), inp.getPreferredHeight());
        setPositionChild(inp, left, (getPreferredHeight()-inp.getHeight())/2);
        
        setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	
	public String getText(){
		return editField.getText();
	}
	
	public void setText(String txt){
		_printHint = "";
		editField.setText(txt);
	}
	
	public void setEditable(boolean editable){
		this.editField.setEditable(editable);
		invalidate();
	}
	
	public void setHint(String hint){
		this._hint = hint;
		invalidate();
	}
}
