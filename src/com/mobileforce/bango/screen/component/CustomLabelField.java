package com.mobileforce.bango.screen.component;

import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.component.LabelField;

public class CustomLabelField extends LabelField {
	int _color = 0;
	int _focusColor = 0;
	boolean isfocus = false;
	
	public CustomLabelField(String label, int color, long style) {
		super(label, style);
		
		_focusColor = color;
		_color = color;
	}
	
	public CustomLabelField(String label, long style, int focusColor, int unfocusColor){
		super(label, style);
		
		_focusColor = focusColor;
		_color = unfocusColor;
	}  

	protected void paint(Graphics graphics) {
		if(isfocus){
			graphics.setColor(_focusColor);
		}else{
			graphics.setColor(_color);
		}
		
		super.paint(graphics);
	}
	
	protected void drawFocus(Graphics g, boolean on){
	}
	
	//Invoked when this field receives the focus.
	protected void onFocus(int direction){
		this.invalidate();
    	isfocus = true;
        super.onFocus(direction);
    }

    //Invoked when a field loses the focus.
    protected void onUnfocus(){
    	this.invalidate();
        isfocus = false;
        super.onUnfocus();
    }
    
    protected boolean keyChar(char character, int status, int time){
		if (character == Keypad.KEY_ENTER) {
			fieldChangeNotify(0);
		}
		return super.keyChar(character, status, time);
	}
	
	protected boolean navigationClick(int status, int time){
		fieldChangeNotify(0);
		return true;
	}
}
