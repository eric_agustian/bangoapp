package com.mobileforce.bango.screen.component;

import net.rim.device.api.ui.Graphics;

public class CustomLineField extends BaseButtonField {
	private int width = 0;
	private int height = 0;
	private int color = 0x000000;
	
	private boolean isfocus;
	private int type = 0; //0:horizontal, 1=vertical
	
	public CustomLineField(long style){
		super(style);
	}
	
	public void setHorizontalLine(int width, int size, int color){
		this.width = width;
		this.height = size;
		this.color = color;
		
		type = 0;
	}
	
	public void setVerticallLine(int width, int size, int color){
		this.width = width;
		this.height = size;
		this.color = color;
		
		type = 1;
	}
	
	public int getPreferredWidth() {
		return width;
	}
	
	public int getPreferredHeight() {
		return height;
	}
	
	protected void layout(int width, int height){
		super.setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	//Invoked when this field receives the focus.
	protected void onFocus(int direction){
    	isfocus = true;
        super.onFocus(direction);
    }

    //Invoked when a field loses the focus.
    protected void onUnfocus(){
        isfocus = false;
        super.onUnfocus();
    }
    
	protected void paint(Graphics g){
		if(isfocus){
			g.setGlobalAlpha(155);
		}
		else{
			g.setGlobalAlpha(255);
		}
		
		g.setColor(color);
		
		if(type == 0){
			g.fillRect(0, 0, width, height);
		}
		else{
			g.fillRect(0, 0, height, width);
		}
		
	}
}
