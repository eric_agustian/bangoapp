package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;

public class CustomListField1 extends BaseButtonField {
	private int _width = 0;
	private int _height = 0;
	private int _widthPercent = -1;
	private int _widthPixels = -1;
	
	private Bitmap icon;
	
	private int fontSize = ModelConfig.getConfig().getFontMediumHeight();
	private int fontColorFocus = 0xDDDDDD;
	private int fontColorUnfocus = 0xFFFFFF;
	
	private Font fontDefault = DeviceProperties.getDefaultApplicationFont(fontSize);
	
	private String _label;
	
	private boolean isfocus = false;
	private boolean nonfocusableMode = false;
	
	public CustomListField1(String label){
		this(label, null, Field.FOCUSABLE);
	}
	
	public CustomListField1(String label, long style){
		super(style);
		
		this._label = label;
		this.icon = null;
		
		prepareElement();
	}
	
	public CustomListField1(String label, Bitmap icon){
		this(label, icon, Field.FOCUSABLE);
	}
	
	public CustomListField1(String label, Bitmap icon, long style){
		super(Field.FOCUSABLE | style);
		
		this._label = label;
		this.icon = icon;
		
		prepareElement();
	}
	
	public void setIcon(Bitmap icon){
		this.icon = icon;
	}
	
	public void setFontSize(int newSize){
		this.fontSize = newSize;
		fontDefault = DeviceProperties.getDefaultApplicationFont(fontSize);
	}
	
	public void setFontColor(int newFocusColor, int newUnfocusColor){
		this.fontColorFocus = newFocusColor;
		this.fontColorUnfocus = newUnfocusColor;
	}
	
	public void setFontFamily(Font fontFamily){
		this.fontDefault = fontFamily;
	}
	
	public void setWidthInPercent(int width){
		this._widthPercent = width;
	}
	
	public void setWidthInPixel(int width){
		this._widthPixels = Display.getWidth() - width;
	}
	
	public int getPreferredWidth() {
		if(_widthPixels != -1){
			return Display.getWidth()-_widthPixels;
		}
		else if(_widthPercent != -1){
			return (Display.getWidth() * _widthPercent) / 100;
		}
		else{
			return _width;
		}
	}
	
	public int getPreferredHeight() {
		return _height;
	}
	
	public void prepareElement(){
		_width = fontDefault.getAdvance(_label) + ModelConfig.getConfig().getValue10px()*2;
		_width += icon != null ? icon.getWidth() + ModelConfig.getConfig().getValue10px() : 0;
		
		if(icon != null){
			_height = fontDefault.getHeight() > icon.getHeight() ? fontDefault.getHeight() : icon.getHeight();
		}
		else{
			_height = fontDefault.getHeight();
		}
		_height += ModelConfig.getConfig().getValue10px()*2;
	}
	
	protected void layout(int width, int height){
		super.setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	//Invoked when this field receives the focus.
	protected void onFocus(int direction){
    	isfocus = true;
        super.onFocus(direction);
    }

    //Invoked when a field loses the focus.
    protected void onUnfocus(){
        isfocus = false;
        super.onUnfocus();
    }
    
	protected void paint(Graphics g){
		if(isfocus){
			g.setColor(Properties._COLOR_LINE_FOCUS);
			g.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
			
			// font color
			g.setColor(fontColorFocus);
			g.setFont(fontDefault);
		}
		else if(nonfocusableMode){
			g.setGlobalAlpha(225);
			
			g.setColor(Properties._COLOR_LINE_FOCUS);
			g.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
			
			// font color
			g.setColor(fontColorFocus);
			g.setFont(fontDefault);
		}
		else{
			g.setColor(Properties._COLOR_LINE_UNFOCUS);
			g.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
			
			// font color
			g.setColor(fontColorUnfocus);
			g.setFont(fontDefault);
		}
		
								
		if(icon != null){
			// draw text
			if(fontDefault.getAdvance(_label) <= getPreferredWidth()-icon.getWidth()-(ModelConfig.getConfig().getValue10px()*3)){
				g.drawText(_label, ModelConfig.getConfig().getValue10px(), (getPreferredHeight() - fontDefault.getHeight()) / 2);
			}
			else{
				g.drawText(_label, ModelConfig.getConfig().getValue10px(), (getPreferredHeight() - fontDefault.getHeight()) / 2, DrawStyle.ELLIPSIS, getPreferredWidth()-icon.getWidth()-(ModelConfig.getConfig().getValue10px()*3));
			}
			
			//draw icon
			g.drawBitmap(getPreferredWidth()-(icon.getHeight()+ModelConfig.getConfig().getValue10px()), (getPreferredHeight()-icon.getHeight())/2, icon.getWidth(), icon.getHeight(), icon, 0, 0);
		}
		else{
			// draw text
			if(fontDefault.getAdvance(_label) <= getPreferredWidth()-(ModelConfig.getConfig().getValue10px()*2)){
				g.drawText(_label, ModelConfig.getConfig().getValue10px(), (getPreferredHeight() - fontDefault.getHeight()) / 2);
			}
			else{
				g.drawText(_label, ModelConfig.getConfig().getValue10px(), (getPreferredHeight() - fontDefault.getHeight()) / 2, DrawStyle.ELLIPSIS, getPreferredWidth()-(ModelConfig.getConfig().getValue10px()*2));
			}
		}
	}
	
	public void setNonFocusableMode(){
		this.nonfocusableMode = true;
		invalidate();
	}
}
