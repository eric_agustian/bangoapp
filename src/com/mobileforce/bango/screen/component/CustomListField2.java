package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.ImageScaler;
import com.mobileforce.bango.utility.ImageUtil;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;

public class CustomListField2 extends BaseButtonField {
	private int _width = 0;
	private int _height = 0;
	private int _widthPercent = -1;
	private int _widthPixels = -1;
	
	private Bitmap icon, separator, rating, arrow;
	
	private int fontSize = ModelConfig.getConfig().getFontMediumHeight();
	private int fontColor = Properties._COLOR_FONT_LABEL_GREEN;
	
	private Font fontDefault = DeviceProperties.getDefaultApplicationFont(fontSize);
	private Font fontDefaultBold = DeviceProperties.getDefaultApplicationFont(Font.BOLD, fontSize);
	
	/*
	 * data[0] = nama
	 * data[1] = jarak
	 * data[2] = rating
	 */
	private String[] _data;
	
	private boolean isfocus;
	
	public CustomListField2(String[] data){
		this(data, Field.FOCUSABLE);
	}
	
	public CustomListField2(String[] data, long style){
		super(style);
		
		this._data = data;
		
		prepareElement();
	}
	
	public void setIcon(Bitmap icon){
		this.icon = ImageUtil.getMaskedCircledIcon(ModelConfig.getConfig().getIconMasking(), icon, ModelConfig.getConfig().getIconMasking().getWidth(), ModelConfig.getConfig().getIconMasking().getHeight());
	}
	
	public void setFontSize(int newSize){
		this.fontSize = newSize;
		fontDefault = DeviceProperties.getDefaultApplicationFont(fontSize);
	}
	
	public void setFontColor(int newColor){
		this.fontColor = newColor;
	}
	
	public void setFontFamily(Font fontFamily){
		this.fontDefault = fontFamily;
	}
	
	public void setWidthInPercent(int width){
		this._widthPercent = width;
	}
	
	public void setWidthInPixel(int width){
		this._widthPixels = Display.getWidth() - width;
	}
	
	public int getPreferredWidth() {
		if(_widthPixels != -1){
			return Display.getWidth()-_widthPixels;
		}
		else if(_widthPercent != -1){
			return (Display.getWidth() * _widthPercent) / 100;
		}
		else{
			return _width;
		}
	}
	
	public int getPreferredHeight() {
		return _height;
	}
	
	public void prepareElement(){
		// default icon
		icon = ImageUtil.getMaskedCircledIcon(ModelConfig.getConfig().getIconMasking(), ModelConfig.getConfig().getTemporerKuliner(), ModelConfig.getConfig().getIconMasking().getWidth(), ModelConfig.getConfig().getIconMasking().getHeight());
		
		// separator
		separator = ImageScaler.resizeBitmapARGB(ModelConfig.getConfig().getDivider(), Display.getWidth(), ModelConfig.getConfig().getDivider().getHeight());
		
		// rating
		rating = ModelConfig.getConfig().getIconSmallRating();
		
		// arrow
		arrow = ModelConfig.getConfig().getIconLeftArrow();
		
		_width = fontDefault.getAdvance(_data[0]) + ModelConfig.getConfig().getValue10px()*2;
		_width += icon != null ? icon.getWidth() + ModelConfig.getConfig().getValue10px() : 0;
		
		_height = fontDefault.getHeight()*2 > icon.getHeight() ? fontDefault.getHeight()*2 : icon.getHeight();
		_height += ModelConfig.getConfig().getValue10px()*2;
	}
	
	protected void layout(int width, int height){
		super.setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	//Invoked when this field receives the focus.
	protected void onFocus(int direction){
    	isfocus = true;
        super.onFocus(direction);
    }

    //Invoked when a field loses the focus.
    protected void onUnfocus(){
        isfocus = false;
        super.onUnfocus();
    }
    
	protected void paint(Graphics g){
		if(isfocus){
			g.setGlobalAlpha(100);
		}
		else{
			g.setGlobalAlpha(255);
		}
		
								
		// draw text
		g.setColor(fontColor);
		int posY = (getPreferredHeight() - (fontDefault.getHeight() + fontDefaultBold.getHeight()))/2;
		if(fontDefault.getAdvance(_data[0]) <= getPreferredWidth()-(icon.getWidth()+(ModelConfig.getConfig().getValue10px()*4)+arrow.getWidth())){
			// name
			g.setFont(fontDefaultBold);
			g.drawText(_data[0], icon.getWidth() + (ModelConfig.getConfig().getValue10px()*2), posY);
			
			// jarak
			g.setFont(fontDefault);
			g.drawText(_data[1], icon.getWidth() + (ModelConfig.getConfig().getValue10px()*2), posY + fontDefaultBold.getHeight());
		}
		else{
			// name
			g.setFont(fontDefaultBold);
			g.drawText(_data[0], icon.getWidth() + (ModelConfig.getConfig().getValue10px()*2), posY, DrawStyle.ELLIPSIS, getPreferredWidth()-(icon.getWidth()+(ModelConfig.getConfig().getValue10px()*4)+arrow.getWidth()));
			
			// jarak
			g.setFont(fontDefault);
			g.drawText(_data[1], icon.getWidth() + (ModelConfig.getConfig().getValue10px()*2), posY + fontDefaultBold.getHeight());
		}
		
		// rating
		int rateValue = Integer.parseInt(_data[2]) != -1 ? Integer.parseInt(_data[2]) : 0;
		int posX = icon.getWidth() + (ModelConfig.getConfig().getValue10px()*3) + fontDefault.getAdvance(_data[1]);
		for(int n=0; n<rateValue; n++){
			g.drawBitmap(posX, (posY + fontDefaultBold.getHeight()) + ((fontDefault.getHeight() - rating.getHeight())/2), rating.getWidth(), rating.getHeight(), rating, 0, 0);
			posX += rating.getWidth();
		}
			
		// draw icon
		g.drawBitmap(ModelConfig.getConfig().getValue10px(), (getPreferredHeight()-icon.getHeight())/2, icon.getWidth(), icon.getHeight(), icon, 0, 0);
		
		// separator
		g.drawBitmap((getPreferredWidth()-separator.getWidth())/2, getPreferredHeight()-separator.getHeight(), separator.getWidth(), separator.getHeight(), separator, 0, 0);
		
		// draw arrow
		g.drawBitmap(getPreferredWidth()-(arrow.getWidth() + ModelConfig.getConfig().getValue10px()), (getPreferredHeight()-arrow.getHeight())/2, arrow.getWidth(), arrow.getHeight(), arrow, 0, 0);
	}
}
