package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.ImageScaler;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;

public class CustomListField3 extends BaseButtonField {
	private int _width = 0;
	private int _height = 0;
	private int _widthPercent = -1;
	private int _widthPixels = -1;
	
	private Bitmap separator;
	
	private int fontSize = ModelConfig.getConfig().getFontMediumHeight();
	private int fontColorFocus = 0xDDDDDD;
	private int fontColorUnfocus = 0xFFFFFF;
	
	private Font fontDefault = DeviceProperties.getDefaultApplicationFont(fontSize);
	private Font fontDefaultBold = DeviceProperties.getDefaultApplicationFont(Font.BOLD, fontSize);
	
	private String[] _data;
	
	private boolean isfocus = false;
	private boolean unread = true;
	
	public CustomListField3(String[] data){
		this(data, Field.FOCUSABLE);
	}
	
	public CustomListField3(String[] data, long style){
		super(style);
		
		this._data = data;
		
		prepareElement();
	}
	
	public void setFontSize(int newSize){
		this.fontSize = newSize;
		fontDefault = DeviceProperties.getDefaultApplicationFont(fontSize);
	}
	
	public void setFontColor(int newFocusColor, int newUnfocusColor){
		this.fontColorFocus = newFocusColor;
		this.fontColorUnfocus = newUnfocusColor;
	}
	
	public void setFontFamily(Font fontFamily){
		this.fontDefault = fontFamily;
	}
	
	public void setWidthInPercent(int width){
		this._widthPercent = width;
	}
	
	public void setWidthInPixel(int width){
		this._widthPixels = Display.getWidth() - width;
	}
	
	public int getPreferredWidth() {
		if(_widthPixels != -1){
			return Display.getWidth()-_widthPixels;
		}
		else if(_widthPercent != -1){
			return (Display.getWidth() * _widthPercent) / 100;
		}
		else{
			return _width;
		}
	}
	
	public int getPreferredHeight() {
		return _height;
	}
	
	public void prepareElement(){
		// separator
		separator = ImageScaler.resizeBitmapARGB(ModelConfig.getConfig().getDivider(), Display.getWidth(), ModelConfig.getConfig().getDivider().getHeight());
		
		_width = fontDefault.getAdvance(_data[0]) + ModelConfig.getConfig().getValue10px()*2;
		
		_height = fontDefault.getHeight()*2;
		_height += ModelConfig.getConfig().getValue10px();
	}
	
	protected void layout(int width, int height){
		super.setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	//Invoked when this field receives the focus.
	protected void onFocus(int direction){
    	isfocus = true;
        super.onFocus(direction);
    }

    //Invoked when a field loses the focus.
    protected void onUnfocus(){
        isfocus = false;
        super.onUnfocus();
    }
    
	protected void paint(Graphics g){
		if(isfocus){
			g.setGlobalAlpha(255);
			
			g.setColor(Properties._COLOR_LINE_FOCUS);
			g.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
			g.setColor(fontColorFocus);
		}
		else{
			if(unread){
				g.setGlobalAlpha(255);
			}
			else{
				g.setGlobalAlpha(155);
			}
			
			g.setColor(Properties._COLOR_LINE_UNFOCUS);
			g.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
			g.setColor(fontColorUnfocus);
		}
		
								
		// draw text
		int posY = (getPreferredHeight() - (fontDefault.getHeight() + fontDefaultBold.getHeight()))/2;
		if(fontDefault.getAdvance(_data[0]) <= getPreferredWidth()-(ModelConfig.getConfig().getValue10px()*2)){
			// name
			g.setFont(fontDefaultBold);
			g.drawText(_data[0], ModelConfig.getConfig().getValue10px(), posY);
			
			// time
			g.setFont(fontDefault);
			g.drawText(_data[1], ModelConfig.getConfig().getValue10px(), posY + fontDefaultBold.getHeight());
		}
		else{
			// name
			g.setFont(fontDefaultBold);
			g.drawText(_data[0], ModelConfig.getConfig().getValue10px(), posY, DrawStyle.ELLIPSIS, getPreferredWidth()-(ModelConfig.getConfig().getValue10px()*2));
			
			// time
			g.setFont(fontDefault);
			g.drawText(_data[1], ModelConfig.getConfig().getValue10px(), posY + fontDefaultBold.getHeight());
		}
		
		// separator
		g.drawBitmap((getPreferredWidth()-separator.getWidth())/2, getPreferredHeight()-separator.getHeight(), separator.getWidth(), separator.getHeight(), separator, 0, 0);
	}
	
	public void setUnread(boolean read){
		this.unread = read;
		invalidate();
	}
}
