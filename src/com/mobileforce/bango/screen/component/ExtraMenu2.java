//#preprocess

package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.manager.ClassManager;
import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.screen.BerbagiKulinerScreen;
import com.mobileforce.bango.screen.JajananSpecialScreen;
import com.mobileforce.bango.services.callback.FilePickerCallback;
import com.mobileforce.bango.storage.query.TemporerStore;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.FilePickerListener;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.picker.FilePicker;

public class ExtraMenu2 extends HorizontalFieldManager implements FieldChangeListener, FilePickerCallback{
	private HorizontalFieldManager _contentManager;
	private VerticalFieldManager _vMan1, _vMan2, _vMan3;
	
	private CustomBitmapButton buttonLokasi, buttonBerbagi, buttonJajan;
	private CustomLabelField labelLokasi, labelBerbagi, labelJajan;
	
	private int margin = 0;
	
	public ExtraMenu2(){
		super(NO_HORIZONTAL_SCROLL | FIELD_HCENTER);
		
		initComponent();
		initSubLayout();
	}
	
	private void initComponent(){
		buttonLokasi = new CustomBitmapButton(ModelConfig.getConfig().getIconBigTunjukJalan(), FOCUSABLE | FIELD_HCENTER);
		buttonLokasi.setChangeListener(this);
		
		buttonBerbagi = new CustomBitmapButton(ModelConfig.getConfig().getIconBigBerbagi(), FOCUSABLE | FIELD_HCENTER);
		buttonBerbagi.setChangeListener(this);
		
		buttonJajan = new CustomBitmapButton(ModelConfig.getConfig().getIconBigJajananSpecial(), FOCUSABLE | FIELD_HCENTER);
		buttonJajan.setChangeListener(this);
		
		labelLokasi = new CustomLabelField(Properties._LBL_LOKASI_KULINER, Properties._COLOR_FONT_LABEL_WHITE, FIELD_HCENTER | NON_FOCUSABLE);
		labelLokasi.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, ModelConfig.getConfig().getFontSmallHeight()));
		
		labelBerbagi = new CustomLabelField(Properties._LBL_SHARE, Properties._COLOR_FONT_LABEL_WHITE, FIELD_HCENTER | NON_FOCUSABLE);
		labelBerbagi.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, ModelConfig.getConfig().getFontSmallHeight()));
		
		labelJajan = new CustomLabelField(Properties._LBL_JAJANAN, Properties._COLOR_FONT_LABEL_WHITE, FIELD_HCENTER | NON_FOCUSABLE);
		labelJajan.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, ModelConfig.getConfig().getFontSmallHeight()));
		
		if(ModelConfig.getConfig().isOrientationPortrait()){
			margin = ModelConfig.getConfig().getValue5px();
		}
		else{
			margin = ModelConfig.getConfig().getValue8px()*2;
		}
	}
	
	private void initSubLayout(){
		_contentManager = new HorizontalFieldManager(FIELD_HCENTER);
		
		_vMan1 = new VerticalFieldManager(FIELD_HCENTER);
		_vMan1.setMargin(0, margin, 0, margin);
		
		_vMan2 = new VerticalFieldManager(FIELD_HCENTER);
		_vMan2.setMargin(0, margin, 0, margin);
		
		_vMan3 = new VerticalFieldManager(FIELD_HCENTER);
		_vMan3.setMargin(0, margin, 0, margin);
		
		_vMan1.add(buttonLokasi);
		_vMan1.add(labelLokasi);
		
		_vMan2.add(buttonBerbagi);
		_vMan2.add(labelBerbagi);
		
		_vMan3.add(buttonJajan);
		_vMan3.add(labelJajan);
		
		_contentManager.add(_vMan1);
		_contentManager.add(_vMan2);
		_contentManager.add(_vMan3);
		
		this.add(_contentManager);
		
		invalidate();
	}
	
	protected void paintBackground(Graphics g) {
		g.setColor(Properties._COLOR_BG_EXRA_MENU);
		g.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
	}
	
	protected void sublayout(int width, int height) {
		Field content = getField(0);
		layoutChild(content, content.getPreferredWidth(), content.getPreferredHeight());
	    setPositionChild(content, (getPreferredWidth()-content.getWidth()) / 2, (getPreferredHeight() - content.getHeight()) / 2);
	    
	    setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	public int getPreferredWidth() {
		return Display.getWidth();
	}

	public int getPreferredHeight() {
		return buttonLokasi.getPreferredHeight() + labelLokasi.getHeight() + ModelConfig.getConfig().getValue8px();
	}

	public void fieldChanged(Field field, int arg1) {
		if(field == buttonLokasi){
			ScreenManager.clearAllNonFirstMapScreen();
		}
		else if(field == buttonBerbagi){
			//on OS 5 using this
			//#ifdef BlackBerrySDK5.0.0
			String[] filter = {".jpg", ".jpeg", ".png", ".gif" };
			new FilePickerListener(Properties._LBL_CHOOSE_PICTURE, -1, filter, System.getProperty("fileconn.dir.photos"), this);
			
			//on OS 6 or newer using this
			//#else
			new FilePickerListener(Properties._LBL_CHOOSE_PICTURE, FilePicker.VIEW_PICTURES, null, null, this);
			//#endif
		}
		else if(field == buttonJajan){
			if(TemporerStore.getMenuSpecial().size() > 0){
				JajananSpecialScreen screen = ClassManager.getInstance().getScreenManager().getJajananSpecialScreen();
				screen.getContent(0);
				screen.setTitleCustom(Properties._LBL_JAJANAN);
				ScreenManager.getBangoApp().pushScreen(screen);
			}
			else{
				Dialog.alert(Properties._INF_NO_DATA);
			}
		}
	}
	
	public void onPicked(String path) {
		// TODO Auto-generated method stub
		try{
			BerbagiKulinerScreen screen = ClassManager.getInstance().getScreenManager().getBerbagiKulinerScreen();
			screen.onPicked(path);
			ScreenManager.getBangoApp().pushScreen(screen);
		}catch(Exception x){
			System.out.println("unable picked picture : " + x);
		}
	}
}
