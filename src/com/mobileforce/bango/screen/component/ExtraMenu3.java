package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.services.callback.ButtonPressedCallback;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class ExtraMenu3 extends HorizontalFieldManager implements FieldChangeListener{
	private HorizontalFieldManager _contentManager;
	private VerticalFieldManager _vMan1, _vMan2;
	
	private CustomBitmapButton buttonTulis, buttonTunjuk;
	private CustomLabelField labelTulis, labelTunjuk;
	
	private int margin = 0;
	
	private ButtonPressedCallback callback;
	
	public ExtraMenu3(){
		super(NO_HORIZONTAL_SCROLL | FIELD_HCENTER);
		
		initComponent();
		initSubLayout();
	}
	
	private void initComponent(){
		buttonTulis = new CustomBitmapButton(ModelConfig.getConfig().getIconBigTulisReview(), FOCUSABLE | FIELD_HCENTER);
		buttonTulis.setChangeListener(this);
		
		buttonTunjuk = new CustomBitmapButton(ModelConfig.getConfig().getIconBigTunjukJalan(), FOCUSABLE | FIELD_HCENTER);
		buttonTunjuk.setChangeListener(this);
		
		labelTulis = new CustomLabelField(Properties._LBL_TULIS_REVIEW, Properties._COLOR_FONT_LABEL_WHITE, FIELD_HCENTER | NON_FOCUSABLE);
		labelTulis.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, ModelConfig.getConfig().getFontSmallHeight()));
		
		labelTunjuk = new CustomLabelField(Properties._LBL_RUTE, Properties._COLOR_FONT_LABEL_WHITE, FIELD_HCENTER | NON_FOCUSABLE);
		labelTunjuk.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, ModelConfig.getConfig().getFontSmallHeight()));
		
		if(ModelConfig.getConfig().isOrientationPortrait()){
			margin = ModelConfig.getConfig().getValue5px();
		}
		else{
			margin = ModelConfig.getConfig().getValue8px()*2;
		}
	}
	
	private void initSubLayout(){
		_contentManager = new HorizontalFieldManager(FIELD_HCENTER);
		
		_vMan1 = new VerticalFieldManager(FIELD_HCENTER);
		_vMan1.setMargin(0, margin, 0, margin);
		
		_vMan2 = new VerticalFieldManager(FIELD_HCENTER);
		_vMan2.setMargin(0, margin, 0, margin);
		
		_vMan1.add(buttonTulis);
		_vMan1.add(labelTulis);
		
		_vMan2.add(buttonTunjuk);
		_vMan2.add(labelTunjuk);
		
		_contentManager.add(_vMan1);
		_contentManager.add(_vMan2);
		
		this.add(_contentManager);
		
		invalidate();
	}
	
	protected void paintBackground(Graphics g) {
		g.setColor(Properties._COLOR_BG_EXRA_MENU);
		g.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
	}
	
	protected void sublayout(int width, int height) {
		Field content = getField(0);
		layoutChild(content, content.getPreferredWidth(), content.getPreferredHeight());
	    setPositionChild(content, (getPreferredWidth()-content.getWidth()) / 2, (getPreferredHeight() - content.getHeight()) / 2);
	    
	    setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	public int getPreferredWidth() {
		return Display.getWidth();
	}

	public int getPreferredHeight() {
		return buttonTulis.getPreferredHeight() + labelTulis.getHeight() + ModelConfig.getConfig().getValue8px();
	}

	public void fieldChanged(Field field, int arg1) {
		if(field == buttonTulis){
			if(callback != null) this.callback.onPressed(Properties._LBL_TULIS_REVIEW);
		}
		else if(field == buttonTunjuk){
			if(callback != null) this.callback.onPressed(Properties._LBL_RUTE);
		}
	}
	
	public void setButtonPressedCallback(ButtonPressedCallback callback){
		this.callback = callback;
	}
}
