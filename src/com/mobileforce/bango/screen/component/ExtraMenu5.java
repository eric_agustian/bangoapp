//#preprocess

package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.services.callback.ButtonPressedCallback;
import com.mobileforce.bango.services.callback.FilePickerCallback;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.FilePickerListener;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.picker.FilePicker;

public class ExtraMenu5 extends HorizontalFieldManager implements FieldChangeListener, FilePickerCallback{
	private CustomBitmapButton buttonTambah;
	private CustomLabelField labelTambah;
	
	private ButtonPressedCallback callback;
	
	public ExtraMenu5(){
		super(NO_HORIZONTAL_SCROLL | FIELD_HCENTER);
		
		initComponent();
		initSubLayout();
	}
	
	private void initComponent(){
		buttonTambah = new CustomBitmapButton(ModelConfig.getConfig().getButtonTambahKuliner(), FOCUSABLE | FIELD_HCENTER);
		buttonTambah.setMargin(ModelConfig.getConfig().getValue5px(), ModelConfig.getConfig().getValue10px(), ModelConfig.getConfig().getValue5px(), ModelConfig.getConfig().getValue10px());
		buttonTambah.setChangeListener(this);
		
		labelTambah = new CustomLabelField(Properties._LBL_TIDAK_MENEMUKAN, Properties._COLOR_FONT_LABEL_WHITE, FIELD_HCENTER | NON_FOCUSABLE);
		labelTambah.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, ModelConfig.getConfig().getFontMediumHeight()));
	}
	
	private void initSubLayout(){
		this.add(labelTambah);
		this.add(buttonTambah);
		
		invalidate();
	}
	
	protected void paintBackground(Graphics g) {
		g.setColor(Properties._COLOR_BG_EXRA_MENU);
		g.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
	}
	
	protected void sublayout(int width, int height) {
		Field label = getField(0);
		Field btn = getField(1);
		
		layoutChild(label, label.getPreferredWidth(), label.getPreferredHeight());
		layoutChild(btn, btn.getPreferredWidth(), btn.getPreferredHeight());
		
		if(label.getWidth() + btn.getWidth() + (ModelConfig.getConfig().getValue10px()*3) > getPreferredWidth()){
			layoutChild(label, getPreferredWidth()-(buttonTambah.getPreferredWidth() + (ModelConfig.getConfig().getValue10px()*4)), label.getPreferredHeight());
			setPositionChild(label, ModelConfig.getConfig().getValue10px(), (getPreferredHeight() - (labelTambah.getPreferredHeight()*2)) / 2);
			setPositionChild(btn, getPreferredWidth()-(btn.getWidth()+ModelConfig.getConfig().getValue10px()), (getPreferredHeight() - btn.getHeight()) / 2);
		}
		else{
			int posX = (getPreferredWidth()-(label.getWidth() + btn.getWidth() + (ModelConfig.getConfig().getValue10px()*2))) / 2;
			
			setPositionChild(label, posX, (getPreferredHeight() - label.getHeight()) / 2);
			setPositionChild(btn, getPreferredWidth()-(btn.getWidth()+posX), (getPreferredHeight() - btn.getHeight()) / 2);
		}
	    
	    setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	public int getPreferredWidth() {
		return Display.getWidth();
	}

	public int getPreferredHeight() {
		return buttonTambah.getPreferredHeight() + ModelConfig.getConfig().getValue10px();
	}

	public void fieldChanged(Field field, int arg1) {
		if(field == buttonTambah){
			//on OS 5 using this
			//#ifdef BlackBerrySDK5.0.0
			String[] filter = {".jpg", ".jpeg", ".png", ".gif" };
			new FilePickerListener(Properties._LBL_CHOOSE_PICTURE, -1, filter, System.getProperty("fileconn.dir.photos"), this);
			
			//on OS 6 or newer using this
			//#else
			new FilePickerListener(Properties._LBL_CHOOSE_PICTURE, FilePicker.VIEW_PICTURES, null, null, this);
			//#endif
		}
	}
	
	public void setButtonPressedCallback(ButtonPressedCallback callback){
		this.callback = callback;
	}
	
	public void onPicked(String path) {
		// TODO Auto-generated method stub
		try{
			if(callback != null){
				this.callback.onPressed(path);
			}
		}catch(Exception x){
			System.out.println("unable picked picture : " + x);
		}
	}
}
