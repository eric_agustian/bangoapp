package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.services.callback.ButtonPressedCallback;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.container.HorizontalFieldManager;

public class ExtraMenu6 extends HorizontalFieldManager implements FieldChangeListener{
	private CustomBitmapButton buttonTambah;
	
	private ButtonPressedCallback callback;
	
	public ExtraMenu6(){
		super(NO_HORIZONTAL_SCROLL | FIELD_HCENTER);
		
		initComponent();
		initSubLayout();
	}
	
	private void initComponent(){
		buttonTambah = new CustomBitmapButton(ModelConfig.getConfig().getButtonMasukkanLokasi(), FOCUSABLE | FIELD_HCENTER);
		buttonTambah.setMargin(ModelConfig.getConfig().getValue5px(), ModelConfig.getConfig().getValue10px(), ModelConfig.getConfig().getValue5px(), ModelConfig.getConfig().getValue10px());
		buttonTambah.setChangeListener(this);
	}
	
	private void initSubLayout(){
		this.add(buttonTambah);
		
		invalidate();
	}
	
	protected void paintBackground(Graphics g) {
		g.setColor(Properties._COLOR_BG_EXRA_MENU);
		g.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
	}
	
	protected void sublayout(int width, int height) {
		Field btn = getField(0);
		layoutChild(btn, btn.getPreferredWidth(), btn.getPreferredHeight());
		setPositionChild(btn, (getPreferredWidth()-btn.getWidth())/2, (getPreferredHeight()-btn.getHeight())/2);
	    
	    setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	public int getPreferredWidth() {
		return Display.getWidth();
	}

	public int getPreferredHeight() {
		return buttonTambah.getPreferredHeight() + ModelConfig.getConfig().getValue10px();
	}

	public void fieldChanged(Field field, int arg1) {
		if(field == buttonTambah){
			if(callback != null) this.callback.onPressed(Properties._LBL_TAMBAH_LOKASI);
		}
	}
	
	public void setButtonPressedCallback(ButtonPressedCallback callback){
		this.callback = callback;
	}
}
