package com.mobileforce.bango.screen.component;

import java.util.Timer;
import java.util.TimerTask;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.services.callback.ButtonPressedCallback;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.UiApplication;

public class FetchingAnimation extends Field {
	private int _maxW, _maxH;
	private int _widthPercent = -1;
	private int _widthPixels = -1;
	
	private Bitmap[] animeF;
	private int index = 0;
	private TimerTask timerTask;
	private boolean isloading = false;
	
	private String _label = "";
	private Font fontDefault = DeviceProperties.getDefaultApplicationFont(ModelConfig.getConfig().getFontMediumHeight());
	private int fColorOn = Properties._COLOR_FONT_LABEL_GREEN;
	private int fColorUn = Properties._COLOR_FONT_LABEL_BLACK;
	private boolean isfocus = false;
	private boolean focusAction = false;
	
	private ButtonPressedCallback callback;
	private String type;
	
	public FetchingAnimation(String label, long style){
		super(FOCUSABLE | style);
		
		_label = label;
		
		prepareElement();
	}
	
	private void prepareElement(){
		animeF = ModelConfig.getConfig().getLoadingImage();
		
		_maxW = Display.getWidth();
		_maxH = animeF[0].getHeight() > fontDefault.getHeight() ? animeF[0].getHeight() : fontDefault.getHeight();
		_maxH += ModelConfig.getConfig().getValue10px();
	}
	
	public int getPreferredWidth(){
		if(_widthPixels != -1){
			return Display.getWidth()-_widthPixels;
		}
		else if(_widthPercent != -1){
			return (Display.getWidth() * _widthPercent) / 100;
		}
		else{
			return _maxW;
		}
	}
	
	public int getPreferredHeight(){
		return _maxH;
	}
	
	public void setWidthInPercent(int width){
		this._widthPercent = width;
	}
	
	public void setWidthInPixel(int width){
		this._widthPixels = Display.getWidth() - width;
	}
	
	protected void layout(int width, int height) {
		setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	public void start() {
		try {			
			timerTask = new TimerTask() {
				public void run() {
					index ++;
					index = index % animeF.length;
					synchronized (UiApplication.getUiApplication().getAppEventLock()) {
						invalidate();
					}
				}
			};
			new Timer().scheduleAtFixedRate(timerTask, 100, 100);
		} catch (Exception e) {
		}
	}
	
	protected boolean navigationClick(int status, int time){
		doAction();
		return true;
	}
	
	//Invoked when this field receives the focus.
	protected void onFocus(int direction){
    	isfocus = true;
    	if(focusAction) doAction();
        super.onFocus(direction);
    }

    //Invoked when a field loses the focus.
    protected void onUnfocus(){
        isfocus = false;
        super.onUnfocus();
    }
    
    protected void drawFocus(Graphics g, boolean on){
    	invalidate();
    }

	protected void paint(Graphics g) {
		if(isloading){
			g.drawBitmap((getPreferredWidth()-animeF[index].getWidth())/2, (getPreferredHeight()-animeF[index].getHeight())/2, animeF[index].getWidth(), animeF[index].getHeight(), animeF[index], 0, 0);
		}
		else{
			g.setFont(fontDefault);
			if(isfocus){
				g.setColor(fColorOn);
			}
			else{
				g.setColor(fColorUn);
			}
			g.drawText(_label, (getPreferredWidth()-fontDefault.getAdvance(_label))/2, (getPreferredHeight()-fontDefault.getHeight())/2, DrawStyle.ELLIPSIS, fontDefault.getAdvance(_label));
		}
	}
	
	public void setButtonPressedCallback(ButtonPressedCallback callback, String type){
		this.callback = callback;
		this.type = type;
	}
	
	public void setFocusAction(){
		this.focusAction = true;
	}
	
	private void doAction(){
		if(!isloading){
			isloading = true;
			start();
			
			if(callback != null){
				callback.onPressed(type);
			}
		}
	}
}
