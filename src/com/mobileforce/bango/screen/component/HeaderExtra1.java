package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.UiApp;
import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.services.callback.ButtonPressedCallback;
import com.mobileforce.bango.utility.ImageUtil;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

public class HeaderExtra1 extends HorizontalFieldManager implements FieldChangeListener{
	private Bitmap background;
	
	private CustomButton3 buttonCari;
	private CustomEditField3 searchBar;
	
	private int marginLR = 0;
	
	private ButtonPressedCallback callback;
	
	public HeaderExtra1(){
		super(NO_HORIZONTAL_SCROLL | FIELD_HCENTER);
		
		initComponent();
		initSubLayout();
	}
	
	private void initComponent(){
		marginLR = (ModelConfig.getConfig().getIconMenu().getWidth()*2) + (ModelConfig.getConfig().getValue10px()*4);
		
		background = ModelConfig.getConfig().getHeaderBgRepeat();
		
		buttonCari = new CustomButton3(Properties._LBL_CARI);
		buttonCari.setFontColor(Properties._COLOR_FONT_LABEL_WHITE, Properties._COLOR_FONT_LABEL_WHITE);
		buttonCari.setButtonBackgroundMode(true);
		buttonCari.setButtonWidthInPixel(buttonCari.getPreferredWidth()+(ModelConfig.getConfig().getValue10px()*2));
		buttonCari.setChangeListener(this);
		
		searchBar = new CustomEditField3("", 35, FIELD_HCENTER, EditField.FILTER_DEFAULT, ModelConfig.getConfig().getIconSearch());
		searchBar.setFontColor(Properties._COLOR_FONT_LABEL_WHITE);
		searchBar.setWidthInPixel(Display.getWidth() - (marginLR + ModelConfig.getConfig().getValue5px() + buttonCari.getPreferredWidth()));
		searchBar.editField.setChangeListener(this);
	}
	
	private void initSubLayout(){
		this.add(searchBar);
		this.add(buttonCari);
		
		invalidate();
	}
	
	protected void paintBackground(Graphics g) {
		ImageUtil.drawSpriteTexture(background, g, 0, getPreferredWidth(), getPreferredWidth(), 0, 0, 0, getPreferredHeight(), getPreferredHeight());
	}
	
	protected void sublayout(int width, int height) {
		Field SEARCH = getField(0);
		layoutChild(SEARCH, SEARCH.getPreferredWidth(), SEARCH.getPreferredHeight());
	    setPositionChild(SEARCH, marginLR/2, (getPreferredHeight() - SEARCH.getHeight()) / 2);
	    
	    Field BTN = getField(1);
		layoutChild(BTN, BTN.getPreferredWidth(), BTN.getPreferredHeight());
	    setPositionChild(BTN, getPreferredWidth() - (BTN.getWidth() + (marginLR/2)), (getPreferredHeight() - BTN.getHeight()) / 2);
	    
	    setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	public int getPreferredWidth() {
		return Display.getWidth();
	}

	public int getPreferredHeight() {
		return background.getHeight();
	}
	
	private UiApp getBangoApp(){
		return ((UiApp)UiApplication.getUiApplication());
	}

	public void fieldChanged(Field field, int arg1) {
		if(field == buttonCari || (field == searchBar.editField && arg1 == 2)){
			String keyword = getSearchText().trim();
			if(keyword.length() == 0){
				Dialog.alert(Properties._INF_KETIK_KEYWORD);
			}
			else{
//				Dialog.alert(keyword);
//				if(!(getBangoApp().getActiveScreen() instanceof FirstMapScreen)){
//					
//				}
//				else if(){
//					
//				}
				
				this.callback.onPressed(Properties._CONN_TYPE_SEARCH_CLICKED);
			}
		}
	}
	
	public void setSearchText(String text){
		this.searchBar.setText(text);
	}
	
	public String getSearchText(){
		return this.searchBar.getText();
	}
	
	public void setSearchHint(String text){
		this.searchBar.setHint(text);
	}
	
	public void setButtonPressedCallback(ButtonPressedCallback callback){
		this.callback = callback;
	}
}
