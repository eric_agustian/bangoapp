package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.services.callback.ButtonPressedCallback;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.container.HorizontalFieldManager;

public class HeaderExtra2 extends HorizontalFieldManager implements FieldChangeListener{
	private CustomButton3 buttonTerdekat, buttonTerbaru, buttonTerpopuler;
	
	private int marginLR = 0;
	
	private ButtonPressedCallback callback;
	
	public HeaderExtra2(){
		super(NO_HORIZONTAL_SCROLL | FIELD_HCENTER);
		
		initComponent();
		initSubLayout();
	}
	
	private void initComponent(){
		marginLR = (ModelConfig.getConfig().getIconMenu().getWidth()*2) + (ModelConfig.getConfig().getValue10px()*4);
		
		int btnW = (Display.getWidth() - (marginLR + (ModelConfig.getConfig().getValue10px()*2))) / 3; 
		
		buttonTerdekat = new CustomButton3(Properties._LBL_TERDEKAT);
		buttonTerdekat.setFontSize(ModelConfig.getConfig().getFontSmallHeight());
		buttonTerdekat.setFontColor(Properties._COLOR_FONT_LABEL_WHITE, Properties._COLOR_FONT_LABEL_WHITE);
		buttonTerdekat.setButtonWidthInPixel(btnW);
		buttonTerdekat.setChangeListener(this);
		
		buttonTerbaru = new CustomButton3(Properties._LBL_TERBARU);
		buttonTerbaru.setFontSize(ModelConfig.getConfig().getFontSmallHeight());
		buttonTerbaru.setFontColor(Properties._COLOR_FONT_LABEL_WHITE, Properties._COLOR_FONT_LABEL_WHITE);
		buttonTerbaru.setButtonWidthInPixel(btnW);
		buttonTerbaru.setChangeListener(this);
		
		buttonTerpopuler = new CustomButton3(Properties._LBL_TERPOPULER);
		buttonTerpopuler.setFontSize(ModelConfig.getConfig().getFontSmallHeight());
		buttonTerpopuler.setFontColor(Properties._COLOR_FONT_LABEL_WHITE, Properties._COLOR_FONT_LABEL_WHITE);
		buttonTerpopuler.setButtonWidthInPixel(btnW);
		buttonTerpopuler.setChangeListener(this);
	}
	
	private void initSubLayout(){
		this.add(buttonTerdekat);
		this.add(buttonTerbaru);
		this.add(buttonTerpopuler);
		
		invalidate();
	}
	
	protected void paintBackground(Graphics g) {
		g.setColor(Properties._COLOR_BG_DARK_GREEN);
		g.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
	}
	
	protected void sublayout(int width, int height) {
		Field B1 = getField(0);
		layoutChild(B1, B1.getPreferredWidth(), B1.getPreferredHeight());
	    setPositionChild(B1, marginLR/2, (getPreferredHeight() - B1.getHeight()) / 2);
	    
	    Field B2 = getField(1);
		layoutChild(B2, B2.getPreferredWidth(), B2.getPreferredHeight());
	    setPositionChild(B2, (getPreferredWidth()-B2.getWidth())/2, (getPreferredHeight() - B2.getHeight()) / 2);
	    
	    Field B3 = getField(2);
		layoutChild(B3, B3.getPreferredWidth(), B3.getPreferredHeight());
	    setPositionChild(B3, getPreferredWidth() - (B3.getWidth() + (marginLR/2)), (getPreferredHeight() - B3.getHeight()) / 2);
	    
	    setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	public int getPreferredWidth() {
		return Display.getWidth();
	}

	public int getPreferredHeight() {
		return buttonTerdekat.getPreferredHeight() + (ModelConfig.getConfig().getValue10px()*2);
	}
	
//	private UiApp getBangoApp(){
//		return ((UiApp)UiApplication.getUiApplication());
//	}

	public void fieldChanged(Field field, int arg1) {
		if(field == buttonTerdekat){
			setClicked(1);
			
			callback.onPressed(Properties._CONN_TYPE_HOME_DISTANCE);
		}
		else if(field == buttonTerbaru){
			setClicked(2);
			
			callback.onPressed(Properties._CONN_TYPE_HOME_POPULAR);
		}
		else if(field == buttonTerpopuler){
			setClicked(3);
			
			callback.onPressed(Properties._CONN_TYPE_HOME_NEW);
		}
	}
	
	public void setTerdekatClicked(){
		setClicked(1);
	}
	
	private void setClicked(int index){
		if(index == 1){
			buttonTerdekat.setClickedMode(true);
			buttonTerbaru.setClickedMode(false);
			buttonTerpopuler.setClickedMode(false);
		}
		else if(index == 2){
			buttonTerdekat.setClickedMode(false);
			buttonTerbaru.setClickedMode(true);
			buttonTerpopuler.setClickedMode(false);
		}
		else if(index == 3){
			buttonTerdekat.setClickedMode(false);
			buttonTerbaru.setClickedMode(false);
			buttonTerpopuler.setClickedMode(true);
		}
	}
	
	public void setButtonPressedCallback(ButtonPressedCallback callback){
		this.callback = callback;
	}
}
