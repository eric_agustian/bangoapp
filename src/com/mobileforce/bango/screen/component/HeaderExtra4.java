package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.services.callback.ButtonPressedCallback;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.container.HorizontalFieldManager;

public class HeaderExtra4 extends HorizontalFieldManager{
	private Font font1, font2;
	private CustomLabelField label1, label2;
	
	private ButtonPressedCallback callback;
	
	public HeaderExtra4(){
		super(NO_HORIZONTAL_SCROLL | FIELD_HCENTER);
		
		initComponent();
		initSubLayout();
	}
	
	private void initComponent(){
		font1 = DeviceProperties.getDefaultApplicationFont(Font.PLAIN, ModelConfig.getConfig().getFontSmallHeight());
		font2 = DeviceProperties.getDefaultApplicationFont(Font.BOLD, ModelConfig.getConfig().getFontSmallHeight());
		
		label1 = new CustomLabelField(Properties._LBL_HASIL_CARI, Properties._COLOR_FONT_LABEL_WHITE, NON_FOCUSABLE);
		label1.setFont(font1);
		
		label2 = new CustomLabelField("\"\"", Properties._COLOR_FONT_LABEL_WHITE, NON_FOCUSABLE);
		label2.setFont(font2);
	}
	
	private void initSubLayout(){
		this.add(label1);
		this.add(label2);
		
		invalidate();
	}
	
	protected void paintBackground(Graphics g) {
		g.setColor(Properties._COLOR_BG_GREY_GREEN);
		g.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
	}
	
	protected void sublayout(int width, int height) {
		Field L1 = getField(0);
		Field L2 = getField(1);
		
		layoutChild(L1, L1.getPreferredWidth(), L1.getPreferredHeight());
		layoutChild(L2, L2.getPreferredWidth(), L2.getPreferredHeight());
		
		int posX = (getPreferredWidth() - (L1.getWidth() + L2.getWidth() + ModelConfig.getConfig().getValue5px()))/2;
		
	    setPositionChild(L1, posX, (getPreferredHeight() - L1.getHeight()) / 2);
	    setPositionChild(L2, getPreferredWidth() - (posX + L2.getWidth()), (getPreferredHeight() - L2.getHeight()) / 2);
	    
	    setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	public int getPreferredWidth() {
		return Display.getWidth();
	}

	public int getPreferredHeight() {
		return label1.getPreferredHeight() + (ModelConfig.getConfig().getValue5px()*2);
	}
	
	public void setLabel2Text(String text){
		this.label2.setText("\"" + text + "\"");
	}
	
	public void setButtonPressedCallback(ButtonPressedCallback callback){
		this.callback = callback;
	}
}
