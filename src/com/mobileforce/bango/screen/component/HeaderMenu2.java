package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.UiApp;
import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.manager.ClassManager;
import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.screen.CariKulinerScreen;
import com.mobileforce.bango.screen.CariMapScreen;
import com.mobileforce.bango.screen.DaftarKulinerScreen;
import com.mobileforce.bango.screen.FirstMapScreen;
import com.mobileforce.bango.screen.TambahLokasiScreen;
import com.mobileforce.bango.storage.query.NotificationStatusStore;
import com.mobileforce.bango.utility.ImageUtil;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

public class HeaderMenu2 extends HorizontalFieldManager implements FieldChangeListener{
	private Bitmap background;
	
	private HorizontalFieldManager _leftManager, _rightManager;
	
	private CustomBitmapButton buttonMenu, buttonBack;
	private CustomEditField3 searchBar;
	
	private boolean isSearchBarIncreased = false;
	
	public HeaderMenu2(){
		super(NO_HORIZONTAL_SCROLL | FIELD_HCENTER);
		
		initComponent();
		initSubLayout();
	}
	
	private void initComponent(){
		background = ModelConfig.getConfig().getHeaderBgRepeat();
		
		buttonMenu = new CustomBitmapButton(ModelConfig.getConfig().getIconMenu());
		buttonMenu.setChangeListener(this);
		
		buttonBack = new CustomBitmapButton(ModelConfig.getConfig().getIconBack());
		buttonBack.setChangeListener(this);
		
		searchBar = new CustomEditField3("", 35, FIELD_HCENTER, EditField.FILTER_DEFAULT, ModelConfig.getConfig().getIconSearch());
		searchBar.setFontColor(Properties._COLOR_FONT_LABEL_WHITE);
		searchBar.setWidthInPixel(Display.getWidth() - (buttonMenu.getPreferredWidth() + buttonBack.getPreferredWidth() + (ModelConfig.getConfig().getValue5px()*8)));
		searchBar.editField.setChangeListener(this);
	}
	
	private void initSubLayout(){
		_leftManager = new HorizontalFieldManager();
		_rightManager = new HorizontalFieldManager();
		
		_leftManager.add(buttonBack);
		_rightManager.add(buttonMenu);
		
		this.add(_leftManager);
		this.add(searchBar);
		this.add(_rightManager);
		
		invalidate();
	}
	
	protected void paintBackground(Graphics g) {
		ImageUtil.drawSpriteTexture(background, g, 0, getPreferredWidth(), getPreferredWidth(), 0, 0, 0, getPreferredHeight(), getPreferredHeight());
	}
	
	protected void sublayout(int width, int height) {
		Field M1 = getField(0);
		layoutChild(M1, M1.getPreferredWidth(), M1.getPreferredHeight());
	    setPositionChild(M1, ModelConfig.getConfig().getValue10px(), (getPreferredHeight() - M1.getHeight()) / 2);
	    
		Field L = getField(1);
		layoutChild(L, L.getPreferredWidth(), L.getPreferredHeight());
		if(isSearchBarIncreased){
			setPositionChild(L, ModelConfig.getConfig().getValue10px(), (getPreferredHeight() - L.getHeight()) / 2);
		}
		else{
			setPositionChild(L, (getPreferredWidth() - L.getWidth()) / 2, (getPreferredHeight() - L.getHeight()) / 2);
		}
	    
	    Field M2 = getField(2);
		layoutChild(M2, M2.getPreferredWidth(), M2.getPreferredHeight());
	    setPositionChild(M2, getPreferredWidth() - (M2.getWidth() + ModelConfig.getConfig().getValue10px()), (getPreferredHeight() - M2.getHeight()) / 2);
	    
	    setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	public int getPreferredWidth() {
		return Display.getWidth();
	}

	public int getPreferredHeight() {
		return background.getHeight();
	}
	
	private UiApp getBangoApp(){
		return ((UiApp)UiApplication.getUiApplication());
	}

	public void fieldChanged(Field field, int arg1) {
		if(field == buttonMenu){
			// show menu screen
			synchronized (UiApplication.getUiApplication().getAppEventLock()) {
				setMenuNotificationStatus(false);
				NotificationStatusStore.updateNotificationStatus(false);
				
				ScreenManager.showSideBarScreen();
			}
		}
		else if(field == buttonBack){
			// close all screen except Dashboard
			ScreenManager.closeCurrentScreen();
		}
		else if(field == searchBar.editField && arg1 == 2){
			String keyword = getSearchText().trim();
			if(keyword.length() == 0){
				Dialog.alert(Properties._INF_KETIK_KEYWORD);
			}
			else{
				setSearchText("");
				if(getBangoApp().getActiveScreen() instanceof DaftarKulinerScreen){
					CariKulinerScreen screen = ClassManager.getInstance().getScreenManager().getCariKulinerScreen();
					screen.searchKeyword(keyword);
					ScreenManager.getBangoApp().pushScreen(screen);
				}
				else if(getBangoApp().getActiveScreen() instanceof CariMapScreen){
					ScreenManager.clearAllNonFirstMapScreen();
					
					// show search store on map view
					CariMapScreen screen = ClassManager.getInstance().getScreenManager().getCariMapScreen();
					screen.searchKeyword(keyword);
					ScreenManager.getBangoApp().pushScreen(screen);
				}
				else if(getBangoApp().getActiveScreen() instanceof FirstMapScreen){
					// show search store on map view
					CariMapScreen screen = ClassManager.getInstance().getScreenManager().getCariMapScreen();
					screen.searchKeyword(keyword);
					ScreenManager.getBangoApp().pushScreen(screen);
				}
				else if(getBangoApp().getActiveScreen() instanceof TambahLokasiScreen){
					// show location on tambah lokasi
					TambahLokasiScreen screen = (TambahLokasiScreen) getBangoApp().getActiveScreen();
					screen.searchLocationByName(keyword);
				}
			}
		}
	}
	
	public void setSearchText(String text){
		this.searchBar.setText(text);
	}
	
	public String getSearchText(){
		return this.searchBar.getText();
	}
	
	public void removeLeftMenu(){
		this._leftManager.deleteAll();
	}
	
	public void removeRightMenu(){
		this._rightManager.deleteAll();
	}
	
	public void setSearchHint(String text){
		this.searchBar.setHint(text);
	}
	
	public void setMenuNotificationStatus(boolean status){
		this.buttonMenu.setNotificationStatus(status);
	}
	
	public void increaseSearchBarSize(){
		searchBar.setWidthInPixel(Display.getWidth() - (buttonMenu.getPreferredWidth() + (ModelConfig.getConfig().getValue5px()*7)));
		isSearchBarIncreased = true;
		invalidate();
	}
}
