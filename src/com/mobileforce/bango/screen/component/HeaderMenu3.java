package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.storage.query.NotificationStatusStore;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.ImageUtil;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.HorizontalFieldManager;

public class HeaderMenu3 extends HorizontalFieldManager implements FieldChangeListener{
	private Bitmap background;
	
	private HorizontalFieldManager _leftManager, _rightManager, _middleManager;
	
	private CustomBitmapButton buttonMenu, buttonBack;
	private CustomLabelField labelTitle1, labelTitle2;
	
	public HeaderMenu3(){
		super(NO_HORIZONTAL_SCROLL | FIELD_HCENTER);
		
		initComponent();
		initSubLayout();
	}
	
	private void initComponent(){
		background = ModelConfig.getConfig().getHeaderBgRepeat();
		
		buttonMenu = new CustomBitmapButton(ModelConfig.getConfig().getIconMenu());
		buttonMenu.setChangeListener(this);
		
		buttonBack = new CustomBitmapButton(ModelConfig.getConfig().getIconBack());
		buttonBack.setChangeListener(this);
		
		labelTitle1 = new CustomLabelField("", Properties._COLOR_FONT_LABEL_WHITE, FIELD_HCENTER | NON_FOCUSABLE);
		labelTitle1.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, ModelConfig.getConfig().getFontLargeHeight()));
		
		labelTitle2 = new CustomLabelField("", Properties._COLOR_FONT_LABEL_WHITE, FIELD_HCENTER | NON_FOCUSABLE | DrawStyle.ELLIPSIS);
		labelTitle2.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, ModelConfig.getConfig().getFontLargeHeight()));
	}
	
	private void initSubLayout(){
		_leftManager = new HorizontalFieldManager();
		_rightManager = new HorizontalFieldManager();
		_middleManager = new HorizontalFieldManager();
		
		_leftManager.add(buttonBack);
		_rightManager.add(buttonMenu);
		
		_middleManager.add(labelTitle1);
		_middleManager.add(labelTitle2);
		
		this.add(_leftManager);
		this.add(_middleManager);
		this.add(_rightManager);
		
		invalidate();
	}
	
	protected void paintBackground(Graphics g) {
		ImageUtil.drawSpriteTexture(background, g, 0, getPreferredWidth(), getPreferredWidth(), 0, 0, 0, getPreferredHeight(), getPreferredHeight());
	}
	
	protected void sublayout(int width, int height) {
		Field M1 = getField(0);
		layoutChild(M1, M1.getPreferredWidth(), M1.getPreferredHeight());
	    setPositionChild(M1, ModelConfig.getConfig().getValue10px(), (getPreferredHeight() - M1.getHeight()) / 2);
	    
		Field M2 = getField(2);
		layoutChild(M2, M2.getPreferredWidth(), M2.getPreferredHeight());
	    setPositionChild(M2, getPreferredWidth() - (M2.getWidth() + ModelConfig.getConfig().getValue10px()), (getPreferredHeight() - M2.getHeight()) / 2);
	    
	    Field L = getField(1);
		layoutChild(L, L.getPreferredWidth(), L.getPreferredHeight());
		if(L.getWidth() > getPreferredWidth()-(M1.getWidth()+M2.getWidth()+(ModelConfig.getConfig().getValue10px()*4))){
			layoutChild(L, getPreferredWidth()-(M1.getWidth()+M2.getWidth()+(ModelConfig.getConfig().getValue10px()*4)), L.getPreferredHeight());
			setPositionChild(L, M1.getWidth()+(ModelConfig.getConfig().getValue10px()*2), (getPreferredHeight() - L.getHeight()) / 2);
		}
		else{
			setPositionChild(L, (getPreferredWidth() - L.getWidth()) / 2, (getPreferredHeight() - L.getHeight()) / 2);
		}
	    
	    setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	public int getPreferredWidth() {
		return Display.getWidth();
	}

	public int getPreferredHeight() {
		return background.getHeight();
	}

	public void fieldChanged(Field field, int arg1) {
		if(field == buttonMenu){
			// show menu screen
			synchronized (UiApplication.getUiApplication().getAppEventLock()) {
				setMenuNotificationStatus(false);
				NotificationStatusStore.updateNotificationStatus(false);
				
				ScreenManager.showSideBarScreen();
			}
		}
		else if(field == buttonBack){
			// close all screen except Dashboard
			ScreenManager.closeCurrentScreen();
		}
	}
	
	public void setHeaderTitle1(String text){
		this.labelTitle1.setText(text);
	}
	
	public void setHeaderTitle2(String text){
		this.labelTitle2.setText(text);
	}
	
	public void removeLeftMenu(){
		this._leftManager.deleteAll();
	}
	
	public void removeRightMenu(){
		this._rightManager.deleteAll();
	}
	
	public void setMenuNotificationStatus(boolean status){
		this.buttonMenu.setNotificationStatus(status);
	}
}
