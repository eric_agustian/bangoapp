package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.services.ImageFetching;
import com.mobileforce.bango.utility.ImageScaler;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class ImageZoomPopupScreen extends PopupScreen{
	private static ImageZoomPopupScreen _self;
	private static VerticalFieldManager _bodyManager;
	
	private static Bitmap previewBig;

	private static CustomBitmapButton btnClose;
	private static boolean isUpdated = false;
	
	public ImageZoomPopupScreen(VerticalFieldManager manager){
		super(manager, DEFAULT_CLOSE);
		//transparent bg
		super.setBackground(BackgroundFactory.createSolidTransparentBackground(0x000000, 125));
	}
	
	public void sublayout(int width, int height) {
		super.sublayout(width, height);
		setPosition(0, 0);
	}
	
	public static ImageZoomPopupScreen showImage(String urlImage){
		initComponent();
		initSublayout();
		initContent();
		
		ImageZoomPopupScreen screen = new ImageZoomPopupScreen(_bodyManager);
		_self = screen;
		
		String[] imgUrl = { urlImage };
		new ImageFetching(screen, imgUrl, Properties._CONN_TYPE_IMAGE_ZOOM);
		
		return screen;
	}
	
	protected void applyTheme(){
	}
	
	private static void initComponent(){
		previewBig = ImageScaler.resizeBitmapARGB(ModelConfig.getConfig().getIconTemporerImage(), ModelConfig.getConfig().getBigPreviewWidth()-(ModelConfig.getConfig().getValue10px()*2), ModelConfig.getConfig().getBigPreviewHeight());
		
		btnClose = new CustomBitmapButton(ModelConfig.getConfig().getIconCloseGreen());
		btnClose.setChangeListener(new FieldChangeListener() {
			public void fieldChanged(Field field, int context) {
				// TODO Auto-generated method stub
				closeMe();
			}
		});
		btnClose.setMargin(0, ModelConfig.getConfig().getValue10px(), 0, ModelConfig.getConfig().getValue10px());
		btnClose.setBorderFocus(false);
	}
	
	private static void initSublayout(){
		_bodyManager = new VerticalFieldManager(NO_VERTICAL_SCROLL | NO_VERTICAL_SCROLLBAR){
			protected void paintBackground(Graphics g){	
				// draw bitmap
				g.drawBitmap((getWidth()-previewBig.getWidth())/2, (getHeight()-previewBig.getHeight())/2, previewBig.getWidth(), previewBig.getHeight(), previewBig, 0, 0);
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aW = Display.getWidth();
				int aH = Display.getHeight();
				
				Field content = getField(0);
			    layoutChild(content, content.getPreferredWidth(), content.getPreferredHeight());
			    if(isUpdated){
			    	setPositionChild(content, ((aW-previewBig.getWidth())/2) + previewBig.getWidth() - (btnClose.getPreferredWidth()/2), ((aH-previewBig.getHeight())/2) - (btnClose.getPreferredHeight()/2));
			    }
			    else{
			    	setPositionChild(content, aW-(content.getWidth()+ModelConfig.getConfig().getValue5px()), ((aH-ModelConfig.getConfig().getBigPreviewHeight())/2)-(content.getHeight()/2));
			    }
			    
			    setExtent(aW, aH);
			}
		};
	}
	
	private static void initContent(){
		_bodyManager.add(btnClose);
	}

	private static void closeMe(){
		ScreenManager.closeImageZoomPopup(_self);
	}
	
	public void setZoomImage(Bitmap image){
		isUpdated = true;
		previewBig = image;
		this.invalidate();
		this.updateLayout();
	}
}
