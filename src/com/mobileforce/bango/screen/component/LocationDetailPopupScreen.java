package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.manager.ClassManager;
import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.screen.StoreDetailScreen;
import com.mobileforce.bango.services.ImageFetching;
import com.mobileforce.bango.services.InvokeBBMaps;
import com.mobileforce.bango.storage.persistable.BodyPersistable;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class LocationDetailPopupScreen extends PopupScreen implements FieldChangeListener{
	private static VerticalFieldManager _bodyManager, _contentManager;
	private static HorizontalFieldManager _hManager;
	private static BodyPersistable _data;

	private static Bitmap background;
	private int x, y;
	
	private static BitmapField preview;
	private static CustomLabelField lblJudul, lblJarak, lblAlamat;
	private static SmallRatingField rating;
	private static CustomButton3 buttonReview, buttonRute;
	
	public LocationDetailPopupScreen(VerticalFieldManager manager, int x, int y){
		super(manager, DEFAULT_CLOSE);
		//transparent bg
		super.setBackground(BackgroundFactory.createSolidTransparentBackground(0xffffff, 0));
		
		this.x = x;
		this.y = y;
	}
	
	public void sublayout(int width, int height) {
		super.sublayout(width, height);
		setPosition(x, y);
	}
	
	public static LocationDetailPopupScreen showDetail(BodyPersistable data, int x, int y){
		_data = data;
		
		initComponent();
		initSublayout();
		initContent();
		
//		//mengambil informasi koordinat & size field yg saat ini sedang menerima focus
//		XYRect startRect = new XYRect();
//		XYRect activeField = new XYRect();
//		UiApplication.getUiApplication().getActiveScreen().getFocusRect(startRect);
//		activeField.set(startRect);
//		//****************************************************************************
//				
//		//positioning
//		int x = (activeField.x + (activeField.width/2)) - (background.getWidth()/2);
//		int y = (activeField.y + (activeField.height/2)) - background.getHeight();
		
		LocationDetailPopupScreen screen = new LocationDetailPopupScreen(_bodyManager, x, y);
		
		buttonReview.setChangeListener(screen);
		buttonRute.setChangeListener(screen);
		
		String[] imgUrl = { data.get_urlimg() };
		new ImageFetching(screen, imgUrl, Properties._CONN_TYPE_POPUP_DETAIL_IMAGE);
		
		return screen;
	}
	
	protected void applyTheme(){
	}
	
	private static void initComponent(){
		background = ModelConfig.getConfig().getBgBaseDetail();
		preview = new BitmapField(ModelConfig.getConfig().getIconTemporerImage(), NON_FOCUSABLE | FIELD_HCENTER);
		
		lblJudul = new CustomLabelField(_data.get_title(), Properties._COLOR_FONT_LABEL_BLACK, FIELD_HCENTER | FOCUSABLE);
		lblJudul.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, ModelConfig.getConfig().getFontSmallHeight()));
		lblJudul.setMargin(0, 0, 0, 0);
		
		lblJarak = new CustomLabelField(_data.get_distance() + " KM", Properties._COLOR_FONT_LABEL_GREEN, FIELD_HCENTER | FIELD_VCENTER | NON_FOCUSABLE);
		lblJarak.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, ModelConfig.getConfig().getFontSmallHeight()));
		lblJarak.setMargin(0, 0, 0, 0);
		
		lblAlamat = new CustomLabelField(_data.get_address(), Properties._COLOR_FONT_LABEL_BLACK, FIELD_HCENTER | NON_FOCUSABLE);
		lblAlamat.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, ModelConfig.getConfig().getFontSmallHeight()));
		lblAlamat.setMargin(0, 0, 0, 0);
		
		rating = new SmallRatingField(_data.get_rating(), NON_FOCUSABLE | FIELD_HCENTER | FIELD_VCENTER);
		rating.setMargin(ModelConfig.getConfig().getValue5px(), 0, 0, 0);
		
		buttonReview = new CustomButton3(Properties._LBL_REVIEW, FOCUSABLE | FIELD_HCENTER);
		buttonReview.setButtonWidthInPixel(background.getWidth()*8/10);
		buttonReview.setButtonBackgroundMode(true);
		buttonReview.setMargin(ModelConfig.getConfig().getValue10px(), 0, ModelConfig.getConfig().getValue5px(), 0);
		
		buttonRute = new CustomButton3(Properties._LBL_RUTE, FOCUSABLE | FIELD_HCENTER);
		buttonRute.setButtonWidthInPixel(background.getWidth()*8/10);
		buttonRute.setFontSize(ModelConfig.getConfig().getFontSuperSmallHeight());
		buttonRute.setButtonBackgroundMode(true);
		buttonRute.enableMultiRowMode();
		buttonRute.setMargin(0, 0, ModelConfig.getConfig().getValue10px(), 0);
	}
	
	private static void initSublayout(){
		_bodyManager = new VerticalFieldManager(NO_VERTICAL_SCROLL | NO_VERTICAL_SCROLLBAR){
			protected void paintBackground(Graphics g){	
				//draw background
				g.drawBitmap(0, 0, background.getWidth(), background.getHeight(), background, 0, 0);
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aW = background.getWidth();
				int aH = background.getHeight();
				
				Field content = getField(0);
			    layoutChild(content, aW-ModelConfig.getConfig().getValue10px(), aH-(ModelConfig.getConfig().getValue10px()*2));
			    setPositionChild(content, ModelConfig.getConfig().getValue5px(), ModelConfig.getConfig().getValue5px());
				
			    setExtent(aW, aH);
			}
		};
		
		_contentManager = new VerticalFieldManager(VERTICAL_SCROLL | VERTICAL_SCROLLBAR | FIELD_HCENTER);
		_hManager = new HorizontalFieldManager(FIELD_HCENTER | FIELD_VCENTER);
	}
	
	private static void initContent(){
		_hManager.add(lblJarak);
		_hManager.add(rating);
		
		_contentManager.add(preview);
		_contentManager.add(lblJudul);
		_contentManager.add(_hManager);
		_contentManager.add(lblAlamat);
		_contentManager.add(buttonReview);
		_contentManager.add(buttonRute);
		
		_bodyManager.add(_contentManager);
	}

	public void fieldChanged(Field field, int arg1) {
		if(field == buttonReview){
			StoreDetailScreen screen = ClassManager.getInstance().getScreenManager().getStoreDetailScreen();
			screen.setDetailData(_data);
			ScreenManager.getBangoApp().pushScreen(screen);
			
			closeMe();
		}
		else if(field == buttonRute){
			String lat_1 = Properties._LATITUDE;
			String lon_1 = Properties._LONGITUDE;
			String lat_2 = _data.get_lat();
			String lon_2 = _data.get_lon();
			String title1 = "It is me!";
			String title2 = _data.get_title();
			String desc1 = "";
			String desc2 = _data.get_address();
			
			new InvokeBBMaps(lat_1, lon_1, lat_2, lon_2, title1, title2, desc1, desc2);
			
			closeMe();
		}
	}

	private void closeMe(){
		ScreenManager.closeLocationDetailPopup(this);
	}
	
	public void setPreviewImage(Bitmap image){
		synchronized (UiApplication.getUiApplication().getAppEventLock()) {
			preview.setBitmap(image);
			invalidate();
		}
	}
}
