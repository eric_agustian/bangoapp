package com.mobileforce.bango.screen.component;



import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.ImageUtil;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class NotificationHawkerScreen extends MainScreen{
	private static VerticalFieldManager _bodyManager;
	private static String _text;

	private static Bitmap background;
	private static int x, y;
	
	private static CustomLabelField lblText;
	
	public NotificationHawkerScreen(VerticalFieldManager manager){
		super(NO_VERTICAL_SCROLL | NO_HORIZONTAL_SCROLL);
		//transparent bg
		super.setBackground(BackgroundFactory.createSolidTransparentBackground(0xffffff, 0));
		
		add(manager);
	}
	
	public static NotificationHawkerScreen showDetail(String text, int posY){
		_text = text;
		
		initComponent();
		initSublayout();
		initContent();
				
		//positioning
		x = 0;
		y = posY;
		
		NotificationHawkerScreen screen = new NotificationHawkerScreen(_bodyManager);
		
		return screen;
	}
	
	public void sublayout(int width, int height) {
		setExtent(Display.getWidth(), background.getHeight());
        setPosition(x, y);
        layoutDelegate(Display.getWidth(), background.getHeight());
	}
	
	protected boolean keyDown( int keycode, int status ) {
        if ( Keypad.key( keycode ) == Keypad.KEY_ESCAPE ) {
        	closeMe();
            return true;
        }
        return super.keyDown( keycode, status );
    }
	
	private static void initComponent(){
		background = ModelConfig.getConfig().getHeaderBgRepeat();
		
		lblText = new CustomLabelField(_text, Properties._COLOR_FONT_LABEL_WHITE, FIELD_HCENTER | FOCUSABLE);
		lblText.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, ModelConfig.getConfig().getFontSmallHeight()));
		lblText.setMargin(0, 0, 0, ModelConfig.getConfig().getValue10px());
	}
	
	private static void initSublayout(){
		_bodyManager = new VerticalFieldManager(NO_VERTICAL_SCROLL | NO_VERTICAL_SCROLLBAR){
			protected void paintBackground(Graphics g){	
				g.setGlobalAlpha(185);
				ImageUtil.drawSpriteTexture(background, g, 0, Display.getWidth(), Display.getWidth(), 0, 0, 0, background.getHeight(), background.getHeight());
				g.setGlobalAlpha(255);
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aW = Display.getWidth();
				int aH = background.getHeight();
				
				Field content = getField(0);
			    layoutChild(content, content.getPreferredWidth(), content.getPreferredHeight());
			    
			    if(content.getWidth() > aW-(ModelConfig.getConfig().getValue10px()*2)){
			    	layoutChild(content, aW-(ModelConfig.getConfig().getValue10px()*2), content.getPreferredHeight());
			    	setPositionChild(content, ModelConfig.getConfig().getValue10px(), ModelConfig.getConfig().getValue5px());
			    }
			    else{
			    	setPositionChild(content, (aW-content.getWidth())/2, (aH-content.getHeight())/2);
			    }
			    
			    setExtent(aW, aH);
			}
		};
	}
	
	private static void initContent(){
		_bodyManager.add(lblText);
	}
	
	protected boolean onSavePrompt() {
		return true;
	}

	private void closeMe(){
		ScreenManager.closeHawkerNotification(this);
	}
	
	protected void onUiEngineAttached(boolean attached) {
		super.onUiEngineAttached(attached);
		
		new ShownThread(5000).start(); // stay for 5 seconds
	}
	
	//THREAD FOR SHOW
	private class ShownThread extends Thread {
		private int _stay;

		public ShownThread(int stay) {
			_stay = stay;
		}

		public void interrupt() {
			super.interrupt();
		}

		public void run() {
			try {
				Thread.sleep(_stay);
			} catch (InterruptedException e) {
				System.out.println("NotificationHawkerScreen : " + e);
			} finally{
				if (this != null && this.isAlive()) {
					closeMe();
					Thread.currentThread().interrupt();
				}
			}
		}
	}
}

/*import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.ImageUtil;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class NotificationHawkerScreen extends PopupScreen{
	private static VerticalFieldManager _bodyManager;
	private static String _text;

	private static Bitmap background, bgshot;
	private int x, y;
	
	private static CustomLabelField lblText;
	
	public NotificationHawkerScreen(VerticalFieldManager manager, int x, int y){
		super(manager, DEFAULT_CLOSE | USE_ALL_WIDTH);
		//transparent bg
		super.setBackground(BackgroundFactory.createSolidTransparentBackground(0xffffff, 0));
		
		this.x = x;
		this.y = y;
	}
	
	public void sublayout(int width, int height) {
		super.sublayout(width, height);
		setPosition(x, y);
	}
	
	public static NotificationHawkerScreen showDetail(String text, int posY){
		ImageUtil.captureCurrentScreen();
		bgshot = Properties._SCREEN_CAPTURE;
		_text = text;
		
		initComponent();
		initSublayout();
		initContent();
				
		//positioning
		int x = 0;
		int y = posY;
		
		NotificationHawkerScreen screen = new NotificationHawkerScreen(_bodyManager, x, y);
		
		return screen;
	}
	
	protected void applyTheme(){
	}
	
	private static void initComponent(){
		background = ModelConfig.getConfig().getHeaderBgRepeat();
		
		lblText = new CustomLabelField(_text, Properties._COLOR_FONT_LABEL_WHITE, FIELD_HCENTER | FOCUSABLE);
		lblText.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, ModelConfig.getConfig().getFontSmallHeight()));
		lblText.setMargin(0, 0, 0, ModelConfig.getConfig().getValue10px());
	}
	
	private static void initSublayout(){
		_bodyManager = new VerticalFieldManager(NO_VERTICAL_SCROLL | NO_VERTICAL_SCROLLBAR){
			protected void paintBackground(Graphics g){	
				g.setGlobalAlpha(185);
				ImageUtil.drawSpriteTexture(background, g, 0, Display.getWidth(), Display.getWidth(), 0, 0, 0, background.getHeight(), background.getHeight());
				g.setGlobalAlpha(255);
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aW = Display.getWidth();
				int aH = background.getHeight();
				
				Field content = getField(0);
			    layoutChild(content, content.getPreferredWidth(), content.getPreferredHeight());
			    
			    if(content.getWidth() > aW-(ModelConfig.getConfig().getValue10px()*2)){
			    	layoutChild(content, aW-(ModelConfig.getConfig().getValue10px()*2), content.getPreferredHeight());
			    	setPositionChild(content, ModelConfig.getConfig().getValue10px(), ModelConfig.getConfig().getValue5px());
			    }
			    else{
			    	setPositionChild(content, (aW-content.getWidth())/2, (aH-content.getHeight())/2);
			    }
			    
			    setExtent(aW, aH);
			}
		};
	}
	
	private static void initContent(){
		_bodyManager.add(lblText);
	}

	private void closeMe(){
		ScreenManager.closeHawkerNotification(this);
	}
	
	protected void onUiEngineAttached(boolean attached) {
		super.onUiEngineAttached(attached);
		
		new ShownThread(5000).start(); // stay for 5 seconds
	}
	
	//THREAD FOR SHOW
	private class ShownThread extends Thread {
		private int _stay;

		public ShownThread(int stay) {
			_stay = stay;
		}

		public void interrupt() {
			super.interrupt();
		}

		public void run() {
			try {
				Thread.sleep(_stay);
			} catch (InterruptedException e) {
				System.out.println("NotificationHawkerScreen : " + e);
			} finally{
				if (this != null && this.isAlive()) {
					closeMe();
					Thread.currentThread().interrupt();
				}
			}
		}
	}
}*/
