package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.UiApp;
import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.manager.ClassManager;
import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.screen.StoreDetailScreen;
import com.mobileforce.bango.services.InvokeBBMaps;
import com.mobileforce.bango.storage.persistable.BodyPersistable;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.ImageScaler;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class PreviewHalf1 extends VerticalFieldManager implements FieldChangeListener{
	private HorizontalFieldManager extraButton;
	private HorizontalFieldManager row1, row2;
	
	private CustomBitmapButton preview;
	private CustomLabelField nama, jarak, alamat;
	private CustomButton3 buttonReview, buttonTunjuk;
	private BigRatingField rating;
	
	private BitmapField separator;
	
	private boolean shown = false;
	private int whitespace = 0;
	
	private BodyPersistable _data;
	
	public PreviewHalf1(BodyPersistable data){
		super(NO_VERTICAL_SCROLL | FIELD_HCENTER);
		
		this._data = data;
		
		initComponent();
		initSubLayout();
	}
	
	private void initComponent(){
		preview = new CustomBitmapButton(ImageScaler.resizeBitmapARGB(ModelConfig.getConfig().getTemporerKuliner(), ModelConfig.getConfig().getPreviewWidth(), ModelConfig.getConfig().getPreviewHeight()));
		preview.setChangeListener(this);
		preview.setBorderFocus(false);
		
		nama = new CustomLabelField(_data.get_title(), Properties._COLOR_FONT_LABEL_BLACK, NON_FOCUSABLE | FIELD_HCENTER | DrawStyle.ELLIPSIS);
		nama.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, ModelConfig.getConfig().getFontMediumHeight()));
		
		jarak = new CustomLabelField(_data.get_distance() + " KM", Properties._COLOR_FONT_LABEL_GREEN, NON_FOCUSABLE | FIELD_VCENTER);
		jarak.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, ModelConfig.getConfig().getFontSmallHeight()));
		
		alamat = new CustomLabelField(_data.get_address(), Properties._COLOR_FONT_LABEL_BLACK, NON_FOCUSABLE | FIELD_HCENTER | DrawStyle.ELLIPSIS);
		alamat.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, ModelConfig.getConfig().getFontSmallHeight()));
		
		rating = new BigRatingField(_data.get_rating(), NON_FOCUSABLE);
		rating.setMaximumRate(5);
		if(ModelConfig.getConfig().isOrientationPortrait()) rating.resizeStarSize(70);
		
		separator = new BitmapField(ImageScaler.resizeBitmapARGB(ModelConfig.getConfig().getSeparatorImage(), getPreferredWidth(), ModelConfig.getConfig().getSeparatorImage().getHeight()), NON_FOCUSABLE);
		
		int btnW = (getPreferredWidth() - (ModelConfig.getConfig().getValue5px()*3)) / 2; 
		
		buttonReview = new CustomButton3(Properties._LBL_REVIEW);
		buttonReview.setFontFamily(DeviceProperties.getDefaultApplicationFont(ModelConfig.getConfig().getFontSmallHeight()));
		buttonReview.setFontColor(Properties._COLOR_FONT_LABEL_WHITE, Properties._COLOR_FONT_LABEL_WHITE);
		buttonReview.setButtonWidthInPixel(btnW);
		buttonReview.setMargin(ModelConfig.getConfig().getValue10px(), ModelConfig.getConfig().getValue5px(), ModelConfig.getConfig().getValue10px(), ModelConfig.getConfig().getValue5px());
		buttonReview.setButtonBackgroundMode(true);
		buttonReview.setChangeListener(this);
		
		buttonTunjuk = new CustomButton3(Properties._LBL_RUTE);
		buttonTunjuk.setFontSize(ModelConfig.getConfig().getFontSuperSmallHeight());
		buttonTunjuk.setFontColor(Properties._COLOR_FONT_LABEL_WHITE, Properties._COLOR_FONT_LABEL_WHITE);
		buttonTunjuk.setButtonWidthInPixel(btnW);
		buttonTunjuk.setMargin(ModelConfig.getConfig().getValue10px(), ModelConfig.getConfig().getValue5px(), ModelConfig.getConfig().getValue10px(), 2);
		buttonTunjuk.setButtonBackgroundMode(true);
		buttonTunjuk.enableMultiRowMode();
		buttonTunjuk.setChangeListener(this);
		
		whitespace = ModelConfig.getConfig().isOrientationPortrait() ? 3 : 2;
	}
	
	private void initSubLayout(){
		extraButton = new HorizontalFieldManager(FIELD_HCENTER);
		row1 = new HorizontalFieldManager(FIELD_HCENTER | FIELD_VCENTER);
		row2 = new HorizontalFieldManager(FIELD_HCENTER){
			protected void paintBackground(Graphics g) {
				g.setColor(Properties._COLOR_BG_DARK_GREEN);
				g.fillRect(0, 0, getWidth(), getHeight());
			}
		};
		
		row1.add(jarak);
		row1.add(rating);
		
		row2.add(buttonReview);
		row2.add(buttonTunjuk);
		
		this.add(preview);
		this.add(separator);
		this.add(nama);
		this.add(row1);
		this.add(alamat);
		this.add(extraButton);
		
		invalidate();
	}
	
	protected void paintBackground(Graphics g) {
		g.setColor(Properties._COLOR_BG_WHITE);
		g.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
	}
	
	protected void sublayout(int width, int height) {
		Field R1 = getField(0);
		layoutChild(R1, R1.getPreferredWidth(), R1.getPreferredHeight());
		
		Field R2 = getField(1);
		layoutChild(R2, R2.getPreferredWidth(), R2.getPreferredHeight());
		
		Field R3 = getField(2);
		layoutChild(R3, getPreferredWidth()-(ModelConfig.getConfig().getValue10px()*2), R3.getPreferredHeight());
		
		Field R4 = getField(3);
		layoutChild(R4, R4.getPreferredWidth(), R4.getPreferredHeight());
		
		Field R5 = getField(4);
		layoutChild(R5, getPreferredWidth()-(ModelConfig.getConfig().getValue10px()*2), R5.getPreferredHeight());
		
		Field R6 = getField(5);
		layoutChild(R6, R6.getPreferredWidth(), R6.getPreferredHeight());
		
		int posY = 0 - R6.getHeight();
		setPositionChild(R1, (getPreferredWidth()-R1.getWidth())/2, posY);
	    
	    posY += R1.getHeight();
	    setPositionChild(R2, (getPreferredWidth()-R2.getWidth())/2, posY);
	    
	    posY += R2.getHeight() + ModelConfig.getConfig().getValue10px();
	    setPositionChild(R3, ModelConfig.getConfig().getValue10px(), posY);
	    
	    posY += R3.getHeight();
	    setPositionChild(R4, (getPreferredWidth()-R4.getWidth())/2, posY);
	    
	    posY += R4.getHeight();
	    setPositionChild(R5, ModelConfig.getConfig().getValue10px(), posY);
	    
	    posY += R5.getHeight() + (ModelConfig.getConfig().getValue10px()*whitespace);
	    setPositionChild(R6, (getPreferredWidth()-R6.getWidth())/2, posY);
	    
	    setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	public int getPreferredWidth() {
		return Display.getWidth()/2;
	}

	public int getPreferredHeight() {
		return preview.getPreferredHeight() 
				+ separator.getBitmapHeight()
				+ nama.getHeight() + ModelConfig.getConfig().getValue10px()
				+ rating.getPreferredHeight()
				+ alamat.getHeight() + (ModelConfig.getConfig().getValue10px()*whitespace)
				;
	}
	
	private UiApp getBangoApp(){
		return ((UiApp)UiApplication.getUiApplication());
	}
	
	public void setPreviewBitmap(Bitmap bitmap){
		synchronized (UiApplication.getUiApplication().getAppEventLock()) {
			this.preview.setBitmap(bitmap);
		}
	}

	public void fieldChanged(Field field, int arg1) {
		if(field == preview){
			shown = !shown;
			
			if(shown){
				extraButton.add(row2);
			}
			else{
				extraButton.deleteAll();
			}
		}
		else if(field == buttonReview){
			StoreDetailScreen screen = ClassManager.getInstance().getScreenManager().getStoreDetailScreen();
			screen.setDetailData(_data);
			ScreenManager.getBangoApp().pushScreen(screen);
		}
		else if(field == buttonTunjuk){
			String lat_1 = Properties._LATITUDE;
			String lon_1 = Properties._LONGITUDE;
			String lat_2 = _data.get_lat();
			String lon_2 = _data.get_lon();
			String title1 = "It is me!";
			String title2 = _data.get_title();
			String desc1 = "";
			String desc2 = _data.get_address();
			
			new InvokeBBMaps(lat_1, lon_1, lat_2, lon_2, title1, title2, desc1, desc2);
		}
	}
}
