package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.UiApp;
import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.manager.ClassManager;
import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.screen.ResepDetailScreen;
import com.mobileforce.bango.storage.persistable.BodyPersistable2;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.ImageScaler;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class PreviewHalf2 extends VerticalFieldManager implements FieldChangeListener{
	private CustomBitmapButton preview;
	private CustomLabelField nama;
	private BigRatingField rating;
	
	private BodyPersistable2 _data;
	
	public PreviewHalf2(BodyPersistable2 data){
		super(NO_VERTICAL_SCROLL | FIELD_HCENTER);
		
		this._data = data;
		
		initComponent();
		initSubLayout();
	}
	
	private void initComponent(){
		preview = new CustomBitmapButton(ImageScaler.resizeBitmapARGB(ModelConfig.getConfig().getTemporerResep(), ModelConfig.getConfig().getPreviewWidth(), ModelConfig.getConfig().getPreviewHeight()));
		preview.setChangeListener(this);
		preview.setBorderFocus(false);
		
		nama = new CustomLabelField(_data.get_title(), Properties._COLOR_FONT_LABEL_BLACK, NON_FOCUSABLE | FIELD_HCENTER | DrawStyle.ELLIPSIS);
		nama.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, ModelConfig.getConfig().getFontMediumHeight()));
		
		rating = new BigRatingField(_data.get_rating(), NON_FOCUSABLE);
		rating.setMaximumRate(5);
		rating.setUnfocusMode();
		if(ModelConfig.getConfig().isOrientationPortrait()) rating.resizeStarSize(80);
	}
	
	private void initSubLayout(){
		this.add(preview);
		this.add(nama);
		this.add(rating);
		
		invalidate();
	}
	
	protected void paintBackground(Graphics g) {
		g.setColor(Properties._COLOR_BG_WHITE);
		g.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
	}
	
	protected void sublayout(int width, int height) {
		Field R1 = getField(0);
		layoutChild(R1, R1.getPreferredWidth(), R1.getPreferredHeight());
		setPositionChild(R1, (getPreferredWidth()-R1.getWidth())/2, 0);
		
		Field R2 = getField(1);
		layoutChild(R2, R2.getPreferredWidth(), R2.getPreferredHeight());
		if(R2.getWidth() > getPreferredWidth()-(ModelConfig.getConfig().getValue10px()*2)){
			layoutChild(R2, getPreferredWidth()-(ModelConfig.getConfig().getValue10px()*2), R2.getPreferredHeight());
			setPositionChild(R2, ModelConfig.getConfig().getValue10px(), R1.getHeight() + ModelConfig.getConfig().getValue10px());
		}
		else{
			layoutChild(R2, R2.getPreferredWidth(), R2.getPreferredHeight());
			setPositionChild(R2, (getPreferredWidth()-R2.getWidth())/2, R1.getHeight() + ModelConfig.getConfig().getValue10px());
		}
		
		Field R3 = getField(2);
		layoutChild(R3, R3.getPreferredWidth(), R3.getPreferredHeight());
		setPositionChild(R3, (getPreferredWidth()-R3.getWidth())/2, getPreferredHeight() - (R3.getHeight() + ModelConfig.getConfig().getValue10px()));
		
	    setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	public int getPreferredWidth() {
		return Display.getWidth()/2;
	}

	public int getPreferredHeight() {
		return preview.getPreferredHeight() + ModelConfig.getConfig().getValue10px()
				+ nama.getHeight() + ModelConfig.getConfig().getValue10px()
				+ rating.getPreferredHeight() + ModelConfig.getConfig().getValue10px()
				;
	}
	
	private UiApp getBangoApp(){
		return ((UiApp)UiApplication.getUiApplication());
	}
	
	public void setPreviewBitmap(Bitmap bitmap){
		synchronized (UiApplication.getUiApplication().getAppEventLock()) {
			this.preview.setBitmap(bitmap);
		}
	}

	public void fieldChanged(Field field, int arg1) {
		if(field == preview){
			ResepDetailScreen screen = ClassManager.getInstance().getScreenManager().getResepDetailScreen();
			screen.setDetailData(_data);
			ScreenManager.getBangoApp().pushScreen(screen);
		}
	}
}
