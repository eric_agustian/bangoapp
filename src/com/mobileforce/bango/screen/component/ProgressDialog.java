package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.configuration.ModelConfig;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class ProgressDialog extends PopupScreen {
	private boolean closeable = true;
	private LabelField lblMessage = null;
	private ProgressAnimationField animField = null;
	private int countToForceClose = 0;

	public ProgressDialog(String text) {

		super(new VerticalFieldManager(VerticalFieldManager.VERTICAL_SCROLL
				| VerticalFieldManager.VERTICAL_SCROLLBAR));

		try {
			lblMessage = new LabelField(text, Field.FIELD_HCENTER);
			this.add(lblMessage);
		} catch (final Exception e) {
			UiApplication.getUiApplication().invokeAndWait(new Runnable() {
				
				public void run() {
					Dialog.alert("Error "+e.toString());
				}
			});
		}
	}
	
	public ProgressDialog(String text, Bitmap[] animation) {

		super(new VerticalFieldManager(VerticalFieldManager.VERTICAL_SCROLL
				| VerticalFieldManager.VERTICAL_SCROLLBAR));

		try {
			lblMessage = new LabelField(text, Field.FIELD_HCENTER);
			this.add(lblMessage);
			
			animField =  new ProgressAnimationField(animation, animation.length, FIELD_HCENTER);
			animField.setMargin(ModelConfig.getConfig().getValue10px(), 0, 0, 0);
			this.add(animField);
		} catch (final Exception e) {
			UiApplication.getUiApplication().invokeAndWait(new Runnable() {
				
				public void run() {
					Dialog.alert("Error "+e.toString());
				}
			});
		}
	}
	
	public ProgressDialog(String text, Bitmap[] animation, boolean closeable) {
		this(text, animation);
		this.closeable = closeable;
	}

	protected boolean keyChar(char c, int status, int time) {
		if (c == Characters.ESCAPE) {
			countToForceClose++;
		}
		
		if(closeable || countToForceClose == 5){
			if (c == Characters.ESCAPE) {
				countToForceClose = 0;
				close();
			}
			return super.keyChar(c, status, time);
		}
		else{
			return true;
		}
	}

}
