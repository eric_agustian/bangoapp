package com.mobileforce.bango.screen.component;

import java.util.Vector;

import com.mobileforce.bango.UiApp;
import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.manager.ClassManager;
import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.screen.OthersProfilScreen;
import com.mobileforce.bango.services.DataLoader;
import com.mobileforce.bango.services.FacebookConnector;
import com.mobileforce.bango.services.ImageFetching;
import com.mobileforce.bango.services.ShareCreator;
import com.mobileforce.bango.services.TwitterConnector;
import com.mobileforce.bango.services.UserProfileManager;
import com.mobileforce.bango.services.callback.ButtonPressedCallback;
import com.mobileforce.bango.services.callback.DataLoaderCallback;
import com.mobileforce.bango.services.callback.OtherScreenCallback;
import com.mobileforce.bango.storage.persistable.BodyPersistable4;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.GStringUtil;
import com.mobileforce.bango.utility.ImageUtil;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.system.PNGEncodedImage;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class ReviewListField extends VerticalFieldManager implements FieldChangeListener, OtherScreenCallback, DataLoaderCallback{
	private HorizontalFieldManager userM, user1, detail1, imageM;
	private VerticalFieldManager detailM, v1;
	private Bitmap commentBase;
	
	private SmallRatingField starRating;
	private CustomBitmapButton avatarUser, btnLike, btnShare, btnLapor, btnDelete, imageShare;
	private CustomLabelField lblUser, lblLike, lblShare, lblLapor, lblDetail;
	
	private BodyPersistable4 data;
	
	private ButtonPressedCallback callback;
	private String masterTitle;
	private Bitmap masterImage;
	
	public ReviewListField(BodyPersistable4 data){
		super(NO_HORIZONTAL_SCROLL | NO_VERTICAL_SCROLL | FIELD_HCENTER);
		
		this.data = data;
		
		initComponent();
		initSubLayout();
	}
	
	private void initComponent(){
		commentBase = ModelConfig.getConfig().getIconCommentBase();
		
		lblUser = new CustomLabelField(data.get_fullname(), Properties._COLOR_FONT_LABEL_GREEN, NON_FOCUSABLE);
		lblUser.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, ModelConfig.getConfig().getFontMediumHeight()));
		lblUser.setMargin(0, 0, 0, ModelConfig.getConfig().getValue10px());
		
		lblLike = new CustomLabelField(data.get_numlike(), Properties._COLOR_FONT_LABEL_BLACK, NON_FOCUSABLE);
		lblLike.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, ModelConfig.getConfig().getFontSmallHeight()));
		lblLike.setMargin(0, 0, 0, ModelConfig.getConfig().getValue5px());
		
		lblShare = new CustomLabelField(Properties._LBL_SHARING, Properties._COLOR_FONT_LABEL_BLACK, NON_FOCUSABLE);
		lblShare.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, ModelConfig.getConfig().getFontSmallHeight()));
		lblShare.setMargin(0, 0, 0, ModelConfig.getConfig().getValue5px());
		
		lblLapor = new CustomLabelField(Properties._LBL_LAPOR, Properties._COLOR_FONT_LABEL_BLACK, NON_FOCUSABLE);
		lblLapor.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD, ModelConfig.getConfig().getFontSmallHeight()));
		lblLapor.setMargin(0, 0, 0, ModelConfig.getConfig().getValue5px());
		
		lblDetail = new CustomLabelField(data.get_comment(), FOCUSABLE, Properties._COLOR_FONT_LABEL_BLACK, Properties._COLOR_FONT_LABEL_BLACK);
		lblDetail.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, ModelConfig.getConfig().getFontMediumHeight()));
		
		avatarUser = new CustomBitmapButton(ImageUtil.getMaskedCircledIcon(ModelConfig.getConfig().getIconMasking(), ModelConfig.getConfig().getTemporerKuliner(), ModelConfig.getConfig().getIconMasking().getWidth(), ModelConfig.getConfig().getIconMasking().getHeight()), FOCUSABLE);
		avatarUser.setChangeListener(this);
		
		btnLike = new CustomBitmapButton(ModelConfig.getConfig().getIconCommentLove());
		btnLike.setMargin(0, 0, 0, ModelConfig.getConfig().getValue10px());
		btnLike.setChangeListener(this);
		
		btnShare = new CustomBitmapButton(ModelConfig.getConfig().getIconCommentShare());
		btnShare.setMargin(0, 0, 0, ModelConfig.getConfig().getValue10px());
		btnShare.setChangeListener(this);
		
		btnLapor = new CustomBitmapButton(ModelConfig.getConfig().getIconCommentLapor());
		btnLapor.setMargin(0, 0, 0, ModelConfig.getConfig().getValue10px());
		btnLapor.setChangeListener(this);
		
		btnDelete = new CustomBitmapButton(ModelConfig.getConfig().getIconCloseRed());
		btnDelete.setMargin(0, 0, 0, ModelConfig.getConfig().getValue10px());
		btnDelete.setChangeListener(this);
		
		imageShare = new CustomBitmapButton(ModelConfig.getConfig().getIconBlank(), FOCUSABLE | FIELD_RIGHT);
		imageShare.setChangeListener(this);
		
		starRating = new SmallRatingField(data.get_rating(), NON_FOCUSABLE);
		starRating.setMaximumRate(5);
		starRating.setMargin(0, 0, 0, ModelConfig.getConfig().getValue10px());
	}
	
	private void initSubLayout(){
		userM = new HorizontalFieldManager(FIELD_VCENTER);
		v1 = new VerticalFieldManager(FIELD_LEFT);
		
		user1 = new HorizontalFieldManager(FIELD_LEFT);
		user1.add(starRating);
		user1.add(btnLike);
		user1.add(lblLike);
		user1.add(btnShare);
		user1.add(lblShare);
		user1.add(btnLapor);
		user1.add(lblLapor);
		
		v1.add(lblUser);
		v1.add(user1);
		
		userM.add(avatarUser);
		userM.add(v1);
		
		if(data.get_is_mine().equalsIgnoreCase("1")){
			detailM = new VerticalFieldManager(FOCUSABLE){
				protected void paintBackground(Graphics g) {
					// draw top
					g.drawBitmap(0, 0, commentBase.getWidth(), commentBase.getHeight(), commentBase, 0, 0);
					
					// draw rounded
					g.setColor(0xFFFFFF);
					g.fillRoundRect(0, commentBase.getHeight(), getWidth()-(ModelConfig.getConfig().getValue10px()*2), getHeight()-(commentBase.getHeight()), ModelConfig.getConfig().getValue10px()*2, ModelConfig.getConfig().getValue10px()*2);
				}
				
				protected void sublayout(int maxWidth, int maxHeight){
					int aW = Display.getWidth();
					int aH = ModelConfig.getConfig().getValue10px();
				    
				    Field c2 = getField(0);
				    layoutChild(c2, c2.getPreferredWidth(), c2.getPreferredHeight());
				    setPositionChild(c2, ModelConfig.getConfig().getValue10px(), 0);
				    
				    aH += c2.getHeight() + (ModelConfig.getConfig().getValue10px()-commentBase.getHeight());
					
				    setExtent(aW, aH);
				}
			};
			

			detail1 = new HorizontalFieldManager(FIELD_TOP){
				protected void sublayout(int maxWidth, int maxHeight){
					int aW = Display.getWidth()-(ModelConfig.getConfig().getValue10px()*2);
					int aH = ModelConfig.getConfig().getValue10px()+commentBase.getHeight();
					
					Field c1 = getField(0);
					Field c2 = getField(1);
					
					Field c3 = getField(2);
					layoutChild(c3, c3.getPreferredWidth(), c3.getPreferredHeight());
					setPositionChild(c3, aW-c3.getWidth(), 0);
					
				    layoutChild(c2, c2.getPreferredWidth(), c2.getPreferredHeight());
				    layoutChild(c1, aW-(c2.getWidth()+(ModelConfig.getConfig().getValue10px()*4)), c1.getPreferredHeight());
				    
				    setPositionChild(c1, 0, aH);
				    setPositionChild(c2, aW-(c2.getWidth()+(ModelConfig.getConfig().getValue10px()*2)), aH);
				    
				    aH += c1.getHeight() > c2.getHeight() ? c1.getHeight() : c2.getHeight();
					
				    setExtent(aW, aH);
				}
			};
		}
		else{
			detailM = new VerticalFieldManager(FOCUSABLE){
				protected void paintBackground(Graphics g) {
					// draw top
					g.drawBitmap(0, 0, commentBase.getWidth(), commentBase.getHeight(), commentBase, 0, 0);
					
					// draw rounded
					g.setColor(0xFFFFFF);
					g.fillRoundRect(0, commentBase.getHeight(), getWidth(), getHeight()-(commentBase.getHeight()), ModelConfig.getConfig().getValue10px()*2, ModelConfig.getConfig().getValue10px()*2);
				}
				
				protected void sublayout(int maxWidth, int maxHeight){
					int aW = Display.getWidth()-(ModelConfig.getConfig().getValue10px()*2);
					int aH = ModelConfig.getConfig().getValue10px();
				    
				    Field c = getField(0);
				    layoutChild(c, aW-(ModelConfig.getConfig().getValue10px()*2), c.getPreferredHeight());
				    setPositionChild(c, ModelConfig.getConfig().getValue10px(), aH);
				    
				    aH += c.getHeight() + (ModelConfig.getConfig().getValue10px()-commentBase.getHeight());
					
				    setExtent(aW, aH);
				}
			};
			

			detail1 = new HorizontalFieldManager(FIELD_TOP){
				protected void sublayout(int maxWidth, int maxHeight){
					int aW = Display.getWidth()-(ModelConfig.getConfig().getValue10px()*4);
					int aH = 0;
					
					Field c1 = getField(0);
					Field c2 = getField(1);
					
				    layoutChild(c2, c2.getPreferredWidth(), c2.getPreferredHeight());
				    layoutChild(c1, aW-(c2.getWidth()+(ModelConfig.getConfig().getValue10px()*2)), c1.getPreferredHeight());
				    
				    setPositionChild(c1, 0, 0);
				    setPositionChild(c2, aW-c2.getWidth(), 0);
				    
				    aH += c1.getHeight() > c2.getHeight() ? c1.getHeight() : c2.getHeight();
					
				    setExtent(aW, aH);
				}
			};
		}
		
		
		
		imageM = new HorizontalFieldManager(FOCUSABLE);
		detail1.add(lblDetail);
		detail1.add(imageM);
		
		if(data.get_is_mine().equalsIgnoreCase("1")){
			detail1.add(btnDelete);
		}
		
		if(GStringUtil.checkHTTP(data.get_urlimg())){
			imageM.add(imageShare);
		}
		
		detailM.add(detail1);
		
		this.add(userM);
		this.add(detailM);
		
		invalidate();
	}
	
	protected void sublayout(int width, int height) {
		Field USER = getField(0);
		layoutChild(USER, USER.getPreferredWidth(), USER.getPreferredHeight());
	    setPositionChild(USER, ModelConfig.getConfig().getValue10px(), ModelConfig.getConfig().getValue10px());
	    
	    Field DETAIL = getField(1);
		layoutChild(DETAIL, DETAIL.getPreferredWidth(), DETAIL.getPreferredHeight());
	    setPositionChild(DETAIL, ModelConfig.getConfig().getValue10px(), ModelConfig.getConfig().getValue10px() + USER.getHeight());
	    
	    int h = USER.getHeight() + DETAIL.getHeight() + ModelConfig.getConfig().getValue10px();
	    
	    setExtent(Display.getWidth(), h);
	}
	
	private UiApp getBangoApp(){
		return ((UiApp)UiApplication.getUiApplication());
	}

	public void fieldChanged(Field field, int arg1) {
		if(field == avatarUser){
			if(data.get_is_mine().equalsIgnoreCase("1")){
				ScreenManager.getBangoApp().pushScreen(ClassManager.getInstance().getScreenManager().getProfilScreen());
			}
			else{
				String userid = data.get_userid();
				OthersProfilScreen screen = ClassManager.getInstance().getScreenManager().getOthersProfilScreen();
				screen.getUserProfile(userid);
				
				ScreenManager.getBangoApp().pushScreen(screen);
			}
		}
		else if(field == btnLike){
			String param = "commentid=" + data.get_commentid();
			new DataLoader(Properties._API_LIKE_REVIEW, param, Properties._HTTP_METHOD_POST, Properties._CONN_TYPE_LIKE_REVIEW, false, true, this);
		}
		else if(field == btnShare){
			ScreenManager.showShareSelectionDialog(new ShareSelectionDialog(this, ""));
		}
		else if(field == btnLapor){
			final ReviewListField screen = this;
			Runnable runnable = new Runnable() {
	    		public void run() {
	    			try {
	    				if(Dialog.ask(Dialog.D_OK_CANCEL, Properties._INF_LAPORKAN_REVIEW + "\"" + data.get_fullname() +"\"") == Dialog.OK){  
	    					String param = "commentid=" + data.get_commentid();
	    					new DataLoader(Properties._API_REPORT_REVIEW, param, Properties._HTTP_METHOD_POST, Properties._CONN_TYPE_REPORT_REVIEW, false, true, screen);
						}
	    			} catch (Exception e) {}	
	    		}
			};
			getBangoApp().invokeLater(runnable);
		}
		else if(field == btnDelete){
			final ReviewListField screen = this;
			Runnable runnable = new Runnable() {
	    		public void run() {
	    			try {
	    				if(Dialog.ask(Dialog.D_OK_CANCEL, Properties._INF_HAPUS_REVIEW) == Dialog.OK){  
	    					String param = "commentid=" + data.get_commentid();
	    					new DataLoader(Properties._API_DELETE_REVIEW, param, Properties._HTTP_METHOD_POST, Properties._CONN_TYPE_DELETE_REVIEW, false, true, screen);
						}
	    			} catch (Exception e) {}	
	    		}
			};
			getBangoApp().invokeLater(runnable);
		}
		else if(field == imageShare){
			ScreenManager.showImageZoomPopup(ImageZoomPopupScreen.showImage(data.get_urlimg()));
		}
	}
	
	public void setIcon(Bitmap icon){
		synchronized (UiApplication.getUiApplication().getAppEventLock()) {
			this.avatarUser.setBitmap(ImageUtil.getMaskedCircledIcon(ModelConfig.getConfig().getIconMasking(), icon, ModelConfig.getConfig().getIconMasking().getWidth(), ModelConfig.getConfig().getIconMasking().getHeight()));
		}
	}
	
	public void setImageShare(Bitmap bitmap){
		synchronized (UiApplication.getUiApplication().getAppEventLock()) {
			this.imageShare.setBitmap(bitmap);
			super.updateLayout();
			invalidate();
		}
	}

	public void onScreenCallback(String result, String type) {
		// TODO Auto-generated method stub
		if(type.equalsIgnoreCase(Properties._LBL_SHARE_FACEBOOK)){
			// share to facebook
			String text_1 = UserProfileManager.getUserProfile() == null ? "Anda" : UserProfileManager.getUserProfile().get_fullname();
			text_1 += " telah me-review";
			String text_2 = "oleh " + data.get_fullname();
			String title = masterTitle;
			int rating = data.get_rating();
			
			Bitmap share = ShareCreator.getShareImage(masterImage, text_1, title, text_2, rating);
			PNGEncodedImage encoded = PNGEncodedImage.encode(share);
			
			new FacebookConnector(Properties._CONN_TYPE_SHARE_FACEBOOK, Properties._LBL_SHARE_TEXT_FACEBOOK, encoded);
		}
		else if(type.equalsIgnoreCase(Properties._LBL_SHARE_TWITTER)){
			// share to twitter
			String text_1 = UserProfileManager.getUserProfile() == null ? "Anda" : UserProfileManager.getUserProfile().get_fullname();
			text_1 += " telah me-review";
			String text_2 = "oleh " + data.get_fullname();
			String title = masterTitle;
			int rating = data.get_rating();
			
			Bitmap share = ShareCreator.getShareImage(masterImage, text_1, title, text_2, rating);
			
			ScreenManager.getBangoApp().pushScreen(new TwitterConnector(Properties._CONN_TYPE_SHARE_TWITTER, Properties._LBL_SHARE_TEXT_TWITTER, share));
		}
	}
	
	public void setButtonPressedCallback(ButtonPressedCallback callback){
		this.callback = callback;
	}
	
	public void setMasterTitle(String title){
		this.masterTitle = title;
	}
	
	public void setMasterImage(String image){
		String[] imgUrl = { image };
		new ImageFetching(this, imgUrl, Properties._CONN_TYPE_REVIEW_SHARE_IMAGE);
	}
	
	public void setMasterImage(Bitmap bitmap){
		synchronized (UiApplication.getUiApplication().getAppEventLock()) {
			masterImage = bitmap;
		}
	}

	public void onRequestCompleted(Vector vector, String type) {
//		if(type.indexOf(Properties._CONN_TYPE_LIKE_REVIEW) != -1){
//			ScreenManager.showDialogTypeScreen(type.substring(0, type.indexOf(Properties._CONN_TYPE_LIKE_REVIEW)));
//		}
		
		// update previous screen
		if(callback != null){
			if(type.indexOf(Properties._CONN_TYPE_LIKE_REVIEW) != -1){
				this.callback.onPressed(type);
				
				Vector temporer = new Vector();
				temporer.addElement(data.get_fullname());
				temporer.addElement(Integer.toString(data.get_rating()));
				
				this.callback.onPressedValue(temporer, Properties._CONN_TYPE_LIKE_REVIEW);
			}
			else{
				ScreenManager.showDialogTypeScreen(type);
				this.callback.onPressed(Properties._CONN_TYPE_REFRESH);
			}
		}
	}

	public void onScreenCallbackValue(Vector value, String type) {
		// TODO Auto-generated method stub
	}
}
