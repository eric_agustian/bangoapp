package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.services.callback.OtherScreenCallback;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.ImageUtil;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;


public class SharePopupScreen extends MainScreen{
	private static VerticalFieldManager _bodyManager;
	private static HorizontalFieldManager _textManager, _hManager;
	private static String _text;
	private static OtherScreenCallback _callback;
	private static String _resultType;

	private static Bitmap background;
	private static int x, y;
	
	private static CustomLabelField lblText;
	private static CustomBitmapButton btnShare;
	
	public SharePopupScreen(VerticalFieldManager manager){
		super(NO_VERTICAL_SCROLL | NO_HORIZONTAL_SCROLL);
		//transparent bg
		super.setBackground(BackgroundFactory.createSolidTransparentBackground(0xffffff, 0));
		
		add(manager);
		
		btnShare.setFocus();
	}
	
	public static SharePopupScreen showDetail(String text, int posY, OtherScreenCallback callback, String resultType){
		_text = text;
		_callback = callback;
		_resultType = resultType;
		
		initComponent();
		initSublayout();
		initContent();
				
		//positioning
		x = 0;
		y = posY;
		
		SharePopupScreen screen = new SharePopupScreen(_bodyManager);
		
		return screen;
	}
	

    protected void sublayout(int width, int height) {
        setExtent(Display.getWidth(), background.getHeight());
        setPosition(x, y);
        layoutDelegate(Display.getWidth(), background.getHeight());
    }
    
//    protected void paintBackground( Graphics g ) {
//    	XYRect myExtent = getExtent();
//        int color = g.getColor();
//        int alpha = g.getGlobalAlpha();
//        g.setColor( 0xE3E3E3 );
//        g.fillRect( 0, 0, myExtent.width, myExtent.height );
//        g.setColor( color );
//        g.setGlobalAlpha( alpha );
//    }
    
    protected boolean keyDown( int keycode, int status ) {
        if ( Keypad.key( keycode ) == Keypad.KEY_ESCAPE ) {
        	closeMe();
            return true;
        }
        return super.keyDown( keycode, status );
    }
    
    private static void initComponent(){
		background = ModelConfig.getConfig().getHeaderBgRepeat();
		
		lblText = new CustomLabelField(_text, Properties._COLOR_FONT_LABEL_WHITE, FIELD_HCENTER | FOCUSABLE);
		lblText.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, ModelConfig.getConfig().getFontSmallHeight()));
		lblText.setMargin(0, 0, 0, ModelConfig.getConfig().getValue10px());

		btnShare = new CustomBitmapButton(ModelConfig.getConfig().getButtonShare());
		btnShare.setChangeListener(new FieldChangeListener() {
			public void fieldChanged(Field field, int context) {
				// TODO Auto-generated method stub
				_callback.onScreenCallback(_resultType, Properties._CONN_TYPE_SHARE_REVIEW);
			}
		});
		btnShare.setMargin(0, ModelConfig.getConfig().getValue10px(), 0, ModelConfig.getConfig().getValue10px());
	}
	
	private static void initSublayout(){
		_bodyManager = new VerticalFieldManager(NO_VERTICAL_SCROLL | NO_VERTICAL_SCROLLBAR | USE_ALL_WIDTH){
			protected void paintBackground(Graphics g){	
				g.setGlobalAlpha(185);
				ImageUtil.drawSpriteTexture(background, g, 0, Display.getWidth(), Display.getWidth(), 0, 0, 0, background.getHeight(), background.getHeight());
				g.setGlobalAlpha(255);
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aW = Display.getWidth();
				int aH = background.getHeight();
				
				Field content = getField(0);
			    layoutChild(content, content.getPreferredWidth(), content.getPreferredHeight());
			    setPositionChild(content, (aW-content.getWidth())/2, (aH-content.getHeight())/2);
			    
			    setExtent(aW, aH);
			}
		};
		
		_textManager = new HorizontalFieldManager(FIELD_HCENTER | FIELD_VCENTER);
		
		_hManager = new HorizontalFieldManager(FIELD_HCENTER | FIELD_VCENTER | USE_ALL_WIDTH);
	}
	
	private static void initContent(){
		_textManager.add(lblText);
		
		_hManager.add(_textManager);
		_hManager.add(btnShare);
		
		_bodyManager.add(_hManager);
	}
	
	protected boolean onSavePrompt() {
		return true;
	}
    
    private void closeMe(){
		ScreenManager.closeSharePopup(this);
	}
	
	protected void onUiEngineAttached(boolean attached) {
		super.onUiEngineAttached(attached);
		
		new ShownThread(5000).start(); // stay for 5 seconds
	}
	
	//THREAD FOR SHOW
	private class ShownThread extends Thread {
		private int _stay;

		public ShownThread(int stay) {
			_stay = stay;
		}

		public void interrupt() {
			super.interrupt();
		}

		public void run() {
			try {
				Thread.sleep(_stay);
			} catch (InterruptedException e) {
				System.out.println("SharePopupScreen : " + e);
			} finally{
				if (this != null && this.isAlive()) {
					closeMe();
					Thread.currentThread().interrupt();
				}
			}
		}
	}
}









/*import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.services.callback.OtherScreenCallback;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.ImageUtil;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.XYEdges;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.decor.Border;
import net.rim.device.api.ui.decor.BorderFactory;

public class SharePopupScreen extends PopupScreen{
	private static VerticalFieldManager _bodyManager;
	private static HorizontalFieldManager _textManager, _hManager;
	private static String _text;
	private static OtherScreenCallback _callback;
	private static String _resultType;

	private static Bitmap background;
	private int x, y;
	
	private static CustomLabelField lblText;
	private static CustomBitmapButton btnShare;
	
	public SharePopupScreen(VerticalFieldManager manager, int x, int y){
		super(manager, DEFAULT_CLOSE | USE_ALL_WIDTH);
		//transparent bg
		super.setBackground(BackgroundFactory.createSolidTransparentBackground(0xffffff, 0));
		
		this.x = x;
		this.y = y;
		
		btnShare.setFocus();
		setBorder(BorderFactory.createSimpleBorder(new XYEdges(), Border.STYLE_TRANSPARENT));
	}
	
	public void sublayout(int width, int height) {
		super.sublayout(width, height);
		setExtent(width, height);
		setPosition(x, y);
	}
	
	public static SharePopupScreen showDetail(String text, int posY, OtherScreenCallback callback, String resultType){
		_text = text;
		_callback = callback;
		_resultType = resultType;
		
		initComponent();
		initSublayout();
		initContent();
				
		//positioning
		int x = 0;
		int y = posY;
		
		SharePopupScreen screen = new SharePopupScreen(_bodyManager, x, y);
		
		return screen;
	}
	
	protected void applyTheme(){
	}
	
	private static void initComponent(){
		background = ModelConfig.getConfig().getHeaderBgRepeat();
		
		lblText = new CustomLabelField(_text, Properties._COLOR_FONT_LABEL_WHITE, FIELD_HCENTER | FOCUSABLE);
		lblText.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN, ModelConfig.getConfig().getFontSmallHeight()));
		lblText.setMargin(0, 0, 0, ModelConfig.getConfig().getValue10px());

		btnShare = new CustomBitmapButton(ModelConfig.getConfig().getButtonShare());
		btnShare.setChangeListener(new FieldChangeListener() {
			public void fieldChanged(Field field, int context) {
				// TODO Auto-generated method stub
				_callback.onScreenCallback(_resultType, Properties._CONN_TYPE_SHARE_REVIEW);
			}
		});
		btnShare.setMargin(0, ModelConfig.getConfig().getValue10px(), 0, ModelConfig.getConfig().getValue10px());
	}
	
	private static void initSublayout(){
		_bodyManager = new VerticalFieldManager(NO_VERTICAL_SCROLL | NO_VERTICAL_SCROLLBAR | USE_ALL_WIDTH){
			protected void paintBackground(Graphics g){	
				g.setGlobalAlpha(185);
				ImageUtil.drawSpriteTexture(background, g, 0, Display.getWidth(), Display.getWidth(), 0, 0, 0, background.getHeight(), background.getHeight());
				g.drawBitmap(0, 0, background.getWidth(), background.getHeight(), background, 0, 0);
				g.setGlobalAlpha(255);
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aW = Display.getWidth();
				int aH = background.getHeight();
				
				Field content = getField(0);
			    layoutChild(content, content.getPreferredWidth(), content.getPreferredHeight());
			    setPositionChild(content, (aW-content.getWidth())/2, (aH-content.getHeight())/2);
			    
			    setExtent(aW, aH);
			}
		};
		
		_textManager = new HorizontalFieldManager(FIELD_HCENTER | FIELD_VCENTER);
		
		_hManager = new HorizontalFieldManager(FIELD_HCENTER | FIELD_VCENTER | USE_ALL_WIDTH);
	}
	
	private static void initContent(){
		_textManager.add(lblText);
		
		_hManager.add(_textManager);
		_hManager.add(btnShare);
		
		_bodyManager.add(_hManager);
	}

	private void closeMe(){
		ScreenManager.closeSharePopup(this);
	}
	
	protected void onUiEngineAttached(boolean attached) {
		super.onUiEngineAttached(attached);
		
		new ShownThread(5000).start(); // stay for 5 seconds
	}
	
	//THREAD FOR SHOW
	private class ShownThread extends Thread {
		private int _stay;

		public ShownThread(int stay) {
			_stay = stay;
		}

		public void interrupt() {
			super.interrupt();
		}

		public void run() {
			try {
				Thread.sleep(_stay);
			} catch (InterruptedException e) {
				System.out.println("SharePopupScreen : " + e);
			} finally{
				if (this != null && this.isAlive()) {
					closeMe();
					Thread.currentThread().interrupt();
				}
			}
		}
	}
}*/
