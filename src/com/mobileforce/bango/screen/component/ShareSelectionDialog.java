package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.services.callback.OtherScreenCallback;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.component.SeparatorField;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class ShareSelectionDialog extends PopupScreen implements FieldChangeListener{
	private CustomLabelField title, shareFB, shareTW;
	
	private OtherScreenCallback callback;
	private String resultType;

	public ShareSelectionDialog(OtherScreenCallback callback, String resultType) {
		super(new VerticalFieldManager(VerticalFieldManager.VERTICAL_SCROLL
				| VerticalFieldManager.VERTICAL_SCROLLBAR), DEFAULT_CLOSE);

		this.callback = callback;
		this.resultType = resultType;
		
		title = new CustomLabelField(Properties._LBL_SHARE_REVIEW,
				Properties._COLOR_FONT_LABEL_WHITE, FIELD_HCENTER
						| NON_FOCUSABLE);
		title.setFont(DeviceProperties.getDefaultApplicationFont(Font.BOLD,
				ModelConfig.getConfig().getFontLargeHeight()));
		title.setMargin(ModelConfig.getConfig().getValue5px(), 0, ModelConfig
				.getConfig().getValue5px(), 0);

		shareFB = new CustomLabelField(Properties._LBL_SHARE_FACEBOOK,
				FIELD_LEFT | FOCUSABLE, Properties._COLOR_FONT_LABEL_GREEN,
				Properties._COLOR_FONT_LABEL_WHITE);
		shareFB.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN,
				ModelConfig.getConfig().getFontLargeHeight()));
		shareFB.setMargin(ModelConfig.getConfig().getValue10px(), ModelConfig
				.getConfig().getValue10px(), ModelConfig.getConfig()
				.getValue10px(), ModelConfig.getConfig().getValue10px());
		shareFB.setChangeListener(this);

		shareTW = new CustomLabelField(Properties._LBL_SHARE_TWITTER,
				FIELD_LEFT | FOCUSABLE, Properties._COLOR_FONT_LABEL_GREEN,
				Properties._COLOR_FONT_LABEL_WHITE);
		shareTW.setFont(DeviceProperties.getDefaultApplicationFont(Font.PLAIN,
				ModelConfig.getConfig().getFontLargeHeight()));
		shareTW.setMargin(ModelConfig.getConfig().getValue10px(), ModelConfig
				.getConfig().getValue10px(), ModelConfig.getConfig()
				.getValue10px(), ModelConfig.getConfig().getValue10px());
		shareTW.setChangeListener(this);

		this.add(title);
		this.add(new SeparatorField());
		this.add(shareFB);
		this.add(shareTW);
	}

	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		if(field == shareFB){
			this.callback.onScreenCallback(resultType, Properties._LBL_SHARE_FACEBOOK);
			ScreenManager.closeShareSelectionDialog(this);
		}
		else if(field == shareTW){
			this.callback.onScreenCallback(resultType, Properties._LBL_SHARE_TWITTER);
			ScreenManager.closeShareSelectionDialog(this);
		}
	}
}
