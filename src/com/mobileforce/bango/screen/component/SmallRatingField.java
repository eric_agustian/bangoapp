package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.configuration.ModelConfig;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;

public class SmallRatingField extends BaseButtonField {
	private Bitmap _bitmap;
	private int rate = 1;
	private int maximumRate = 4;
	
	private boolean isfocus;
	
	public SmallRatingField(int rate){
		this(rate, Field.FOCUSABLE);
	}
	
	public SmallRatingField(int rate, long style){
		super(style);
		
		this.rate = rate;
		
		prepareElement();
	}
	
	public int getPreferredWidth() {
		return _bitmap.getWidth() * maximumRate;
	}
	
	public int getPreferredHeight() {
		return _bitmap.getHeight();
	}
	
	public void prepareElement(){
		_bitmap = ModelConfig.getConfig().getIconSmallRating();
	}
	
	protected void layout(int width, int height){
		super.setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	//Invoked when this field receives the focus.
	protected void onFocus(int direction){
    	isfocus = true;
        super.onFocus(direction);
    }

    //Invoked when a field loses the focus.
    protected void onUnfocus(){
        isfocus = false;
        super.onUnfocus();
    }
    
	protected void paint(Graphics g){
		if(isfocus){
			g.setGlobalAlpha(155);
		}
		else{
			g.setGlobalAlpha(255);
		}
		
		// draw bitmap
		for(int n = 0; n < maximumRate; n++){
			g.setGlobalAlpha(25);
			g.drawBitmap(n * _bitmap.getWidth(), (getPreferredHeight()-_bitmap.getHeight())/2, _bitmap.getWidth(), _bitmap.getHeight(), _bitmap, 0, 0);
			g.setGlobalAlpha(255);
		}
		
		for(int n = 0; n < rate; n++){
			g.drawBitmap(n * _bitmap.getWidth(), (getPreferredHeight()-_bitmap.getHeight())/2, _bitmap.getWidth(), _bitmap.getHeight(), _bitmap, 0, 0);
		}
	}
	
	public void setMaximumRate(int rate){
		this.maximumRate = rate;
		this.updateLayout();
	}
	
	public void setCurrentRate(int rate){
		this.rate = rate;
		invalidate();
	}
}
