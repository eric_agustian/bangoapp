package com.mobileforce.bango.screen.component;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.services.SplashPromoManager;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class SplashPromoPopupScreen extends PopupScreen{
	private static SplashPromoPopupScreen _self;
	private static VerticalFieldManager _bodyManager;
	
	private static Bitmap promo;

	private static CustomBitmapButton btnClose;
	private static CustomCheckBoxField checkBox;
	
	private static boolean isCheck = false;
	
	public SplashPromoPopupScreen(VerticalFieldManager manager){
		super(manager, DEFAULT_CLOSE);
		//transparent bg
		super.setBackground(BackgroundFactory.createSolidTransparentBackground(0x000000, 125));
	}
	
	public void sublayout(int width, int height) {
		super.sublayout(width, height);
		setPosition(0, 0);
	}
	
	public static SplashPromoPopupScreen showImage(Bitmap promo){
		initComponent(promo);
		initSublayout();
		initContent();
		
		SplashPromoPopupScreen screen = new SplashPromoPopupScreen(_bodyManager);
		_self = screen;
		
		return screen;
	}
	
	protected void applyTheme(){
	}
	
	private static void initComponent(Bitmap bmp){
		promo = bmp;
		
		btnClose = new CustomBitmapButton(ModelConfig.getConfig().getIconCloseGreen());
		btnClose.setChangeListener(new FieldChangeListener() {
			public void fieldChanged(Field field, int context) {
				// TODO Auto-generated method stub
				closeMe();
			}
		});
		btnClose.setMargin(0, ModelConfig.getConfig().getValue10px(), 0, ModelConfig.getConfig().getValue10px());
		btnClose.setBorderFocus(false);
		
		checkBox = new CustomCheckBoxField(Properties._LBL_JANGAN_TAMPILKAN, DeviceProperties.getDefaultApplicationFont(Font.PLAIN, ModelConfig.getConfig().getFontSmallHeight()), Properties._COLOR_FONT_LABEL_GREEN, 0xffffff, true);
		checkBox.setChangeListener(new FieldChangeListener() {
			public void fieldChanged(Field field, int context) {
				// TODO Auto-generated method stub
				isCheck = !isCheck;
			}
		});
	}
	
	private static void initSublayout(){
		final int posY = (Display.getHeight() - (promo.getHeight() + checkBox.getPreferredHeight() + ModelConfig.getConfig().getValue10px())) / 2;
		
		_bodyManager = new VerticalFieldManager(NO_VERTICAL_SCROLL | NO_VERTICAL_SCROLLBAR){
			protected void paintBackground(Graphics g){	
				// draw bitmap
				g.drawBitmap((getWidth()-promo.getWidth())/2, posY, promo.getWidth(), promo.getHeight(), promo, 0, 0);
				
				// draw blackbox
				g.setColor(0x252525);
				g.fillRect((getWidth()-promo.getWidth())/2, posY + promo.getHeight(), promo.getWidth(), checkBox.getPreferredHeight() + ModelConfig.getConfig().getValue10px());
			}
			
			protected void sublayout(int maxWidth, int maxHeight){
				int aW = Display.getWidth();
				int aH = Display.getHeight();
				
				Field close = getField(0);
			    layoutChild(close, close.getPreferredWidth(), close.getPreferredHeight());
			    setPositionChild(close, aW-close.getWidth(), posY-(close.getHeight()/2));
			    
			    Field check = getField(1);
			    layoutChild(check, check.getPreferredWidth(), check.getPreferredHeight());
			    setPositionChild(check, (aW-promo.getWidth())/2, posY+promo.getHeight()+ModelConfig.getConfig().getValue5px());
			    
			    setExtent(aW, aH);
			}
		};
	}
	
	private static void initContent(){
		_bodyManager.add(btnClose);
		_bodyManager.add(checkBox);
	}

	private static void closeMe(){
		// update setting splash popup
		if(isCheck){
			// jangan tampilkan 24 jam kedepan
			SplashPromoManager.setSplashSetting(true);
		}
		
		ScreenManager.closeSplashPromoPopup(_self);
	}
}
