package com.mobileforce.bango.screen.component.maps;

import javax.microedition.location.Coordinates;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.screen.component.LocationDetailPopupScreen;
import com.mobileforce.bango.storage.persistable.BodyPersistable;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.TouchGesture;
import net.rim.device.api.ui.XYPoint;

public class CustomPOIMap extends CustomMap{
	private BodyPersistable data;
	private int posX = 0;
	private int posY = 0;
	private int posYCorrection = 0;

	public CustomPOIMap(Bitmap crosshair) {
		super(crosshair);
		// TODO Auto-generated constructor stub
	}

	public void paint(Graphics graphics) {
        super.paint(graphics);

        XYPoint actualFieldOut = new XYPoint();
        Coordinates actualCord = new Coordinates(((double) getLatitude()) / 100000.0, ((double) getLongitude()) / 100000.0, 0);
        convertWorldToField(actualCord, actualFieldOut);

        drawPopup(graphics, actualFieldOut);
    }

    private void drawPopup(Graphics graphics, XYPoint fieldOut) {
    	Marker marker = getOpenLocation();
    	
        if (marker != null) {
//            XYRect rect = null;
            Bitmap image = ModelConfig.getConfig().getBgBaseDetail();
            if (fieldOut.x > (-image.getWidth()) && fieldOut.x < getWidth() + image.getWidth()) {
                if (fieldOut.y > (-image.getHeight()) && fieldOut.y < getHeight() + image.getHeight()) {
                    int imgW = image.getWidth();
                    int imgH = image.getHeight();
                    int popupX = fieldOut.x - imgW / 2;
                    int popupY = fieldOut.y - imgH;

//                    rect = new XYRect(popupX, popupY, imgW, imgH);
//                    graphics.drawBitmap(rect, image, 0, 0);
                    
                    if(marker.getData() != null){
                    	data = marker.getData();
                    	posX = popupX;
                    	posY = popupY;
//                    	ScreenManager.showLocationDetailPopup(LocationDetailPopupScreen.showDetail(marker.getData(), popupX, popupY));
                    }
                    else{
//                    	String text = "I'm here!";
//                    	graphics.setColor(Color.GREEN);
//                    	graphics.drawText(text, fieldOut.x-(Font.getDefault().getAdvance(text)/2), fieldOut.y-Font.getDefault().getHeight());
                    }
                }
            }
        }   
    }
    
    /* (non-Javadoc)
     * @see net.rim.device.api.ui.Field#navigationClick(int, int)
     */
    protected boolean navigationClick( int status, int time ) {
    	Marker marker = getOpenLocation();
    	if (marker != null) {
        	showPopup();
        }
    	else{
    		resetPopup();
    	}
    	
        return super.navigationClick(status, time);    
    }
    
    /* (non-Javadoc)
     * @see net.rim.device.api.ui.Field#trackwheelClick(int, int)
     */
    protected boolean trackwheelClick( int status, int time ){ 
    	Marker marker = getOpenLocation();
    	if (marker != null) {
    		showPopup();
    	}
    	else{
    		resetPopup();
    	}
    	
        return super.trackwheelClick(status, time);
    }
    
    protected boolean touchEvent(TouchEvent message) {
    	//handling basic touch action
//    	int eventCode = message.getEvent(); 
//    	if(eventCode == TouchEvent.CLICK) {
//    		showPopup();
//    	}
    	
    	TouchGesture gesture = message.getGesture();
    	try{
			if(gesture instanceof TouchGesture){
				if(gesture.getEvent() == TouchGesture.TAP){
					Marker marker = getOpenLocation();
			    	if (marker != null) {
			        	showPopup();
			        }
				}
			}
    	}catch(Exception x){}
    	
    	return super.touchEvent(message);
    }
    
    public boolean navigationMovement(int dx, int dy, int status, int time) {
    	resetPopup();
    	return super.navigationMovement(dx, dy, status, time);
    }
    
    private void showPopup(){
    	if(data != null){
    		ScreenManager.showLocationDetailPopup(LocationDetailPopupScreen.showDetail(data, posX, posY+posYCorrection));
    	}
    	resetPopup();
    }
    
    private void resetPopup(){
    	data = null;
    	posX = 0;
    	posY = 0;
    }
    
    public void setPosYCorrection(int yValue){
    	this.posYCorrection = yValue;
    }
}
