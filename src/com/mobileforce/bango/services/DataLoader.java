package com.mobileforce.bango.services;

import java.util.Vector;

import net.rim.device.api.ui.UiApplication;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.manager.ClassManager;
import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.screen.component.ProgressDialog;
import com.mobileforce.bango.services.callback.DataLoaderCallback;
import com.mobileforce.bango.storage.parser.GeneralJSONParser;
import com.mobileforce.bango.storage.query.NotificationStatusStore;
import com.mobileforce.bango.storage.query.TemporerStore;
import com.mobileforce.bango.utility.ConnectionChecker;
import com.mobileforce.bango.utility.Properties;

public class DataLoader extends Thread{
	private DataLoader _self;
	private String _url, _parameter, _type, _method;
	private byte[] _multipart_parameters;
	
	private boolean _iscancel, _isloading;
	
	private ProgressDialog loading;
	private GeneralJSONParser parser;
	
	private boolean _is_multipart = false;
	private DataLoaderCallback _callback;
	
	/*normal request*/
	public DataLoader(String url, String parameter, String method, String type, boolean iscancel, boolean isloading){
		this(url, parameter, method, type, iscancel, isloading, null);
	}
	
	/*normal request with callback*/
	public DataLoader(String url, String parameter, String method, String type, boolean iscancel, boolean isloading, DataLoaderCallback callback){
		_url = url;
		_parameter = parameter;
		_method = method;
		_type = type;
		
		_iscancel = iscancel;
		_isloading = isloading;
		_callback = callback;
		
		_self = this;
		
		engineSelector();
	}
	
	/*multipart http request*/
	public DataLoader(String url, byte[] multipart_parameters, String method, String type, boolean iscancel, boolean isloading) {
		this(url, multipart_parameters, method, type, iscancel, isloading, null);
	}
	
	/*multipart http request with callback*/
	public DataLoader(String url, byte[] multipart_parameters, String method, String type, boolean iscancel, boolean isloading, DataLoaderCallback callback) {
		_url = url;
		_multipart_parameters = multipart_parameters;
		_method = method;
		_type = type;
		
		_iscancel = iscancel;
		_isloading = isloading;
		_callback = callback;
		
		_self = this;
		
		_is_multipart = true;

		engineSelector();
	}
	
	void engineSelector(){
//		if (!Properties._IS_CONNECTION_ACTIVE) {
//			Properties._IS_CONNECTION_ACTIVE = true;
			start();
//		}
	}
	
	public void run() {
		try{
			//check apakah koneksi tersedia, jika tidak berikan informasi
			ConnectionChecker _check = new ConnectionChecker();
			_check.Checking();
			if(!_check.isAvailable()){
				showAlertDialog(Properties._INF_NO_CONNECTION);
				
//				Properties._IS_CONNECTION_ACTIVE = false;
				return;
			}
			//-----------------------------------------------------------
			
			if(_isloading) showLoading();
			
			parser = new GeneralJSONParser(_type);
			if(_is_multipart){
				parser.generateFromMultipart(_url, _multipart_parameters, _method);
			}
			else{
				parser.generateFromURL(_url, _parameter, _method);
			}
		}catch(Exception x){
			try {
				if ((x.toString().indexOf("~ 120000") != -1)
						|| (x.toString().indexOf("~ 130000") != -1)) {
					showAlertDialog(Properties._INF_CONNECTION_TIMEOUT);
				} else {
					showAlertDialog(Properties._APP_NAME
							+ " is experiencing an issue, we will have it up and running soon. Thank you for your understanding.\n"
							+ " [" + Properties._HTTP_ERROR_CODE + "]");
				}
			} catch (Exception z) {
			}
			System.out.println("[DataLoader Error]" + x.toString());
		}finally{
//			Properties._IS_CONNECTION_ACTIVE = false;
			if(_isloading) closeLoading();
			
			if(Properties._HTTP_ERROR_CODE==0 && parser.get_general_status().equalsIgnoreCase("OK")){
				nextAction(parser.get_vector());
			}
			else{
				StringBuffer err = new StringBuffer();
				err.append(parser.get_general_message());
				
				if(!parser.get_general_code().equalsIgnoreCase("")){
					err.append(" [");
					err.append(parser.get_general_code());
					err.append("]");
				}
				
				if(parser.get_general_code().equalsIgnoreCase("901")){
					//session error code 901: force logout and then need to login again
					_type = Properties._CONN_TYPE_LOGOUT;
					nextAction(null);
				}
				else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_LOGOUT) && !_iscancel && !_isloading){
					// logout request
					nextAction(null);
				}
				else if(err.toString().length() > 0){
					showAlertDialog(err.toString());
				}
			}
			
			stopMe();
		}
	}
	
	void stopMe(){
		System.out.println("[Trying to close DataLoader]");
		try{
			if(_self.isAlive()){
				_self.interrupt();
				System.out.println("[DataLoader Closed]");
			}
		}catch(Exception x){ 
			System.out.println("[Unable to close DataLoader]"+x);
		}
	}
	
	void nextAction(Vector v){
		System.out.println("[DataLoader Type] >>>"+_type);
		
		if(_type.equalsIgnoreCase(Properties._CONN_TYPE_LOGOUT)){
			UserInfoManager.deleteUserInfo();
			UserProfileManager.deleteUserProfile();
			NotificationStatusStore.deleteNotificationStatus();
			
			synchronized (UiApplication.getUiApplication().getAppEventLock()) {
				ScreenManager.clearAllScreen();
				ScreenManager.getBangoApp().pushScreen(ClassManager.getInstance().getScreenManager().getWelcomeScreen());
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_LOGIN_1)){
			// save
			UserInfoManager.setUserInfo(v);
			
			synchronized (UiApplication.getUiApplication().getAppEventLock()) {
				ScreenManager.clearAllScreen();
				ScreenManager.getBangoApp().pushScreen(ClassManager.getInstance().getScreenManager().getFirstMapScreen());
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_REGISTER_1)){
			// save
			UserInfoManager.setUserInfo(v);
			
			synchronized (UiApplication.getUiApplication().getAppEventLock()) {
				ScreenManager.clearAllScreen();
				ScreenManager.getBangoApp().pushScreen(ClassManager.getInstance().getScreenManager().getFirstMapScreen());
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_LOGIN_2)){
			if(parser.get_general_code().equalsIgnoreCase("FB0")){
				synchronized (UiApplication.getUiApplication().getAppEventLock()) {
					ScreenManager.getBangoApp().pushScreen(ClassManager.getInstance().getScreenManager().getRegisterScreenFb());
				}
			}
			else{
				// save
				UserInfoManager.setUserInfo(v);
				
				synchronized (UiApplication.getUiApplication().getAppEventLock()) {
					ScreenManager.clearAllScreen();
					ScreenManager.getBangoApp().pushScreen(ClassManager.getInstance().getScreenManager().getFirstMapScreen());
				}
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_REGISTER_2)){
			// save
			UserInfoManager.setUserInfo(v);
			
			synchronized (UiApplication.getUiApplication().getAppEventLock()) {
				ScreenManager.clearAllScreen();
				ScreenManager.getBangoApp().pushScreen(ClassManager.getInstance().getScreenManager().getFirstMapScreen());
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_HOME_DISTANCE)){
			TemporerStore.setHomeByDistance(v);
			
			if(_callback != null){
				synchronized (UiApplication.getUiApplication().getAppEventLock()) {
					_callback.onRequestCompleted(v, _type);
				}
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_HOME_POPULAR)){
			TemporerStore.setHomeByPopular(v);
			
			if(_callback != null){
				synchronized (UiApplication.getUiApplication().getAppEventLock()) {
					_callback.onRequestCompleted(v, _type);
				}
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_HOME_NEW)){
			TemporerStore.setHomeByNew(v);
			
			if(_callback != null){
				synchronized (UiApplication.getUiApplication().getAppEventLock()) {
					_callback.onRequestCompleted(v, _type);
				}
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_MENU_SPECIAL)){
			TemporerStore.setMenuSpecial(v);
			
			if(_callback != null){
				synchronized (UiApplication.getUiApplication().getAppEventLock()) {
					_callback.onRequestCompleted(v, _type);
				}
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_JAJANAN_SPECIAL)){
			if(_callback != null){
				synchronized (UiApplication.getUiApplication().getAppEventLock()) {
					_callback.onRequestCompleted(v, _type);
				}
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_RECIPE_NEW)){
			TemporerStore.setRecipeByNew(v);
			
			if(_callback != null){
				synchronized (UiApplication.getUiApplication().getAppEventLock()) {
					_callback.onRequestCompleted(v, _type);
				}
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_RECIPE_POPULAR)){
			TemporerStore.setRecipeByPopular(v);
			
			if(_callback != null){
				synchronized (UiApplication.getUiApplication().getAppEventLock()) {
					_callback.onRequestCompleted(v, _type);
				}
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_NOTIFICATION)){
			TemporerStore.setNotificationList(v);
			
			if(_callback != null){
				synchronized (UiApplication.getUiApplication().getAppEventLock()) {
					_callback.onRequestCompleted(v, _type);
				}
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_GET_PROFILE)){
			Vector profile = (Vector) v.elementAt(0);
			UserProfileManager.setUserProfile(profile);
			
			if(_callback != null){
				synchronized (UiApplication.getUiApplication().getAppEventLock()) {
					TemporerStore.setUserProfile(v);
					_callback.onRequestCompleted(v, _type);
				}
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_USER_REVIEW_PAGING)
				|| _type.equalsIgnoreCase(Properties._CONN_TYPE_USER_REVIEW_PAGING_OTHER)
		){
			if(_callback != null){
				synchronized (UiApplication.getUiApplication().getAppEventLock()) {
					_callback.onRequestCompleted(v, _type);
				}
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_EDIT_USER_PROFILE)
				|| _type.equalsIgnoreCase(Properties._CONN_TYPE_ADD_REVIEW)
				|| _type.equalsIgnoreCase(Properties._CONN_TYPE_REPORT_REVIEW)
				|| _type.equalsIgnoreCase(Properties._CONN_TYPE_DELETE_REVIEW)
				|| _type.equalsIgnoreCase(Properties._CONN_TYPE_ADD_STORE)
		){
			if(_callback != null){
				synchronized (UiApplication.getUiApplication().getAppEventLock()) {
					_callback.onRequestCompleted(null, parser.get_general_message());
				}
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_LIKE_REVIEW)
				|| _type.equalsIgnoreCase(Properties._CONN_TYPE_CHANGE_NOTIF)
		){
			if(_callback != null){
				synchronized (UiApplication.getUiApplication().getAppEventLock()) {
					_callback.onRequestCompleted(null, parser.get_general_message() + _type);
				}
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_DETAIL_STORE)){
			if(_callback != null){
				synchronized (UiApplication.getUiApplication().getAppEventLock()) {
					_callback.onRequestCompleted(v, _type);
				}
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_DETAIL_STORE_REVIEW_PAGING)){
			if(_callback != null){
				synchronized (UiApplication.getUiApplication().getAppEventLock()) {
					_callback.onRequestCompleted(v, _type);
				}
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_DETAIL_RESEP)){
			if(_callback != null){
				synchronized (UiApplication.getUiApplication().getAppEventLock()) {
					_callback.onRequestCompleted(v, _type);
				}
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_DETAIL_RECIPE_REVIEW_PAGING)){
			if(_callback != null){
				synchronized (UiApplication.getUiApplication().getAppEventLock()) {
					_callback.onRequestCompleted(v, _type);
				}
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_SEARCH_HOME_DISTANCE)
				|| _type.equalsIgnoreCase(Properties._CONN_TYPE_SEARCH_HOME_POPULAR)
				|| _type.equalsIgnoreCase(Properties._CONN_TYPE_SEARCH_HOME_NEW)
		){
			if(_callback != null){
				synchronized (UiApplication.getUiApplication().getAppEventLock()) {
					_callback.onRequestCompleted(v, _type);
				}
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_SEARCH_RECIPE_POPULAR)
				|| _type.equalsIgnoreCase(Properties._CONN_TYPE_SEARCH_RECIPE_NEW)
		){
			if(_callback != null){
				synchronized (UiApplication.getUiApplication().getAppEventLock()) {
					_callback.onRequestCompleted(v, _type);
				}
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_GET_PROFILE_OTHER)){
			if(_callback != null){
				synchronized (UiApplication.getUiApplication().getAppEventLock()) {
					_callback.onRequestCompleted(v, _type);
				}
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_GOOGLE_GEOCODING)){
			if(_callback != null){
				synchronized (UiApplication.getUiApplication().getAppEventLock()) {
					_callback.onRequestCompleted(v, _type);
				}
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_STATE_NOTIF)){
			// update settings manager
			boolean notification = parser.get_general_message().equalsIgnoreCase("1") ? true : false;
			SettingManager.setNotificationSetting(notification);
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_SPLASH_PROMO)){
			if(SplashPromoManager.getPromo(0) == null){
				// new
				SplashPromoManager.setSplashPromo(v);
				SplashPromoManager.fetchAndShownPromoRandomly();
			}
			else{
				// update only
				SplashPromoManager.setSplashPromo(v);
			}
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_PAGE_PROMO)){
			Vector promo = (Vector) v.elementAt(1);
			SplashPromoManager.setSplashPromo(promo);
			
			if(_callback != null){
				synchronized (UiApplication.getUiApplication().getAppEventLock()) {
					_callback.onRequestCompleted(v, _type);
				}
			}
		}
	}
	
	public void showLoading(){
		if(_iscancel) {
			loading = new ProgressDialog(Properties._LBL_PLEASE_WAIT, ModelConfig.getConfig().getLoadingImage());
		}
		else{
			loading = new ProgressDialog(Properties._LBL_PLEASE_WAIT, ModelConfig.getConfig().getLoadingImage(), false);
		}
		ScreenManager.showPopupTypeScreen(loading);
	}
	
	public void closeLoading(){
		if(_isloading) ScreenManager.closePopupTypeScreen(loading);
	}
	
	public void showAlertDialog(String message){
		ScreenManager.showDialogTypeScreen(message);
	}
}
