package com.mobileforce.bango.services;

import java.util.Date;
import java.util.Vector;

import com.blackberry.facebook.ApplicationSettings;
import com.blackberry.facebook.BasicAsyncCallback;
import com.blackberry.facebook.Facebook;
import com.blackberry.facebook.FacebookException;
import com.blackberry.facebook.inf.User;
import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.screen.component.ProgressDialog;
import com.mobileforce.bango.storage.persistable.FacebookPersistable;
import com.mobileforce.bango.storage.query.GenericStore;
import com.mobileforce.bango.utility.ConnectionChecker;
import com.mobileforce.bango.utility.GStringUtil;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.i18n.SimpleDateFormat;
import net.rim.device.api.system.EncodedImage;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;

public class FacebookConnector extends Thread {
	private FacebookConnector _self;
	private final static String NEXT_URL = "http://www.facebook.com/connect/login_success.html";
	private final static String APPLICATION_ID = "299369603582056"; //app id 
	private final static String APPLICATION_SECRET = "3b1ee63769d1df012cbde13e8a6eff6b"; //app secret key
	
	private Facebook fb;
	private User user;
	private String pUserId = "me";
	
	private String _type, _text;
	private EncodedImage _image;
	private ProgressDialog loading;
	
	public FacebookConnector(String type){
		this(type, null, null);
	}
	
	public FacebookConnector(String type, String text){
		this(type, text, null);
	}
	
	public FacebookConnector(String type, String text, EncodedImage image){
		_type = type;
		_text = text;
		_image = image;

		initialize();
	}
	
	void initialize(){
		if(isAlreadyConnected()){
			prepareFirst();
			
//			//clear cache fbsdk browser, agar selalu menampilkan screen login ke facebook
//			fb.setAccessToken(null); 
//			fb.logout();
//			//***************************************************************************
			
			start();
		}
		else{
			prepareFirst();
			start();
		}
	}
	
	private String[] preparePermission(){
		String[] result = new String[] {
				"user_about_me", "user_birthday", "email",
				"user_location", "user_notes", "user_photos", "user_status",
				"read_stream", /*"read_mailbox", /*"friends_about_me",*/
				"publish_stream", "offline_access"
		};
		
		return result;
	}
	
	void prepareFirst(){
		fb = Facebook.getInstance(new ApplicationSettings(NEXT_URL, APPLICATION_ID, APPLICATION_SECRET, preparePermission())); //Facebook.Permissions.USER_DATA_PERMISSIONS));
		
		_self = this;
	}
	
	public void run() {
		try {
			//check apakah koneksi tersedia, jika tidak berikan informasi
			ConnectionChecker _check = new ConnectionChecker();
			_check.Checking();
			if(!_check.isAvailable()){
				showAlertDialog(Properties._INF_NO_CONNECTION);
				
				return;
			}
			//-----------------------------------------------------------
			
			//inisialiasi loading popup
			showLoading();
			
			//connect to facebook
			fb.getUser(pUserId, new BasicAsyncCallback() {
				public void onComplete(com.blackberry.facebook.inf.Object[] objects, final java.lang.Object state) {
					user = (User) objects[0];
					
					/*UiApplication.getApplication().invokeLater(new Runnable() {
						public void run() {
							if(Dialog.ask(Dialog.OK, "UID>>>"+user.getId()+" TOKEN>>>"+fb.getAccessToken()+" EMAIL>>>"+user.getEmail()) == Dialog.OK){
								//user.getId();
								//mfb.getAccessToken();
							}
						}
					});*/
					
					saveData(fb.getAccessToken(), user.getId(), user.getName(),
							user.getEmail(), user.getFirstName(),
							user.getLastName(), user.getBirthday(),
							user.getGender(), user.getLocationName());
					
					nextAction();
					
					//close
					//fb.logout();
				}

				public void onException(final Exception e, final java.lang.Object state) {
					e.printStackTrace();
					
					UiApplication.getApplication().invokeLater(new Runnable() {
						public void run() {
							/*if(Dialog.ask(Dialog.OK, "Error Encountered " + "Exception: " + e.getMessage()) == Dialog.OK){
								//"Error Encountered " + "Exception: " + e.getMessage()
							}*/
							if(Dialog.ask(Dialog.OK, "Operation cancelled: " + e.getMessage()) == Dialog.OK){
							}
						}
					});
				}
			}, null);
		} catch (FacebookException e) {
			e.printStackTrace();
		} finally{
			closeLoading();
			stopMe();
		}
	}
	
	void stopMe(){
		System.out.println("[Trying to close FacebookConector]");
		try{
			if(_self.isAlive()){
				_self.interrupt();
				System.out.println("[FacebookConector Closed]");
			}
		}catch(Exception x){ 
			System.out.println("[Unable to close FacebookConector]"+x);
		}
	}
	
	private void saveData(String token, String uid, String name, String email,
			String first_name, String last_name, Date birthday, String gender, String location) {
		String bday = GStringUtil.generateTimeStamp("yyyy-MM-dd"); //default
		if(birthday!=null){
			bday = new SimpleDateFormat("yyyy-MM-dd").format(birthday);
		}
		if(gender.startsWith("m") || gender.startsWith("M")){
			gender = "1";
		}
		else{
			gender = "2";
		}
		
		Vector save = new Vector();
		FacebookPersistable data = new FacebookPersistable();
		data.setFacebook_token(token);
		data.setFacebook_userid(uid);
		data.setFacebook_name(name);
		data.setFacebook_email(email);
		data.setFacebook_firstname(first_name);
		data.setFacebook_lastname(last_name);
		data.setFacebook_birthday(bday);
		data.setFacebook_gender(gender);
		data.setFacebook_location(location);
		
		save.addElement(data);
		
		new GenericStore(Properties._FB_CONNECT_ID).saveDataVector(save);
	}
	
	public static FacebookPersistable loadData(){
		return (FacebookPersistable) new GenericStore(Properties._FB_CONNECT_ID).getVectorOfStoredData().elementAt(0);
	}
	
	private boolean isAlreadyConnected(){
		return new GenericStore(Properties._FB_CONNECT_ID).getVectorOfStoredData().size()>0 ? true : false;
	}
	
	private void nextAction(){
		System.out.println("[OAUTH FB SUCCESSFUL]");
		if(_type.equalsIgnoreCase(Properties._CONN_TYPE_LOGIN_2)){
			StringBuffer parameter = new StringBuffer();
			parameter.append("fbid="); parameter.append(loadData().getFacebook_userid());
			parameter.append("&fname="); parameter.append(loadData().getFacebook_firstname());
			parameter.append("&lname="); parameter.append(loadData().getFacebook_lastname());
			parameter.append("&gender="); parameter.append(loadData().getFacebook_gender());
			parameter.append("&email="); parameter.append(loadData().getFacebook_email());
			parameter.append("&bdate="); parameter.append(loadData().getFacebook_birthday());
			parameter.append("&location="); parameter.append(loadData().getFacebook_location());
			
			new DataLoader(Properties._API_LOGIN_FACEBOOK, parameter.toString(), Properties._HTTP_METHOD_POST, Properties. _CONN_TYPE_LOGIN_2, false, true);
		}
		else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_SHARE_FACEBOOK)){
//			user.publishStatus(_text);
			user.publishPhoto(_image, _text);
		}
	}
	
	public void showLoading(){
		loading = new ProgressDialog(Properties._LBL_PLEASE_WAIT, ModelConfig.getConfig().getLoadingImage());
		ScreenManager.showPopupTypeScreen(loading);
	}
	
	public void closeLoading(){
		ScreenManager.closePopupTypeScreen(loading);
	}
	
	public void showAlertDialog(String message){
		ScreenManager.showDialogTypeScreen(message);
	}
}
