package com.mobileforce.bango.services;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.screen.CariKulinerScreen;
import com.mobileforce.bango.screen.CariResepScreen;
import com.mobileforce.bango.screen.DaftarKulinerScreen;
import com.mobileforce.bango.screen.DaftarResepScreen;
import com.mobileforce.bango.screen.EditProfilScreen;
import com.mobileforce.bango.screen.JajananSpecialScreen;
import com.mobileforce.bango.screen.OthersProfilScreen;
import com.mobileforce.bango.screen.ProfilScreen;
import com.mobileforce.bango.screen.ResepDetailScreen;
import com.mobileforce.bango.screen.StoreDetailScreen;
import com.mobileforce.bango.screen.component.ImageZoomPopupScreen;
import com.mobileforce.bango.screen.component.LocationDetailPopupScreen;
import com.mobileforce.bango.screen.component.ReviewListField;
import com.mobileforce.bango.utility.GStringUtil;
import com.mobileforce.bango.utility.Properties;

public class ImageFetching extends Thread{
	private ImageFetching _self;
	private DaftarKulinerScreen DKS;
	private JajananSpecialScreen JSS;
	private DaftarResepScreen DRS;
	private ProfilScreen PS;
	private EditProfilScreen EPS;
	private StoreDetailScreen SDS;
	private ResepDetailScreen RDS;
	private CariKulinerScreen CKS;
	private CariResepScreen CRS;
	private ImageZoomPopupScreen IZPS;
	private OthersProfilScreen OPS;
	private LocationDetailPopupScreen LDPS;
	private ReviewListField RLF;
	private SplashPromoManager SPM;
	
	private String[] _imgurl;
	private String _type;
	
	public ImageFetching(DaftarKulinerScreen screen, String[] imgurl, String type){
		_self = this;
		_imgurl = imgurl;
		
		DKS = screen;
		_type = type;
		
		start();
	}
	
	public ImageFetching(JajananSpecialScreen screen, String[] imgurl, String type){
		_self = this;
		_imgurl = imgurl;
		
		JSS = screen;
		_type = type;
		
		start();
	}
	
	public ImageFetching(DaftarResepScreen screen, String[] imgurl, String type){
		_self = this;
		_imgurl = imgurl;
		
		DRS = screen;
		_type = type;
		
		start();
	}
	
	public ImageFetching(ProfilScreen screen, String[] imgurl, String type){
		_self = this;
		_imgurl = imgurl;
		
		PS = screen;
		_type = type;
		
		start();
	}
	
	public ImageFetching(EditProfilScreen screen, String[] imgurl, String type){
		_self = this;
		_imgurl = imgurl;
		
		EPS = screen;
		_type = type;
		
		start();
	}
	
	public ImageFetching(StoreDetailScreen screen, String[] imgurl, String type){
		_self = this;
		_imgurl = imgurl;
		
		SDS = screen;
		_type = type;
		
		start();
	}
	
	public ImageFetching(ResepDetailScreen screen, String[] imgurl, String type){
		_self = this;
		_imgurl = imgurl;
		
		RDS = screen;
		_type = type;
		
		start();
	}
	
	public ImageFetching(CariKulinerScreen screen, String[] imgurl, String type){
		_self = this;
		_imgurl = imgurl;
		
		CKS = screen;
		_type = type;
		
		start();
	}
	
	public ImageFetching(CariResepScreen screen, String[] imgurl, String type){
		_self = this;
		_imgurl = imgurl;
		
		CRS = screen;
		_type = type;
		
		start();
	}
	
	public ImageFetching(ImageZoomPopupScreen screen, String[] imgurl, String type){
		_self = this;
		_imgurl = imgurl;
		
		IZPS = screen;
		_type = type;
		
		start();
	}
	
	public ImageFetching(OthersProfilScreen screen, String[] imgurl, String type){
		_self = this;
		_imgurl = imgurl;
		
		OPS = screen;
		_type = type;
		
		start();
	}
	
	public ImageFetching(LocationDetailPopupScreen screen, String[] imgurl, String type){
		_self = this;
		_imgurl = imgurl;
		
		LDPS = screen;
		_type = type;
		
		start();
	}
	
	public ImageFetching(ReviewListField field, String[] imgurl, String type){
		_self = this;
		_imgurl = imgurl;
		
		RLF = field;
		_type = type;
		
		start();
	}
	
	public ImageFetching(SplashPromoManager m, String[] imgurl, String type){
		_self = this;
		_imgurl = imgurl;
		
		SPM = m;
		_type = type;
		
		start();
	}
	
	public void run() {
		try {
			if(_imgurl != null){
				for(int f=0; f<_imgurl.length; f++){
					try{
							if(GStringUtil.checkHTTP(_imgurl[f]) || _imgurl[f].length()>3){
								if(_type.equalsIgnoreCase(Properties._CONN_TYPE_HOME_DISTANCE)){
									DKS.P1[f].setPreviewBitmap(new ImageLoader()
										.selectBitmap(_imgurl[f],
												ModelConfig.getConfig().getPreviewWidth(), 
												ModelConfig.getConfig().getPreviewHeight(),
												3,
												false, false, true, 1));
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_HOME_POPULAR)){
									DKS.P2[f].setPreviewBitmap(new ImageLoader()
									.selectBitmap(_imgurl[f],
											ModelConfig.getConfig().getPreviewWidth(), 
											ModelConfig.getConfig().getPreviewHeight(),
											3,
											false, false, true, 1));
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_HOME_NEW)){
									DKS.P3[f].setPreviewBitmap(new ImageLoader()
									.selectBitmap(_imgurl[f],
											ModelConfig.getConfig().getPreviewWidth(), 
											ModelConfig.getConfig().getPreviewHeight(),
											3,
											false, false, true, 1));
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_JAJANAN_SPECIAL)){
									JSS.JS[f].setPreviewBitmap(new ImageLoader()
									.selectBitmap(_imgurl[f],
											ModelConfig.getConfig().getPreviewWidth(), 
											ModelConfig.getConfig().getPreviewHeight(),
											3,
											false, false, true, 1));
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_RECIPE_NEW)){
									DRS.P1[f].setPreviewBitmap(new ImageLoader()
									.selectBitmap(_imgurl[f],
											ModelConfig.getConfig().getPreviewWidth(), 
											ModelConfig.getConfig().getPreviewHeight(),
											3,
											false, false, true, 1));
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_RECIPE_POPULAR)){
									DRS.P2[f].setPreviewBitmap(new ImageLoader()
									.selectBitmap(_imgurl[f],
											ModelConfig.getConfig().getPreviewWidth(), 
											ModelConfig.getConfig().getPreviewHeight(),
											3,
											false, false, true, 1));
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_AVATAR_PROFILE)){
									PS.setAvatarBitmap(new ImageLoader()
									.selectBitmap(_imgurl[f],
											0, 
											0,
											0,
											false, true, true, 2));
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_BADGES_PROFILE)){
									PS.setBadgesBitmap(new ImageLoader()
									.selectBitmap(_imgurl[f],
											ModelConfig.getConfig().getBadgeWidth(), 
											ModelConfig.getConfig().getBadgeHeight(),
											3,
											false, false, true, 2), f);
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_REVIEWS_ICON)){
									PS.reviewsList[f].setIcon(new ImageLoader()
									.selectBitmap(_imgurl[f],
											0, 
											0,
											3,
											false, true, true, 1));
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_EDIT_AVATAR_PROFILE)){
									EPS.setAvatarBitmap(new ImageLoader()
									.selectBitmap(_imgurl[f],
											0, 
											0,
											0,
											false, true, true, 2));
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_BIG_PREVIEW)){
									SDS.setBigPreview(new ImageLoader()
									.selectBitmap(_imgurl[f],
											ModelConfig.getConfig().getBigPreviewWidth(), 
											0 /*ModelConfig.getConfig().getBigPreviewHeight()*/,
											3,
											false, false, true, 1));
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_USER_REVIEW_AVATAR)){
									SDS.reviewList[f].setIcon(new ImageLoader()
									.selectBitmap(_imgurl[f],
											0, 
											0,
											3,
											false, true, true, 1));
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_USER_REVIEW_IMAGE)){
									SDS.reviewList[f].setImageShare(new ImageLoader()
									.selectBitmap(_imgurl[f],
											ModelConfig.getConfig().getBadgeWidth(), 
											ModelConfig.getConfig().getBadgeHeight(),
											3,
											false, false, true, 1));
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_RESEP_BIG_PREVIEW)){
									RDS.setBigPreview(new ImageLoader()
									.selectBitmap(_imgurl[f],
											ModelConfig.getConfig().getBigPreviewWidth(), 
											0 /*ModelConfig.getConfig().getBigPreviewHeight()*/,
											3,
											false, false, true, 1));
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_RECIPE_REVIEW_AVATAR)){
									RDS.reviewList[f].setIcon(new ImageLoader()
									.selectBitmap(_imgurl[f],
											0, 
											0,
											3,
											false, true, true, 1));
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_RECIPE_REVIEW_IMAGE)){
									RDS.reviewList[f].setImageShare(new ImageLoader()
									.selectBitmap(_imgurl[f],
											ModelConfig.getConfig().getBadgeWidth(), 
											ModelConfig.getConfig().getBadgeHeight(),
											3,
											false, false, true, 1));
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_SEARCH_KULINER_ICON)){
									CKS.searchList[f].setIcon(new ImageLoader()
									.selectBitmap(_imgurl[f],
											0, 
											0,
											3,
											false, true, true, 1));
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_SEARCH_RECIPE_ICON)){
									CRS.searchList[f].setIcon(new ImageLoader()
									.selectBitmap(_imgurl[f],
											0, 
											0,
											3,
											false, true, true, 1));
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_IMAGE_ZOOM)){
									IZPS.setZoomImage(new ImageLoader()
									.selectBitmap(_imgurl[f],
											ModelConfig.getConfig().getBigPreviewWidth()-(ModelConfig.getConfig().getValue10px()*2), 
											0,
											3,
											false, true, true, 1));
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_AVATAR_PROFILE_OTHER)){
									OPS.setAvatarBitmap(new ImageLoader()
									.selectBitmap(_imgurl[f],
											0, 
											0,
											0,
											false, true, true, 2));
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_BADGES_PROFILE_OTHER)){
									OPS.setBadgesBitmap(new ImageLoader()
									.selectBitmap(_imgurl[f],
											ModelConfig.getConfig().getBadgeWidth(), 
											ModelConfig.getConfig().getBadgeHeight(),
											3,
											false, false, true, 2), f);
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_REVIEWS_ICON_OTHER)){
									OPS.reviewsList[f].setIcon(new ImageLoader()
									.selectBitmap(_imgurl[f],
											0, 
											0,
											3,
											false, true, true, 1));
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_POPUP_DETAIL_IMAGE)){
									LDPS.setPreviewImage(new ImageLoader()
									.selectBitmap(_imgurl[f],
											ModelConfig.getConfig().getIconTemporerImage().getWidth(), 
											ModelConfig.getConfig().getIconTemporerImage().getHeight(),
											3,
											false, false, true, 1));
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_REVIEW_SHARE_IMAGE)){
									RLF.setMasterImage(new ImageLoader()
									.selectBitmap(_imgurl[f],
											420, 
											420,
											3,
											false, false, true, 1));
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_SPLASH_PROMO_SINGLE_FETCH)){
									int w = ModelConfig.getConfig().isOrientationPortrait() ? 
											Display.getWidth() - (ModelConfig.getConfig() .getValue10px() * 2) : 
											Display.getHeight() - (ModelConfig.getConfig() .getValue10px() * 6);
									
									Bitmap splash = new ImageLoader()
											.selectBitmap(_imgurl[f],
													w, 
													0,
													3,
													false, true, true, 2);
									System.out.println("splash promo>>> " + splash.getWidth() + "x" + splash.getHeight());
									SPM.showSplashPromo(splash);
								}
								else if(_type.equalsIgnoreCase(Properties._CONN_TYPE_SPLASH_PROMO_MULTIPLE_FETCH)){
									int w = ModelConfig.getConfig().isOrientationPortrait() ? 
											Display.getWidth() - (ModelConfig.getConfig() .getValue10px() * 2) : 
											Display.getHeight() - (ModelConfig.getConfig() .getValue10px() * 6);
									
									new ImageLoader()
									.selectBitmap(_imgurl[f],
											w, 
											0,
											3,
											false, true, true, 2);
								}
							}
					}catch (Exception u){ 
						System.out.println("[unexpected error while fetching image]-> "+_imgurl[f]+" "+u+" ["+_type+"]");
					}
				}
			}
		}catch (Exception x){ System.out.println("error while fetching image= "+x+" ["+_type+"]"); }
		finally{
			stopMe();
		}
	}
	
	void stopMe(){
		System.out.println("[Trying to close ImageFetching]");
		try{
			if(_self.isAlive()){
				_self.interrupt();
				System.out.println("[ImageFetching Closed]");
			}
		}catch(Exception x){ 
			System.out.println("[Unable to close ImageFetching]"+x);
		}
	}
//***************************************************************************************
}
