package com.mobileforce.bango.services;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Hashtable;
import java.util.Vector;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.io.file.FileConnection;

import com.mobileforce.bango.manager.ImageManager;
import com.mobileforce.bango.storage.persistable.ImagePersistable;
import com.mobileforce.bango.storage.query.ImageStore;
import com.mobileforce.bango.storage.query.RuntimeStoreSystem;
import com.mobileforce.bango.utility.GStringUtil;
import com.mobileforce.bango.utility.ImageScaler;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.io.Base64OutputStream;
import net.rim.device.api.math.Fixed32;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.DeviceInfo;
import net.rim.device.api.system.EncodedImage;
import net.rim.device.api.system.JPEGEncodedImage;
import net.rim.device.api.system.PNGEncodedImage;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.util.StringUtilities;

public class ImageLoader {
	private byte[] imageData;
	private ImageScaler scaler = new ImageScaler();
	
	public ImageLoader(){}
	
	//image selection -> save_mode==0 persistent, save_mode==1 runtime store, save_mode==2 fetching
	public BitmapField selectImage(String source, boolean isfocus, int x2, int y2, int mode, boolean ispercent, boolean isproportional, boolean issave, int save_mode){
		BitmapField selected = null;
		Bitmap bitmap = selectBitmap(source, x2, y2, mode, ispercent, isproportional, issave, save_mode);
		
		selected = isfocusable(bitmap, isfocus);
		
		return selected;
	}
	
	//persistent/runtime store/fetching
	public Bitmap selectBitmap(String source, int x2, int y2, int mode, boolean ispercent, boolean isproportional, boolean issave, int save_mode){
		Bitmap selected = null;
		
		if(!GStringUtil.checkHTTP(source)){
			selected = getLocalBitmap(source, x2, y2, mode, ispercent, isproportional);
		}
		else{
			if(issave && save_mode==0){ 
				selected = mode0(source, x2, y2, mode, ispercent, isproportional);
			}
			else if(issave && save_mode==1){ 
				selected = mode1(source, x2, y2, mode, ispercent, isproportional);
			}
			else if(issave && save_mode==2){ 
				selected = mode2(source, x2, y2, mode, ispercent, isproportional);
			}
			else{
				//PERSISTENT/RUNTIME_STORE/FETCHING_LOCALLY INACTIVE
				selected = getHTTPBitmap(source, x2, y2, mode, ispercent, isproportional);
				if(getImageData()==null) selected = getEmptyBitmap("images/blank.png");
			}
		}
		
		return selected;
	}

	//27/05/2010
	/*******************************************************************************/
	//enter, opacity, focusable color
	public BitmapField getAdvImage(String source, boolean focus, boolean isclickable, int x2, int y2, int mode, boolean ispercent, boolean isproportional, int opacity){
		BitmapField advance = null;
		Bitmap bitmap = null;
		
		//PHASE 1 -> source
		if(GStringUtil.checkHTTP(source)){
			//http
			bitmap = getHTTPBitmap(source, x2, y2, mode, ispercent, isproportional);
		}
		else{
			//local
			bitmap = getLocalBitmap(source, x2, y2, mode, ispercent, isproportional);
		}
		
		//PHASE 2 -> define extra param
		long style = Field.FOCUSABLE; if(!focus) style = Field.NON_FOCUSABLE;
		final boolean _isclickable = isclickable;
		final int _opacity = opacity;
		
		//PHASE 3 -> create bitmap
		advance = new BitmapField(bitmap, style){
			protected boolean keyChar(char character, int status, int time) {
				if (character == Keypad.KEY_ENTER) {
					if(_isclickable) fieldChangeNotify(0); //System.out.println("ENTER PRESSED");
				}
				return super.keyChar(character, status, time);
			}
			protected boolean navigationClick(int status, int time) {
				if(_isclickable) fieldChangeNotify(0); //System.out.println("NAVI PRESSED"); 
				return true;
			}
			protected void drawFocus(Graphics graphics, boolean on){
				//no focus color
			}
			protected void paint(Graphics g){
				if(_opacity!=-1) g.setGlobalAlpha(_opacity);
				super.paint(g);
			}
		};
		
		return advance;
	}
	
	/*******************************************************************************/
	
	//07/05/2010
	/*******************************************************************************/
	//local + zoom in/out
	public BitmapField getLocalImage(String source, boolean focus, int x2, int y2, int mode, boolean ispercent, boolean isproportional){
		BitmapField local = null;
		Bitmap bitmap = getLocalBitmap(source, x2, y2, mode, ispercent, isproportional);
        
		local = isfocusable(bitmap, focus);
		
		return local;
	}
	/*******************************************************************************/
	
	//local
	/*******************************************************************************/
	public Bitmap getLocalBitmap(String source, int x2, int y2, int mode, boolean ispercent, boolean isproportional){
		Bitmap bitmap = null;
		EncodedImage image = null;
		InputStream in = getClass().getResourceAsStream("/"+source);
		//NOTE: for stream "/mobi/betterb/demopicture/images/p21.png" or getBitmapResource "mobi/betterb/demopicture/images/p21.png"
		
		byte[] data;
		try {
			data = new byte[in.available()];
			in.read(data);
			image = EncodedImage.createEncodedImage(data, 0, data.length);
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			//24-08-2010
			try {
		        if (in != null) {
		            in.close();
		        }
			}catch (IOException ioe) {
				System.out.println("Error closing streaming "+ioe);
			}
		}
		
		//rescale or not
		image = rescalling(image, x2, y2, mode, ispercent, isproportional);
        
        bitmap = image.getBitmap();
        
        return bitmap;
	}
	/*******************************************************************************/
	
	
	/*******************************************************************************/
	//http
	public BitmapField getHTTPImage(String sUrl, boolean focus, int x2, int y2, int mode, boolean ispercent, boolean isproportional) {
		BitmapField http = null;
		Bitmap bitmap = getHTTPBitmap(sUrl, x2, y2, mode, ispercent, isproportional);
		
		http = isfocusable(bitmap, focus);
        
        return http;
	}
	
	//http
	/*******************************************************************************/
	public Bitmap getHTTPBitmap(String sUrl, int x2, int y2, int mode, boolean ispercent, boolean isproportional){
		Bitmap bitmap = null;
		EncodedImage image = connectNow(sUrl, x2, y2, mode, ispercent, isproportional);
		bitmap = image.getBitmap();
		
		return bitmap;
	}
	/*******************************************************************************/
	
	
	//persistent
	/***************************************************************************************/
	public BitmapField getImageFromByte(byte[] data, boolean focus, int x2, int y2, int mode, boolean ispercent, boolean isproportional) {
		BitmapField bitmap = null;
		EncodedImage image = EncodedImage.createEncodedImage(data,0,data.length);
		
		image = rescalling(image, x2, y2, mode, ispercent, isproportional);
        
		bitmap = isfocusable(image.getBitmap(), focus);
        return bitmap;
	}
	/*************************************************************************************/
	
	//persistent //28-06-2010
	/***************************************************************************************/
	public Bitmap getBitmapFromByte(byte[] data, int x2, int y2, int mode, boolean ispercent, boolean isproportional) {
		Bitmap bitmap = null;
		EncodedImage image = EncodedImage.createEncodedImage(data,0,data.length);
		
		image = rescalling(image, x2, y2, mode, ispercent, isproportional);
		bitmap = image.getBitmap();
        
        return bitmap;
	}
	/*************************************************************************************/
	
	//24/08/2010
	/*******************************************************************************/
	//internal memory/memory card + zoom in/out
	public BitmapField getExplorerImage(String source, boolean focus, int x2, int y2, int mode, boolean ispercent, boolean isproportional){
		BitmapField local = null;
		Bitmap bitmap = getExplorerBitmap(source, x2, y2, mode, ispercent, isproportional);
        
		local = isfocusable(bitmap, focus);
		
		return local;
	}
	/*******************************************************************************/
	
	//internal memory/memory card
	/*******************************************************************************/
	public Bitmap getExplorerBitmap(String source, int x2, int y2, int mode, boolean ispercent, boolean isproportional){
		Bitmap bitmap = null;
		EncodedImage image = null;
		FileConnection file = null;
		InputStream in = null;
		
		byte[] data;
		try {
			if(!source.startsWith("file:///")){
				if(source.startsWith("/")){
					source = "file://"+source;
				}
				else{
					source = "file:///"+source;
				}
			}
			
			file = (FileConnection) Connector.open(source, Connector.READ_WRITE);
	        in = file.openInputStream();
	        
	        int length = in.available();
	        if(length>0){
	        	data = new byte[length];
	        	in.read(data);
	        	image = EncodedImage.createEncodedImage(data, 0, data.length);
	        }
	        else{
	        	length = (int) file.fileSize();
	        	
	        	data = new byte[length];
	        	in.read(data);
				image = EncodedImage.createEncodedImage(data, 0, -1);
	        }
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Error streaming "+e);
		} finally{
			try {
		        if (in != null) {
		            in.close();
		        }
		        if (file != null && file.isOpen()) {
		        	file.close();
		        }
			}catch (IOException ioe) {
				System.out.println("Error closing streaming "+ioe);
			}
		}
		
		image = rescalling(image, x2, y2, mode, ispercent, isproportional);
        
        bitmap = image.getBitmap();
        
        return bitmap;
	}
	/*******************************************************************************/
	
	
	//25-08-2010
	//internal memory/memory card as string
	/*******************************************************************************/
	public String ExplorerImage(String path){
		String result = null;
		FileConnection file = null;
		InputStream in = null;
		Base64OutputStream base64OutputStream = null;
		
		byte[] data;
		try {
			if(!path.startsWith("file:///")){
				if(path.startsWith("/")){
					path = "file://"+path;
				}
				else{
					path = "file:///"+path;
				}
			}
			
			file = (FileConnection) Connector.open(path, Connector.READ_WRITE);
	        in = file.openInputStream();
	        
	        
	        int length = in.available();
	        if(length>0){
	        	data = new byte[length];
	        	in.read(data);
	        }
	        else{
	        	length = (int) file.fileSize();
	        	
	        	data = new byte[length];
	        	in.read(data);
	        }
			//System.out.println("byte available>>> "+in.available());
			//System.out.println("file size>>> "+file.fileSize());
	        
	        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream( length );
	        base64OutputStream = new Base64OutputStream( byteArrayOutputStream );
	    	base64OutputStream.write(data);
	    	base64OutputStream.flush();
	    	base64OutputStream.close();
	    	byteArrayOutputStream.flush();
	    	byteArrayOutputStream.close();
	    	//int sizeofbase64 = byteArrayOutputStream.toString().length();
	    	
	    	result =  byteArrayOutputStream.toString();
	    	//System.out.println("base64>>> "+result);
			//result = new String(data);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Error streaming "+e);
		} finally{
			try {
		        if (in != null) {
		            in.close();
		        }
		        if (file != null && file.isOpen()) {
		        	file.close();
		        }
			}catch (IOException ioe) {
				System.out.println("Error closing streaming "+ioe);
			}
		}
		
		return result;
	}
	/*******************************************************************************/
	
	//16-09-2010
	//rescalling bitmap (bitmap->byte->bitmap)
	/*******************************************************************************/
	public Bitmap rescaleBitmap(Bitmap bitmap, int x2, int y2, int mode, boolean ispercent, boolean isproportional){
		EncodedImage image = null;
		
		//PNGEncoded
		PNGEncodedImage encoder = PNGEncodedImage.encode(bitmap);
		byte[] imageBytes = encoder.getData();
		
		image = EncodedImage.createEncodedImage(imageBytes, 0, imageBytes.length);
		
		image = rescalling(image, x2, y2, mode, ispercent, isproportional);
        
        bitmap = image.getBitmap();
		
		return bitmap;
	}
	/*******************************************************************************/
	
	//19-10-2010
	/*******************************************************************************/
	public void ResizeAndSaveExplorerJPGImage(String source, int x2, int y2, int mode, boolean ispercent, boolean isproportional, int quality, boolean ishidden){
		Bitmap bitmap = null;
		EncodedImage image = null;
		FileConnection file = null;
		InputStream in = null;
		
		byte[] data;
		try {
			if(!source.startsWith("file:///")){
				if(source.startsWith("/")){
					source = "file://"+source;
				}
				else{
					source = "file:///"+source;
				}
			}
			
			file = (FileConnection) Connector.open(source, Connector.READ_WRITE);
	        in = file.openInputStream();
	        
	        int length = in.available();
	        if(length>0){
	        	data = new byte[length];
	        	in.read(data);
	        	image = EncodedImage.createEncodedImage(data, 0, data.length);
	        }
	        else{
	        	length = (int) file.fileSize();
	        	
	        	data = new byte[length];
	        	in.read(data);
				image = EncodedImage.createEncodedImage(data, 0, -1);
	        }
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Error streaming "+e);
		} finally{
			try {
		        if (in != null) {
		            in.close();
		        }
		        if (file != null && file.isOpen()) {
		        	file.close();
		        }
			}catch (IOException ioe) {
				System.out.println("Error closing streaming "+ioe);
			}
		}
		
		image = rescalling(image, x2, y2, mode, ispercent, isproportional);
        
        bitmap = image.getBitmap();
        
        JPEGEncodedImage jpegImg = JPEGEncodedImage.encode(bitmap, quality); //quality
    	byte[] imgData = jpegImg.getData();
    	
    	//overwriting original jpg image
    	try{
	    	/*StringBuffer sbFOut = new StringBuffer();
	    	sbFOut.append(source); //(fname.substring(0, fname.indexOf(".jpg"))).append("_2.jpg");
*/	    	FileConnection fcOut = (FileConnection) Connector.open(source,Connector.READ_WRITE);
	    	if(fcOut.exists()) {
	    		fcOut.delete(); // XXX: WARNING
	    	}
			fcOut.create();
	    	if(fcOut.canWrite()) {
	        	OutputStream os = fcOut.openOutputStream();
	        	os.write(imgData);
	        	os.flush();
	        	os.close();
	        	System.out.println("Resizing+Overwriting image complete");
	    	}
	    	fcOut.close();
    	}catch (IOException e) {
    		System.out.println("Error writing "+e);
		}
    	
    	//set atribute
    	if(ishidden){
    		setHiddenAttribute(source);
    	}
	}
	/*******************************************************************************/
	
	/*******************************************************************************/
	public EncodedImage connectNow(String sUrl, int x2, int y2, int mode, boolean ispercent, boolean isproportional){
		EncodedImage rawimage = null;
		InputStream is = null; 
		try {
			if(!DeviceInfo.isSimulator()) sUrl=sUrl.concat(Properties._APN_BB);
			HttpConnection c = (HttpConnection)Connector.open(sUrl, Connector.READ_WRITE, true);
			
            is = c.openInputStream();    
            
            //06-04-2011
            //supporting for redirecting mechanism
            if(c.getResponseCode() == HttpConnection.HTTP_TEMP_REDIRECT  || c.getResponseCode() == HttpConnection.HTTP_MOVED_TEMP || c.getResponseCode() == HttpConnection.HTTP_MOVED_PERM){
            	//redirecting
            	connectNow(c.getHeaderField("location").trim(), x2, y2, mode, ispercent, isproportional);
            	//System.out.println("redirecting>>>"+c.getHeaderField("location").trim());
            }
            else{
	            int len = (int)c.getLength(); //default
	            
	            int clen = 0;
	            try{
	            	clen = Integer.parseInt(c.getHeaderField("Content-Length"));
	            }catch(Exception x){ clen = 0; /*System.out.println("Content-Length not supported on this image");*/ }	
	            if(clen>0) len = clen;
	            
	            byte[] data; //data image
	            
	            if (len > 0) {
	                int actual = 0;
	                int bytesread = 0 ;
	                data = new byte[len];
	                while ((bytesread != len) && (actual != -1)) {
	                    actual = is.read(data, bytesread, len - bytesread);
	                    bytesread += actual;
	                }
	            }
	            else{
	            	//@23-12-2010
	            	int ch;
	                ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
	                while ((ch = is.read()) != -1) {
	                	bytestream.write(ch);
	                }
	                data = bytestream.toByteArray();
	                bytestream.close();
	            }
	            
	            //System.out.println("debug length>>>"+len+":"+data.length);
	            
	            //set
	            setImageData(data);
	            
	            //get encoded
	            EncodedImage image = null;
	            try{
	            	//auto mode
	            	image = EncodedImage.createEncodedImage(data,0,data.length); 
	            }catch(IllegalArgumentException x){
	            	System.out.println("fail encoded using default mechanism, try using MIME>>>"+x.getMessage());
	            	//try JPG
	            	boolean isstop = false;
	            	try{
	            		image = EncodedImage.createEncodedImage(data,0,data.length,"image/jpg"); 
	            		isstop = true;
	            	}catch(IllegalArgumentException a){
	            		System.out.println("JPG MIME failed>>>"+a.getMessage());
	            		isstop = false;
	            	}
	            	
	            	//try JPEG
	            	if(!isstop){
		            	try{
		            		image = EncodedImage.createEncodedImage(data,0,data.length,"image/jpeg"); 
		            		isstop = true;
		            	}catch(IllegalArgumentException a){
		            		System.out.println("JPEG MIME failed>>>"+a.getMessage());
		            		isstop = false;
		            	}
	            	}
	            	
	            	//try GIF
	            	if(!isstop){
		            	try{
		            		image = EncodedImage.createEncodedImage(data,0,data.length,"image/gif"); 
		            		isstop = true;
		            	}catch(IllegalArgumentException a){
		            		System.out.println("GIF MIME failed>>>"+a.getMessage());
		            		isstop = false;
		            	}
	            	}
	            	
	            	//try BMP
	            	if(!isstop){
		            	try{
		            		image = EncodedImage.createEncodedImage(data,0,data.length,"image/bmp"); 
		            		isstop = true;
		            	}catch(Exception a){
		            		System.out.println("BMP MIME failed>>>"+a);
		            		isstop = false;
		            	}
	            	}
	            }
	            
	            //check rescalling
	            if(image!=null) rawimage = rescalling(image, x2, y2, mode, ispercent, isproportional);
            }
	            
            //close all connection
	        is.close();
	        c.close();
            
        } catch (Exception ex) {
        	System.out.println("Error while streaming image: "+ex);
        }  

        return rawimage;
	}
	/*******************************************************************************/

	//scalling image
	public EncodedImage rescalling(EncodedImage image, int x2, int y2, int mode, boolean ispercent, boolean isproportional){
		if(ispercent){
			x2 = (x2*image.getWidth())/100;
			y2 = (y2*image.getHeight())/100;
		}
		
        if(mode==0){
        	//no scalling
        }else if((mode==1) && (image.getWidth()>x2 || image.getHeight()>y2)){
        	//scalling down
        	image = scaler.autoScalling(image, x2, y2, isproportional);
        }else if((mode==2) && (image.getWidth()<x2 || image.getHeight()<y2)){
        	//scalling up
        	image = scaler.autoScalling(image, x2, y2, isproportional);
        }
        else if((mode==3)){
        	//30-08-2010
        	//scalling up/down + propoptional/un-proportional + percentage/pixel
        	image = scaler.autoScalling(image, x2, y2, isproportional);
        }
        
        return image;
	}
	
	//focusable or not**************************************
	public BitmapField isfocusable(Bitmap bmp, boolean isfocus){
		BitmapField img = null;
		if(isfocus){
        	img = new BitmapField(bmp, Field.FOCUSABLE);
        }else{
        	img = new BitmapField(bmp, Field.NON_FOCUSABLE);
        }
		
		return img;
	}
	//*******************************************************************************
	
	//19-10-2010
	//hidden
	void setHiddenAttribute(String path){
		if(path!=null){
			FileConnection file = null;
			InputStream in = null;
			
			try {
				if(path.startsWith("/")){ path = "file://"+path; }
				else if(path.startsWith("file:///")){ /*dont do any change*/ }
				else{ path = "file:///"+path; }
				
				file = (FileConnection) Connector.open(path, Connector.READ_WRITE);
		        in = file.openInputStream();
		        
		        //change image attribute as HIDDEN
		        file.setHidden(true);
		        
			}catch (IOException e) {
				e.printStackTrace();
				System.out.println("Error streaming "+e);
			} finally{
				//24-08-2010
				try {
			        if (in != null) {
			            in.close();
			        }
			        if (file != null && file.isOpen()) {
			        	file.close();
			        }
				}catch (IOException ioe) {
					System.out.println("Error closing streaming "+ioe);
				}
			}
		}
	}
	
	
	//new method for accuracy sprite, painting start from xOrigin & yOrigin
	public void drawTexture(Bitmap bmp, Graphics g, int x1, int x2, int x3, int x4, int y1, int y2, int y3, int y4, int xOrigin, int yOrigin) {
		int[] xPts = new int[] { x1, x2, x3, x4 };
		int[] yPts = new int[] { y1, y2, y3, y3 };
		byte[] pointTypes = new byte[] { Graphics.CURVEDPATH_END_POINT, Graphics.CURVEDPATH_END_POINT, Graphics.CURVEDPATH_END_POINT, Graphics.CURVEDPATH_END_POINT };
		int dux = Fixed32.toFP(1), dvx = Fixed32.toFP(0), duy = Fixed32.toFP(0), dvy = Fixed32.toFP(1);
		g.drawTexturedPath(xPts, yPts, pointTypes, null, x1, y1, dux, dvx, duy, dvy, bmp); //g.drawTexturedPath(xPts, yPts, pointTypes, null, 0, 0, dux, dvx, duy, dvy, bmp);
	}
	
	//special case*****************************************************************
	public BitmapField getEmptyImage(String source, boolean focus){
		BitmapField empty = null;
		Bitmap bmp = Bitmap.getBitmapResource(source);
		//check focus
		empty = isfocusable(bmp, focus);
		
		return empty;
	}		
	
	public Bitmap getEmptyBitmap(String source){
		return Bitmap.getBitmapResource(source);
	}
	//*******************************************************************************
	
	
	public byte[] getImageData() {
		return imageData;
	}

	public void setImageData(byte[] imageData) {
		this.imageData = imageData;
	}
	
	
	//mode 0 -> persistent
	private Bitmap mode0(String source, int x2, int y2, int mode, boolean ispercent, boolean isproportional){
		Bitmap selected = null;
		
		//PERSISTENT ACTIVE
		long imageID = StringUtilities.stringHashToLong(source);
		ImageStore store = new ImageStore(imageID);
		ImagePersistable persist = new ImagePersistable();
		
		if(store.getVectorOfStoredData().size()>0){
			persist = (ImagePersistable) store.getVectorOfStoredData().elementAt(0);
			if(persist.getImageString()!=null){
				selected = getBitmapFromByte(persist.getImageString().getBytes(), x2, y2, mode, ispercent, isproportional);
			}
		}	
		else{
			EncodedImage image = connectNow(source, 0, 0, 0, false, true);
			
			if(image!=null){
				Vector raw = new Vector();
				persist.set_imageID(imageID);
				persist.setImageString(new String(getImageData()));
				
				//isi vector & persistent
				raw.addElement(persist);
				store.saveDataVector(raw);
			}
			
			image = rescalling(image, x2, y2, mode, ispercent, isproportional);
			
			selected = image.getBitmap();
		}
		
		return selected;
	}
	
	//mode 1 -> runtime store
	private Bitmap mode1(String source, int x2, int y2, int mode, boolean ispercent, boolean isproportional){
		Bitmap selected = null;
		
		//RUNTIME_STORE ACTIVE
		Hashtable storedImage;
		Object o = RuntimeStoreSystem.getData("images");
		if (o instanceof Hashtable) {
			storedImage = (Hashtable) o;
		} 
		else{
			storedImage = new Hashtable();
			RuntimeStoreSystem.setData("images", storedImage);
		}
		
		if(storedImage instanceof Hashtable && storedImage.containsKey(source)){
			EncodedImage image = (EncodedImage) storedImage.get(source);
			image = rescalling(image, x2, y2, mode, ispercent, isproportional);
			selected = image.getBitmap();
		}
		else{
			EncodedImage image = connectNow(source, 0, 0, 0, false, true);
			
			if(image!=null){
				Hashtable saveImage = (Hashtable) RuntimeStoreSystem.getData("images");
				saveImage.put(source, image);
			}
			
			image = rescalling(image, x2, y2, mode, ispercent, isproportional);
			selected = image.getBitmap();
		}
		
		return selected;
	}
	
	//mode 2 -> fetching locally
	private Bitmap mode2(String source, int x2, int y2, int mode, boolean ispercent, boolean isproportional){
		Bitmap selected = null;

		//FETCHING LOCALLY
		long imageID = StringUtilities.stringHashToLong(source);
		
		if(new ImageManager().isImageFetched(imageID) && new ImageManager().decodeAndGetImage(imageID)!=null){
			//sudah di fetched
			byte[] data = new ImageManager().decodeAndGetImage(imageID);
			
			EncodedImage image = EncodedImage.createEncodedImage(data, 0, data.length);
			
			image = rescalling(image, x2, y2, mode, ispercent, isproportional);
			selected = image.getBitmap();
		}
		else{
			EncodedImage image = connectNow(source, 0, 0, 0, false, true);
			
			if(image!=null){
				//save ke image manager
				new ImageManager().manageImageFetching(imageID, getImageData());
				//**************************************************************************
			}
			
			image = rescalling(image, x2, y2, mode, ispercent, isproportional);
			selected = image.getBitmap();
		}
		
		return selected;
	}
	
	public boolean isImageExistAndAvailable(String source, int mode){
		boolean result = false;
		
		if(mode==0){
			//PERSISTENT
			long imageID = StringUtilities.stringHashToLong(source);
			ImageStore store = new ImageStore(imageID);
			ImagePersistable persist = new ImagePersistable();
			
			if(store.getVectorOfStoredData().size()>0){
				persist = (ImagePersistable) store.getVectorOfStoredData().elementAt(0);
				if(persist.getImageString()!=null){
					result = true;
				}
			}
		}
		else if(mode==1){
			//RUNTIME STORE
			Hashtable storedImage;
			Object o = RuntimeStoreSystem.getData("images");
			if (o instanceof Hashtable) {
				storedImage = (Hashtable) o;
			} 
			else{
				storedImage = new Hashtable();
				RuntimeStoreSystem.setData("images", storedImage);
			}
			
			if(storedImage instanceof Hashtable && storedImage.containsKey(source)){
				result = true;
			}
		}
		else if(mode==2){
			//LOCAL
			long imageID = StringUtilities.stringHashToLong(source);
			if(new ImageManager().isImageFetched(imageID) && new ImageManager().decodeAndGetImage(imageID)!=null){
				result = true;
			}
		}
		
		return result;
	}
}
