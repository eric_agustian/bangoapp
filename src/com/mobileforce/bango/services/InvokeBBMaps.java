package com.mobileforce.bango.services;

import com.mobileforce.bango.manager.ScreenManager;

import net.rim.blackberry.api.invoke.Invoke;
import net.rim.blackberry.api.invoke.MapsArguments;

public class InvokeBBMaps {
	public InvokeBBMaps(String lat_1, String lon_1, String lat_2, String lon_2, String title1, String title2, String desc1, String desc2){
		
		//titik awal
		int lat1 = (int) (Double.parseDouble(lat_1) * 100000);
		int lon1 = (int) (Double.parseDouble(lon_2) * 100000);
		// titik akhir
		int lat2 = (int) (Double.parseDouble(lat_2) * 100000);
		int lon2 = (int) (Double.parseDouble(lon_2) * 100000);

		String label1 = title1;
		String label2 = title2;
		String description1 = desc1;
		String description2 = desc2;// + "(" + lat_2 + ", " + lon2 + ")";

		StringBuffer stringBuffer = new StringBuffer("");
		// opsi 1
//		stringBuffer.append("<location-document>");
//		stringBuffer.append("<lbs id='Rute'>");
//		stringBuffer.append("<getRoute>");
//		stringBuffer.append("<location x='" + lon1 + "' y='" + lat1 + "' label='" + label1 + "' description='" + description1 + "' />"); // ini utk keperluan routing navigasi
//		stringBuffer.append("<location x='" + lon2 + "' y='" + lat2 + "' label='" + label2 + "' description='" + description2 + "' />"); // ini utk keperluan routing navigasi
//		stringBuffer.append("</getRoute>");
//		stringBuffer.append("</lbs>");
//		stringBuffer.append("</location-document>");
		
		// opsi 2
		stringBuffer.append("<location-document>");
		stringBuffer.append("<location lon='" + lon1 + "' lat='" + lat1 + "' label='" + label1 + "' description='" + description1 + "' />"); // ini utk tampilin icon poi
		stringBuffer.append("<location lon='" + lon2 + "' lat='" + lat2 + "' label='" + label2 + "' description='" + description2 + "' />"); // ini utk tampilin icon poi
		stringBuffer.append("<getRoute>");
		stringBuffer.append("<location lon='" + lon1 + "' lat='" + lat1+"' label='" + label1 + "' description='" + description1 + "' />"); //ini utk keperluan routing navigasi
		stringBuffer.append("<location lon='" + lon2 + "' lat='" + lat2+"' label='" + label2 + "' description='" + description2 + "' />"); //ini utk keperluan routing navigasi
		stringBuffer.append("</getRoute>");
		stringBuffer.append("</location-document>"); 
		
//		System.out.println("route>>>> " + stringBuffer.toString());

		try {
			Invoke.invokeApplication(Invoke.APP_TYPE_MAPS,
					new MapsArguments(MapsArguments.ARG_LOCATION_DOCUMENT,
							stringBuffer.toString()));
		} catch (Exception m) {
			System.out.println("Error while invoking maps: " + m);
			ScreenManager.showDialogTypeScreen("Error while invoking maps: " + m);
		}
		
	}
}
