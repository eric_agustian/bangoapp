package com.mobileforce.bango.services;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import com.mobileforce.bango.UiApp;
import com.mobileforce.bango.storage.parser.GeneralJSONParser;
import com.mobileforce.bango.storage.persistable.BodyPersistable3;
import com.mobileforce.bango.storage.query.NotificationStatusStore;
import com.mobileforce.bango.utility.BBNotification;
import com.mobileforce.bango.utility.ConnectionChecker;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.i18n.DateFormat;
import net.rim.device.api.system.Application;
import net.rim.device.api.system.GlobalEventListener;
import net.rim.device.api.system.SystemListener;
import net.rim.device.api.util.DateTimeUtilities;

public class NotificationApplicationManager extends Application implements GlobalEventListener, SystemListener{
	private static Timer time = new Timer();
	private BackgroundPooling pooling;
	private boolean isrunning = false;

	public NotificationApplicationManager() {
		Application.getApplication().addGlobalEventListener(this);
		
		startApplication();
	}

	public void startApplication() {
		invokeLater(new Runnable() {
			public void run() {
				//define a pooling
				pooling = new BackgroundPooling();
				
				//define a schedule
		    	time.schedule(pooling, Calendar.getInstance().getTime(), Properties._NOTIFICATION_CHECK_INTERVAL);
		    	
		    	isrunning = true;
			}
		});
	}
	
	public void stopExecuteMe(){
		pooling.stopPolling();
		isrunning = false;
	}
	
	public void startExecuteMe(){
		stopExecuteMe();
		startApplication();
	}
	
	public boolean isBackgroundONRunning(){
		return this.isrunning;
	}

	//background pooling (looping)
	class BackgroundPooling extends TimerTask {
		public BackgroundPooling() {
		}

		public void run() {
			try {
				if (SettingManager.isNotificationEnabled() && UserInfoManager.getUserID().length() > 0) {
					System.out.println("[Executing Notification Thread]");
					// only start when notification setting enabled
					new BackgroundDataLoad(Properties._API_LIST_NOTIFICATION, "");
				} else {
					System.out.println("[Non-Executing Notification Thread]");
				}
			} catch (Exception x) {
				System.out.println("error while running notification thread " + x);
			}
		}

		public void stopPolling() {
			try {
				cancel();
			} catch (Exception x) {
				System.out.println("error while stop notification thread " + x);
			}
		}
	}
		
	class BackgroundDataLoad extends Thread {
		private BackgroundDataLoad _self;
		private String _url, _parameter;
		
		private GeneralJSONParser load;
	
		public BackgroundDataLoad(String url, String parameter){
			_url = url;
			_parameter = parameter;
			
			_self = this;
			
			start();
		}
		
		public void run() {
			try{
				ConnectionChecker _check = new ConnectionChecker();
				_check.Checking();
				if(!_check.isAvailable()){
					System.out.println("<background request stopped, connection not available>");
					return;
				}
				//-----------------------------------------------------------
				load = new GeneralJSONParser(Properties._CONN_TYPE_NOTIFICATION);
				load.generateFromURL(_url, _parameter, Properties._HTTP_METHOD_GET);
			}catch(Exception x){
				System.out.println("<BackgroundThreadProcess> "+x);
			}finally{
				if(Properties._HTTP_ERROR_CODE==0 && load.get_general_status().equalsIgnoreCase("OK")){
					nextAction(load.get_vector());
				}
				else{
					System.out.println("<request fail> "+load.get_general_message());
				}
				
				closeMe();
			}
		}
		
		void closeMe(){
			System.out.println("<Trying to close BackgroundThreadProcess>");
			try{
				if(_self.isAlive()){
					_self.interrupt();
					System.out.println("<BackgroundThreadProcess Closed>");
				}
			}catch(Exception x){ 
				System.out.println("<Unable to close BackgroundThreadProcess>"+x);
			}
		}
	}
	
	private void nextAction(Vector vector){
		if(vector.size() > 0){
//			Vector header = (Vector) vector.elementAt(0);
			Vector body = (Vector) vector.elementAt(1);
			
			// update & check if there is new
			boolean update = false;
			for(int s=0; s<body.size(); s++){
				BodyPersistable3 persistable = (BodyPersistable3) body.elementAt(s);
				
				if(NotificationStatusStore.isNotifIDNew(persistable.get_notifid())){
					update = true;
				}
			}
			
			if(update){
				System.out.println("<NotificationStatus Update Available>");
				// baca settingan notifikasi
				if(SettingManager.isNotificationEnabled() && UserInfoManager.getUserID().length() > 0){
					NotificationStatusStore.updateNotificationStatus(true);
					setAllNotificationON();
				}
			}
		}
	}
	
	private void setAllNotificationON(){
		if(UiApp.getApplication().isForeground()){
			try{
				BBNotification.startBlinking();
			}catch(Exception x) {
				System.out.println("<Cannot update notification from inside NotificationApplication>"+x.getMessage());
			}
		}
		else{
			BBNotification.startAllNotification();
			UiApp.setIconUpdate();
		}
	}
	
	public void eventOccurred(long guid, int data0, int data1, Object object0,
			Object object1) {
		if (guid == DateFormat.GUID_DATE_FORMAT_CHANGED
				|| guid == DateTimeUtilities.GUID_DATE_CHANGED
				|| guid == DateTimeUtilities.GUID_TIMEZONE_CHANGED) {
			stopExecuteMe();
			startApplication();
		}
//		if (guid == GLOBAL_CLOSE_ALARM) {
//			int alarmType = appFactory.getDatabaseFactory().getNextAlarmType().intValue();
//			int typeAlarm = 0;
//			if (alarmType >= 0 && alarmType < appFactory.getDatabaseFactory().getAlarmStatus().length) {
//				typeAlarm = appFactory.getDatabaseFactory().getAlarmStatus()[alarmType];	
//			}
//			CloseAlarmPopup popup = new CloseAlarmPopup(appFactory, alarmType);
//			if(!appFactory.getDatabaseFactory().isCheckFirstTime() && typeAlarm != ALARMSTATUS_OFF){
//				Ui.getUiEngine().pushGlobalScreen(popup, 1, UiEngine.GLOBAL_MODAL);
//			}
//		}

	}

	public void batteryGood() {
	}

	public void batteryLow() {
	}

	public void batteryStatusChange(int status) {
	}

	public void powerOff() {
	}

	public void powerUp() {
	}
}
