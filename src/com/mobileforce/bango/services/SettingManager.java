package com.mobileforce.bango.services;

import java.util.Vector;

import com.mobileforce.bango.storage.persistable.SettingPersistable;
import com.mobileforce.bango.storage.query.GenericStore;
import com.mobileforce.bango.utility.Properties;

public class SettingManager {
	public SettingManager(){}
	
	public static boolean isNotificationEnabled(){
		SettingPersistable data = getSetting();
		
		if(data == null){
			return true; // default activated
		}
		else{
			return data.is_notification();
		}
	}
	
	
	public static void setNotificationSetting(boolean notification){
		try{
//			//create & update
			Vector v = new Vector();
			SettingPersistable sp = new SettingPersistable();
			sp.set_notification(notification);
			v.addElement(sp);
			
			//save
			saveVector(Properties._SETTING_ID, v);
		}catch(Exception x){ System.out.println("unable to update & save Setting >>> "+x.toString()); }
	}
	
	public static void deleteSetting(){
		deleteVector(Properties._SETTING_ID);
	}
	
	//get vector
	private static Vector getVector(long id) {
		Vector data = new GenericStore(id).getVectorOfStoredData();
		return data.size() == 0 ? null : data;
	}

	// save vector
	private static void saveVector(long id, Vector v) {
		new GenericStore(id).saveDataVector(v);
	}

	// delete vector
	private static void deleteVector(long id) {
		try {
			new GenericStore(id).deleteStoredData(id);
			System.out.println("success deleting SettingManager id>>> " + id);
		} catch (Exception x) {
			System.out.println("Fail deleting SettingManager id>>> " + x.toString()
					+ " [" + id + "]");
		}
	}
	
	// get persistable
	public static SettingPersistable getSetting(){
		Vector data = getVector(Properties._SETTING_ID);
		
		if(data == null){
			return null;
		}
		else{
			return (SettingPersistable) data.elementAt(0);
		}
	}
}
