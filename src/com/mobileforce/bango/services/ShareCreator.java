package com.mobileforce.bango.services;

import java.util.Vector;

import com.mobileforce.bango.configuration.ModelConfig;
import com.mobileforce.bango.utility.DeviceProperties;
import com.mobileforce.bango.utility.GStringUtil;
import com.mobileforce.bango.utility.ImageScaler;
import com.mobileforce.bango.utility.ImageUtil;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;

public class ShareCreator {
	private static Bitmap bg = ModelConfig.getConfig().getShareBG();
	private static Bitmap logo = ModelConfig.getConfig().getShareLogo();
	private static Bitmap star = ModelConfig.getConfig().getShareStar();
	
	private static Font font1 = DeviceProperties.getDefaultApplicationFont(Font.PLAIN, 20);
	private static Font font2 = DeviceProperties.getDefaultApplicationFont(Font.PLAIN, 30);
	
	public ShareCreator(){}
	
	public static Bitmap getShareImage(Bitmap preview, String text_1, String title, String text_2, int rating){
		Bitmap result = new Bitmap(bg.getWidth(), bg.getHeight());
		int[] argb = new int[bg.getWidth() * bg.getHeight()];
		bg.getARGB(argb, 0, bg.getWidth(), 0, 0, bg.getWidth(), bg.getHeight());
		result.setARGB(argb, 0, bg.getWidth(), 0, 0, bg.getWidth(), bg.getHeight());
		//paint bitmap
	    Graphics g = Graphics.create(result);
		
		// PHASE 1: draw preview
		if(preview != null){
			if(preview.getWidth() != bg.getWidth() && preview.getHeight() != bg.getHeight()){
				//resize it
				preview = ImageScaler.resizeBitmapARGB(preview, bg.getWidth(), bg.getHeight());
			}
		}
		else{
			preview = ImageScaler.resizeBitmapARGB(ModelConfig.getConfig().getBackgroundSplash(), bg.getWidth(), bg.getHeight());
		}
		g.drawBitmap(-150, 0, preview.getWidth(), preview.getHeight(), preview, 0, 0);
		g.drawBitmap(150, 0, bg.getWidth(), bg.getHeight(), bg, 0, 0);
		
		// PHASE 2: draw header logo
		g.drawBitmap(150, 0, logo.getWidth(), logo.getHeight(), logo, 0, 0);
		
		int posY = logo.getHeight();
		g.setColor(Color.WHITE);
		// PHASE 3: draw text_1
		Vector t1 = GStringUtil.parseMessage(text_1, font1, 20, bg.getWidth()-170);
		for(int n=0; n<t1.size(); n++){
			String txt = (String) t1.elementAt(n);
			g.setFont(font1);
			g.drawText(txt, 160, posY);
			
			posY += font1.getHeight();
		}
		posY += 10;
		
		// PHASE 4: draw title
		Vector t2 = GStringUtil.parseMessage(title, font2, 30, bg.getWidth()-170);
		for(int n=0; n<t2.size(); n++){
			String txt = (String) t2.elementAt(n);
			g.setFont(font2);
			g.drawText(txt, 160, posY);
			
			posY += font2.getHeight();
		}
		posY += 10;
		
		// PHASE 5: draw rating star
		int starX = 160;
		for(int s=0; s<rating; s++){
			g.drawBitmap(starX, posY, star.getWidth(), star.getHeight(), star, 0, 0);
			starX += star.getWidth();
		}
		posY += star.getHeight() + 10;
		
		// PHASE 6: draw text_2 if available
		Vector t3 = GStringUtil.parseMessage(text_2, font2, 20, bg.getWidth()-170);
		for(int n=0; n<t3.size(); n++){
			String txt = (String) t3.elementAt(n);
			g.setFont(font1);
			g.drawText(txt, 160, posY);
			
			posY += font1.getHeight();
		}
		posY += 10;
		
		return result;
	}
	
	public static Bitmap getModifiedSlide(Bitmap preview){
		Bitmap background = ModelConfig.getConfig().getHeaderBgRepeat();
		Bitmap menu = ModelConfig.getConfig().getIconMenu();
		
		int w = menu.getWidth()+(ModelConfig.getConfig().getValue10px()*2);
		int h = background.getHeight();
		
		Bitmap menuBar = new Bitmap(w, h);
//		int[] argb = new int[result.getWidth() * result.getHeight()];
//		bg.getARGB(argb, 0, bg.getWidth(), 0, 0, bg.getWidth(), bg.getHeight());
//		result.setARGB(argb, 0, bg.getWidth(), 0, 0, bg.getWidth(), bg.getHeight());
		//paint bitmap
	    Graphics g = Graphics.create(menuBar);
		
		// PHASE 1: draw background
	    ImageUtil.drawSpriteTexture(bg, g, 0, w, w, 0, 0, 0, h, h);
		
		// PHASE 2: draw menu
		g.drawBitmap(ModelConfig.getConfig().getValue10px(), (h-menu.getHeight())/2, menu.getWidth(), menu.getHeight(), menu, 0, 0);
		
		// PHASE 3: combine with result
		Bitmap result = new Bitmap(preview.getWidth(), preview.getHeight());
		int[] argb = new int[result.getWidth() * result.getHeight()];
		preview.getARGB(argb, 0, preview.getWidth(), 0, 0, preview.getWidth(), preview.getHeight());
		result.setARGB(argb, 0, preview.getWidth(), 0, 0, preview.getWidth(), preview.getHeight());
		
		Graphics gc = Graphics.create(result);
		gc.drawBitmap(preview.getWidth()-menuBar.getWidth(), 0, menuBar.getWidth(), menuBar.getHeight(), menuBar, 0, 0);
		
		return result;
	}
}
