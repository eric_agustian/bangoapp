package com.mobileforce.bango.services;

import java.util.Random;
import java.util.Vector;

import net.rim.device.api.system.Bitmap;

import com.mobileforce.bango.manager.ScreenManager;
import com.mobileforce.bango.screen.component.SplashPromoPopupScreen;
import com.mobileforce.bango.storage.persistable.PromoPersistable;
import com.mobileforce.bango.storage.persistable.PromoSettingPersistable;
import com.mobileforce.bango.storage.query.GenericStore;
import com.mobileforce.bango.utility.GStringUtil;
import com.mobileforce.bango.utility.Properties;

public class SplashPromoManager {
	private static SplashPromoManager _self;
	public SplashPromoManager(){
		_self = this;
	}
	
	public static void loadSplashPromo(){
		new DataLoader(Properties._API_SPLASH_PROMO, "", Properties._HTTP_METHOD_GET, Properties._CONN_TYPE_SPLASH_PROMO, true, false);
	}
	
	public static void fetchAndShownPromoRandomly(){
		// check if already downloaded
		Vector data = getVector(Properties._SPLASH_PROMO_ID);

		if (data == null) {
			// dont show anything
		} else {
			if(isSplashPromoShown()){
				int random = randInt(0, data.size()-1);
				System.out.println("random splash>>> " + random);
				// fetch this splash
				PromoPersistable promo = getPromo(random);
				
				/*****
				 * check if able to shown or not (start & end date applied)
				 * 
				 * 
				 */
				
				
				String[] imgurl = { promo.get_urlimg() };
				new ImageFetching(_self, imgurl, Properties._CONN_TYPE_SPLASH_PROMO_SINGLE_FETCH);
				
				
				// fetch other splash image
				String[] others = new String[data.size()];
				for(int s=0; s<data.size(); s++){
					if(s == random){
						others[s] = "";
					}
					else{
						others[s] = getPromo(s).get_urlimg();
					}
				}
				new ImageFetching(_self, others, Properties._CONN_TYPE_SPLASH_PROMO_MULTIPLE_FETCH);
			}
		}
		
	}
	
	public static void setSplashPromo(Vector user){
		try{
			//save
			saveVector(Properties._SPLASH_PROMO_ID, user);
		}catch(Exception x){ System.out.println("unable to update & save splash promo>>> "+x.toString()); }
	}
	
	public static void setSplashSetting(boolean set){
		try{
			Vector setting = new Vector();
			PromoSettingPersistable data = new PromoSettingPersistable();
			
			if(set){
				// set
				data.setShown(false);
				data.setDayEnd(GStringUtil.generateTimeStamp("dd"));
				data.setHourEnd(GStringUtil.generateTimeStamp("HH"));
				data.setMinuteEnd(GStringUtil.generateTimeStamp("mm"));
			}
			else{
				// reset
				data.setShown(true);
				data.setDayEnd("");
				data.setHourEnd("");
				data.setMinuteEnd("");
			}
			
			setting.addElement(data);
			
			//save
			saveVector(Properties._SETTING_PROMO_ID, setting);
		}catch(Exception x){ System.out.println("unable to update & save setting splash promo>>> "+x.toString()); }
	}
	
	// get persistable
	public static PromoPersistable getPromo(int index) {
		Vector data = getVector(Properties._SPLASH_PROMO_ID);

		if (data == null) {
			return null;
		} else {
			return (PromoPersistable) data.elementAt(index);
		}
	}
	
	public static boolean isSplashPromoShown(){
		Vector data = getVector(Properties._SETTING_PROMO_ID);
		if (data == null) {
			return true;
		} else {
			PromoSettingPersistable setting =  (PromoSettingPersistable) data.elementAt(0);
			if(setting.isShown()){
				return true;
			}
			else{
				// check expired
				String cday = GStringUtil.generateTimeStamp("dd");
				String chour = GStringUtil.generateTimeStamp("HH");
				String cminute = GStringUtil.generateTimeStamp("mm");
				String endDay = setting.getDayEnd();
				String endHour = setting.getHourEnd();
				String endMinute = setting.getMinuteEnd();
				
				if(!cday.equalsIgnoreCase(endDay)){
					if(Integer.parseInt(chour) > Integer.parseInt(endHour)){
						setSplashSetting(false);
						return true;
					}
					else if(Integer.parseInt(chour) == Integer.parseInt(endHour)){
						if(Integer.parseInt(cminute) > Integer.parseInt(endMinute)){
							setSplashSetting(false);
							return true;
						}
						else{
							return false;
						}
					}
					else{
						return false;
					}
				}
				else{
					return false;
				}
			}
		}
	}
	
	public static void deleteSplashPromo(){
		deleteVector(Properties._SPLASH_PROMO_ID);
	}
	
	//get vector
	private static Vector getVector(long id) {
		Vector data = new GenericStore(id).getVectorOfStoredData();
		return data.size() == 0 ? null : data;
	}

	// save vector
	private static void saveVector(long id, Vector v) {
		new GenericStore(id).saveDataVector(v);
	}

	// delete vector
	private static void deleteVector(long id) {
		try {
			new GenericStore(id).deleteStoredData(id);
			System.out.println("success deleting SplashPromoManager id>>> " + id);
		} catch (Exception x) {
			System.out.println("Fail deleting SplashPromoManager id>>> " + x.toString()
					+ " [" + id + "]");
		}
	}
	
	/**
	 * Returns a psuedo-random number between min and max, inclusive.
	 * The difference between min and max can be at most
	 * <code>Integer.MAX_VALUE - 1</code>.
	 *
	 * @param min Minimim value
	 * @param max Maximim value.  Must be greater than min.
	 * @return Integer between min and max, inclusive.
	 * @see java.util.Random#nextInt(int)
	 */
	public static int randInt(int min, int max) {
	    // Usually this can be a field rather than a method variable
	    Random rand = new Random();

	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
	
	
	public void showSplashPromo(Bitmap promo){
		ScreenManager.showSplashPromoPopup(SplashPromoPopupScreen.showImage(promo));
	}
}
