package com.mobileforce.bango.services;

import impl.rim.com.twitterapime.xauth.ui.BrowserFieldOAuthDialogWrapper;

import java.io.IOException;
import java.util.Vector;

import com.mobileforce.bango.storage.persistable.TwitterPersistable;
import com.mobileforce.bango.storage.query.GenericStore;
import com.mobileforce.bango.utility.Properties;
import com.twitterapime.rest.Credential;
import com.twitterapime.rest.TweetER;
import com.twitterapime.rest.UserAccountManager;
import com.twitterapime.search.LimitExceededException;
import com.twitterapime.search.Tweet;
import com.twitterapime.xauth.Token;
import com.twitterapime.xauth.ui.OAuthDialogListener;
import com.twitterapime.xauth.ui.OAuthDialogWrapper;

import net.rim.device.api.browser.field2.BrowserField;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.MainScreen;


public class TwitterConnector extends MainScreen implements OAuthDialogListener{
	private TwitterConnector _self;
	private final String CONSUMER_KEY = "EJGidkfi7o5C8Tuze2izJd5uV";
	private final String CONSUMER_SECRET = "SHknNmCUydRclqZ4KGW75HOPDHQCQ51W0WYr8lz12xbLL6V8Vm";
	private final String CALLBACK_URL = "http://www.google.co.id/";
	
	private String _type, _text;
	private Bitmap _image;
	
	public TwitterConnector(String type, String text){
		this(type, text, null);
	}
	
	public TwitterConnector(String type, String text, Bitmap image){
		_self = this;
		_type = type;
		_image = image;
		setTitle("Connecting to Twitter");
		
		prepareConnection();
	}
	
	private void prepareConnection(){
		BrowserField myBrowserField = new BrowserField();
		add(myBrowserField);
		
		OAuthDialogWrapper loginWrapper =
			new BrowserFieldOAuthDialogWrapper(
				myBrowserField,
				CONSUMER_KEY, //"<Enter your consumer key here>",
				CONSUMER_SECRET, //"<Enter your consumer secret here>",
				CALLBACK_URL,//"<Enter your callback url here>",
				this);
		
		loginWrapper.login();
	}
	
	public void onAuthorize(Token token) {
		System.out.println("onAuthorize: " + token);
		
//		Credential c = new Credential("conKey", "conSecret", token);
		Credential c = new Credential(CONSUMER_KEY, CONSUMER_SECRET, token);
		UserAccountManager m = UserAccountManager.getInstance(c);
		//_action.pushDialogScreen("Twitter Connect: "+token.getUserId()+"\n\n"+token.getUsername()+"\n\n"+token.getSecret());
		
		TwitterPersistable tp = new TwitterPersistable();
		tp.setTwitter_screen_name(token.getUsername());
		tp.setTwitter_user_id(token.getUserId());
		tp.setTwitter_token(token.getSecret());
		tp.setTwitter_token(token.getToken());
		Vector v = new Vector();
		v.addElement(tp);
		
		new GenericStore(Properties._TW_CONNECT_ID).saveDataVector(v);
		
		try {
			if (m.verifyCredential()) {
				//user authorized!
				
				if(_type.equalsIgnoreCase(Properties._CONN_TYPE_SHARE_TWITTER)){
					// upload image first
					
					TweetER.getInstance(m).post(new Tweet(_text));
				}
				else{
					TweetER.getInstance(m).post(new Tweet(_text));
					closeMe();
				}
			}
		} catch (IOException e) {
		} catch (LimitExceededException e) {
		}	
	}

	public void onAccessDenied(String message) {
		System.out.println("access_denied: " + message);
	}

	public void onFail(String error, String message) {
		System.out.println("error: " + error + " message: " + message);
	}
	
	private void closeMe(){
		//close this twitter connect
		synchronized (UiApplication.getUiApplication().getAppEventLock()) {
			UiApplication.getUiApplication().popScreen(_self);
		}
	}

}
