package com.mobileforce.bango.services;

import java.util.Vector;

import com.mobileforce.bango.storage.persistable.UserInfoPersistable;
import com.mobileforce.bango.storage.query.GenericStore;
import com.mobileforce.bango.utility.Properties;

/*
 * Fungsi dari class ini adalah untuk menangani Session ID yang selalu berubah, disesuaikan dengan kondisi pada backend
 * 
 */
public class UserInfoManager {
	public UserInfoManager(){}
	
	public static String getUserID(){
		UserInfoPersistable data = getUserInfo();
		
		if(data == null){
			return "";
		}
		else{
			return data.get_UserID();
		}
	}
	
	public static String getSessionID(){
		UserInfoPersistable data = getUserInfo();
		
		if(data == null){
			return "";
		}
		else{
			return data.get_SessionID();
		}
	}
	
	/*
	 * Session ID for Header
	 */
	public static String getUserSessionID(){
		UserInfoPersistable data = getUserInfo();
		
		if(data == null){
			return "";
		}
		else{
			try{
				return data.get_UserID() + ";" + data.get_SessionID();
			}catch(Exception x){ 
				System.out.println("unable to get username combined with session id >>> "+x.toString()); 
				return "";
			}
		}
	}
	
	public static void setUserInfo(Vector user){
		try{
//			//create & update
//			Vector v = new Vector();
//			UserInfoPersistable sp = new UserInfoPersistable();
//			sp.set_session_id(ID);
//			v.addElement(sp);
			
			//save
			saveVector(Properties._USER_INFO_ID, user);
		}catch(Exception x){ System.out.println("unable to update & save UserInfo >>> "+x.toString()); }
	}
	
	public static void deleteUserInfo(){
		deleteVector(Properties._USER_INFO_ID);
	}
	
	//get vector
	private static Vector getVector(long id) {
		Vector data = new GenericStore(id).getVectorOfStoredData();
		return data.size() == 0 ? null : data;
	}

	// save vector
	private static void saveVector(long id, Vector v) {
		new GenericStore(id).saveDataVector(v);
	}

	// delete vector
	private static void deleteVector(long id) {
		try {
			new GenericStore(id).deleteStoredData(id);
			System.out.println("success deleting UserInfoManager id>>> " + id);
		} catch (Exception x) {
			System.out.println("Fail deleting UserInfoManager id>>> " + x.toString()
					+ " [" + id + "]");
		}
	}
	
	// get persistable
	private static UserInfoPersistable getUserInfo(){
		Vector data = getVector(Properties._USER_INFO_ID);
		
		if(data == null){
			return null;
		}
		else{
			return (UserInfoPersistable) data.elementAt(0);
		}
	}
}
