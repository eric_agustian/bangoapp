package com.mobileforce.bango.services;

import java.util.Vector;

import com.mobileforce.bango.storage.persistable.ProfilePersistable;
import com.mobileforce.bango.storage.query.GenericStore;
import com.mobileforce.bango.utility.Properties;

public class UserProfileManager {
	public UserProfileManager(){}
	
	
	public static void setUserProfile(Vector user){
		try{
			//save
			saveVector(Properties._USER_PROFILE_ID, user);
		}catch(Exception x){ System.out.println("unable to update & save UserProfile >>> "+x.toString()); }
	}
	
	// get persistable
	public static ProfilePersistable getUserProfile() {
		Vector data = getVector(Properties._USER_PROFILE_ID);

		if (data == null) {
			return null;
		} else {
			return (ProfilePersistable) data.elementAt(0);
		}
	}
	
	public static void deleteUserProfile(){
		deleteVector(Properties._USER_PROFILE_ID);
	}
	
	//get vector
	private static Vector getVector(long id) {
		Vector data = new GenericStore(id).getVectorOfStoredData();
		return data.size() == 0 ? null : data;
	}

	// save vector
	private static void saveVector(long id, Vector v) {
		new GenericStore(id).saveDataVector(v);
	}

	// delete vector
	private static void deleteVector(long id) {
		try {
			new GenericStore(id).deleteStoredData(id);
			System.out.println("success deleting UserProfileManager id>>> " + id);
		} catch (Exception x) {
			System.out.println("Fail deleting UserProfileManager id>>> " + x.toString()
					+ " [" + id + "]");
		}
	}
}
