package com.mobileforce.bango.services.callback;

import java.util.Vector;

public interface ButtonPressedCallback {

	public void onPressed(String type);
	
	public void onPressedValue(Vector value, String type);
}
