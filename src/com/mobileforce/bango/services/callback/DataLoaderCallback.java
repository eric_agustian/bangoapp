package com.mobileforce.bango.services.callback;

import java.util.Vector;

public interface DataLoaderCallback {

	public void onRequestCompleted(Vector vector, String type);
}
