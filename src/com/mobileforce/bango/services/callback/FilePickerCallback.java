package com.mobileforce.bango.services.callback;

public interface FilePickerCallback {

	public void onPicked(String path);
}
