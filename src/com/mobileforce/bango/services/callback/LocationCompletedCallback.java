package com.mobileforce.bango.services.callback;

public interface LocationCompletedCallback {

	public void onLocationCompleted();
}
