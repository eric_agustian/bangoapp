package com.mobileforce.bango.services.callback;

import java.util.Vector;

public interface OtherScreenCallback {

	public void onScreenCallback(String result, String type);
	
	public void onScreenCallbackValue(Vector value, String type);
}
