package com.mobileforce.bango.services.callback;

public interface SelectionCallback {

	public void onSelected(String type);
}
