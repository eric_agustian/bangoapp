package com.mobileforce.bango.storage.parser;

import java.util.Vector;

import com.mobileforce.bango.storage.persistable.BadgesPersistable;
import com.mobileforce.bango.storage.persistable.BodyPersistable;
import com.mobileforce.bango.storage.persistable.BodyPersistable2;
import com.mobileforce.bango.storage.persistable.BodyPersistable3;
import com.mobileforce.bango.storage.persistable.BodyPersistable4;
import com.mobileforce.bango.storage.persistable.DetailResepPersistable;
import com.mobileforce.bango.storage.persistable.DetailStorePersistable;
import com.mobileforce.bango.storage.persistable.GeocodingPersistable;
import com.mobileforce.bango.storage.persistable.HeaderPersistable;
import com.mobileforce.bango.storage.persistable.ProfilePersistable;
import com.mobileforce.bango.storage.persistable.PromoPersistable;
import com.mobileforce.bango.storage.persistable.ReviewsPersistable;
import com.mobileforce.bango.storage.persistable.UserInfoPersistable;
import com.mobileforce.bango.utility.JSONCallBack;
import com.mobileforce.bango.utility.Properties;
import com.mobileforce.bango.utility.network.HTTPConnector;
import com.mobileforce.bango.utility.network.HTTPMultiPartConnector;
import com.mobileforce.bango.utility.network.HTTPSConnector;

import ext.json.me.JSONArray;
import ext.json.me.JSONObject;
import ext.json.me.JSONTokener;


import net.rim.device.api.ui.component.Dialog;

public class GeneralJSONParser implements JSONCallBack {
	private String _action = "";
	
	private Vector _vector;
	
	private String _general_status = ""; //general message
	private String _general_message = ""; //general error/message
	private String _general_code = ""; //general error/code
	
	public GeneralJSONParser(String action){
		_action = action;
		_vector = new Vector();
		
		System.out.println("PARSING TYPE>>>"+_action);
	}
	
	public void generateFromURL(String url, String parameter, String method) throws Exception {
		//select using http or https
		if(url.startsWith("https")){
			new HTTPSConnector(url, parameter, method, this);
		}
		else{
			new HTTPConnector(url, parameter, method, this);
		}
	}
	
	public void generateFromMultipart(String url, byte[] multipart_parameters, String method) {
		try {
			new HTTPMultiPartConnector(url, multipart_parameters, method, this);
		} catch (Exception e) {
			Dialog.alert(e.getMessage());
		}
	}

	public void callback(String data) {
		try{
			// PHASE 1: buat JSON TOKENER
			JSONTokener jtokener = new JSONTokener(data); //data = String return dari backend

			// PHASE 2: buat JSON OBJECT
			JSONObject jobject = new JSONObject(jtokener);
			
			// PHASE 3: handling error if exist
			if(!jobject.optString("msg").equals("") && !jobject.optString("code").equals("")){
				set_general_message(jobject.optString("msg"));
				set_general_code(jobject.optString("code"));
				set_general_status(jobject.optString("result"));
			}
			else{
				// PHASE 4: ambil data
				set_general_status("OK");
				
				if(_action.equalsIgnoreCase(Properties._CONN_TYPE_LOGIN_1)
				){
					// Login Normal
					UserInfoPersistable persistable = new UserInfoPersistable();
					persistable.set_UserID(jobject.optString("userid"));
					persistable.set_SessionID(jobject.optString("sessid"));
					
					_vector.addElement(persistable);
				}
				else if(_action.equalsIgnoreCase(Properties._CONN_TYPE_REGISTER_1)
				){
					// Register Normal
					UserInfoPersistable persistable = new UserInfoPersistable();
					persistable.set_UserID(jobject.optString("userid"));
					persistable.set_SessionID(jobject.optString("sessid"));
							
					_vector.addElement(persistable);
				}
				else if(_action.equalsIgnoreCase(Properties._CONN_TYPE_LOGIN_2)
				){
					// Login via Facebook
					// check never logged-in or not
					if(jobject.optString("is_register").equals("1")){
						UserInfoPersistable persistable = new UserInfoPersistable();
						persistable.set_UserID(jobject.optString("userid"));
						persistable.set_SessionID(jobject.optString("sessid"));
								
						_vector.addElement(persistable);
					}
					else{
						// can be 0 or never
						// need to push register screen
						set_general_code("FB0");
					}
				}
				else if(_action.equalsIgnoreCase(Properties._CONN_TYPE_REGISTER_2)
				){
					// Register Facebook
					UserInfoPersistable persistable = new UserInfoPersistable();
					persistable.set_UserID(jobject.optString("userid"));
					persistable.set_SessionID(jobject.optString("sessid"));
									
					_vector.addElement(persistable);
				}
				else if(_action.equalsIgnoreCase(Properties._CONN_TYPE_HOME_DISTANCE)
						|| _action.equalsIgnoreCase(Properties._CONN_TYPE_HOME_POPULAR)
						|| _action.equalsIgnoreCase(Properties._CONN_TYPE_HOME_NEW)
						|| _action.equalsIgnoreCase(Properties._CONN_TYPE_JAJANAN_SPECIAL)
						|| _action.equalsIgnoreCase(Properties._CONN_TYPE_SEARCH_HOME_DISTANCE)
						|| _action.equalsIgnoreCase(Properties._CONN_TYPE_SEARCH_HOME_POPULAR)
						|| _action.equalsIgnoreCase(Properties._CONN_TYPE_SEARCH_HOME_NEW)
				){
					Vector vector_1 = new Vector();
					Vector vector_2 = new Vector();
					
					HeaderPersistable header = new HeaderPersistable();
					header.set_numrows(jobject.optString("numrows"));
					header.set_page(jobject.optString("page"));
					header.set_nexturl(jobject.optString("nexturl"));
					vector_1.addElement(header);
					
					JSONArray array = jobject.optJSONArray("rows");
					if(array!=null){
						if(array instanceof JSONArray){
							int i = 0;
							while(i<array.length()){
								BodyPersistable body = new BodyPersistable();
								body.set_id(array.optJSONObject(i).optString("id"));
								body.set_title(array.optJSONObject(i).optString("title"));
								body.set_address(array.optJSONObject(i).optString("address"));
								body.set_city(array.optJSONObject(i).optString("city"));
								body.set_lat(array.optJSONObject(i).optString("lat"));
								body.set_lon(array.optJSONObject(i).optString("lon"));
								body.set_distance(array.optJSONObject(i).optInt("distance"));
								body.set_rating(array.optJSONObject(i).optInt("rating"));
								body.set_urldetail(array.optJSONObject(i).optString("urldetail"));
								body.set_urlimg(array.optJSONObject(i).optString("urlimg"));
								
								vector_2.addElement(body);
								
								i++;
							}
						}
					}
					
					_vector.addElement(vector_1);
					_vector.addElement(vector_2);
				}
				else if(_action.equalsIgnoreCase(Properties._CONN_TYPE_MENU_SPECIAL)
				){
					// get menu special list
					JSONArray array = jobject.optJSONArray("array");
					if(array!=null){
						if(array instanceof JSONArray){
							int i = 0;
							while(i<array.length()){
								HeaderPersistable persistable = new HeaderPersistable();
								persistable.set_id(array.optJSONObject(i).optString("id"));
								persistable.set_title(array.optJSONObject(i).optString("title"));
								persistable.set_nexturl(array.optJSONObject(i).optString("nexturl"));
												
								_vector.addElement(persistable);
								
								i++;
							}
						}
					}
				}
				else if(_action.equalsIgnoreCase(Properties._CONN_TYPE_RECIPE_NEW)
						|| _action.equalsIgnoreCase(Properties._CONN_TYPE_RECIPE_POPULAR)
						|| _action.equalsIgnoreCase(Properties._CONN_TYPE_SEARCH_RECIPE_NEW)
						|| _action.equalsIgnoreCase(Properties._CONN_TYPE_SEARCH_RECIPE_POPULAR)
				){
					Vector vector_1 = new Vector();
					Vector vector_2 = new Vector();
					
					HeaderPersistable header = new HeaderPersistable();
					header.set_numrows(jobject.optString("numrows"));
					header.set_page(jobject.optString("page"));
					header.set_nexturl(jobject.optString("nexturl"));
					vector_1.addElement(header);
					
					JSONArray array = jobject.optJSONArray("rows");
					if(array!=null){
						if(array instanceof JSONArray){
							int i = 0;
							while(i<array.length()){
								BodyPersistable2 body = new BodyPersistable2();
								body.set_id(array.optJSONObject(i).optString("id"));
								body.set_title(array.optJSONObject(i).optString("title"));
								body.set_rating(array.optJSONObject(i).optInt("rating"));
								body.set_userid(array.optJSONObject(i).optString("userid"));
								body.set_fullname(array.optJSONObject(i).optString("fullname"));
								body.set_date(array.optJSONObject(i).optString("date"));
								
								body.set_urldetail(array.optJSONObject(i).optString("urldetail"));
								body.set_urlimg(array.optJSONObject(i).optString("urlimg"));
								
								vector_2.addElement(body);
								
								i++;
							}
						}
					}
					
					_vector.addElement(vector_1);
					_vector.addElement(vector_2);
				}
				else if(_action.equalsIgnoreCase(Properties._CONN_TYPE_NOTIFICATION)
				){
					Vector vector_1 = new Vector();
					Vector vector_2 = new Vector();
					
					HeaderPersistable header = new HeaderPersistable();
					header.set_numrows(jobject.optString("numrows"));
					header.set_page(jobject.optString("page"));
					header.set_nexturl(jobject.optString("nexturl"));
					vector_1.addElement(header);
					
					JSONArray array = jobject.optJSONArray("rows");
					if(array!=null){
						if(array instanceof JSONArray){
							int i = 0;
							while(i<array.length()){
								BodyPersistable3 body = new BodyPersistable3();
								body.set_notifid(array.optJSONObject(i).optString("notifid"));
								body.set_notiftxt(array.optJSONObject(i).optString("notiftxt"));
								body.set_reltime(array.optJSONObject(i).optString("reltime"));
								body.set_pagetype(array.optJSONObject(i).optString("pagetype"));
								body.set_urldetail(array.optJSONObject(i).optString("urldetail"));
								
								vector_2.addElement(body);
								
								i++;
							}
						}
					}
					
					_vector.addElement(vector_1);
					_vector.addElement(vector_2);
				}
				else if(_action.equalsIgnoreCase(Properties._CONN_TYPE_GET_PROFILE)
						|| _action.equalsIgnoreCase(Properties._CONN_TYPE_GET_PROFILE_OTHER)
				){
					Vector vector_1 = new Vector();
					Vector vector_2 = new Vector();
					Vector vector_3 = new Vector();
					Vector vector_4 = new Vector();
					
					// parse profile
					JSONObject jprofile = jobject.optJSONObject("profile");
					
					ProfilePersistable profile = new ProfilePersistable();
					profile.set_usertype(jprofile.optString("usertype"));
					profile.set_fullname(jprofile.optString("fullname"));
					profile.set_gender(jprofile.optString("gender"));
					profile.set_email(jprofile.optString("email"));
					profile.set_bdate(jprofile.optString("bdate"));
					profile.set_address(jprofile.optString("address"));
					profile.set_city(jprofile.optString("city"));
					profile.set_zip(jprofile.optString("zip"));
					profile.set_id_type(jprofile.optString("id_type"));
					profile.set_id_number(jprofile.optString("id_number"));
					profile.set_phone(jprofile.optString("phone"));
					profile.set_avaimg(jprofile.optString("avaimg"));
					profile.set_userrank(jprofile.optString("userrank"));
					vector_1.addElement(profile);
					
					// parse badges
					JSONArray arrayB = jobject.optJSONArray("badges");
					if(arrayB!=null){
						if(arrayB instanceof JSONArray){
							
							JSONArray badgesArray = arrayB.getJSONArray(0);
							if(badgesArray!=null){
								if(badgesArray instanceof JSONArray){
									int i = 0;
									while(i<badgesArray.length()){
										BadgesPersistable badges = new BadgesPersistable();
										badges.set_badgeid(badgesArray.optJSONObject(i).optString("badgeid"));
										badges.set_urlbadge(badgesArray.optJSONObject(i).optString("urlbadge"));
										
										vector_2.addElement(badges);
										
										i++;
									}
								}
							}
						}
					}
					
					// parse reviews
					HeaderPersistable header = new HeaderPersistable();
					header.set_numrows(jobject.optString("numrows"));
					header.set_page(jobject.optString("page"));
					header.set_nexturl(jobject.optString("nexturl"));
					vector_3.addElement(header);
					
					JSONArray array = jobject.optJSONArray("rows");
					if(array!=null){
						if(array instanceof JSONArray){
							int i = 0;
							while(i<array.length()){
								ReviewsPersistable body = new ReviewsPersistable();
								body.set_ref_id(array.optJSONObject(i).optString("id"));
								body.set_title(array.optJSONObject(i).optString("title"));
								body.set_rating(array.optJSONObject(i).optInt("rating"));
								body.set_date(array.optJSONObject(i).optString("date"));
								body.set_pagetype(array.optJSONObject(i).optString("pagetype"));
								body.set_nexturl(array.optJSONObject(i).optString("nexturl"));
								body.set_urlimg(array.optJSONObject(i).optString("urlimg"));
								
								vector_4.addElement(body);
								
								i++;
							}
						}
					}
					
					_vector.addElement(vector_1);
					_vector.addElement(vector_2);
					_vector.addElement(vector_3);
					_vector.addElement(vector_4);
				}
				else if(_action.equalsIgnoreCase(Properties._CONN_TYPE_USER_REVIEW_PAGING)
						|| _action.equalsIgnoreCase(Properties._CONN_TYPE_USER_REVIEW_PAGING_OTHER)
				) {
					Vector vector_1 = new Vector();
					Vector vector_2 = new Vector();

					// parse reviews
					HeaderPersistable header = new HeaderPersistable();
					header.set_numrows(jobject.optString("numrows"));
					header.set_page(jobject.optString("page"));
					header.set_nexturl(jobject.optString("nexturl"));
					vector_1.addElement(header);

					JSONArray array = jobject.optJSONArray("rows");
					if (array != null) {
						if (array instanceof JSONArray) {
							int i = 0;
							while (i < array.length()) {
								ReviewsPersistable body = new ReviewsPersistable();
								body.set_ref_id(array.optJSONObject(i)
										.optString("id"));
								body.set_title(array.optJSONObject(i)
										.optString("title"));
								body.set_rating(array.optJSONObject(i).optInt(
										"rating"));
								body.set_date(array.optJSONObject(i).optString(
										"date"));
								body.set_pagetype(array.optJSONObject(i)
										.optString("pagetype"));
								body.set_nexturl(array.optJSONObject(i)
										.optString("nexturl"));
								body.set_urlimg(array.optJSONObject(i)
										.optString("urlimg"));

								vector_2.addElement(body);

								i++;
							}
						}
					}

					_vector.addElement(vector_1);
					_vector.addElement(vector_2);
				}
				else if(_action.equalsIgnoreCase(Properties._CONN_TYPE_EDIT_USER_PROFILE)
						|| _action.equalsIgnoreCase(Properties._CONN_TYPE_ADD_REVIEW)
						|| _action.equalsIgnoreCase(Properties._CONN_TYPE_LIKE_REVIEW)
						|| _action.equalsIgnoreCase(Properties._CONN_TYPE_REPORT_REVIEW)
						|| _action.equalsIgnoreCase(Properties._CONN_TYPE_DELETE_REVIEW)
						|| _action.equalsIgnoreCase(Properties._CONN_TYPE_CHANGE_NOTIF)
						|| _action.equalsIgnoreCase(Properties._CONN_TYPE_ADD_STORE)
				){
					// Edit user profile
					set_general_code(jobject.optString("code"));
					set_general_message(jobject.optString("msg"));
				}
				else if(_action.equalsIgnoreCase(Properties._CONN_TYPE_STATE_NOTIF)){
					set_general_code("200");
					set_general_message(jobject.optString("isnotif"));
				}
				else if(_action.equalsIgnoreCase(Properties._CONN_TYPE_DETAIL_STORE)
				){
					Vector vector_1 = new Vector();
					Vector vector_2 = new Vector();
					Vector vector_3 = new Vector();
					
					
					JSONObject store = jobject.getJSONObject("detailstore");
					DetailStorePersistable detail = new DetailStorePersistable();
					detail.set_id(store.optString("id"));
					detail.set_title(store.optString("title"));
					detail.set_address(store.optString("address"));
					detail.set_city(store.optString("city"));
					detail.set_desc(store.optString("desc"));
					detail.set_lat(store.optString("lat"));
					detail.set_lon(store.optString("lon"));
					detail.set_distance(store.optString("distance"));
					detail.set_rating(store.optInt("rating"));
					detail.set_num_rating(store.optString("num_rating"));
					detail.set_urlimg(store.optString("urlimg"));
					vector_1.addElement(detail);
					
					
					HeaderPersistable header = new HeaderPersistable();
					header.set_numrows(jobject.optString("numrows"));
					header.set_page(jobject.optString("page"));
					header.set_nexturl(jobject.optString("nexturl"));
					vector_2.addElement(header);
					
					JSONArray array = jobject.optJSONArray("rows");
					if(array!=null){
						if(array instanceof JSONArray){
							int i = 0;
							while(i<array.length()){
								BodyPersistable4 body = new BodyPersistable4();
								body.set_commentid(array.optJSONObject(i).optString("commentid"));
								body.set_comment(array.optJSONObject(i).optString("comment"));
								body.set_userid(array.optJSONObject(i).optString("userid"));
								body.set_commentdate(array.optJSONObject(i).optString("commentdate"));
								body.set_fullname(array.optJSONObject(i).optString("fullname"));
								body.set_avaimg(array.optJSONObject(i).optString("avaimg"));
								body.set_rating(array.optJSONObject(i).optInt("rating"));
								body.set_numlike(array.optJSONObject(i).optString("numlike"));
								body.set_urlimg(array.optJSONObject(i).optString("urlimg"));
								body.set_is_mine(array.optJSONObject(i).optString("is_mine"));
								
								vector_3.addElement(body);
								
								i++;
							}
						}
					}
					
					_vector.addElement(vector_1);
					_vector.addElement(vector_2);
					_vector.addElement(vector_3);
				}
				else if(_action.equalsIgnoreCase(Properties._CONN_TYPE_DETAIL_STORE_REVIEW_PAGING)
				){
					Vector vector_1 = new Vector();
					Vector vector_2 = new Vector();

					HeaderPersistable header = new HeaderPersistable();
					header.set_numrows(jobject.optString("numrows"));
					header.set_page(jobject.optString("page"));
					header.set_nexturl(jobject.optString("nexturl"));
					vector_1.addElement(header);

					JSONArray array = jobject.optJSONArray("rows");
					if (array != null) {
						if (array instanceof JSONArray) {
							int i = 0;
							while (i < array.length()) {
								BodyPersistable4 body = new BodyPersistable4();
								body.set_commentid(array.optJSONObject(i)
										.optString("commentid"));
								body.set_comment(array.optJSONObject(i)
										.optString("comment"));
								body.set_userid(array.optJSONObject(i)
										.optString("userid"));
								body.set_commentdate(array.optJSONObject(i)
										.optString("commentdate"));
								body.set_fullname(array.optJSONObject(i)
										.optString("fullname"));
								body.set_avaimg(array.optJSONObject(i)
										.optString("avaimg"));
								body.set_rating(array.optJSONObject(i).optInt(
										"rating"));
								body.set_numlike(array.optJSONObject(i)
										.optString("numlike"));
								body.set_urlimg(array.optJSONObject(i)
										.optString("urlimg"));
								body.set_is_mine(array.optJSONObject(i)
										.optString("is_mine"));

								vector_2.addElement(body);

								i++;
							}
						}
					}

					_vector.addElement(vector_1);
					_vector.addElement(vector_2);
				}
				else if(_action.equalsIgnoreCase(Properties._CONN_TYPE_DETAIL_RESEP)
				) {
					Vector vector_1 = new Vector();
					Vector vector_2 = new Vector();
					Vector vector_3 = new Vector();

					JSONObject resep = jobject.getJSONObject("detailrecipe");
					DetailResepPersistable detail = new DetailResepPersistable();
					detail.set_id(resep.optString("id"));
					detail.set_title(resep.optString("title"));
					detail.set_desc(resep.optString("desc"));
					detail.set_ingredients(resep.optString("ingredients"));
					detail.set_steps(resep.optString("steps"));
					detail.set_rating(resep.optInt("rating"));
					detail.set_num_rating(resep.optString("num_rating"));
					detail.set_userid(resep.optString("userid"));
					detail.set_fullname(resep.optString("fullname"));
					detail.set_date(resep.optString("date"));
					detail.set_urlimg(resep.optString("urlimg"));
					vector_1.addElement(detail);

					HeaderPersistable header = new HeaderPersistable();
					header.set_numrows(jobject.optString("numrows"));
					header.set_page(jobject.optString("page"));
					header.set_nexturl(jobject.optString("nexturl"));
					vector_2.addElement(header);

					JSONArray array = jobject.optJSONArray("rows");
					if (array != null) {
						if (array instanceof JSONArray) {
							int i = 0;
							while (i < array.length()) {
								BodyPersistable4 body = new BodyPersistable4();
								body.set_commentid(array.optJSONObject(i)
										.optString("commentid"));
								body.set_comment(array.optJSONObject(i)
										.optString("comment"));
								body.set_userid(array.optJSONObject(i)
										.optString("userid"));
								body.set_commentdate(array.optJSONObject(i)
										.optString("commentdate"));
								body.set_fullname(array.optJSONObject(i)
										.optString("fullname"));
								body.set_avaimg(array.optJSONObject(i)
										.optString("avaimg"));
								body.set_rating(array.optJSONObject(i).optInt(
										"rating"));
								body.set_numlike(array.optJSONObject(i)
										.optString("numlike"));
								body.set_urlimg(array.optJSONObject(i)
										.optString("urlimg"));
								body.set_is_mine(array.optJSONObject(i).optString("is_mine"));

								vector_3.addElement(body);

								i++;
							}
						}
					}

					_vector.addElement(vector_1);
					_vector.addElement(vector_2);
					_vector.addElement(vector_3);
				}
				else if(_action.equalsIgnoreCase(Properties._CONN_TYPE_DETAIL_RECIPE_REVIEW_PAGING)
				) {
					Vector vector_1 = new Vector();
					Vector vector_2 = new Vector();
					
					HeaderPersistable header = new HeaderPersistable();
					header.set_numrows(jobject.optString("numrows"));
					header.set_page(jobject.optString("page"));
					header.set_nexturl(jobject.optString("nexturl"));
					vector_1.addElement(header);

					JSONArray array = jobject.optJSONArray("rows");
					if (array != null) {
						if (array instanceof JSONArray) {
							int i = 0;
							while (i < array.length()) {
								BodyPersistable4 body = new BodyPersistable4();
								body.set_commentid(array.optJSONObject(i)
										.optString("commentid"));
								body.set_comment(array.optJSONObject(i)
										.optString("comment"));
								body.set_userid(array.optJSONObject(i)
										.optString("userid"));
								body.set_commentdate(array.optJSONObject(i)
										.optString("commentdate"));
								body.set_fullname(array.optJSONObject(i)
										.optString("fullname"));
								body.set_avaimg(array.optJSONObject(i)
										.optString("avaimg"));
								body.set_rating(array.optJSONObject(i).optInt(
										"rating"));
								body.set_numlike(array.optJSONObject(i)
										.optString("numlike"));
								body.set_urlimg(array.optJSONObject(i)
										.optString("urlimg"));
								body.set_is_mine(array.optJSONObject(i)
										.optString("is_mine"));

								vector_2.addElement(body);

								i++;
							}
						}
					}

					_vector.addElement(vector_1);
					_vector.addElement(vector_2);
				}
				else if(_action.equalsIgnoreCase(Properties._CONN_TYPE_GOOGLE_GEOCODING)
				) {
					JSONArray array = jobject.optJSONArray("results");
					if (array != null) {
						if (array instanceof JSONArray) {
							int i = 0;
							while (i < array.length()) {
								if(i == 0){
									JSONObject obj = array.optJSONObject(i);
									JSONObject geometry = obj.optJSONObject("geometry").optJSONObject("location");
									
									GeocodingPersistable geo = new GeocodingPersistable();
									geo.set_formatted_address(obj.optString("formatted_address"));
									geo.set_latitude(geometry.optString("lat"));
									geo.set_longitude(geometry.optString("lng"));
									
									_vector.addElement(geo);
								}
								
								i++;
							}
						}
					}
				}
				else if (_action.equalsIgnoreCase(Properties._CONN_TYPE_SPLASH_PROMO)) {
					JSONArray array = jobject.optJSONArray("array");
					if (array != null) {
						if (array instanceof JSONArray) {
							int i = 0;
							while (i < array.length()) {
								PromoPersistable promo = new PromoPersistable();
								promo.set_id(array.optJSONObject(i).optString("id"));
								promo.set_title(array.optJSONObject(i).optString("title"));
								promo.set_sdate(array.optJSONObject(i).optString("sdate"));
								promo.set_edate(array.optJSONObject(i).optString("edate"));
								promo.set_urlimg(array.optJSONObject(i).optString("urlimg"));

								_vector.addElement(promo);

								i++;
							}
						}
					}
				}
				else if (_action.equalsIgnoreCase(Properties._CONN_TYPE_PAGE_PROMO)) {
					Vector vector_1 = new Vector();
					Vector vector_2 = new Vector();
					
					HeaderPersistable header = new HeaderPersistable();
					header.set_numrows(jobject.optString("numrows"));
					header.set_page(jobject.optString("page"));
					header.set_nexturl(jobject.optString("nexturl"));
					vector_1.addElement(header);
					
					JSONArray array = jobject.optJSONArray("rows");
					if (array != null) {
						if (array instanceof JSONArray) {
							int i = 0;
							while (i < array.length()) {
								PromoPersistable promo = new PromoPersistable();
								promo.set_id(array.optJSONObject(i).optString("id"));
								promo.set_title(array.optJSONObject(i).optString("title"));
								promo.set_sdate(array.optJSONObject(i).optString("sdate"));
								promo.set_edate(array.optJSONObject(i).optString("edate"));
								promo.set_urlimg(array.optJSONObject(i).optString("urlimg"));

								vector_2.addElement(promo);

								i++;
							}
						}
					}
					
					_vector.addElement(vector_1);
					_vector.addElement(vector_2);
				}
			}
		}catch(Exception x){ 
			System.out.println("Error while parsing>>>"+x); 
			set_general_status("KO");
			set_general_code("XPARSE : " + data);
			set_general_message(Properties._APP_NAME+" is experiencing an issue, we will have it up and running soon. Thank you for your understanding.");
		}
	}
	

//	private String[] getArrayString(JSONArray array){
//		String[] result = null;
//		
//		if(array!=null){
//			if(array instanceof JSONArray){
//				if(array.length()>0){
//					String[] tmp = new String[array.length()];
//					
//					int i = 0;
//					while(i<array.length()){
//						tmp[i] = array.optString(i);
//						//System.out.println("array "+i+">>>>"+tmp[i]);
//						i++;
//					}
//					
//					result = tmp;
//					tmp = null;
//				}
//			}
//		}
//		
//		return result;
//	}
//	
//	private String[] getArrayString(JSONArray array, String key){
//		String[] result = null;
//		
//		if(array!=null){
//			if(array instanceof JSONArray){
//				if(array.length()>0){
//					String[] tmp = new String[array.length()];
//					
//					int i = 0;
//					while(i<array.length()){
//						tmp[i] = tmp[i] = array.optJSONObject(i).optString(key);
//						//System.out.println("array "+i+">>>>"+tmp[i]);
//						i++;
//					}
//					
//					result = tmp;
//					tmp = null;
//				}
//			}
//		}
//		
//		return result;
//	}
//	
//	private boolean[] getArrayBoolean(JSONArray array, String key){
//		boolean[] result = null;
//		
//		if(array!=null){
//			if(array instanceof JSONArray){
//				if(array.length()>0){
//					boolean[] tmp = new boolean[array.length()];
//					
//					int i = 0;
//					while(i<array.length()){
//						tmp[i] = tmp[i] = array.optJSONObject(i).optBoolean(key);
//						//System.out.println("array "+i+">>>>"+tmp[i]);
//						i++;
//					}
//					
//					result = tmp;
//					tmp = null;
//				}
//			}
//		}
//		
//		return result;
//	}
//	
//	private int[] getArrayInt(JSONArray array, String key){
//		int[] result = null;
//		
//		if(array!=null){
//			if(array instanceof JSONArray){
//				if(array.length()>0){
//					int[] tmp = new int[array.length()];
//					
//					int i = 0;
//					while(i<array.length()){
//						tmp[i] = tmp[i] = array.optJSONObject(i).optInt(key);
//						//System.out.println("array "+i+">>>>"+tmp[i]);
//						i++;
//					}
//					
//					result = tmp;
//					tmp = null;
//				}
//			}
//		}
//		
//		return result;
//	}

	public Vector get_vector() {
		return _vector;
	}
	
	public String get_general_status() {
		return _general_status;
	}

	public void set_general_status(String txt) {
		this._general_status = txt;
	}
	
	public String get_general_message() {
		return _general_message;
	}

	public void set_general_message(String txt) {
		this._general_message = txt;
	}

	public String get_general_code() {
		return _general_code;
	}

	public void set_general_code(String txt) {
		this._general_code = txt;
	}
}
