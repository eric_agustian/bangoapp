package com.mobileforce.bango.storage.persistable;

import net.rim.device.api.util.Persistable;

public class BadgesPersistable implements Persistable {
	private String _badgeid;
	private String _urlbadge;
	
	public BadgesPersistable(){}

	public String get_badgeid() {
		return _badgeid;
	}

	public void set_badgeid(String _badgeid) {
		this._badgeid = _badgeid;
	}

	public String get_urlbadge() {
		return _urlbadge;
	}

	public void set_urlbadge(String _urlbadge) {
		this._urlbadge = _urlbadge;
	}

}
