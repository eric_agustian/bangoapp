package com.mobileforce.bango.storage.persistable;

import net.rim.device.api.util.Persistable;

public class BodyPersistable2 implements Persistable {
	private String _id;
	private String _title;
	private int _rating;
	private String _userid;
	private String _fullname;
	private String _date;
	private String _urldetail;
	private String _urlimg;
	
	public BodyPersistable2(){}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String get_title() {
		return _title;
	}

	public void set_title(String _title) {
		this._title = _title;
	}

	public int get_rating() {
		return _rating;
	}

	public void set_rating(int _rating) {
		this._rating = _rating;
	}

	public String get_userid() {
		return _userid;
	}

	public void set_userid(String _userid) {
		this._userid = _userid;
	}

	public String get_fullname() {
		return _fullname;
	}

	public void set_fullname(String _fullname) {
		this._fullname = _fullname;
	}

	public String get_date() {
		return _date;
	}

	public void set_date(String _date) {
		this._date = _date;
	}

	public String get_urldetail() {
		return _urldetail;
	}

	public void set_urldetail(String _urldetail) {
		this._urldetail = _urldetail;
	}

	public String get_urlimg() {
		return _urlimg;
	}

	public void set_urlimg(String _urlimg) {
		this._urlimg = _urlimg;
	}

}