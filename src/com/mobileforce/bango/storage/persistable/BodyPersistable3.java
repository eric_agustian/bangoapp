package com.mobileforce.bango.storage.persistable;

import net.rim.device.api.util.Persistable;

public class BodyPersistable3 implements Persistable {
	private String _notifid;
	private String _notiftxt;
	private String _reltime;
	private String _pagetype;
	private String _urldetail;
	
	public BodyPersistable3(){}

	public String get_notifid() {
		return _notifid;
	}

	public void set_notifid(String _notifid) {
		this._notifid = _notifid;
	}

	public String get_notiftxt() {
		return _notiftxt;
	}

	public void set_notiftxt(String _notiftxt) {
		this._notiftxt = _notiftxt;
	}

	public String get_reltime() {
		return _reltime;
	}

	public void set_reltime(String _reltime) {
		this._reltime = _reltime;
	}

	public String get_pagetype() {
		return _pagetype;
	}

	public void set_pagetype(String _pagetype) {
		this._pagetype = _pagetype;
	}

	public String get_urldetail() {
		return _urldetail;
	}

	public void set_urldetail(String _urldetail) {
		this._urldetail = _urldetail;
	}

}