package com.mobileforce.bango.storage.persistable;

import net.rim.device.api.util.Persistable;

public class BodyPersistable4 implements Persistable {
	private String _commentid;
	private String _comment;
	private String _userid;
	private String _commentdate;
	private String _fullname;
	private String _avaimg;
	private int _rating;
	private String _urlimg;
	private String _numlike;
	private String _is_mine;
	
	public BodyPersistable4(){}

	public String get_commentid() {
		return _commentid;
	}

	public void set_commentid(String _commentid) {
		this._commentid = _commentid;
	}

	public String get_comment() {
		return _comment;
	}

	public void set_comment(String _comment) {
		this._comment = _comment;
	}

	public String get_userid() {
		return _userid;
	}

	public void set_userid(String _userid) {
		this._userid = _userid;
	}

	public String get_commentdate() {
		return _commentdate;
	}

	public void set_commentdate(String _commentdate) {
		this._commentdate = _commentdate;
	}

	public String get_fullname() {
		return _fullname;
	}

	public void set_fullname(String _fullname) {
		this._fullname = _fullname;
	}

	public String get_avaimg() {
		return _avaimg;
	}

	public void set_avaimg(String _avaimg) {
		this._avaimg = _avaimg;
	}

	public int get_rating() {
		return _rating;
	}

	public void set_rating(int _rating) {
		this._rating = _rating;
	}

	public String get_urlimg() {
		return _urlimg;
	}

	public void set_urlimg(String _urlimg) {
		this._urlimg = _urlimg;
	}

	public String get_numlike() {
		return _numlike;
	}

	public void set_numlike(String _numlike) {
		this._numlike = _numlike;
	}

	public String get_is_mine() {
		return _is_mine;
	}

	public void set_is_mine(String _is_mine) {
		this._is_mine = _is_mine;
	}

}
