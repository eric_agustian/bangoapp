package com.mobileforce.bango.storage.persistable;

import net.rim.device.api.util.Persistable;

public class DetailResepPersistable implements Persistable {
	private String _id;
	private String _title;
	private String _desc;
	private String _ingredients;
	private String _steps;
	private int _rating;
	private String _num_rating;
	private String _userid;
	private String _fullname;
	private String _date;
	private String _urlimg;
	
	public DetailResepPersistable(){}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String get_title() {
		return _title;
	}

	public void set_title(String _title) {
		this._title = _title;
	}

	public String get_desc() {
		return _desc;
	}

	public void set_desc(String _desc) {
		this._desc = _desc;
	}

	public String get_ingredients() {
		return _ingredients;
	}

	public void set_ingredients(String _ingredients) {
		this._ingredients = _ingredients;
	}

	public String get_steps() {
		return _steps;
	}

	public void set_steps(String _steps) {
		this._steps = _steps;
	}

	public int get_rating() {
		return _rating;
	}

	public void set_rating(int _rating) {
		this._rating = _rating;
	}

	public String get_num_rating() {
		return _num_rating;
	}

	public void set_num_rating(String _num_rating) {
		this._num_rating = _num_rating;
	}

	public String get_userid() {
		return _userid;
	}

	public void set_userid(String _userid) {
		this._userid = _userid;
	}

	public String get_fullname() {
		return _fullname;
	}

	public void set_fullname(String _fullname) {
		this._fullname = _fullname;
	}

	public String get_date() {
		return _date;
	}

	public void set_date(String _date) {
		this._date = _date;
	}

	public String get_urlimg() {
		return _urlimg;
	}

	public void set_urlimg(String _urlimg) {
		this._urlimg = _urlimg;
	}

}
