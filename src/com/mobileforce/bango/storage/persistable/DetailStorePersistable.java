package com.mobileforce.bango.storage.persistable;

import net.rim.device.api.util.Persistable;

public class DetailStorePersistable implements Persistable {
	private String _id;
	private String _title;
	private String _address;
	private String _city;
	private String _desc;
	private String _lat;
	private String _lon;
	private String _distance;
	private int _rating;
	private String _num_rating;
	private String _urlimg;
	
	public DetailStorePersistable(){}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String get_title() {
		return _title;
	}

	public void set_title(String _title) {
		this._title = _title;
	}

	public String get_address() {
		return _address;
	}

	public void set_address(String _address) {
		this._address = _address;
	}

	public String get_city() {
		return _city;
	}

	public void set_city(String _city) {
		this._city = _city;
	}

	public String get_desc() {
		return _desc;
	}

	public void set_desc(String _desc) {
		this._desc = _desc;
	}

	public String get_lat() {
		return _lat;
	}

	public void set_lat(String _lat) {
		this._lat = _lat;
	}

	public String get_lon() {
		return _lon;
	}

	public void set_lon(String _lon) {
		this._lon = _lon;
	}

	public String get_distance() {
		return _distance;
	}

	public void set_distance(String _distance) {
		this._distance = _distance;
	}

	public int get_rating() {
		return _rating;
	}

	public void set_rating(int _rating) {
		this._rating = _rating;
	}

	public String get_num_rating() {
		return _num_rating;
	}

	public void set_num_rating(String _num_rating) {
		this._num_rating = _num_rating;
	}

	public String get_urlimg() {
		return _urlimg;
	}

	public void set_urlimg(String _urlimg) {
		this._urlimg = _urlimg;
	}

}
