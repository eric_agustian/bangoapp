package com.mobileforce.bango.storage.persistable;

import net.rim.device.api.util.Persistable;

public class FacebookPersistable implements Persistable {
	public FacebookPersistable(){}
	
	private String facebook_token;
	private String facebook_userid;
	private String facebook_email;
	private String facebook_name;
	private String facebook_firstname;
	private String facebook_lastname;
	private String facebook_birthday;
	private String facebook_gender;
	private String facebook_location;
	
	public String getFacebook_token() {
		return facebook_token;
	}
	public void setFacebook_token(String facebook_token) {
		this.facebook_token = facebook_token;
	}
	public String getFacebook_userid() {
		return facebook_userid;
	}
	public void setFacebook_userid(String facebook_userid) {
		this.facebook_userid = facebook_userid;
	}
	public String getFacebook_email() {
		return facebook_email;
	}
	public void setFacebook_email(String facebook_email) {
		this.facebook_email = facebook_email;
	}
	public String getFacebook_name() {
		return facebook_name;
	}
	public void setFacebook_name(String facebook_name) {
		this.facebook_name = facebook_name;
	}
	public String getFacebook_firstname() {
		return facebook_firstname;
	}
	public void setFacebook_firstname(String facebook_firstname) {
		this.facebook_firstname = facebook_firstname;
	}
	public String getFacebook_lastname() {
		return facebook_lastname;
	}
	public void setFacebook_lastname(String facebook_lastname) {
		this.facebook_lastname = facebook_lastname;
	}
	public String getFacebook_birthday() {
		return facebook_birthday;
	}
	public void setFacebook_birthday(String facebook_birthday) {
		this.facebook_birthday = facebook_birthday;
	}
	public String getFacebook_gender() {
		return facebook_gender;
	}
	public void setFacebook_gender(String facebook_gender) {
		this.facebook_gender = facebook_gender;
	}
	public String getFacebook_location() {
		return facebook_location;
	}
	public void setFacebook_location(String facebook_location) {
		this.facebook_location = facebook_location;
	}
}
