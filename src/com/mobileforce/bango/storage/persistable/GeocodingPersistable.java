package com.mobileforce.bango.storage.persistable;

import net.rim.device.api.util.Persistable;

public class GeocodingPersistable implements Persistable {
	private String _formatted_address;
	private String _latitude;
	private String _longitude;
	
	public GeocodingPersistable(){}

	public String get_formatted_address() {
		return _formatted_address;
	}

	public void set_formatted_address(String _formatted_address) {
		this._formatted_address = _formatted_address;
	}

	public String get_latitude() {
		return _latitude;
	}

	public void set_latitude(String _latitude) {
		this._latitude = _latitude;
	}

	public String get_longitude() {
		return _longitude;
	}

	public void set_longitude(String _longitude) {
		this._longitude = _longitude;
	}
}
