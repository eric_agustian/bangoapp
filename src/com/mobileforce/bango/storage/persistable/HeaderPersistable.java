package com.mobileforce.bango.storage.persistable;

import net.rim.device.api.util.Persistable;

public class HeaderPersistable implements Persistable {
	private String _numrows;
	private String _page;
	private String _nexturl;
	
	private String _id;
	private String _title;
	
	public HeaderPersistable(){}


	public String get_numrows() {
		return _numrows;
	}


	public void set_numrows(String _numrows) {
		this._numrows = _numrows;
	}


	public String get_page() {
		return _page;
	}


	public void set_page(String _page) {
		this._page = _page;
	}


	public String get_nexturl() {
		return _nexturl;
	}


	public void set_nexturl(String _nexturl) {
		this._nexturl = _nexturl;
	}


	public String get_id() {
		return _id;
	}


	public void set_id(String _id) {
		this._id = _id;
	}


	public String get_title() {
		return _title;
	}


	public void set_title(String _title) {
		this._title = _title;
	}

}
