package com.mobileforce.bango.storage.persistable;

import net.rim.device.api.util.Persistable;

public class ImagePersistable implements Persistable {
	private long _imageID;
	private String _imagePath;
	private String _imageString;
	
	public ImagePersistable(){ }


	public long get_imageID() {
		return _imageID;
	}


	public String get_imagePath() {
		return _imagePath;
	}


	public void set_imageID(long _imageid) {
		_imageID = _imageid;
	}


	public void set_imagePath(String path) {
		_imagePath = path;
	}


	public String getImageString() {
		return _imageString;
	}

	public void setImageString(String string) {
		_imageString = string;
	}
}