package com.mobileforce.bango.storage.persistable;

import net.rim.device.api.util.Persistable;

public class LocationPersistable implements Persistable {
	private String _latitude;
	private String _longitude;
	private String _type;
	
	public LocationPersistable(){}

	public String get_latitude() {
		return _latitude;
	}

	public void set_latitude(String _latitude) {
		this._latitude = _latitude;
	}

	public String get_longitude() {
		return _longitude;
	}

	public void set_longitude(String _longitude) {
		this._longitude = _longitude;
	}

	public String get_type() {
		return _type;
	}

	public void set_type(String _type) {
		this._type = _type;
	}

}
