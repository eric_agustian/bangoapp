package com.mobileforce.bango.storage.persistable;

import net.rim.device.api.util.Persistable;

public class ProfilePersistable implements Persistable {
	private String _ismine;
	private String _usertype;
	private String _fullname;
	private String _gender;
	private String _email;
	private String _bdate;
	private String _address;
	private String _city;
	private String _zip;
	private String _id_type;
	private String _id_number;
	private String _phone;
	private String _avaimg;
	private String _userrank;
	
	public ProfilePersistable(){}

	public String get_ismine() {
		return _ismine;
	}

	public void set_ismine(String _ismine) {
		this._ismine = _ismine;
	}

	public String get_usertype() {
		return _usertype;
	}

	public void set_usertype(String _usertype) {
		this._usertype = _usertype;
	}

	public String get_fullname() {
		return _fullname;
	}

	public void set_fullname(String _fullname) {
		this._fullname = _fullname;
	}

	public String get_gender() {
		return _gender;
	}

	public void set_gender(String _gender) {
		this._gender = _gender;
	}

	public String get_email() {
		return _email;
	}

	public void set_email(String _email) {
		this._email = _email;
	}

	public String get_bdate() {
		return _bdate;
	}

	public void set_bdate(String _bdate) {
		this._bdate = _bdate;
	}

	public String get_address() {
		return _address;
	}

	public void set_address(String _address) {
		this._address = _address;
	}

	public String get_city() {
		return _city;
	}

	public void set_city(String _city) {
		this._city = _city;
	}

	public String get_zip() {
		return _zip;
	}

	public void set_zip(String _zip) {
		this._zip = _zip;
	}

	public String get_id_type() {
		return _id_type;
	}

	public void set_id_type(String _id_type) {
		this._id_type = _id_type;
	}

	public String get_id_number() {
		return _id_number;
	}

	public void set_id_number(String _id_number) {
		this._id_number = _id_number;
	}

	public String get_phone() {
		return _phone;
	}

	public void set_phone(String _phone) {
		this._phone = _phone;
	}

	public String get_avaimg() {
		return _avaimg;
	}

	public void set_avaimg(String _avaimg) {
		this._avaimg = _avaimg;
	}

	public String get_userrank() {
		return _userrank;
	}

	public void set_userrank(String _userrank) {
		this._userrank = _userrank;
	}

}
