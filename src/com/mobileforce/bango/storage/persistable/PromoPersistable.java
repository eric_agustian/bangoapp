package com.mobileforce.bango.storage.persistable;

import net.rim.device.api.util.Persistable;

public class PromoPersistable implements Persistable {
	private String _id;
	private String _title;
	private String _sdate;
	private String _edate;
	private String _urlimg;
	
	public PromoPersistable(){}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String get_title() {
		return _title;
	}

	public void set_title(String _title) {
		this._title = _title;
	}

	public String get_sdate() {
		return _sdate;
	}

	public void set_sdate(String _sdate) {
		this._sdate = _sdate;
	}

	public String get_edate() {
		return _edate;
	}

	public void set_edate(String _edate) {
		this._edate = _edate;
	}

	public String get_urlimg() {
		return _urlimg;
	}

	public void set_urlimg(String _urlimg) {
		this._urlimg = _urlimg;
	}

}
