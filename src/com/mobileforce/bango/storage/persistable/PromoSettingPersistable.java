package com.mobileforce.bango.storage.persistable;

import net.rim.device.api.util.Persistable;

public class PromoSettingPersistable implements Persistable {
	private String dayEnd;
	private String hourEnd;
	private String minuteEnd;
	private boolean shown;
	
	public PromoSettingPersistable(){}

	public String getDayEnd() {
		return dayEnd;
	}

	public void setDayEnd(String dayEnd) {
		this.dayEnd = dayEnd;
	}

	public String getHourEnd() {
		return hourEnd;
	}

	public void setHourEnd(String hourEnd) {
		this.hourEnd = hourEnd;
	}

	public String getMinuteEnd() {
		return minuteEnd;
	}

	public void setMinuteEnd(String minuteEnd) {
		this.minuteEnd = minuteEnd;
	}

	public boolean isShown() {
		return shown;
	}

	public void setShown(boolean shown) {
		this.shown = shown;
	}

}
