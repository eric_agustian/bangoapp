package com.mobileforce.bango.storage.persistable;

import net.rim.device.api.util.Persistable;

public class ReviewsPersistable implements Persistable {
	private String _ref_id;
	private String _title;
	private int _rating;
	private String _date;
	private String _pagetype;
	private String _nexturl;
	private String _urlimg;
	
	public ReviewsPersistable(){}

	public String get_ref_id() {
		return _ref_id;
	}

	public void set_ref_id(String _ref_id) {
		this._ref_id = _ref_id;
	}

	public String get_title() {
		return _title;
	}

	public void set_title(String _title) {
		this._title = _title;
	}

	public int get_rating() {
		return _rating;
	}

	public void set_rating(int _rating) {
		this._rating = _rating;
	}

	public String get_date() {
		return _date;
	}

	public void set_date(String _date) {
		this._date = _date;
	}

	public String get_pagetype() {
		return _pagetype;
	}

	public void set_pagetype(String _pagetype) {
		this._pagetype = _pagetype;
	}

	public String get_nexturl() {
		return _nexturl;
	}

	public void set_nexturl(String _nexturl) {
		this._nexturl = _nexturl;
	}

	public String get_urlimg() {
		return _urlimg;
	}

	public void set_urlimg(String _urlimg) {
		this._urlimg = _urlimg;
	}

}
