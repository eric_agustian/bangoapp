package com.mobileforce.bango.storage.persistable;

import net.rim.device.api.util.Persistable;

public class SettingPersistable implements Persistable {
	private boolean _notification;
	
	public SettingPersistable(){}

	public boolean is_notification() {
		return _notification;
	}

	public void set_notification(boolean _notification) {
		this._notification = _notification;
	}

}
