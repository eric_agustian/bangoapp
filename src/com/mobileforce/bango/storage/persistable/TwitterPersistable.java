package com.mobileforce.bango.storage.persistable;

import net.rim.device.api.util.Persistable;

public class TwitterPersistable implements Persistable {
	public TwitterPersistable(){}
	
	private String twitter_screen_name;
	private String twitter_user_id;
	private String twitter_secret;
	private String twitter_token;
	
	public String getTwitter_screen_name() {
		return twitter_screen_name;
	}
	public void setTwitter_screen_name(String twitter_screen_name) {
		this.twitter_screen_name = twitter_screen_name;
	}
	public String getTwitter_user_id() {
		return twitter_user_id;
	}
	public void setTwitter_user_id(String twitter_user_id) {
		this.twitter_user_id = twitter_user_id;
	}
	public String getTwitter_secret() {
		return twitter_secret;
	}
	public void setTwitter_secret(String twitter_secret) {
		this.twitter_secret = twitter_secret;
	}
	public String getTwitter_token() {
		return twitter_token;
	}
	public void setTwitter_token(String twitter_token) {
		this.twitter_token = twitter_token;
	}
	
}
