package com.mobileforce.bango.storage.persistable;

import net.rim.device.api.util.Persistable;

public class UserInfoPersistable implements Persistable {
	private String _UserID;
	private String _SessionID;
	
//	private String _UserName;
//	private String _UserFName;
//	private String _UserLName;
//	private String _UserPassword;
//	private String _UserEmail;
//	private String _UserGender;
//	private String _UserBirthdate;
//	private String _UserLocation;
	
	public UserInfoPersistable(){}

	public String get_UserID() {
		return _UserID;
	}

	public void set_UserID(String UserID) {
		this._UserID = UserID;
	}

	public String get_SessionID() {
		return _SessionID;
	}

	public void set_SessionID(String SessionID) {
		this._SessionID = SessionID;
	}
}
