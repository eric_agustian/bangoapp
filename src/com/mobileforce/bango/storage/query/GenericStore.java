package com.mobileforce.bango.storage.query;

import java.util.Vector;

import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.system.PersistentStore;

public class GenericStore {
	public PersistentObject dataStore;
	public Vector dataStores;
	
	public GenericStore(long id) {
		dataStore = PersistentStore.getPersistentObject(id);
		synchronized (dataStore) {
			if (dataStore.getContents()==null) {
				dataStore.setContents(new Vector());
				dataStore.commit();
			}
			dataStores = (Vector)dataStore.getContents();			
		}
	}
	
	public void saveDataVector(Vector datas) {		
		synchronized (dataStore) {	
			dataStore.setContents(datas);
			dataStore.commit();
		}		
	}
	
	public Vector getVectorOfStoredData() {
		synchronized(dataStore) {
			dataStores = (Vector)dataStore.getContents();				
		}
		return dataStores;
	}
	
	public void deleteStoredData(long id) {		
		PersistentStore.destroyPersistentObject(id);
	}			
}
