package com.mobileforce.bango.storage.query;

import java.util.Vector;

import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.system.PersistentStore;

public class ImageStore {
	private PersistentObject imageStore;
	static Vector imageStores;
	
	public ImageStore(long id) {
		imageStores = new Vector();
		imageStore = PersistentStore.getPersistentObject(id);
		synchronized (imageStore) {
			if (imageStore.getContents()==null) {
				imageStore.setContents(new Vector());
				imageStore.commit();
			}
			imageStores = (Vector)imageStore.getContents();			
		}
	}
	
	public void saveDataVector(Vector threads) {		
		synchronized (imageStore) {	
			imageStore.setContents(threads);
			imageStore.commit();
		}		
	}
	
	public Vector getVectorOfStoredData() {
		synchronized(imageStore) {
			imageStores = (Vector)imageStore.getContents();				
		}
		return imageStores;
	}
	
	public void deleteStoredData(long id) {		
		PersistentStore.destroyPersistentObject(id);
	}
}
