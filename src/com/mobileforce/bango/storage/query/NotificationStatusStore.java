package com.mobileforce.bango.storage.query;

import java.util.Vector;

import com.mobileforce.bango.UiApp;
import com.mobileforce.bango.utility.BBNotification;
import com.mobileforce.bango.utility.Properties;

public class NotificationStatusStore {
	public NotificationStatusStore(){}
	
	public static boolean isNotifRead(String id){
		boolean result = false;
		if(getNotifVector() != null){
			Vector saved = getNotifVector();
			for(int s=0; s<saved.size(); s++){
				String savedID = (String) saved.elementAt(s);
				if(savedID.equalsIgnoreCase(id)){
					result = true;
					break;
				}
			}
		}
		
		saveAllNotifID(id);

		return result;
	}
	
	public static boolean isNotifIDNew(String id){
		boolean result = true;
		if(getAllNotifIDVector() != null){
			Vector saved = getAllNotifIDVector();
			for(int s=0; s<saved.size(); s++){
				String savedID = (String) saved.elementAt(s);
				if(savedID.equalsIgnoreCase(id)){
					result = false;
					break;
				}
			}
		}
		
		saveAllNotifID(id);

		return result;
	}
	
	public static boolean isNotificationUpdate(){
		boolean result = false;
		if(getNotifUpdate() != null){
			Vector saved = getNotifUpdate();
			String status = (String) saved.elementAt(0);
			
			if(status.equalsIgnoreCase("1")){
				result = true;
			}
		}

		return result;
	}
	
	public static void updateNotificationStatus(boolean status){
		// create new and add
		Vector vector = new Vector();

		if (status) {
			vector.addElement("1");
		} else {
			vector.addElement("0");
		}

		saveVector(vector, Properties._NOTIF_STORE_UPDATE);
		
		if(!status){
			BBNotification.clearAllNotification();
			UiApp.setIconDefault();
		}
	}
	
	public static void saveNotifRead(String id){
		if(getNotifVector() == null){
			// create new and add
			Vector vector = new Vector();
			vector.addElement(id);
			
			saveVector(vector, Properties._NOTIF_STORE);
		}
		else{
			// find if available
			Vector saved = getNotifVector();
			boolean add = true;
			for(int s=0; s<saved.size(); s++){
				String savedID = (String) saved.elementAt(s);
				if(savedID.equalsIgnoreCase(id)){
					add = false;
					break;
				}
			}
			
			if(add){
				// update
				saved.addElement(id);
				saveVector(saved, Properties._NOTIF_STORE);
			}
		}
	}
	
	public static void saveAllNotifID(String id){
		if(getAllNotifIDVector() == null){
			// create new and add
			Vector vector = new Vector();
			vector.addElement(id);
			
			saveVector(vector, Properties._NOTIF_STORE_ALL);
		}
		else{
			// find if available
			Vector saved = getAllNotifIDVector();
			boolean add = true;
			for(int s=0; s<saved.size(); s++){
				String savedID = (String) saved.elementAt(s);
				if(savedID.equalsIgnoreCase(id)){
					add = false;
					break;
				}
			}
			
			if(add){
				// update
				saved.addElement(id);
				saveVector(saved, Properties._NOTIF_STORE_ALL);
			}
		}
	}
	
	private static Vector getNotifVector(){
		Vector data = new GenericStore(Properties._NOTIF_STORE).getVectorOfStoredData();
		return data.size() == 0 ? null : data;
	}
	
	private static Vector getAllNotifIDVector(){
		Vector data = new GenericStore(Properties._NOTIF_STORE_ALL).getVectorOfStoredData();
		return data.size() == 0 ? null : data;
	}
	
	private static Vector getNotifUpdate(){
		Vector data = new GenericStore(Properties._NOTIF_STORE_UPDATE).getVectorOfStoredData();
		return data.size() == 0 ? null : data;
	}
	
	private static void saveVector(Vector v, long id) {
		new GenericStore(id).saveDataVector(v);
	}
	
	public static void deleteNotificationStatus() {		
		try {
			new GenericStore(Properties._NOTIF_STORE).deleteStoredData(Properties._NOTIF_STORE);
			new GenericStore(Properties._NOTIF_STORE_ALL).deleteStoredData(Properties._NOTIF_STORE_ALL);
		} catch (Exception x) {
			System.out.println("Fail deleting NotificationStatus>>> " + x.toString());
		}
	}
}
