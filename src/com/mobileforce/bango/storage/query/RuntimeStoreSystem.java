package com.mobileforce.bango.storage.query;

import java.util.Hashtable;

import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.ControlledAccessException;
import net.rim.device.api.system.RuntimeStore;

public class RuntimeStoreSystem {
	private static RuntimeStore store;
	private static Hashtable data;
	
	static {
		store = RuntimeStore.getRuntimeStore();
		Object o = store.get(Properties._RUNTIME_STORE_ID);
		if (o == null) {
			data = new Hashtable();
			store.put(Properties._RUNTIME_STORE_ID, data);
		} else {
			data = (Hashtable) o;
		}
	}
	
	public static void saveData() {
		try {
			store.replace(Properties._RUNTIME_STORE_ID, data);
		} catch (ControlledAccessException e) {
			System.out.println("Failed to save to runtime store. "+e.getMessage());
		}
	}
	
	public static void setData(String fieldName, Object content) {
		data.put(fieldName, content);
		saveData();
	}
	
	public static Object getData(String fieldName) {
		if (data.containsKey(fieldName)) return data.get(fieldName);
		else return null;
	}
	
	public static void deleteData(String fieldName) {
		if (data.containsKey(fieldName)) data.remove(fieldName);
		saveData();
	}
	
	public static Hashtable getData() {
		Hashtable result = null;
		try {
			result = (Hashtable) store.get(Properties._RUNTIME_STORE_ID);
		} catch (ControlledAccessException e) {
			System.out.println("Failed to get data from runtime store. "+e.getMessage());
		}
		return result;
	}
}
