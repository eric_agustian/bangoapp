package com.mobileforce.bango.storage.query;

import java.util.Vector;

import com.mobileforce.bango.storage.persistable.BodyPersistable;
import com.mobileforce.bango.storage.persistable.BodyPersistable2;
import com.mobileforce.bango.storage.persistable.BodyPersistable3;
import com.mobileforce.bango.storage.persistable.HeaderPersistable;
import com.mobileforce.bango.storage.persistable.ReviewsPersistable;

public class TemporerStore {
	public TemporerStore(){}
	
	private static Vector homeByDistance = new Vector();
	private static Vector homeByPopular = new Vector();
	private static Vector homeByNew = new Vector();
	private static Vector menuSpecial = new Vector();
	private static Vector recipeByPopular = new Vector();
	private static Vector recipeByNew = new Vector();
	private static Vector notifList = new Vector();
	private static Vector userProfile = new Vector();
	
	public static Vector getHomebyDistance(){
		return homeByDistance;
	}
	
	public static Vector getHomebyPopular(){
		return homeByPopular;
	}
	
	public static Vector getHomebyNew(){
		return homeByNew;
	}
	
	public static Vector getRecipebyPopular(){
		return recipeByPopular;
	}
	
	public static Vector getRecipebyNew(){
		return recipeByNew;
	}
	
	public static Vector getMenuSpecial(){
		return menuSpecial;
	}
	
	public static Vector getNotificationList(){
		return notifList;
	}
	
	public static Vector getUserProfile(){
		return userProfile;
	}
	
//	public static HeaderPersistable getHomeHeaderbyDistance(){
//		return getSelectedHeaderPersistable(homeByDistance);
//	}
//	
//	public static HeaderPersistable getHomeHeaderbyPopular(){
//		return getSelectedHeaderPersistable(homeByPopular);
//	}
//	
//	public static HeaderPersistable getHomeHeaderbyNew(){
//		return getSelectedHeaderPersistable(homeByNew);
//	}
//	
//	public static HeaderPersistable getRecipeHeaderbyPopular(){
//		return getSelectedHeaderPersistable(recipeByPopular);
//	}
//	
//	public static HeaderPersistable getRecipeHeaderbyNew(){
//		return getSelectedHeaderPersistable(recipeByNew);
//	}
	
	public static HeaderPersistable getNotificationHeader(){
		return getSelectedHeaderPersistable(notifList);
	}
	
	public static HeaderPersistable getMenuSpecialData(int index){
		HeaderPersistable data = (HeaderPersistable) menuSpecial.elementAt(index);
		return data;
	}
	
//	public static BodyPersistable getHomeBodybyDistance(int index){
//		return getSelectedBodyPersistable(homeByDistance, index);
//	}
//	
//	public static BodyPersistable getHomeBodybyPopular(int index){
//		return getSelectedBodyPersistable(homeByPopular, index);
//	}
//	
//	public static BodyPersistable getHomeBodybyNew(int index){
//		return getSelectedBodyPersistable(homeByNew, index);
//	}
	
	public static void setHomeByDistance(Vector vector){
		homeByDistance = updateVector(homeByDistance, vector);
	}
	
	public static void resetHomeByDistance(){
		homeByDistance = new Vector();
	}
	
	public static void setHomeByPopular(Vector vector){
		homeByPopular = updateVector(homeByPopular, vector);
	}
	
	public static void setHomeByNew(Vector vector){
		homeByNew = updateVector(homeByNew, vector);
	}
	
	public static void setRecipeByPopular(Vector vector){
		recipeByPopular = updateVector2(recipeByPopular, vector);
	}
	
	public static void setRecipeByNew(Vector vector){
		recipeByNew = updateVector2(recipeByNew, vector);
	}
	
	public static void setMenuSpecial(Vector vector){
		menuSpecial = vector;
	}
	
	public static void setNotificationList(Vector vector){
		notifList = updateVector3(notifList, vector);
	}
	
	public static void setUserProfile(Vector vector){
		userProfile = updateVector4(userProfile, vector);
	}
	
	public static void resetNotificationList(){
		notifList = new Vector();
	}
	
	public static void resetUserProfile(){
		userProfile = new Vector();
	}
	
	private static Vector updateVector(Vector old, Vector vector){
		if(old.size() == 0){
			return vector;
		}
		else{
			Vector update = new Vector();
			
			//update head
			Vector head = (Vector) vector.elementAt(0);
			update.addElement(head);
			
			//update body
			Vector body = new Vector();
			
			Vector bodyOld = (Vector) old.elementAt(1);
			for(int v=0; v<bodyOld.size(); v++){
				BodyPersistable data = (BodyPersistable) bodyOld.elementAt(v);
				body.addElement(data);
			}
			
			Vector bodyNew = (Vector) vector.elementAt(1);
			for(int v=0; v<bodyNew.size(); v++){
				BodyPersistable data = (BodyPersistable) bodyNew.elementAt(v);
				body.addElement(data);
			}
			
			update.addElement(body);
			
			return update;
		}
	}
	
	private static Vector updateVector2(Vector old, Vector vector){
		if(old.size() == 0){
			return vector;
		}
		else{
			Vector update = new Vector();
			
			//update head
			Vector head = (Vector) vector.elementAt(0);
			update.addElement(head);
			
			//update body
			Vector body = new Vector();
			
			Vector bodyOld = (Vector) old.elementAt(1);
			for(int v=0; v<bodyOld.size(); v++){
				BodyPersistable2 data = (BodyPersistable2) bodyOld.elementAt(v);
				body.addElement(data);
			}
			
			Vector bodyNew = (Vector) vector.elementAt(1);
			for(int v=0; v<bodyNew.size(); v++){
				BodyPersistable2 data = (BodyPersistable2) bodyNew.elementAt(v);
				body.addElement(data);
			}
			
			update.addElement(body);
			
			return update;
		}
	}
	
	private static Vector updateVector3(Vector old, Vector vector){
		if(old.size() == 0){
			return vector;
		}
		else{
			Vector update = new Vector();
			
			//update head
			Vector head = (Vector) vector.elementAt(0);
			update.addElement(head);
			
			//update body
			Vector body = new Vector();
			
			Vector bodyOld = (Vector) old.elementAt(1);
			for(int v=0; v<bodyOld.size(); v++){
				BodyPersistable3 data = (BodyPersistable3) bodyOld.elementAt(v);
				body.addElement(data);
			}
			
			Vector bodyNew = (Vector) vector.elementAt(1);
			for(int v=0; v<bodyNew.size(); v++){
				BodyPersistable3 data = (BodyPersistable3) bodyNew.elementAt(v);
				body.addElement(data);
			}
			
			update.addElement(body);
			
			return update;
		}
	}
	
	private static Vector updateVector4(Vector old, Vector vector){
		if(old.size() == 0){
			return vector;
		}
		else{
			Vector update = new Vector();
			
			// update profile
			Vector profile = (Vector) vector.elementAt(0);
			update.addElement(profile);
			
			// update badges
			Vector badges = (Vector) vector.elementAt(1);
			update.addElement(badges);
			
			//update head
			Vector head = (Vector) vector.elementAt(2);
			update.addElement(head);
			
			//update body
			Vector reviews = new Vector();
			
			Vector bodyOld = (Vector) old.elementAt(3);
			for(int v=0; v<bodyOld.size(); v++){
				ReviewsPersistable data = (ReviewsPersistable) bodyOld.elementAt(v);
				reviews.addElement(data);
			}
			
			Vector bodyNew = (Vector) vector.elementAt(3);
			for(int v=0; v<bodyNew.size(); v++){
				ReviewsPersistable data = (ReviewsPersistable) bodyNew.elementAt(v);
				reviews.addElement(data);
			}
			
			update.addElement(reviews);
			
			return update;
		}
	}
	
//	private static BodyPersistable getSelectedBodyPersistable(Vector vector, int index){
//		Vector body = (Vector) vector.elementAt(1);
//		BodyPersistable data = (BodyPersistable) body.elementAt(index);
//		
//		return data;
//	}
	
	private static HeaderPersistable getSelectedHeaderPersistable(Vector vector){
		Vector header = (Vector) vector.elementAt(0);
		HeaderPersistable data = (HeaderPersistable) header.elementAt(0);
		
		return data;
	}
}
