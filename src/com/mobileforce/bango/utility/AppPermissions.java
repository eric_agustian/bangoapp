package com.mobileforce.bango.utility;

import net.rim.device.api.applicationcontrol.ApplicationPermissions;
import net.rim.device.api.applicationcontrol.ApplicationPermissionsManager;

public class AppPermissions {
	private static final ApplicationPermissionsManager apm = ApplicationPermissionsManager.getInstance();
	private static final ApplicationPermissions original = apm.getApplicationPermissions();
	
	public AppPermissions(){}
	
	public void setPermissions() {
		//application ability //PERMISSION_INPUT_SIMULATION
		if( original.getPermission( ApplicationPermissions.PERMISSION_INPUT_SIMULATION ) != ApplicationPermissions.VALUE_ALLOW){
			setPermission(ApplicationPermissions.PERMISSION_INPUT_SIMULATION);
		}
		
		//file api
		if( original.getPermission( ApplicationPermissions.PERMISSION_FILE_API ) != ApplicationPermissions.VALUE_ALLOW){
			setPermission(ApplicationPermissions.PERMISSION_FILE_API);
		}
		
		//setting device //PERMISSION_DEVICE_SETTINGS
		if( original.getPermission( ApplicationPermissions.PERMISSION_DEVICE_SETTINGS ) != ApplicationPermissions.VALUE_ALLOW){
			setPermission(ApplicationPermissions.PERMISSION_DEVICE_SETTINGS);
		}
		
		//media file
		if( original.getPermission( ApplicationPermissions.PERMISSION_MEDIA ) != ApplicationPermissions.VALUE_ALLOW){
			setPermission(ApplicationPermissions.PERMISSION_MEDIA);
		}
		
		//screen capture //PERMISSION_RECORDING
		if( original.getPermission( ApplicationPermissions.PERMISSION_RECORDING ) != ApplicationPermissions.VALUE_ALLOW){
			setPermission(ApplicationPermissions.PERMISSION_RECORDING);
		}
		
		//cross application communication //PERMISSION_CROSS_APPLICATION_COMMUNICATION
		if( original.getPermission( ApplicationPermissions.PERMISSION_CROSS_APPLICATION_COMMUNICATION ) != ApplicationPermissions.VALUE_ALLOW){
			setPermission(ApplicationPermissions.PERMISSION_CROSS_APPLICATION_COMMUNICATION);
		}
	}

	private boolean setPermission(int permission) {
	    boolean updatedPermissions = false;
	    ApplicationPermissions ap = apm.getApplicationPermissions();
	    if (ap.containsPermissionKey(permission)) {
	      int eventInjectorPermission = ap.getPermission(permission);
	      if (eventInjectorPermission != ApplicationPermissions.VALUE_ALLOW) {
	    	  	ap.addPermission(permission);
	    	  	updatedPermissions = apm.invokePermissionsRequest(ap);
	      }
	    } else {
	      ap.addPermission(permission);
	      updatedPermissions = apm.invokePermissionsRequest(ap);
	    }
	   
	    return updatedPermissions;
	  }
}