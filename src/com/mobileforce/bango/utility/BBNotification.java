package com.mobileforce.bango.utility;

import net.rim.blackberry.api.messagelist.ApplicationIcon;
import net.rim.blackberry.api.messagelist.ApplicationIndicatorRegistry;
import net.rim.device.api.system.Alert;
import net.rim.device.api.system.EncodedImage;
import net.rim.device.api.system.LED;

public class BBNotification {
    public static void startVibrate(int time){
    	try{
    		if(Alert.isVibrateSupported()){
    			Alert.startVibrate(time);
    		}
    	}catch(Exception v){ System.out.println("Could not start vibrate: "+v); }
    }
    
    public static void stopVibrate(){
    	try{
    		if(Alert.isVibrateSupported()){
	    		Alert.stopVibrate();
	    	}
    	}catch(Exception v){ System.out.println("Could not stop vibrate: "+v); }
    }
    
    public static void startBlinking(){
    	try{
    		if(LED.isSupported(LED.LED_TYPE_STATUS)){
		    	LED.setColorConfiguration(1000, 1000, 0x00FF0000);
		        LED.setState(LED.STATE_BLINKING);
    		}
    	}catch(Exception l){ System.out.println("Could not turn on led: "+l); }
    }
    
    public static void stopBlinking(){
    	try{
    		if(LED.isSupported(LED.LED_TYPE_STATUS)){
	    		LED.setState(LED.STATE_OFF);
	    	}
    	}catch(Exception l){ System.out.println("Could not turn off led: "+l); }
    }
    
    //work from OS 4.6
    public static void registerIndicator() {
    	try {
	    	EncodedImage mImageGreen = EncodedImage.getEncodedImageResource(Properties._INDICATOR);
	        ApplicationIcon mIconGreen = new ApplicationIcon(mImageGreen);
	        ApplicationIcon mIcon = mIconGreen;
	        
		    ApplicationIndicatorRegistry.getInstance().register(mIcon, false, true);
		} catch (Exception e) { }
	}

	public static void unregisterIndicator() {
		try {
			ApplicationIndicatorRegistry.getInstance().unregister();
		} catch (Exception e) { }
	}
    //******************************************************************************************
	
	public static void startAllNotification(){
		startBlinking();
		startVibrate(1000);
		registerIndicator();
    }
	
	public static void clearAllNotification(){
    	stopBlinking(); //no blinking LED
    	stopVibrate();
    	unregisterIndicator();
    }
}
