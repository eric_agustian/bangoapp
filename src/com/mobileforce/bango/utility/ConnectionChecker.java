package com.mobileforce.bango.utility;

import net.rim.device.api.servicebook.ServiceBook;
import net.rim.device.api.servicebook.ServiceRecord;
import net.rim.device.api.system.CoverageInfo;
import net.rim.device.api.system.DeviceInfo;
import net.rim.device.api.system.WLANInfo;

/**
 *  beta implementation @30-03-2010 by SEAL555
 */

public class ConnectionChecker {
	private int conn_type = 0;
	private String conn_params = "";
	public boolean conn_available = false;
	
	public ConnectionChecker(){
		//if(RadioInfo.getSignalLevel() != RadioInfo.LEVEL_NO_COVERAGE) the BlackBerry smartphone has radio signal.
		//if(RadioInfo.getNetworkService() & RadioInfo.NETWORK_SERVICE_DATA)>0 the BlackBerry smartphone has data connectivity.
	}
	
	public void Checking(){
	 //ini yg biasa, bukan BES
		//;deviceside=false;ConnectionType=mds-public
		//;deviceside=true;interface=wifi
		
		try{
			 if((CoverageInfo.isCoverageSufficient(CoverageInfo.COVERAGE_MDS)) 
					 || (CoverageInfo.isCoverageSufficient(CoverageInfo.COVERAGE_BIS_B))
					 || (CoverageInfo.isCoverageSufficient(CoverageInfo.COVERAGE_DIRECT)) != false){
			//System.out.println("coverage status::: "+CoverageInfo.getCoverageStatus());
			//System.out.println("coverage 1::: "+CoverageInfo.isCoverageSufficient(CoverageInfo.COVERAGE_MDS));
			//System.out.println("coverage 2::: "+CoverageInfo.isCoverageSufficient(CoverageInfo.COVERAGE_BIS_B));
			//System.out.println("coverage 3::: "+CoverageInfo.isCoverageSufficient(CoverageInfo.COVERAGE_DIRECT));
			//System.out.println("coverage v1::: "+CoverageInfo.COVERAGE_MDS);
			//System.out.println("coverage v2::: "+CoverageInfo.COVERAGE_BIS_B);
			//System.out.println("coverage v3::: "+CoverageInfo.COVERAGE_DIRECT);
			
			//if(CoverageInfo.getCoverageStatus()!= CoverageInfo.COVERAGE_NONE){
				conn_available = true;
			}
		}catch(Exception c){}
		
		if(conn_available){
			//@ 20-09-2010
			//is BIS/BES
			if(!DeviceInfo.isSimulator()){
				try{
					ServiceBook sb = ServiceBook.getSB();
					ServiceRecord[] records = sb.getRecords();
					
					for (int i = 0; i < records.length; i++) {
						ServiceRecord myRecord = records[i];
						String cid, uid;
	
						if (myRecord.isValid() && !myRecord.isDisabled()) {
							cid = myRecord.getCid().toLowerCase();
							uid = myRecord.getUid().toLowerCase();
							
							//System.out.println("IPPP "+cid);
							//System.out.println("GPMDS "+uid);
							// BIS & BES
							if (cid.indexOf("ippp") != -1 && uid.indexOf("gpmds") != -1) {
								conn_type = 0;
								break;
							}
							else{
								conn_type = -1;
							}
						}
					}
				}catch(Exception b){ System.out.println("BIS/BES inactive"); }
			}
			
			//is WLAN
			try{
				if(WLANInfo.getWLANState() == WLANInfo.WLAN_STATE_CONNECTED){
					conn_type = 1 ;
					conn_params = ";deviceside=true;interface=wifi";
				}
			}catch(Exception wlan){ System.out.println("WiFi not supported"); }
			
			if(conn_type==0){
				conn_params = ";deviceside=false;ConnectionType=mds-public";
				Properties._APN_BB = conn_params;
				System.out.println("MDS (BIS/BES) used");
			}else if(conn_type==1){
				Properties._APN_BB = conn_params;
				System.out.println("WiFi used");
			}else{
				//no action
				conn_available = false;
				System.out.println("No connection available");
			}
		}
	}
	
	public boolean isAvailable(){
		return conn_available;
	}
}
