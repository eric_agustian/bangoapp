package com.mobileforce.bango.utility;

import net.rim.blackberry.api.phone.Phone;
import net.rim.device.api.system.DeviceInfo;
import net.rim.device.api.system.Display;
import net.rim.device.api.system.RadioInfo;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.FontFamily;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;

public class DeviceProperties {
	public static String getBBPIN(){
		return Integer.toHexString(DeviceInfo.getDeviceId()).toUpperCase(); //pin BB dalam format HexaDecimal; //test "88888888";//
	}
	
	public static String getBBType(){
		return DeviceInfo.getDeviceName();	//tipe BB
	}
	
	public static String getBBOS(){
		return DeviceInfo.getSoftwareVersion(); //versi software
	}
	
	public static String getBBOP(){
		return RadioInfo.getCurrentNetworkName(); //network operator name
	}
	
	public static String getBBMSISDN(){
		String msisdn = Phone.getDevicePhoneNumber(true); //MSISDN
		
		if(msisdn!=null){
            if(msisdn.indexOf("+")!= -1) msisdn = msisdn.substring(msisdn.indexOf("+")+1);
            if(msisdn.indexOf("(")!= -1) msisdn = msisdn.substring(msisdn.indexOf("(")+1);
            msisdn = GStringUtil.cleanNonNumericString(msisdn);
            if(msisdn.startsWith("0")) msisdn = msisdn.substring(1);
        }
		else{
			msisdn = "";
		}
		
		return msisdn;
	}
	
	public static String getBBIMEI(){
		String result = "";
		
		try{
			if(RadioInfo.getNetworkType()==RadioInfo.NETWORK_CDMA){
				result = net.rim.device.api.system.CDMAInfo.getHexMEID();
			}
			else{
				result = net.rim.device.api.system.GPRSInfo.imeiToString(net.rim.device.api.system.GPRSInfo.getIMEI()); //IMEI
			}
		}catch(Exception x){
			System.out.println("[DevicePorperties - unable retrieve IMEI/IMSI] "+x.getMessage());
		}
		
		return result;
	}
	
	public static void hideVirtualKeyboard(){
  		try{
  			if(net.rim.device.api.ui.Touchscreen.isSupported()){
  				if(UiApplication.getUiApplication().getActiveScreen().getVirtualKeyboard().getVisibility() == net.rim.device.api.ui.VirtualKeyboard.SHOW) UiApplication.getUiApplication().getActiveScreen().getVirtualKeyboard().setVisibility(net.rim.device.api.ui.VirtualKeyboard.HIDE);
  			}
  		}catch(Exception x){ System.out.println("[unable hiding virtual keyboard]"); }
  	}
	
	public static void setDisplayPortraitDirection() {
		try{
		  UiEngineInstance engine = Ui.getUiEngineInstance();
		  int directions = Display.ORIENTATION_PORTRAIT | Display.DIRECTION_NORTH;
		  engine.setAcceptableDirections(directions);
		}catch(Exception x){ System.out.println("[unable set portrait direction]"); }
	}
	
	public static Font getDefaultApplicationFont(int fontSize){
		FontFamily fontFamily;
		try {
			fontFamily = FontFamily.forName("Arial");
			return fontFamily.getFont(Font.PLAIN, fontSize);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return Font.getDefault().derive(Font.PLAIN, fontSize);
		}
	}
	
	public static Font getDefaultApplicationFont(int fontStyle, int fontSize){
		FontFamily fontFamily;
		try {
			fontFamily = FontFamily.forName("Arial");
			return fontFamily.getFont(fontStyle, fontSize);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return Font.getDefault().derive(Font.PLAIN, fontSize);
		}
	}
}
