//#preprocess

package com.mobileforce.bango.utility;

import com.mobileforce.bango.services.callback.FilePickerCallback;
import net.rim.device.api.ui.picker.FilePicker;

public class FilePickerListener implements FilePicker.Listener {
	private FilePicker filePicker;
	private FilePickerCallback callback;
	
	public FilePickerListener(String title, int view, String[] filter, String path, FilePickerCallback callback){
		//intialize
		filePicker = FilePicker.getInstance();
		
		//set title
		if(title!=null){
			//#ifdef BlackBerrySDK5.0.0
			//#else
			filePicker.setTitle(title);
			//#endif
		}
		
		//select view or filter
		if(view!=-1){
			//#ifdef BlackBerrySDK5.0.0
			//#else
			filePicker.setView(view);
			//#endif
		}
		else{
			if(filter!=null){
				int f = 0;
				while(f<filter.length){
					filePicker.setFilter(filter[f]);
					
					f++;
				}
			}
		}
		
		//select default path to open if available
		if(path!=null){
			//path = System.getProperty("fileconn.dir.music");
			//path = System.getProperty("fileconn.dir.photos");
			//path = System.getProperty("fileconn.dir.videos");
			filePicker.setPath(path);
		}
		
		//set action type
		this.callback = callback;
		
		//set listener
		filePicker.setListener(this);
		
		//show the file picker
		filePicker.show();
	}
	
	public void selectionDone(String selection) {
		System.out.println("FilePickerListerner >>>"+selection);
		if(selection != null && selection.length() > 0){
			callback.onPicked(selection);
		}
	}

}
