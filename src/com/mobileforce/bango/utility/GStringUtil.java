package com.mobileforce.bango.utility;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import net.rim.device.api.i18n.DateFormat;
import net.rim.device.api.i18n.SimpleDateFormat;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Ui;

public class GStringUtil {

	public static String[] split(String data, char separatorChar) {
    	
    	if (data==null || data.length()==0) {
            return new String[0];
		}
        Vector elementList = new Vector();
        
        int s = 0;
        int e = data.length();
        String str;
        while(true) {
        	e = data.indexOf(separatorChar, s);
        	if (e == -1) {
				e = data.length();
				if (s < e) {
					str = data.substring(s, e);
					elementList.addElement(str);
				}
				break;
			}
			str = data.substring(s, e);
			elementList.addElement(str);
			s = e + 1;
        }
        
        if (elementList.size()==0) {
			return new String[0];
		}

        String[] elements = new String[elementList.size()];
        elementList.copyInto(elements);

        return elements;
    }
	
	public static String[] split(String data, String separatorChar) {
    	
    	if (data==null || data.length()==0) {
            return new String[0];
		}
        Vector elementList = new Vector();
        
        int s = 0;
        int e = data.length();
        String str;
        while(true) {
        	e = data.indexOf(separatorChar, s);
        	if (e == -1) {
				e = data.length();
				if (s < e) {
					str = data.substring(s, e);
					elementList.addElement(str);
				}
				break;
			}
			str = data.substring(s, e);
			elementList.addElement(str);
			s = e + 1;
        }
        
        if (elementList.size()==0) {
			return new String[0];
		}

        String[] elements = new String[elementList.size()];
        elementList.copyInto(elements);

        return elements;
    }
	
	public static String[] splitMixed(String data, char separatorChar) {
        
        if (data==null || data.length()==0) {
            return new String[0];
        }
        Vector elementList = new Vector();
        
        int s = 0;
        int e = data.length();
        int count = 0;
        String str;
        while(true && count <= 5) {
            e = data.indexOf(separatorChar, s);
            if (e == -1) {
                e = data.length();
                if (s < e) {
                    str = data.substring(s, e);
                    elementList.addElement(str);
                    count++;
                }
                break;
            }
            str = data.substring(s, e);
            elementList.addElement(str);
            count++;
            s = e + 1;
        }
        if(count > 5)
        {
            str = data.substring(s, data.length());
            elementList.addElement(str); 
        }
        
        if (elementList.size()==0) {
            return new String[0];
        }

        String[] elements = new String[elementList.size()];
        elementList.copyInto(elements);

        return elements;
    }
    
	
	
	public static Vector splitToVector(String data, char separatorChar) {
    	
    	if (data==null || data.length()==0) {
            return new Vector(0);
		}
        Vector elementList = new Vector(1);
        
        int s = 0;
        int e = data.length();
        String str;
        while(true) {
        	e = data.indexOf(separatorChar, s);
        	if (e == -1) {
				e = data.length();
				if (s < e) {
					str = data.substring(s, e);
					elementList.addElement(str);
				}
				break;
			}
			str = data.substring(s, e);
			elementList.addElement(str);
			s = e + 1;
        }
        
        if (elementList.size()==0) {
			return new Vector(0);
		}

        return elementList;
    }

    public static String[] splitBy2(String data, char separatorChar) {

    	int i = data.indexOf(separatorChar);
		if (i == -1 || i == 0) {
			String strs[] = new String[1];
			strs[0] = data;
			return strs;
		}
		String str1 = data.substring(0, i).trim();
		String str2 = data.substring(i + 1).trim();
		
		String strs[] = new String[2];
		strs[0] = str1;
		strs[1] = str2;

		return strs;
    }

    public static String join(String[] elements, String separator) {
        StringBuffer sb = new StringBuffer();
        int len = elements.length;
        for (int i = 0; i < len; i++) {
            sb.append(elements[i]);
            if (i != len-1) {
                sb.append(separator);
            }
        }
        return sb.toString();
    }

    public static String join(Vector elements, String separator) {
        StringBuffer sb = new StringBuffer();
        int len = elements.size();
        for (int i = 0; i < len; i++) {
            sb.append(elements.elementAt(i));
            if (i != len-1) {
                sb.append(separator);
            }
        }
        return sb.toString();
    }
    
    public static String getLastPathComponent(String str) {
    	
    	if (str == null) {
    		throw new IllegalArgumentException("Argument 'str' is NULL");
    	}
    	
    	int point = str.lastIndexOf('/');
    	return (point > -1) ? str.substring(point+1) : null; 
    }

    public static boolean isEmptyString(String str) {

		return (str == null) || (str.trim().length() < 1);
	}
    
    public static String encodePercent(String s) {
    	
    	 if (isEmptyString(s)) {
    		 throw new IllegalArgumentException("Argument 'str' is NULL");
         }
    	 
    	 StringBuffer sbResult = new StringBuffer();
         ByteArrayInputStream bais = null;
         try {
        	 bais = new ByteArrayInputStream(s.getBytes("UTF-8"));
         }
         catch (UnsupportedEncodingException e) {
        	 bais = new ByteArrayInputStream(s.getBytes());
         }
         
         int c = bais.read();
         while (c >= 0) {
             if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || c == '.' || c == '-' || c == '_'/* || c == '*' */) {
            	 sbResult.append((char) c);
             }
             else if (c == ' ') {
            	 sbResult.append("%20");
             }
             else {
                 if (c < 128) {
                	 sbResult.append(getHexChar(c));
                 }
                 else if (c < 224) {
                	 sbResult.append(getHexChar(c));
                	 sbResult.append(getHexChar(bais.read()));
                 }
                 else if (c < 240) {
                	 sbResult.append(getHexChar(c));
                	 sbResult.append(getHexChar(bais.read()));
                	 sbResult.append(getHexChar(bais.read()));
                 }
             }
             
             c = bais.read();
         }
         
         return sbResult.toString();
    }
    
    private static String getHexChar(int c) {
    	
        return (c < 16 ? "%0" : "%") + Integer.toHexString(c).toUpperCase();
    }

	public static String decodePercent(String s) {

		ByteArrayOutputStream out = new ByteArrayOutputStream(s.length());

		for (int i = 0; i < s.length(); i++) {
			int c = (int) s.charAt(i);
			if (c == '+') {
				out.write(' ');
			}
			else if (c == '%') {
				int c1 = Character.digit(s.charAt(++i), 16);
				int c2 = Character.digit(s.charAt(++i), 16);
				out.write((char) (c1 * 16 + c2));
			}
			else {
				out.write(c);
			}
		}

		return out.toString();
	}

	public static String arrayToString(String[] strArr, String separator) {

		if (strArr == null || strArr.length < 1) {
			return "";
		}

		StringBuffer result = new StringBuffer();

		int length = strArr.length;
		for (int i = 0; i < length; i++) {
			if (result.length() > 0) {
				result.append(separator);
			}
			result.append(strArr[i]);
		}

		return result.toString();
	}
	
	public static String cleanNonNumericString(String string){
	    if(string != null){
	        StringBuffer result = new StringBuffer();

	        int length = string.length();
	        for (int i = 0; i < length; i++) {
	            String cut = string.substring(i, i+1);
	            if (cut.equalsIgnoreCase("0") || cut.equalsIgnoreCase("1") || cut.equalsIgnoreCase("2") || cut.equalsIgnoreCase("3") || cut.equalsIgnoreCase("4") || cut.equalsIgnoreCase("5") || cut.equalsIgnoreCase("6") || cut.equalsIgnoreCase("7") || cut.equalsIgnoreCase("8") || cut.equalsIgnoreCase("9")
	                    
	            ) {
	                result.append(cut);
	            }
	        }
	        
	        string = result.toString();
	    }
	    
	    return string;
	}
	
	public static boolean equalString(String str1, String str2){
        if(str1 == null || str2 == null){
            return false;
        }
        return str1.equals(str2);
    }
	
	public static String toString(Object[] strings, String separator) {
        if (strings == null || strings.length == 0) {
            return null;
        }
        int a = 2;
        int b = 3;
        StringBuffer result = new StringBuffer();
        result.append(strings[0].toString());

        for (int i = 1; i < strings.length; i++) {
            result.append(separator).append(strings[i].toString());
        }

        return result.toString();
    }
	
	public static boolean notIn(Object valA, Object[] valsB){
        if(valA == null || valsB == null){
            return false;
        }
        return !in(valA, valsB);
    }
	
	public static boolean in(Object valA, Object[] valsB){
        if(valA == null || valsB == null){
            return false;
        }
        for (int i = 0; i < valsB.length; i++) {
            if(valA.equals(valsB[i])){
                return true;
            }
        }
        return false;
    }
	
	public static String generateTimeStamp(String format){
		//format sample:
		//"yyyy-MM-dd-HH:mm:ss" 2011-06-17-07:13:13
		
		Date date = new Date(System.currentTimeMillis() | DateFormat.TIME_FULL);
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		
		return sdf.format(date);
	}
	
	public static String dateFormatddMMMyyyy(String date){
		
		String dStr[] = split(date, '/');
		String day = dStr[0];
		String month = dStr[1];
		String year = dStr[2];
		
		Date d = new Date(0);
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		
		cal.set(Calendar.MONTH,Integer.parseInt(month)-1);
		cal.set(Calendar.DATE,Integer.parseInt(day));
		cal.set(Calendar.YEAR,Integer.parseInt(year));
		
		d = cal.getTime();
		
		SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
		return df.format(d);
	}
	
	public static String replace( String str, String pattern, String replace ) 
	{
	    int s = 0;
	    int e = 0;
	    StringBuffer result = new StringBuffer();

	    while ( (e = str.indexOf( pattern, s ) ) >= 0 ) 
	    {
	        result.append(str.substring( s, e ) );
	        result.append( replace );
	        s = e+pattern.length();
	    }
	    result.append( str.substring( s ) );
	    return result.toString();
	}   
	
	public static boolean checkHTTP(String source){
		boolean result = false;
		//http://
		//0123456
		if(source!=null){
			if(source.length()>7){
				//modified on 24-09-2010
				if(source.indexOf("http://")!=-1 || source.indexOf("https://")!=-1){
					result = true;
				}
			}
		}
		
		return result;
	}
	
	public static String convertURL(String source){
		for(int t=0; t<source.length(); t++){
			if(source.indexOf("%26")!=-1){
				source = source.substring(0, source.indexOf("%26"))+"&"+source.substring(source.indexOf("%26")+3, source.length());
			}
		}
		
		return source;
	}
	
	public static Vector parseMessage(String message, Font fnt, int fontSize,
			int width) {
		Vector parsedStrings = new Vector();
		int start = 0, stop = 0;
		Font f = fnt.derive(Font.PLAIN, fontSize, Ui.UNITS_px);
		int lineWidth = width;
		int numChars = message.length();

		boolean ok = false;
		start = 0;
		stop = 0;

		Character space = new Character(' ');
		Character nLine = new Character('\n');
		Character comma = new Character(',');
		Character period = new Character('.');
		Character dash = new Character('-');
		Character fslash = new Character('/');
		Character bslash = new Character('\\');

		while ((start < numChars)) {
			int check = start;
			int subLength = 0;
			while (!ok) {
				if (space.charValue() == message.charAt(check)) {
					subLength = f.getAdvance(message.substring(start, check));
					if (subLength < lineWidth)
						stop = check;
					else
						ok = true;
				} else if (comma.charValue() == message.charAt(check)) {
					subLength = f.getAdvance(message.substring(start, check));
					if (subLength < lineWidth)
						stop = check;
					else
						ok = true;
				} else if (period.charValue() == message.charAt(check)) {
					subLength = f.getAdvance(message.substring(start, check));
					if (subLength < lineWidth)
						stop = check;
					else
						ok = true;
				} else if (dash.charValue() == message.charAt(check)) {
					subLength = f.getAdvance(message.substring(start, check));
					if (subLength < lineWidth)
						stop = check;
					else
						ok = true;
				} else if (fslash.charValue() == message.charAt(check)) {
					subLength = f.getAdvance(message.substring(start, check));
					if (subLength < lineWidth)
						stop = check;
					else
						ok = true;
				} else if (bslash.charValue() == message.charAt(check)) {
					subLength = f.getAdvance(message.substring(start, check));
					if (subLength < lineWidth)
						stop = check;
					else
						ok = true;
				} else if (nLine.charValue() == message.charAt(check)) {
					stop = check;
					ok = true;
				} else if (f.getAdvance(message.substring(start, check)) >= lineWidth) {
					ok = true;
				}

				if (!ok) {
					if (check == (numChars - 1)) {
						stop = check + 1;
						ok = true;
					} else {
						check = check + 1;
					}
				}
			}

			if (stop == numChars) {
				parsedStrings.addElement(message.substring(start));
			} else {
				parsedStrings.addElement(message.substring(start, stop));
			}

			start = stop + 1;
			stop = start;
			ok = false;

		}
		return parsedStrings;
	}
	
}
