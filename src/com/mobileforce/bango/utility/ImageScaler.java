package com.mobileforce.bango.utility;
/*
 * version 2.0
 * more function, select between proportional/non proportional scalling
 */
import net.rim.device.api.math.Fixed32;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.EncodedImage;

public class ImageScaler {
	int x1 = 0;
	int y1 = 0;
	int x2 = 0;
	int y2 = 0;
	int fixedX = 0;
	int fixedY = 0;
	
	
	public ImageScaler(){}
	
	public EncodedImage autoScalling(EncodedImage img, int _x2, int _y2, boolean isproportional){
		x1 = img.getWidth();
		y1 = img.getHeight();
		
		x2 = _x2;
		y2 = _y2;
		
		//System.out.println("x2="+_x2+" y2="+_y2);
		
		//prevent if x2 or y2 = 0
		if(x2==0) x2 = x1;
		if(y2==0) y2 = y1;
		//**********************************************
        
        int _processScale1 = x1*100/x2;
		int _processScale2 = y1*100/y2;
		
		//prevent scale >100 x zoom in/out
		if(_processScale1==0){
			_processScale1 = Fixed32.mul(Fixed32.toFP(x1), Fixed32.toFP(100));
			_processScale1 = Fixed32.div(_processScale1, Fixed32.toFP(x2));
		}
		if(_processScale2==0){
			_processScale2 = Fixed32.mul(Fixed32.toFP(y1), Fixed32.toFP(100));
			_processScale2 = Fixed32.div(_processScale2, Fixed32.toFP(y2));
		}
		
		//System.out.println("x1="+x1+" y1="+y1+" x2="+x2+" y2="+y2);
		//System.out.println("scale1= "+_processScale1);
		//System.out.println("scale2= "+_processScale2);
		
		//@18-05-2010
		//if proportional
		if(isproportional){
			if(_processScale1 > _processScale2){
				_processScale2 = _processScale1;
			}
			else if(_processScale1 < _processScale2){
				_processScale1 = _processScale2;
			}
		}
		//*************************************************************
		
		int divisor = Fixed32.toFP(100);

		int multiplierx = Fixed32.toFP(_processScale1);
		int multipliery = Fixed32.toFP(_processScale2);
	
		fixedX = Fixed32.toFP(1);
		fixedX = Fixed32.div(fixedX,divisor);
		fixedX = Fixed32.mul(fixedX,multiplierx);

		fixedY = Fixed32.toFP(1);
		fixedY = Fixed32.div(fixedY,divisor);
		fixedY = Fixed32.mul(fixedY,multipliery);
		
		//System.out.println("fixed x="+fixedX+" fixed y="+fixedY);

		img = img.scaleImage32(fixedX,fixedY);
		
		return img;
	}
	
	
	public static Bitmap resizeBitmapARGB(Bitmap image, int width, int height) {
		 int imageWidth = image.getWidth();
		 int imageHeight = image.getHeight();

		 // Need an array (for RGB, with the size of original image)
		 int rgb[] = new int[imageWidth * imageHeight];

		 // Get the RGB array of image into "rgb"
		 image.getARGB(rgb, 0, imageWidth, 0, 0, imageWidth, imageHeight);

		 // Call to our function and obtain rgb2
		 int rgb2[] = rescaleArray(rgb, imageWidth, imageHeight, width, height);

		 // Create an image with that RGB array
		 Bitmap temp2 = new Bitmap(width, height);

		 temp2.setARGB(rgb2, 0, width, 0, 0, width, height);

		 return temp2;
	}

	private static int[] rescaleArray(int[] ini, int x, int y, int x2, int y2){
	  int out[] = new int[x2*y2];
	  for (int yy = 0; yy < y2; yy++){
	      int dy = yy * y / y2;
	      for (int xx = 0; xx < x2; xx++){
	          int dx = xx * x / x2;
	          out[(x2 * yy) + xx] = ini[(x * dy) + dx];
	      }
	  }
	  return out;
	}
}
