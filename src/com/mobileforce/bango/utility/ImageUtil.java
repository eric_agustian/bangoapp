package com.mobileforce.bango.utility;

import java.io.IOException;
import java.io.InputStream;

import javax.microedition.io.Connector;
import javax.microedition.io.file.FileConnection;
import javax.microedition.media.control.MetaDataControl;

import net.rim.device.api.math.Fixed32;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.system.EncodedImage;
import net.rim.device.api.system.PNGEncodedImage;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.XYEdges;

public class ImageUtil {
	
	public static Bitmap getBitmapFromExternalSource(String path){
		Bitmap bitmap = null;
		if(getEncodedImageFromExternalSource(path) != null) bitmap = getEncodedImageFromExternalSource(path).getBitmap();
		
		return bitmap;
	}

	public static EncodedImage getEncodedImageFromExternalSource(String path){
		EncodedImage encoded = null;
		
		FileConnection file = null;
		InputStream in = null;
		
		byte[] data;
		try {
			if(!path.startsWith("file:///")){
				if(path.startsWith("/")) path = "file://"+path;
				else path = "file:///"+path;
			}
			
			file = (FileConnection) Connector.open(path, Connector.READ_WRITE);
	        in = file.openInputStream();
	        
	        int length = in.available();
	        if(length>0){
	        	data = new byte[length];
	        	in.read(data);
	        	encoded = EncodedImage.createEncodedImage(data, 0, data.length);
	        }
	        else{
	        	length = (int) file.fileSize();
	        	
	        	data = new byte[length];
	        	in.read(data);
	        	encoded = EncodedImage.createEncodedImage(data, 0, -1);
	        }
		} catch (IOException e) {
			System.out.println("Error streaming "+e);
		} finally{
			try {
		        if (in != null) {
		            in.close();
		        }
		        if (file != null && file.isOpen()) {
		        	file.close();
		        }
			}catch (IOException ioe) {
				System.out.println("Error closing streaming "+ioe);
			}
		}
		
		return encoded;
	}
	
	public static byte[] getBitmapByteFromExternalSource(String path){
		byte[] data = null;
		
		if(getEncodedImageFromExternalSource(path) != null) data = getEncodedImageFromExternalSource(path).getData();
		
		return data;
	}
	
	// draw sprite
	public static void drawSpriteTexture(Bitmap bmp, Graphics g, int x1, int x2, int x3, int x4, int y1, int y2, int y3, int y4) {
		int[] xPts = new int[] { x1, x2, x3, x4 };
		int[] yPts = new int[] { y1, y2, y3, y3 };
		byte[] pointTypes = new byte[] { Graphics.CURVEDPATH_END_POINT, Graphics.CURVEDPATH_END_POINT, Graphics.CURVEDPATH_END_POINT, Graphics.CURVEDPATH_END_POINT };
		int dux = Fixed32.toFP(1), dvx = Fixed32.toFP(0), duy = Fixed32.toFP(0), dvy = Fixed32.toFP(1);
		g.drawTexturedPath(xPts, yPts, pointTypes, null, x1, y1, dux, dvx, duy, dvy, bmp); //g.drawTexturedPath(xPts, yPts, pointTypes, null, 0, 0, dux, dvx, duy, dvy, bmp);
	}
	
	public static void captureCurrentScreen(){
		Bitmap capture = new Bitmap(Display.getWidth(), Display.getHeight());
		Display.screenshot(capture);
		Properties._SCREEN_CAPTURE = capture;
	}
	
	public static Bitmap getMaskedCircledIcon(Bitmap mask, Bitmap icon, int width, int height){
		//resize icon & mask to match masking procedure
		icon = ImageScaler.resizeBitmapARGB(icon, mask.getWidth(), mask.getHeight());
		
		BitmapMask bitmapMask = new BitmapMask( new XYEdges( mask.getWidth()/2, mask.getWidth()/2, mask.getWidth()/2, mask.getWidth()/2 ), mask );
		bitmapMask.applyMask(icon);
		
		if(icon.getWidth() != width || icon.getHeight() != height){
			icon = ImageScaler.resizeBitmapARGB(icon, width, height);
		}
		
		return icon;
	}
	
	public static Bitmap getOrientationCorrectedImage(Bitmap original){
		byte[] raw = null;
		try{
			if(original!=null){
				raw = PNGEncodedImage.encode(original).getData();
			}
		}
		catch(Exception f){ raw = null; }
		
		EncodedImage fullImage = EncodedImage.createEncodedImage(raw, 0, raw.length);

		MetaDataControl m = fullImage.getMetaData();

		String orientation = "";
		try {
			orientation = m.getKeyValue("orientation");
		} catch (Exception e) {
		}
		
	
		
		Bitmap bmp1 = Bitmap.createBitmapFromBytes(raw, 0, -1, 1);
		Bitmap bmp  ;
		
//		if (orientation.equals("6")) {
//			bmp = ImageManipulator.rotate(bmp1, -90);
//			
//		}
//		else if (orientation.equals("3")) {
//			bmp = ImageManipulator.rotate(bmp1, -180);
//		}
//		else{
			bmp = bmp1 ; 
//		}
		
		
		// another check
		String bbType = DeviceProperties.getBBType();
		if(bbType.startsWith("95") || bbType.startsWith("98") || bbType.startsWith("938")){
//			if(ModelConfig.getConfig().isOrientationPortrait()){
				bmp = ImageManipulator.rotate(bmp, -90);
//			}
//			else{
//				bmp = ImageManipulator.rotate(bmp, 180);
//			}
		}
		
		
		return bmp;
	}
}