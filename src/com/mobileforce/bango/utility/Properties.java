package com.mobileforce.bango.utility;

import net.rim.device.api.system.Bitmap;

public class Properties {
	//app
	public static String _APP_VERSION = "1.0.0";
	public static String _APP_NAME = "Bango";
	public static String _ABOUT = _APP_NAME+" ver. "+_APP_VERSION+
			  "(c)"+GStringUtil.generateTimeStamp("yyyy")+" Bango";
	
	//persistent ID
	public static long _MENU_NOTIFICATION_ID = 0x4ebf789c2a3655f6L; //menu notification id
	public static long _RUNTIME_STORE_ID = 0x7c3d7e6bfafbe38dL; //runtime store
	public static long _IMAGE_MANAGER_ID = 0x331fa154e5b6938dL; //image store
	public static long _USER_INFO_ID = 0xfc90dd3d498cd38dL; //user info
	public static long _USER_PROFILE_ID = 0xfc555d3d498cd38dL; //user profile
	public static long _FB_CONNECT_ID = 0x7346a0ff73b5038dL; //fb connect
	public static long _TW_CONNECT_ID = 0x77555061a27fa38dL; //tw connect
	public static long _NOTIFICATION_ID = 0x2e6f990779333538L; //notification
	public static long _SETTING_ID = 0xc1770b51a8733d38L; //setting
	public static long _NOTIF_STORE = 0xc1770b5155555d38L; //notification store read/unread
	public static long _NOTIF_STORE_ALL = 0xc1770b5155333d38L; //notification store all id
	public static long _NOTIF_STORE_UPDATE = 0xc1770b5155888d38L; //notification store all id
	public static long _LOCATION_ID = 0xc1770b51a8123438L; //location manager
	public static long _SPLASH_PROMO_ID = 0xc1770b51a8155555L; //splash promo manager
	public static long _SETTING_PROMO_ID = 0xc1770b51a8155333L; //splash promo setting
	
	//sys
	public static String _APN_BB = ";deviceside=false;ConnectionType=mds-public"; //default value
	public static String _BB_PIN = DeviceProperties.getBBPIN();
	public static String _BB_TYPE = DeviceProperties.getBBType();
	public static String _BB_OS = DeviceProperties.getBBOS();
	public static String _BB_OP = DeviceProperties.getBBOP();
	public static String _BB_PHONE = DeviceProperties.getBBMSISDN();
	public static String _BB_IMEI = DeviceProperties.getBBIMEI();
	
	//api
	public static String _URL_BASE_API = "http://bango.mobileforce.mobi/apiv02/api/bangoapi";
	public static String _API_LOGIN_APPLICATION = _URL_BASE_API + "/loginapps";
	public static String _API_LOGIN_FACEBOOK = _URL_BASE_API + "/loginfb";
	public static String _API_REGISTER_USER = _URL_BASE_API + "/register";
	public static String _API_REGISTER_FACEBOOK = _URL_BASE_API + "/registerfb";
	public static String _API_HOME_DISTANCE = _URL_BASE_API + "/home/distance";
	public static String _API_HOME_POPULAR = _URL_BASE_API + "/home/popular";
	public static String _API_HOME_NEW = _URL_BASE_API + "/home/new";
	public static String _API_MENU_SPECIAL = _URL_BASE_API + "/menuspec";
	public static String _API_LIST_RECIPE_NEW = _URL_BASE_API + "/listrecipe/new";
	public static String _API_LIST_RECIPE_POPULAR = _URL_BASE_API + "/listrecipe/popular";
	public static String _API_LIST_NOTIFICATION = _URL_BASE_API + "/listnotif";
	public static String _API_POST_NEW_STORE = _URL_BASE_API + "/addstore";
	public static String _API_POST_NEW_STORE_REVIEW = _URL_BASE_API + "/addstorecomment";
	public static String _API_POST_NEW_RECIPE_REVIEW = _URL_BASE_API + "/addrecipecomment";
	public static String _API_GET_PROFILE = _URL_BASE_API + "/getprofile";
	public static String _API_EDIT_USER_PROFILE = _URL_BASE_API + "/editprofile";
	public static String _API_DETAIL_STORE = _URL_BASE_API + "/detailstore";
	public static String _API_SEARCH_STORE = _URL_BASE_API + "/searchstore";
	public static String _API_SEARCH_RECIPE = _URL_BASE_API + "/searchrecipe";
	public static String _API_LIKE_REVIEW = _URL_BASE_API + "/likecomment";
	public static String _API_REPORT_REVIEW = _URL_BASE_API + "/reportcomment";
	public static String _API_DELETE_REVIEW = _URL_BASE_API + "/delcomment";
	public static String _API_LOGOUT = _URL_BASE_API + "/logout";
	public static String _API_STATE_NOTIF = _URL_BASE_API + "/statenotif";
	public static String _API_CHANGE_NOTIF = _URL_BASE_API + "/changenotif";
	public static String _API_SPLASH_PROMO = _URL_BASE_API + "/splashpromo";
	public static String _API_PAGE_PROMO = _URL_BASE_API + "/pagepromo";
	public static String _API_GOOGLE_GEOCODING = "http://maps.google.com/maps/api/geocode/json";
	
	//config
	public static int _NOTIFICATION_CHECK_INTERVAL = 1500000; //check notification on background
	public static int _SPLASH_INTERVAL = 3000;
	
	//connection
	public static int _HTTP_ERROR_CODE = 0;
	public static String _HTTP_METHOD_POST = "POST";
	public static String _HTTP_METHOD_GET = "GET";
	public static String _CONN_TYPE_LOGIN_1 = "LOGIN NORMAL";
	public static String _CONN_TYPE_LOGIN_2 = "LOGIN FACEBOOK";
	public static String _CONN_TYPE_REGISTER_1 = "REGISTER NORMAL";
	public static String _CONN_TYPE_REGISTER_2 = "REGISTER FACEBOOK";
	public static String _CONN_TYPE_HOME_DISTANCE = "HOME BY DISTANCE";
	public static String _CONN_TYPE_HOME_POPULAR = "HOME BY POPULAR";
	public static String _CONN_TYPE_HOME_NEW = "HOME BY NEW";
	public static String _CONN_TYPE_SEARCH_HOME_DISTANCE = "SEARCH STORE BY DISTANCE";
	public static String _CONN_TYPE_SEARCH_HOME_POPULAR = "SEARCH STORE BY POPULAR";
	public static String _CONN_TYPE_SEARCH_HOME_NEW = "SEARCH STORE BY NEW";
	public static String _CONN_TYPE_SEARCH_RECIPE_POPULAR = "SEARCH RECIPE BY POPULAR";
	public static String _CONN_TYPE_SEARCH_RECIPE_NEW = "SEARCH RECIPE BY NEW";
	public static String _CONN_TYPE_MENU_SPECIAL = "MENU SPECIAL";
	public static String _CONN_TYPE_JAJANAN_SPECIAL = "JAJANAN SPECIAL";
	public static String _CONN_TYPE_RECIPE_NEW = "LIST RECIPE NEW";
	public static String _CONN_TYPE_RECIPE_POPULAR = "LIST RECIPE POPULAR";
	public static String _CONN_TYPE_NOTIFICATION = "LIST NOTIFICATION";
	public static String _CONN_TYPE_ADD_STORE = "ADD NEW STORE";
	public static String _CONN_TYPE_ADD_REVIEW = "ADD NEW REVIEW";
	public static String _CONN_TYPE_GET_PROFILE = "GET PROFILE";
	public static String _CONN_TYPE_GET_PROFILE_OTHER = "GET PROFILE OTHERS";
	public static String _CONN_TYPE_USER_REVIEW_PAGING = "USER REVIEW PAGING";
	public static String _CONN_TYPE_USER_REVIEW_PAGING_OTHER = "USER REVIEW PAGING OTHERS";
	public static String _CONN_TYPE_DETAIL_STORE_REVIEW_PAGING = "STORE REVIEW PAGING";
	public static String _CONN_TYPE_DETAIL_RECIPE_REVIEW_PAGING = "RECIPE REVIEW PAGING";
	public static String _CONN_TYPE_AVATAR_PROFILE = "AVATAR PROFILE";
	public static String _CONN_TYPE_AVATAR_PROFILE_OTHER = "AVATAR PROFILE OTHERS";
	public static String _CONN_TYPE_EDIT_AVATAR_PROFILE = "EDIT AVATAR PROFILE";
	public static String _CONN_TYPE_BADGES_PROFILE = "BADGES PROFILE";
	public static String _CONN_TYPE_BADGES_PROFILE_OTHER = "BADGES PROFILE OTHERS";
	public static String _CONN_TYPE_BIG_PREVIEW = "BIG PREVIEW";
	public static String _CONN_TYPE_RESEP_BIG_PREVIEW = "RESEP BIG PREVIEW";
	public static String _CONN_TYPE_REVIEWS_ICON = "REVIEWS ICON";
	public static String _CONN_TYPE_REVIEWS_ICON_OTHER = "REVIEWS ICON OTHERS";
	public static String _CONN_TYPE_SEARCH_KULINER_ICON = "SEARCH KULINER ICON";
	public static String _CONN_TYPE_SEARCH_RECIPE_ICON = "SEARCH RECIPE ICON";
	public static String _CONN_TYPE_EDIT_USER_PROFILE = "EDIT USER PROFILE";
	public static String _CONN_TYPE_DETAIL_STORE = "DETAIL STORE";
	public static String _CONN_TYPE_DETAIL_RESEP = "DETAIL RESEP";
	public static String _CONN_TYPE_SEARCH_CLICKED = "SEARCH CLICKED";
	public static String _CONN_TYPE_SHARE_REVIEW = "SHARE REVIEW";
	public static String _CONN_TYPE_SHARE_FACEBOOK = "SHARE TO FACEBOOK";
	public static String _CONN_TYPE_SHARE_TWITTER = "SHARE TO TWITTER";
	public static String _CONN_TYPE_USER_REVIEW_AVATAR = "USER REVIEW AVATAR";
	public static String _CONN_TYPE_USER_REVIEW_IMAGE = "USER REVIEW IMAGE";
	public static String _CONN_TYPE_RECIPE_REVIEW_AVATAR = "RECIPE REVIEW AVATAR";
	public static String _CONN_TYPE_RECIPE_REVIEW_IMAGE = "RECIPE REVIEW IMAGE";
	public static String _CONN_TYPE_POPUP_DETAIL_IMAGE = "POPUP DETAIL IMAGE";
	public static String _CONN_TYPE_REVIEW_SHARE_IMAGE = "REVIEW SHARE IMAGE";
	public static String _CONN_TYPE_IMAGE_ZOOM = "IMAGE ZOOM";
	public static String _CONN_TYPE_PAGING = "_PAGING";
	public static String _CONN_TYPE_LIKE_REVIEW = "LIKE REVIEW";
	public static String _CONN_TYPE_REPORT_REVIEW = "REPORT REVIEW";
	public static String _CONN_TYPE_DELETE_REVIEW = "DELETE REVIEW";
	public static String _CONN_TYPE_REFRESH = "REFRESH";
	public static String _CONN_TYPE_LOGOUT = "LOGOUT";
	public static String _CONN_TYPE_STATE_NOTIF = "STATE NOTIF";
	public static String _CONN_TYPE_CHANGE_NOTIF = "CHANGE NOTIF";
	public static String _CONN_TYPE_SPLASH_PROMO = "SPLASH PROMO";
	public static String _CONN_TYPE_PAGE_PROMO = "PAGE PROMO";
	public static String _CONN_TYPE_GOOGLE_GEOCODING = "GOOGLE GEOCODING";
	public static String _CONN_TYPE_SPLASH_PROMO_SINGLE_FETCH = "SPLASH PROMO SINGLE FETCH";
	public static String _CONN_TYPE_SPLASH_PROMO_MULTIPLE_FETCH = "SPLASH PROMO MULTIPLE FETCH";
	
	// geo
	public static String _LATITUDE = "0";
	public static String _LONGITUDE = "0";
	
	//etc
	public static String _ICON_NORMAL = "icon.png";
	public static String _ICON_FOCUS = "icon2.png";
	public static String _ICON_UPDATE = "icon3.png";
	public static String _INDICATOR = "indicator.png";
	public static String _RED_STAR = "red_star.png";
	
	public static int _COLOR_BG_CONTENT = 0xE9E5DC;
	public static int _COLOR_BG_SIDEBAR_MENU = 0x117970;
	public static int _COLOR_BG_LINE_SEPARATOR = 0x138F84;
	public static int _COLOR_BG_EXRA_MENU = 0x22BAAD;
	public static int _COLOR_BG_DARK_GREEN = 0x1B736B;
	public static int _COLOR_BG_GREY_GREEN = 0x8AB2AC;
	public static int _COLOR_BG_PALE_GREEN = 0x86AEA8;
	public static int _COLOR_BG_WHITE = 0xFFFFFF;
	public static int _COLOR_FONT_LABEL_WHITE = 0xFFFFFF;
	public static int _COLOR_FONT_LABEL_BLACK = 0x000000;
	public static int _COLOR_FONT_LABEL_GREEN = 0x16736B;
	public static int _COLOR_SEMI_BLACK = 0x787878;
	public static int _COLOR_LINE_FOCUS = 0x0E635C;
	public static int _COLOR_LINE_UNFOCUS = 0x117970;
	
	public static String[] _FONT_NAME = {
		"BBCapital",
		"BBCasual",
		"BBClarity",
		"BBCondensed",
		"BBMillbank",
		"BBMillbankTall",
		"BBSansSerif",
		"BBSansSerifSquare",
		"BBSerif",
		"BBSerifFixed"
	};
	
	//label, etc
	public static String _LBL_SELAMAT_DATANG = "Selamat Datang";
	public static String _LBL_ATAU = "atau";
	public static String _LBL_BELUM_PUNYA_AKUN = "Belum punya akun?";
	public static String _LBL_DAFTAR_DISINI = "Daftar di sini";
	public static String _LBL_SIGNIN = "Sign In";
	public static String _LBL_DAFTAR = "Daftar";
	public static String _LBL_NAMA = "Nama";
	public static String _LBL_FNAMA = "Nama Depan";
	public static String _LBL_LNAMA = "Nama Belakang";
	public static String _LBL_PASSWORD = "Password";
	public static String _LBL_PASSWORD_ULANG = "Ulangi Password";
	public static String _LBL_EMAIL = "Email";
	public static String _LBL_GENDER = "Gender";
	public static String _LBL_GENDER_L = "Pria";
	public static String _LBL_GENDER_P = "Wanita";
	public static String _LBL_TELEPON = "Telepon";
	public static String _LBL_ALAMAT = "Alamat";
	public static String _LBL_KOTA = "Kota";
	public static String _LBL_POS = "Kode POS";
	public static String _LBL_IDENTITAS = "Identitas";
	public static String _LBL_NOMOR_IDENTITAS = "Nomor Identitas";
	public static String _LBL_TGL_LAHIR = "Tanggal Lahir";
	public static String _LBL_LOKASI = "Lokasi";
	public static String _LBL_HOME = "Home";
	public static String _LBL_SHARE = "Berbagi Kuliner";
	public static String _LBL_JAJANAN = "Jajanan Spesial";
	public static String _LBL_RESEP = "Resep Bango";
	public static String _LBL_PROFIL = "Profil";
	public static String _LBL_PROMO = "Promo";
	public static String _LBL_NOTIFIKASI = "Notifikasi";
	public static String _LBL_REVIEW = "Review";
	public static String _LBL_RUTE = "Tunjukkan Jalan";
	public static String _LBL_DAFTAR_KULINER = "Daftar Kuliner";
	public static String _LBL_LOKASI_KULINER = "Lokasi Kuliner";
	public static String _LBL_BERBAGI_KULINER = "Berbagi Kuliner";
	public static String _LBL_TULIS_REVIEW = "Tulis Review";
	public static String _LBL_DETAIL = "Detail";
	public static String _LBL_EDIT_PROFIL = "Edit Profil";
	public static String _LBL_EDIT_FOTO = "Edit Photo";
	public static String _LBL_MENGAMBIL_LOKASI = "Retrieving GPS Location...";
	public static String _LBL_TAMBAH_LOKASI = "Tambah Lokasi";
	public static String _LBL_CARI = "Cari";
	public static String _LBL_SELESAI = "Selesai";
	public static String _LBL_TERDEKAT = "Terdekat";
	public static String _LBL_TERBARU = "Terbaru";
	public static String _LBL_TERPOPULER = "Terpopuler";
	public static String _LBL_HASIL_CARI = "Hasil pencarian";
	public static String _LBL_PLEASE_WAIT = "Please wait...";
	public static String _LBL_CHOOSE_PICTURE = "Choose picture...";
	public static String _LBL_LOAD_NOTIFICATION = "Loading notification...";
	public static String _LBL_BADGES = "BADGES";
	public static String _LBL_REVIEWS = "REVIEWS";
	public static String _LBL_BANGO_NONE = "Telur Bango";
	public static String _LBL_BANGO_BRONZE = "BANGO BRONZE";
	public static String _LBL_BANGO_SILVER = "BANGO SILVER";
	public static String _LBL_BANGO_GOLD = "BANGO GOLD";
	public static String _LBL_NO_BADGES = "Belum ada badge";
	public static String _LBL_NO_REVIEWS = "Tidak ada reviews";
	public static String _LBL_LOAD_MORE = "Load More";
	public static String _LBL_HASIL_PENCARIAN = "Hasil pencarian";
	public static String _LBL_TIDAK_MENEMUKAN = "Tidak menemukan yang kamu cari?";
	public static String _LBL_SHARE_REVIEW = "Share Review";
	public static String _LBL_SHARE_FACEBOOK = "Facebook";
	public static String _LBL_SHARE_TWITTER = "Twitter";
	public static String _LBL_LIKE = "Like";
	public static String _LBL_SHARING = "Share";
	public static String _LBL_LAPOR = "Lapor";
	public static String _LBL_LOGOUT = "Logout";
	public static String _LBL_SHARE_TEXT_TWITTER = "Temukan kuliner ini dan kuliner lainnya di app #WarisanKuliner http://goo.gl/WYsBOi";
	public static String _LBL_SHARE_TEXT_FACEBOOK = "Temukan kuliner ini dan kuliner lainnya di app #WarisanKuliner http://goo.gl/WYsBOi";
	public static String _LBL_JANGAN_TAMPILKAN = "Jangan tampilkan sampai 24 jam kedepan";
	
	public static String[] _ARRAY_IDENTITAS = { "KTP", "SIM", "PASSPORT" };
	public static String[] _ARRAY_NOTIFIKASI = { "Enabled", "Disabled" };
	
	public static String _LBL_HINT_NAMA = "Nama Lengkap...";
	public static String _LBL_HINT_FNAMA = "Nama Depan...";
	public static String _LBL_HINT_LNAMA = "Nama Belakang...";
	public static String _LBL_HINT_EMAIL = "email";
	public static String _LBL_HINT_EMAIL2 = "Alamat Email...";
	public static String _LBL_HINT_PASSWORD = "password";
	public static String _LBL_HINT_PASSWORD2 = "Pilih Password...";
	public static String _LBL_HINT_PASSWORD_ULANG = "Ulangi Password...";
	public static String _LBL_HINT_TGL_LAHIR = "Tanggal Lahir...";
	public static String _LBL_HINT_LOKASI = "Lokasi Anda...";
	public static String _LBL_HINT_NAMA_KULINER = "Nama tempat kuliner...";
	public static String _LBL_HINT_DETAIL_KULINER = "Daftar kuliner...";
	public static String _LBL_HINT_IDENTITAS = "Identitas...";
	public static String _LBL_HINT_CARI_KULINER = "Cari Kuliner";
	public static String _LBL_HINT_CARI_RESEP = "Cari Kuliner";
	public static String _LBL_HINT_CARI_LOKASI = "Cari Lokasi";
	public static String _LBL_HINT_TULIS_REVIEW = "Tulis Review";
	
	//dialog, alert, inform, etc
	public static String _INF_KETIK_KEYWORD = "Kata Pencarian kosong!";
	public static String _INF_KETIK_EMAIL = "Email belum diisi!";
	public static String _INF_KETIK_PASSWORD = "Password belum diisi!";
	public static String _INF_KETIK_NAMA = "Nama lengkap belum diisi!";
	public static String _INF_KETIK_FNAMA = "Nama depan belum diisi!";
	public static String _INF_KETIK_LNAMA = "Nama belakang belum diisi!";
	public static String _INF_KETIK_LOKASI = "Lokasi belum diisi!";
	public static String _INF_PASSWORD_BEDA = "Password tidak sama!";
	public static String _INF_PILIH_GENDER = "Gender belum dipilih!";
	public static String _INF_PILIH_TANGGAL_LAHIR = "Tanggal lahir belum dipilih!";
	public static String _INF_PILIH_IDENTITAS = "Identitas belum dipilih!";
	public static String _INF_KETIK_NAMA_KULINER = "Nama tempat kuliner belum diisi!";
	public static String _INF_KETIK_TELEPON = "Telepon belum diisi!";
	public static String _INF_KETIK_ALAMAT = "Alamat belum diisi!";
	public static String _INF_KETIK_KOTA = "Kota belum diisi!";
	public static String _INF_KETIK_KODE_POS = "Kode POS belum diisi!";
	public static String _INF_KETIK_NOMOR_IDENTITAS = "Nomor identitas belum diisi!";
	public static String _INF_KETIK_DETAIL_KULINER = "Detail daftar kuliner belum diisi!";
	public static String _INF_ISI_RATING = "Rating belum dipilih!";
	public static String _INF_PILIH_FOTO = "Foto belum dipilih!";
	public static String _INF_PILIH_FOTO_PROFIL = "Foto profil belum dipilih!";
	public static String _INF_KETIK_REVIEW = "Review belum diisi!";
	public static String _INF_LATLON_GAGAL = "Lokasi anda belum terdeteksi. Silahkan pilih Tambah Lokasi.";
	public static String _INF_ALAMAT_KOSONG = "Alamat belum diisi!";
	public static String _INF_KOTA_KOSONG = "Kota belum diisi!";
	public static String _INF_LAPORKAN_REVIEW = "Yakin akan melaporkan review dari ";
	public static String _INF_HAPUS_REVIEW = "Yakin ingin menghapus review ini?";
	public static String _INF_NO_CONNECTION = "Connection not available, please try again later!";
	public static String _INF_CONNECTION_TIMEOUT = "Connection timeout. Please try again later!";
	public static String _INF_NO_DATA = "Data tidak tersedia!";
	public static String _INF_LOKASI_TIDAK_DITEMUKAN = "Lokasi tidak ditemukan, silahkan cari alamat di kotak pencarian!";
	
	// slider
	public static Bitmap _SCREEN_CAPTURE = null;
	
	// photo
	public static int _TEMPLATE_PHOTO_SIZE_W = 640;
	public static int _TEMPLATE_PHOTO_SIZE_H = 480;
}
