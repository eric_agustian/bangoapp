package com.mobileforce.bango.utility.geolocator;

import javax.microedition.location.LocationException;
import javax.microedition.location.LocationProvider;

import com.mobileforce.bango.utility.geolocator.slp.SimpleLocationProvider;

import net.rim.device.api.gps.BlackBerryLocation;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;


public class GPSEngine {
	private int round = 5;
//	private LocationProvider _locationProvider; 
//	private static int _interval = 5;
	
	private double _latitude = -1;
	private double _longitude = -1;
//	private float _heading = -1;
//	private float _altitude = -1;
//	private float _speed = -1;
	
	private SimpleLocationProvider simpleProvider;
	private BlackBerryLocation location;
	
	public GPSEngine(){}
	
	public boolean startGPS(){
		boolean retval = false;

		try {
			LocationProvider locationProvider = LocationProvider
					.getInstance(null);

			if (locationProvider == null) {
				Runnable runnable = new Runnable() {
					public void run() {
						try {
							Dialog.inform("GPS is not supported on this platform");
						} catch (Exception e) {
						}
					}
				};
				UiApplication.getUiApplication().invokeLater(runnable);
			} else {
				retval = true;
				
				new Thread() {
					public void run() {
						if(simpleProvider==null){
							try{
								simpleProvider = new SimpleLocationProvider(SimpleLocationProvider.MODE_GPS);
							} catch(final Exception le){
								UiApplication.getUiApplication().invokeLater(new Runnable(){
									public void run(){
										Dialog.alert(le.getMessage());
									}
								});			
							} 
							if(simpleProvider!=null){
								simpleProvider.setGPSTimeout(30);
							}
						}
						
						location = simpleProvider.getLocation(30);

						if (location != null && location.isValid()) {
							_longitude = location.getQualifiedCoordinates()
									.getLongitude();
							_latitude = location.getQualifiedCoordinates()
									.getLatitude();
						}
					}
				}.start();
			}
		} catch (LocationException le) {
			System.err.println("Failed to instantiate the LocationProvider object");
			System.err.println(le);
		}

		return retval;
	}
	
//	public boolean startGPS(){
//        boolean retval = false;
//        
//        try{
//            _locationProvider = LocationProvider.getInstance(null);
//            
//            if(_locationProvider == null){
//                Runnable runnable = new Runnable() {
//    	    		public void run() {
//    	    			try {
//    	    				Dialog.inform("GPS is not supported on this platform");
//    	    			} catch (Exception e) {}	
//    	    		}
//    			};
//    			UiApplication.getUiApplication().invokeLater(runnable);
//            }
//            else{
//                _locationProvider.setLocationListener(new LocationListenerImpl(), _interval, -1, -1);
//                retval = true;
//            }
//        }
//        catch (LocationException le){
//            System.err.println("Failed to instantiate the LocationProvider object");
//            System.err.println(le); 
//        }        
//        return retval;
//    }
//	
//	
//	private class LocationListenerImpl implements LocationListener{
//        public void locationUpdated(LocationProvider provider, Location location){
//            if(location.isValid()){
//                _heading = location.getCourse();
//                _longitude = location.getQualifiedCoordinates().getLongitude();
//                _latitude = location.getQualifiedCoordinates().getLatitude();
//                _altitude = location.getQualifiedCoordinates().getAltitude();
//                _speed = location.getSpeed();                
//                
//                //System.out.println("GPS [heading]>>> "+_heading);
//                //System.out.println("GPS [longitude]>>> "+_longitude);
//                //System.out.println("GPS [latitude]>>> "+_latitude);
//                //System.out.println("GPS [altitude]>>> "+_altitude);
//                //System.out.println("GPS [speed]>>> "+_speed);
//            }
//        }
//  
//        public void providerStateChanged(LocationProvider provider, int newState){
//            // Not implemented.
//        }        
//    }
	
	private static double round(double d, int decimal){
        double powerOfTen = 1;
        
        while (decimal-- > 0){
            powerOfTen *= 10.0;
        }
        
        double d1 = d * powerOfTen;
        int d1asint = (int)d1; // Clip the decimal portion away and cache the cast, this is a costly transformation.
        double d2 = d1 - d1asint; // Get the remainder of the double.
        
        // Is the remainder > 0.5? if so, round up, otherwise round down (lump in .5 with > case for simplicity).
        return ( d2 >= 0.5 ? (d1asint + 1)/powerOfTen : (d1asint)/powerOfTen);
    }
	
	//*********************************************************************************************************
	//GETTERS
//    public void stopGPS(){
//    	if(_locationProvider != null){
//            _locationProvider.reset();
//            _locationProvider.setLocationListener(null, -1, -1, -1);
//        }
//    }
    
    public String getLatitude(){
    	if(_latitude!=-1){
    		return String.valueOf(round(_latitude, round));
    	}else{ return null; }
    }
    
    public String getLongitude(){
    	if(_longitude!=-1){
    		return String.valueOf(round(_longitude, round));
    	}else{ return null; }
    }
    
//    public String getAltitude(){
//    	if(_altitude!=-1){
//    		return String.valueOf(round(_altitude, round));
//    	}else{ return null; }
//    }
//    
//    public String getSpeed(){
//    	if(_speed!=-1){
//    		return String.valueOf(round(_speed, round));
//    	}else{ return null; }
//    }
//    
//    public String getHeading(){
//    	if(_heading!=-1){
//    		return String.valueOf(round(_heading, round));
//    	}else{ return null; }
//    }
}