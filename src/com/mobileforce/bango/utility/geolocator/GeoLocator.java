package com.mobileforce.bango.utility.geolocator;

public class GeoLocator {
	private GPSEngine GPS; //first priority
	private LocationProviderEngine LPE; //second priority
	private OpenCellIdEngine OCE; //third priority
	private OpenSignalEngine OSE; //third priority
	
	private boolean isgpssupported = false;
	private String _latitude, _longitude;
	
	public GeoLocator(){ }
	
	public void startGeo(){
		System.out.println("<start geolocation engine>");
		_latitude = null;
		_longitude = null;
		
		//initialize GPS
		GPS = new GPSEngine();
		isgpssupported = GPS.startGPS();

		// initialize LPE
		LPE = new LocationProviderEngine();
		LPE.getLocationFromCELLSITE();

		// initialize OCE
		OCE = new OpenCellIdEngine();
//		OCE.startConversion();
		
		// initialize OSE
		OSE = new OpenSignalEngine();
//		OSE.startConversion();
		
		retrievingLocation();
	}
	
//	public void resetLocation(){
//		if(isgpssupported){
//			if(GPS.getLatitude()==null || GPS.getLongitude()==null){
//				GPS.stopGPS();
//				GPS.startGPS();
//			}
//		}
//		
//		LPE = new LocationProviderEngine();
//		LPE.getLocationFromCELLSITE();
//		
//		OCE = new OpenCellIdEngine();
//		OCE.startConversion();
//		OSE = new OpenSignalEngine();
//		OSE.startConversion();
//	}
	
	public boolean retrievingLocation(){
		if(isgpssupported){
			if(GPS.getLatitude()!=null && GPS.getLongitude()!=null){
				_latitude = GPS.getLatitude();
				_longitude = GPS.getLongitude();
				
				LocationManager.setLocation(_latitude, _longitude, "GPS");
				
				return true;
			}
		}
		
		if(LPE.getLatitude()!=null && LPE.getLongitude()!=null){
			_latitude = LPE.getLatitude();
			_longitude = LPE.getLongitude();
			
			LocationManager.setLocation(_latitude, _longitude, "LPE");

			return true;
		}
		
		if(OCE.getLatitude()!=null && OCE.getLongitude()!=null){
			_latitude = OCE.getLatitude();
			_longitude = OCE.getLongitude();
			
			LocationManager.setLocation(_latitude, _longitude, "OCE");

			return true;
		}
		
		if(OSE.getLatitude()!=null && OSE.getLongitude()!=null){
			_latitude = OSE.getLatitude();
			_longitude = OSE.getLongitude();
			
			LocationManager.setLocation(_latitude, _longitude, "OSE");

			return true;
		}
		
		
		if(getLatitude()==null || getLongitude()==null){
			return false;
		}
		else{
			return true;
		}
	}
	
	//GETTERS
	public String getLatitude(){
		return _latitude;
	}
	
	public String getLongitude(){
		return _longitude;
	}
	
	//GETTERS type 2 (there is no null)
	public String getLatitudeDefaultZero(){
//		return _latitude==null ? "0" : _latitude;
		if(_latitude == null){
			return LocationManager.getLatitude() == null ? "-6.0886599" : LocationManager.getLatitude();
		}
		else{
			return _latitude;
		}
	}
	
	public String getLongitudeDefaultZero(){
//		return _longitude==null ? "0" : _longitude;
		if(_longitude == null){
			return LocationManager.getLongitude() == null ? "106.972825" : LocationManager.getLongitude();
		}
		else{
			return _longitude;
		}
	}
}
