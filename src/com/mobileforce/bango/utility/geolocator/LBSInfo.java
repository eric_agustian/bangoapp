package com.mobileforce.bango.utility.geolocator;

import net.rim.device.api.system.RadioInfo;

public class LBSInfo {
	public String getCellId() {
		int cellid = getRawCellId();
		String ccid = Integer.toString(cellid);
		return ccid;
	}
	
	public String getLac() {
		int lac = getRawLac();
		String clac = Integer.toString(lac);
		return clac;
	}
	
	public String getMcc() {
		int mcc = getRawMcc();
		String cmcc = Integer.toString(mcc).trim();
		return cmcc;
	}
	
	public String getMnc() {
		int mnc = getRawMnc();
		String cmnc = Integer.toString(mnc).trim();
		return cmnc;
	}
	
	public int getRawCellId(){
		if(RadioInfo.getNetworkType()==RadioInfo.NETWORK_CDMA){
			return net.rim.device.api.system.CDMAInfo.getCellInfo().getBID();
		}
		else{
			return net.rim.device.api.system.GPRSInfo.getCellInfo().getCellId();
		}
	}
	
	public int getRawLac(){
		if(RadioInfo.getNetworkType()==RadioInfo.NETWORK_CDMA){
			return 0;
		}
		else{
			return net.rim.device.api.system.GPRSInfo.getCellInfo().getLAC();
		}
	}
	
	public int getRawMcc(){
		if(RadioInfo.getNetworkType()==RadioInfo.NETWORK_CDMA){
			return net.rim.device.api.system.CDMAInfo.getCellInfo().getSID();
		}
		else{
			return net.rim.device.api.system.GPRSInfo.getCellInfo().getMCC();
		}
	}
	
	public int getRawMnc(){
		if(RadioInfo.getNetworkType()==RadioInfo.NETWORK_CDMA){
			return net.rim.device.api.system.CDMAInfo.getCellInfo().getNID();
		}
		else{
			return net.rim.device.api.system.GPRSInfo.getCellInfo().getMNC();
		}
	}
	
	/*public int getRSSI(){
		if(RadioInfo.getNetworkType()==RadioInfo.NETWORK_CDMA){
			return -1;
		}
		else{
			return net.rim.device.api.system.GPRSInfo.getCellInfo().getRSSI();
		}
	}*/
}
