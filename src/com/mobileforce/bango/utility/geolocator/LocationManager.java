package com.mobileforce.bango.utility.geolocator;

import java.util.Vector;

import com.mobileforce.bango.storage.persistable.LocationPersistable;
import com.mobileforce.bango.storage.query.GenericStore;
import com.mobileforce.bango.utility.Properties;

public class LocationManager {
	public LocationManager(){}
	
	public static String getLatitude(){
		LocationPersistable data = getLocation();
		
		if(data == null){
			return null;
		}
		else{
			return data.get_latitude();
		}
	}
	
	public static String getLongitude(){
		LocationPersistable data = getLocation();
		
		if(data == null){
			return null;
		}
		else{
			return data.get_longitude();
		}
	}
	
	public static String getType(){
		LocationPersistable data = getLocation();
		
		if(data == null){
			return null;
		}
		else{
			return data.get_type();
		}
	}
	
	
	public static void setLocation(String latitude, String longitude, String type){
		try{
//			//create & update
			Vector v = new Vector();
			LocationPersistable lp = new LocationPersistable();
			lp.set_latitude(latitude);
			lp.set_longitude(longitude);
			lp.set_type(type);
			v.addElement(lp);
			
			//save
			saveVector(Properties._LOCATION_ID, v);
		}catch(Exception x){ System.out.println("unable to update & save LocationManager >>> "+x.toString()); }
	}
	
	public static void deleteSetting(){
		deleteVector(Properties._LOCATION_ID);
	}
	
	//get vector
	private static Vector getVector(long id) {
		Vector data = new GenericStore(id).getVectorOfStoredData();
		return data.size() == 0 ? null : data;
	}

	// save vector
	private static void saveVector(long id, Vector v) {
		new GenericStore(id).saveDataVector(v);
	}

	// delete vector
	private static void deleteVector(long id) {
		try {
			new GenericStore(id).deleteStoredData(id);
			System.out.println("success deleting LocationManager id>>> " + id);
		} catch (Exception x) {
			System.out.println("Fail deleting LocationManager id>>> " + x.toString()
					+ " [" + id + "]");
		}
	}
	
	// get persistable
	public static LocationPersistable getLocation(){
		Vector data = getVector(Properties._LOCATION_ID);
		
		if(data == null){
			return null;
		}
		else{
			return (LocationPersistable) data.elementAt(0);
		}
	}
}
