//#preprocess

package com.mobileforce.bango.utility.geolocator;

import javax.microedition.location.Criteria;
import javax.microedition.location.Location;
import javax.microedition.location.LocationProvider;

public class LocationProviderEngine extends Thread{
	private int round = 5;
	
//	private static int _interval_multi = 5;
//	private static int _interval_single = 15;
	private double _latitude = -1;
	private double _longitude = -1;
	
	private Location bbLocation;
	private Criteria criteria;
	private LocationProvider locationProvider = null;	
//	private LocationListenerImpl listener = null;
	
	public LocationProviderEngine(){}
	
	public void getLocationFromCELLSITE(){
		start();
	}
	
	public void run(){
		// define criteria
		criteria = new Criteria();
		criteria.setPreferredResponseTime(16);
		criteria.setCostAllowed(true);
		criteria.setHorizontalAccuracy(Criteria.NO_REQUIREMENT);
		criteria.setVerticalAccuracy(Criteria.NO_REQUIREMENT);
		criteria.setPreferredPowerConsumption(Criteria.POWER_USAGE_LOW);
		
		try {
			locationProvider = LocationProvider.getInstance(criteria);
			if (locationProvider != null) {
				bbLocation = locationProvider.getLocation(100);
				if (bbLocation != null) {	
					if (bbLocation.isValid()) {
						_latitude = bbLocation.getQualifiedCoordinates().getLatitude();
						_longitude = bbLocation.getQualifiedCoordinates().getLongitude();
						
						locationProvider.setLocationListener(null, 0, 0, 0);
						locationProvider.reset();
						locationProvider = null;
					}
				}
			}
		} catch (Exception e) {}
	}
	
//	public void run(){
//		System.out.println("<LPE start>");
//		try{
//			//on OS 5 using this
//			//#ifdef BlackBerrySDK5.0.0
////			criteria = new BlackBerryCriteria(net.rim.device.api.gps.GPSInfo.GPS_MODE_CELLSITE);
//			
//			//on OS 6 or newer using this
//			//#else
////			criteria = new BlackBerryCriteria(net.rim.device.api.gps.LocationInfo.GEOLOCATION_MODE_CELL);
//			//#endif
//			
//			// define criteria
//			criteria = new Criteria();
//			criteria.setPreferredResponseTime(10);
//			criteria.setCostAllowed(true);
//			criteria.setHorizontalAccuracy(Criteria.NO_REQUIREMENT);
//			criteria.setVerticalAccuracy(Criteria.NO_REQUIREMENT);
//			criteria.setPreferredPowerConsumption(Criteria.POWER_USAGE_LOW);
//			
//			//start the engine
////			locationProvider = (BlackBerryLocationProvider) BlackBerryLocationProvider.getInstance(criteria);
//			locationProvider = LocationProvider.getInstance(criteria);
//			
//			//single fix request mode
////			bbLocation = (BlackBerryLocation) locationProvider.getLocation(_interval_single);
//			bbLocation = locationProvider.getLocation(100);
//			
//			//define listener
////			listener = new LocationListenerImpl();
//			
//			//multiple fix request mode -> for backup
////			locationProvider.setLocationListener(listener, _interval_multi, -1, -1);
//		}catch(Exception x){
//            System.err.println("Failed to retrieve location using LPE CELLID");
//            System.err.println(x); 
//        }
//	}
//	
//	private class LocationListenerImpl implements LocationListener{
//        public void locationUpdated(LocationProvider provider, Location location){
//        	System.out.println("<LPE Location Updated - MULTI MODE>");
//        	if(location.isValid()){
//                _longitude = location.getQualifiedCoordinates().getLongitude();
//                _latitude = location.getQualifiedCoordinates().getLatitude();
//            }
//        }
//  
//        public void providerStateChanged(LocationProvider provider, int newState){
//            // Not implemented.
//        }        
//    }
	
	private static double round(double d, int decimal){
        double powerOfTen = 1;
        
        while (decimal-- > 0){
            powerOfTen *= 10.0;
        }
        
        double d1 = d * powerOfTen;
        int d1asint = (int)d1; // Clip the decimal portion away and cache the cast, this is a costly transformation.
        double d2 = d1 - d1asint; // Get the remainder of the double.
        
        // Is the remainder > 0.5? if so, round up, otherwise round down (lump in .5 with > case for simplicity).
        return ( d2 >= 0.5 ? (d1asint + 1)/powerOfTen : (d1asint)/powerOfTen);
    }
	
	//*********************************************************************************************************
	//GETTERS
	public String getLatitude() {
		if (_latitude != -1) {
			return String.valueOf(round(_latitude, round));
		} else {
			return null;
		}
	}

	public String getLongitude() {
		if (_longitude != -1) {
			return String.valueOf(round(_longitude, round));
		} else {
			return null;
		}
	}
}
