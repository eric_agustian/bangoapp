package com.mobileforce.bango.utility.geolocator;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

import com.mobileforce.bango.utility.ConnectionChecker;
import com.mobileforce.bango.utility.GStringUtil;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.DeviceInfo;


public class OpenCellIdEngine extends Thread {
	private OpenCellIdEngine _self;
	private String _url = "http://www.opencellid.org/cell/get";
	private String _parameter = "key=7259c162f255e25e288c6e991fe11592";
	
	private String _latitude, _longitude;
	
	private boolean running = false;
	
	public OpenCellIdEngine(){}
	
	public void startConversion(){
		_self = this;
		running = true;
		_latitude = null;
		_longitude = null;
		LBSInfo lbs = new LBSInfo();
		
		if(DeviceInfo.isSimulator()){
			//test on simulator
			_parameter += "&mcc=510&mnc=10&lac=1044&cellid=10147798";
		}
		else{
			_parameter += "&mcc="+lbs.getMcc()+"&mnc="+lbs.getMnc()+"&lac="+lbs.getLac()+"&cellid="+lbs.getCellId();
		}
		
		start();
	}
	
	public void run() {
		try{
			ConnectionChecker _check = new ConnectionChecker();
			_check.Checking();
			if(_check.isAvailable()){
				InputStream is = null;
				HttpConnection httpConnection = null;

				String URI = GStringUtil.convertURL(_url + "?" + _parameter);
				if (!DeviceInfo.isSimulator()) URI = URI + Properties._APN_BB;
				httpConnection = (HttpConnection) Connector.open(URI, Connector.READ_WRITE);
				httpConnection.setRequestMethod(HttpConnection.GET);

//				System.out.println("URL : " + URI);

				if (httpConnection.getResponseCode() == HttpConnection.HTTP_OK) {
					is = httpConnection.openInputStream();

					// Get the length and process the data
					int len = (int) httpConnection.getLength();
					if (len > 0) {
						int actual = 0;
						int bytesread = 0;
						byte[] data = new byte[len];
						while ((bytesread != len) && (actual != -1)) {
							actual = is.read(data, bytesread, len - bytesread);
							bytesread += actual;
						}
						// **********************************************************
						String reply = new String(data, 0, bytesread, "UTF-8");
//						System.out.println("RETURN MODE 1::: " + reply);
						callback(reply);
						// **********************************************************
					} else {
						// System.out.println("ELSE WHILE");
						int ch;
						ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
						while ((ch = is.read()) != -1) {
							bytestream.write(ch);
						}

						// **********************************************************
						String reply = new String(bytestream.toByteArray(), "UTF-8");
//						System.out.println("RETURN MODE 2::: " + reply);
						callback(reply);
						// **********************************************************
					}
				}

				// close all connections***
				httpConnection.close();
				is.close();
				// ************************
			}
		}catch(Exception x){
			running = false;
			System.out.println("Error converting cellsite>>> "+x.toString());
		}finally{
			stopMe();
		}
	}
	
	void stopMe(){
		System.out.println("[Trying to close OpenCellIdEngine]");
		try{
			if(_self.isAlive()){
				_self.interrupt();
				System.out.println("[OpenCellIdEngine Closed]");
			}
		}catch(Exception x){ 
			System.out.println("[Unable to close OpenCellIdEngine]"+x);
		}
	}

	//parsing data
	public void callback(String data) {
		try{
			if(data.indexOf("<rsp stat=\"fail\">") != -1){
				// fail
				int start = data.indexOf("<err info=") + "<err info=".length() + 1;
				int end = data.indexOf("code=") - 2;
				String err = data.substring(start, end);
				
				System.out.println("Error : "+err);
			}
			else if(data.indexOf("<rsp stat=\"ok\">") != -1){
				// success
				int start1 = data.indexOf("lat=") + "lat=".length() + 1;
				int end1 = data.indexOf(" lon=") - 1;
				
				int start2 = data.indexOf("lon=") + "lon=".length() + 1;
				int end2 = data.indexOf(" mcc=") - 1;
				
				_latitude = data.substring(start1, end1);
				_longitude = data.substring(start2, end2);
				
				System.out.println("Cell Latitude : "+_latitude);
				System.out.println("Cell Longitude : "+_longitude);
			}
			running = false;
		}catch(Exception x){ 
			running = false;
			System.out.println("OpenCellIdEngine parsing error>>>"+x); 
		}
	}
	
	//check status to prevent multiple request
	public boolean isStillRunning(){
		return running;
	}
	
	//*********************************************************************************************************
	//GETTERS
    public String getLatitude(){
    	if (_latitude == null) {
    		return null;
    	}
    	else{
    		return _latitude;
    	}
    }
    
    public String getLongitude(){
    	if(_longitude == null){
    		return null;
    	}
    	else{
    		return _longitude;
    	}
    }
}
