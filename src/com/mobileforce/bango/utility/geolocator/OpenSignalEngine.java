package com.mobileforce.bango.utility.geolocator;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

import com.mobileforce.bango.utility.ConnectionChecker;
import com.mobileforce.bango.utility.GStringUtil;
import com.mobileforce.bango.utility.Properties;

import ext.json.me.JSONObject;
import ext.json.me.JSONTokener;

import net.rim.device.api.system.DeviceInfo;

/*
 * maximum 2000 request per month
 */
public class OpenSignalEngine extends Thread {
	private OpenSignalEngine _self;
	private String _url = "http://api.opensignal.com/v2/towerinfo.json";
	private String _parameter = "apikey=d6cb87ecea12c6c036ade0703d3eea2d";
	
	private String _latitude, _longitude;
	
	private boolean running = false;
	
	public OpenSignalEngine(){}
	
	public void startConversion(){
		_self = this;
		running = true;
		_latitude = null;
		_longitude = null;
		LBSInfo lbs = new LBSInfo();
		
		if(DeviceInfo.isSimulator()){
			//test on simulator
			_parameter += "&cid=10408372&lac=1047&sid=&phone_type=GSM&network_id=10";
		}
		else{
			_parameter += "&mcc="+lbs.getMcc()+"&network_id="+lbs.getMnc()+"&lac="+lbs.getLac()+"&cid="+lbs.getCellId()+"&sid=&phone_type=GSM";
		}
		
		start();
	}
	
	public void run() {
		try{
			ConnectionChecker _check = new ConnectionChecker();
			_check.Checking();
			if(_check.isAvailable()){
				InputStream is = null;
				HttpConnection httpConnection = null;

				String URI = GStringUtil.convertURL(_url + "?" + _parameter);
				if (!DeviceInfo.isSimulator()) URI = URI + Properties._APN_BB;
				httpConnection = (HttpConnection) Connector.open(URI, Connector.READ_WRITE);
				httpConnection.setRequestMethod(HttpConnection.GET);

//				System.out.println("URL : " + URI);

				if (httpConnection.getResponseCode() == HttpConnection.HTTP_OK) {
					is = httpConnection.openInputStream();

					// Get the length and process the data
					int len = (int) httpConnection.getLength();
					if (len > 0) {
						int actual = 0;
						int bytesread = 0;
						byte[] data = new byte[len];
						while ((bytesread != len) && (actual != -1)) {
							actual = is.read(data, bytesread, len - bytesread);
							bytesread += actual;
						}
						// **********************************************************
						String reply = new String(data, 0, bytesread, "UTF-8");
//						System.out.println("RETURN MODE 1::: " + reply);
						callback(reply);
						// **********************************************************
					} else {
						// System.out.println("ELSE WHILE");
						int ch;
						ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
						while ((ch = is.read()) != -1) {
							bytestream.write(ch);
						}

						// **********************************************************
						String reply = new String(bytestream.toByteArray(), "UTF-8");
//						System.out.println("RETURN MODE 2::: " + reply);
						callback(reply);
						// **********************************************************
					}
				}

				// close all connections***
				httpConnection.close();
				is.close();
				// ************************
			}
		}catch(Exception x){
			running = false;
			System.out.println("Error converting cellsite>>> "+x.toString());
		}finally{
			stopMe();
		}
	}
	
	void stopMe(){
		System.out.println("[Trying to close OpenSignalEngine]");
		try{
			if(_self.isAlive()){
				_self.interrupt();
				System.out.println("[OpenSignalEngine Closed]");
			}
		}catch(Exception x){ 
			System.out.println("[Unable to close OpenSignalEngine]"+x);
		}
	}

	//parsing data
	public void callback(String data) {
		try{
			JSONTokener jtokener = new JSONTokener(data);
			JSONObject jobject = new JSONObject(jtokener);
			
			if(jobject.optString("towers").equals("")){
				// ok
				JSONObject tower1 = jobject.optJSONObject("tower1");
				if(tower1 != null){
					_latitude = tower1.optString("est_lat");
			        _longitude = tower1.optString("est_lng");
				}
			}
			else{
				// no result
			}
			
			running = false;
		}catch(Exception x){ 
			running = false;
			System.out.println("OpenSignalEngine parsing error>>>"+x); 
		}
	}
	
	//check status to prevent multiple request
	public boolean isStillRunning(){
		return running;
	}
	
	//*********************************************************************************************************
	//GETTERS
    public String getLatitude(){
    	if (_latitude == null) {
    		return null;
    	}
    	else{
    		return _latitude;
    	}
    }
    
    public String getLongitude(){
    	if(_longitude == null){
    		return null;
    	}
    	else{
    		return _longitude;
    	}
    }
}
