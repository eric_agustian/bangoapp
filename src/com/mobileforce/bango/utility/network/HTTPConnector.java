package com.mobileforce.bango.utility.network;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

import com.mobileforce.bango.services.UserInfoManager;
import com.mobileforce.bango.utility.GStringUtil;
import com.mobileforce.bango.utility.JSONCallBack;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.DeviceInfo;

public class HTTPConnector {
	String _URL;
	String _parameters;
	String _request_type;
	
	private JSONCallBack _callback = null;
	
	public HTTPConnector(String url, String _parameter, String request_type, JSONCallBack callback) throws Exception {
		this._URL = url;    	    	
    	this._parameters = _parameter;
    	this._request_type = request_type;
    	this._callback = callback;
    	this.connectToServer();
    }  
	
	public void connectToServer() throws Exception {
		InputStream is = null;
	    HttpConnection httpConnection = null;
	    
	    //method selection
	    if(_request_type.equalsIgnoreCase("POST")){
	    	//none
	    }
	    else if(_request_type.equalsIgnoreCase("GET")){
	    	if(!_parameters.equalsIgnoreCase("")) _URL += "?"+_parameters;
	    }
	    
	    //convert URL
	    _URL = GStringUtil.convertURL(_URL);
//	    System.out.println("URL : "+_URL+ " PARAMS : "+_parameters+" METHOD : "+_request_type);
	    
	    //define HTTP
	    if(!DeviceInfo.isSimulator()) _URL = _URL + Properties._APN_BB;
	    httpConnection = (HttpConnection) Connector.open(_URL, Connector.READ_WRITE);
	    
	    //define header
	    httpConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		httpConnection.setRequestProperty("User-Agent", UserAgent.getUserAgent());
		httpConnection.setRequestProperty("MFDEVID", Properties._BB_PIN);
		httpConnection.setRequestProperty("MFLATLON", Properties._LATITUDE+","+Properties._LONGITUDE);
		httpConnection.setRequestProperty("MFSESSID", UserInfoManager.getUserSessionID());
		
		
	    if(_request_type.equalsIgnoreCase("POST")){
	    	httpConnection.setRequestMethod(HttpConnection.POST);
	    	
	    	DataOutputStream dos = httpConnection.openDataOutputStream();
	        dos.write(_parameters.getBytes());
	        dos.flush();
			dos.close();
	    }
	    else if(_request_type.equalsIgnoreCase("GET")){
	    	httpConnection.setRequestMethod(HttpConnection.GET);
	    }
	    
		if (httpConnection.getResponseCode() == HttpConnection.HTTP_OK) {
			is = httpConnection.openInputStream();
            
            //Get the length and process the data
            int len = (int)httpConnection.getLength();
            if (len > 0) {
                int actual = 0;
                int bytesread = 0 ;
                byte[] data = new byte[len];
                while ((bytesread != len) && (actual != -1)) {
                   actual = is.read(data, bytesread, len - bytesread);
                   bytesread += actual;
                }
                //**********************************************************
                String reply = new String(data, 0, bytesread, "UTF-8");
//                System.out.println("RETURN MODE 1::: "+reply);
                if(!reply.startsWith("{")){
                	//System.out.println("AUTO COMPLETE JSON RETURN");
                	reply = "{\"array\": "+reply+"}";
                }
    		    if (_callback!=null) _callback.callback(reply);
                //**********************************************************
            } else {
            	//System.out.println("ELSE WHILE");
                int ch;
                ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
                while ((ch = is.read()) != -1) {
                	bytestream.write(ch);
                }
                
                //**********************************************************
                String reply = new String(bytestream.toByteArray(), "UTF-8");
//                System.out.println("RETURN MODE 2::: "+reply);
                if(!reply.startsWith("{")){
                	//System.out.println("AUTO COMPLETE JSON RETURN");
                	reply = "{\"array\": "+reply+"}";
                }
    		    if (_callback!=null) _callback.callback(reply);
                //**********************************************************
            }
            Properties._HTTP_ERROR_CODE = 0;
		} else if (httpConnection.getResponseCode() == HttpConnection.HTTP_NOT_MODIFIED) {
			System.out.println("ERROR CODE1::: "+httpConnection.getResponseCode());
			//304
			Properties._HTTP_ERROR_CODE = 304;
			throw new IOException("HTTP Not Modified");
		} else if (httpConnection.getResponseCode() == HttpConnection.HTTP_BAD_REQUEST) {
			System.out.println("ERROR CODE2::: "+httpConnection.getResponseCode());
			//400
			Properties._HTTP_ERROR_CODE = 400;
	     	throw new IOException("Bad Request");
		} else if (httpConnection.getResponseCode() == HttpConnection.HTTP_UNAUTHORIZED) {
			System.out.println("ERROR CODE3::: "+httpConnection.getResponseCode());
			//401
			Properties._HTTP_ERROR_CODE = 401;
			throw new IOException("FORBIDDEN");
		} else if (httpConnection.getResponseCode() == HttpConnection.HTTP_FORBIDDEN) {
			System.out.println("ERROR CODE4::: "+httpConnection.getResponseCode());
			//403
			Properties._HTTP_ERROR_CODE = 403;
			throw new IOException("FORBIDDEN");
		} else if (httpConnection.getResponseCode() == HttpConnection.HTTP_NOT_FOUND) {
			System.out.println("ERROR CODE5::: "+httpConnection.getResponseCode());
			//404
			Properties._HTTP_ERROR_CODE = 404;
	     	throw new IOException("Not Found");
		} else if (httpConnection.getResponseCode() == HttpConnection.HTTP_BAD_METHOD) {
			System.out.println("ERROR CODE5::: "+httpConnection.getResponseCode());
			//405
			Properties._HTTP_ERROR_CODE = 405;
	     	throw new IOException("Method Not Allowed");
		} else if (httpConnection.getResponseCode() == HttpConnection.HTTP_CLIENT_TIMEOUT) {
			System.out.println("ERROR CODE6::: "+httpConnection.getResponseCode());
			//408
			Properties._HTTP_ERROR_CODE = 408;
	     	throw new IOException("Client Timeout");
		} else if (httpConnection.getResponseCode() == HttpConnection.HTTP_PRECON_FAILED) {
			System.out.println("ERROR CODE7::: "+httpConnection.getResponseCode());
			//412
			Properties._HTTP_ERROR_CODE = 412;
	     	throw new IOException("Precondition Failed");
		} else if (httpConnection.getResponseCode() == HttpConnection.HTTP_INTERNAL_ERROR) {
			System.out.println("ERROR CODE8::: "+httpConnection.getResponseCode());
			//500
			Properties._HTTP_ERROR_CODE = 500;
		    throw new IOException("Internal Server Error");
		} else if (httpConnection.getResponseCode() == HttpConnection.HTTP_BAD_GATEWAY) {
			System.out.println("ERROR CODE9::: "+httpConnection.getResponseCode());
			//502
			Properties._HTTP_ERROR_CODE = 502;
		    throw new IOException("Bad Gateway");
		} 
		
		//close all connections***
		httpConnection.close();	   
		is.close();
		//************************
     }
}
