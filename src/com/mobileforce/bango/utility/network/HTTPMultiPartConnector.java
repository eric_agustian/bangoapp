package com.mobileforce.bango.utility.network;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

import com.mobileforce.bango.services.UserInfoManager;
import com.mobileforce.bango.utility.JSONCallBack;
import com.mobileforce.bango.utility.Properties;
import com.mobileforce.bango.utility.network.cleveruapost.PostPrameters;

import net.rim.device.api.system.DeviceInfo;

public class HTTPMultiPartConnector {
	String _URL;
	byte[] _multipart_parameters;
	String _request_type;
	private JSONCallBack _callback = null;
	
	private static final int DATA_CHUNK_SIZE = 1024;
	private static final String MULTIPART_CONTENT_TYPE = "multipart/form-data; boundary=" + PostPrameters.BOUNDARY_MARKER;
	
	public HTTPMultiPartConnector(String url, byte[] multipart_parameters, String request_type, JSONCallBack callback) throws Exception {
		this._URL = url;    	    	
    	this._multipart_parameters = multipart_parameters;
    	this._request_type = request_type;
    	this._callback = callback;
    	this.connectToServer();
	}

	public void connectToServer() throws Exception {
		HttpConnection httpConnection = null;
		OutputStream outputStream = null;

		try {
			 if(!DeviceInfo.isSimulator()) _URL = _URL + Properties._APN_BB;

			 httpConnection = (HttpConnection) Connector.open(_URL, Connector.READ_WRITE, true);
			 System.out.println("MULTIPART URL : "+_URL+" METHOD : "+_request_type);
			 
			 if(_request_type.equalsIgnoreCase("POST")){
				 httpConnection.setRequestMethod(HttpConnection.POST);
			 }
			 else{
				 httpConnection.setRequestMethod(HttpConnection.GET);
			 }

//			httpConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			httpConnection.setRequestProperty("Content-Type", MULTIPART_CONTENT_TYPE);
			httpConnection.setRequestProperty("User-Agent", UserAgent.getUserAgent());
			httpConnection.setRequestProperty("MFDEVID", Properties._BB_PIN);
			httpConnection.setRequestProperty("MFLATLON", Properties._LATITUDE+","+Properties._LONGITUDE);
			httpConnection.setRequestProperty("MFSESSID", UserInfoManager.getUserSessionID());
				
			 
			 outputStream = httpConnection.openOutputStream();
			 int offset = 0;
			 int chunk = DATA_CHUNK_SIZE;
			 int len = _multipart_parameters.length;
			 while (offset < len) {
				 if (offset + chunk >= len) {
					chunk = len - offset;
				 }
				 outputStream.write(_multipart_parameters, offset, chunk);
				 offset += chunk;
			 }
			
			if (httpConnection.getResponseCode() == HttpConnection.HTTP_OK) {
				String result = getResponseAsSting(httpConnection);
				//System.out.println("HTTP MULTIPART RETURN>>> "+result);
				if (_callback!=null) _callback.callback(result);
				
				Properties._HTTP_ERROR_CODE = 0;
			} else if (httpConnection.getResponseCode() == HttpConnection.HTTP_NOT_MODIFIED) {
				System.out.println("ERROR CODE1::: "+httpConnection.getResponseCode());
				//304
				Properties._HTTP_ERROR_CODE = 304;
				throw new IOException("HTTP Not Modified");
			} else if (httpConnection.getResponseCode() == HttpConnection.HTTP_BAD_REQUEST) {
				System.out.println("ERROR CODE2::: "+httpConnection.getResponseCode());
				//400
				Properties._HTTP_ERROR_CODE = 400;
		     	throw new IOException("Bad Request");
			} else if (httpConnection.getResponseCode() == HttpConnection.HTTP_UNAUTHORIZED) {
				System.out.println("ERROR CODE3::: "+httpConnection.getResponseCode());
				//401
				Properties._HTTP_ERROR_CODE = 401;
				throw new IOException("FORBIDDEN");
			} else if (httpConnection.getResponseCode() == HttpConnection.HTTP_FORBIDDEN) {
				System.out.println("ERROR CODE4::: "+httpConnection.getResponseCode());
				//403
				Properties._HTTP_ERROR_CODE = 403;
				throw new IOException("FORBIDDEN");
			} else if (httpConnection.getResponseCode() == HttpConnection.HTTP_NOT_FOUND) {
				System.out.println("ERROR CODE5::: "+httpConnection.getResponseCode());
				//404
				Properties._HTTP_ERROR_CODE = 404;
		     	throw new IOException("Not Found");
			} else if (httpConnection.getResponseCode() == HttpConnection.HTTP_BAD_METHOD) {
				System.out.println("ERROR CODE5::: "+httpConnection.getResponseCode());
				//405
				Properties._HTTP_ERROR_CODE = 405;
		     	throw new IOException("Method Not Allowed");
			} else if (httpConnection.getResponseCode() == HttpConnection.HTTP_CLIENT_TIMEOUT) {
				System.out.println("ERROR CODE6::: "+httpConnection.getResponseCode());
				//408
				Properties._HTTP_ERROR_CODE = 408;
		     	throw new IOException("Client Timeout");
			} else if (httpConnection.getResponseCode() == HttpConnection.HTTP_PRECON_FAILED) {
				System.out.println("ERROR CODE7::: "+httpConnection.getResponseCode());
				//412
				Properties._HTTP_ERROR_CODE = 412;
		     	throw new IOException("Precondition Failed");
			} else if (httpConnection.getResponseCode() == HttpConnection.HTTP_INTERNAL_ERROR) {
				System.out.println("ERROR CODE8::: "+httpConnection.getResponseCode());
				//500
				Properties._HTTP_ERROR_CODE = 500;
			    throw new IOException("Internal Server Error");
			} else if (httpConnection.getResponseCode() == HttpConnection.HTTP_BAD_GATEWAY) {
				System.out.println("ERROR CODE9::: "+httpConnection.getResponseCode());
				//502
				Properties._HTTP_ERROR_CODE = 502;
			    throw new IOException("Bad Gateway");
			} 
		} finally {
			safelyCloseStream(outputStream);
			safelyCloseStream(httpConnection);
			if (_multipart_parameters != null) {
				_multipart_parameters = null; // notify VM it can safely free the RAM
			}
		}
	}

	private String getResponseAsSting(HttpConnection conn) throws IOException {
		String result = "";
		InputStream inputStream = null;
		try {
			inputStream = conn.openInputStream();
			int len;
			byte[] data = new byte[512];
			do {
				len = inputStream.read(data);
				if (len > 0) {
					result += new String(data, 0, len);
				}
			} while (len > 0);
		} catch (Exception e) {
			if (e instanceof IOException) {
				//throw (IOException) e;
				System.out.println("[Multipart IOException] "+e);
			}
		} finally {
			safelyCloseStream(inputStream);
		}
		
		//System.out.println("result:" + result);
		_callback.callback(result);
		return result;
	}

	private static void safelyCloseStream(InputStream istream) {
		if (istream != null) {
			try {
				istream.close();
				istream = null;
			} catch (Exception e) { /* that's ok */
			}
		}
	}

	private static void safelyCloseStream(OutputStream ostream) {
		if (ostream != null) {
			try {
				ostream.close();
				ostream = null;
			} catch (Exception e) { /* that's ok */
			}
		}
	}

	private static void safelyCloseStream(HttpConnection con) {
		if (con != null) {
			try {
				con.close();
				con = null;
			} catch (Exception e) { /* that's ok */
			}
		}
	}
}
