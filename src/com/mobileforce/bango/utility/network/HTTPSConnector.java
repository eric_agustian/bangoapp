package com.mobileforce.bango.utility.network;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.io.HttpsConnection;
import javax.microedition.pki.CertificateException;

import com.mobileforce.bango.services.UserInfoManager;
import com.mobileforce.bango.utility.GStringUtil;
import com.mobileforce.bango.utility.JSONCallBack;
import com.mobileforce.bango.utility.Properties;

import net.rim.device.api.system.DeviceInfo;

public class HTTPSConnector {
	String _URL;
	String _parameters;
	String _request_type;
	
	private JSONCallBack _callback = null;
	
	public HTTPSConnector(String url, String _parameter, String request_type, JSONCallBack callback) throws CertificateException, Exception {
		this._URL = url;    	    	
    	this._parameters = _parameter;
    	this._request_type = request_type;
    	this._callback = callback;
    	this.connectToServer();
    }  
	
	public void connectToServer() throws CertificateException, Exception {
		InputStream is = null;
	    HttpsConnection httpsConnection = null;
	    
	    //method selection
	    if(_request_type.equalsIgnoreCase("POST")){
	    	//none
	    }
	    else if(_request_type.equalsIgnoreCase("GET")){
	    	if(!_parameters.equalsIgnoreCase("")) _URL += "?"+_parameters;
	    }
	    
	    //convert URL
	    _URL = GStringUtil.convertURL(_URL);
	    System.out.println("HTTPS URL : "+_URL+ " PARAMS : "+_parameters+" METHOD : "+_request_type);
	    
	    //define HTTPS
	    if(!DeviceInfo.isSimulator()) _URL = _URL + Properties._APN_BB;
	    httpsConnection = (HttpsConnection) Connector.open(_URL, Connector.READ_WRITE);
	    
	    //define content type
		httpsConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		httpsConnection.setRequestProperty("User-Agent", UserAgent.getUserAgent());
		httpsConnection.setRequestProperty("MFDEVID", Properties._BB_PIN);
		httpsConnection.setRequestProperty("MFLATLON", Properties._LATITUDE+","+Properties._LONGITUDE);
		httpsConnection.setRequestProperty("MFSESSID", UserInfoManager.getUserSessionID());
		
	    if(_request_type.equalsIgnoreCase("POST")){
	    	httpsConnection.setRequestMethod(HttpConnection.POST);
	    	
	    	DataOutputStream dos = httpsConnection.openDataOutputStream();
	        dos.write(_parameters.getBytes());
	        dos.flush();
			dos.close();
	    }
	    else if(_request_type.equalsIgnoreCase("GET")){
	    	httpsConnection.setRequestMethod(HttpConnection.GET);
	    }
	    
		if (httpsConnection.getResponseCode() == HttpConnection.HTTP_OK) {
			is = httpsConnection.openInputStream();
            
            //Get the length and process the data
            int len = (int)httpsConnection.getLength();
            if (len > 0) {
                int actual = 0;
                int bytesread = 0 ;
                byte[] data = new byte[len];
                while ((bytesread != len) && (actual != -1)) {
                   actual = is.read(data, bytesread, len - bytesread);
                   bytesread += actual;
                }
                //**********************************************************
                String reply = new String(data, 0, bytesread, "UTF-8");
                System.out.println("RETURN HTTPS MODE 1::: "+reply);
                if(!reply.startsWith("{")){
                	//System.out.println("AUTO COMPLETE JSON RETURN");
                	reply = "{\"array\": "+reply+"}";
                }
    		    if (_callback!=null) _callback.callback(reply);
                //**********************************************************
            } else {
            	//System.out.println("ELSE WHILE");
                int ch;
                ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
                while ((ch = is.read()) != -1) {
                	bytestream.write(ch);
                }
                
                //**********************************************************
                String reply = new String(bytestream.toByteArray(), "UTF-8");
                System.out.println("RETURN HTTPS MODE 2::: "+reply);
                if(!reply.startsWith("{")){
                	//System.out.println("AUTO COMPLETE JSON RETURN");
                	reply = "{\"array\": "+reply+"}";
                }
    		    if (_callback!=null) _callback.callback(reply);
                //**********************************************************
            }
            Properties._HTTP_ERROR_CODE = 0;
		} else if (httpsConnection.getResponseCode() == HttpConnection.HTTP_NOT_MODIFIED) {
			System.out.println("HTTPS ERROR CODE1::: "+httpsConnection.getResponseCode());
			//304
			Properties._HTTP_ERROR_CODE = 304;
			throw new IOException("HTTP Not Modified");
		} else if (httpsConnection.getResponseCode() == HttpConnection.HTTP_BAD_REQUEST) {
			System.out.println("HTTPS ERROR CODE2::: "+httpsConnection.getResponseCode());
			//400
			Properties._HTTP_ERROR_CODE = 400;
	     	throw new IOException("Bad Request");
		} else if (httpsConnection.getResponseCode() == HttpConnection.HTTP_UNAUTHORIZED) {
			System.out.println("HTTPS ERROR CODE3::: "+httpsConnection.getResponseCode());
			//401
			Properties._HTTP_ERROR_CODE = 401;
			throw new IOException("FORBIDDEN");
		} else if (httpsConnection.getResponseCode() == HttpConnection.HTTP_FORBIDDEN) {
			System.out.println("HTTPS ERROR CODE4::: "+httpsConnection.getResponseCode());
			//403
			Properties._HTTP_ERROR_CODE = 403;
			throw new IOException("FORBIDDEN");
		} else if (httpsConnection.getResponseCode() == HttpConnection.HTTP_NOT_FOUND) {
			System.out.println("HTTPS ERROR CODE5::: "+httpsConnection.getResponseCode());
			//404
			Properties._HTTP_ERROR_CODE = 404;
	     	throw new IOException("Not Found");
		} else if (httpsConnection.getResponseCode() == HttpConnection.HTTP_BAD_METHOD) {
			System.out.println("HTTPS ERROR CODE5::: "+httpsConnection.getResponseCode());
			//405
			Properties._HTTP_ERROR_CODE = 405;
	     	throw new IOException("Method Not Allowed");
		} else if (httpsConnection.getResponseCode() == HttpConnection.HTTP_CLIENT_TIMEOUT) {
			System.out.println("HTTPS ERROR CODE6::: "+httpsConnection.getResponseCode());
			//408
			Properties._HTTP_ERROR_CODE = 408;
	     	throw new IOException("Client Timeout");
		} else if (httpsConnection.getResponseCode() == HttpConnection.HTTP_PRECON_FAILED) {
			System.out.println("HTTPS ERROR CODE7::: "+httpsConnection.getResponseCode());
			//412
			Properties._HTTP_ERROR_CODE = 412;
	     	throw new IOException("Precondition Failed");
		} else if (httpsConnection.getResponseCode() == HttpConnection.HTTP_INTERNAL_ERROR) {
			System.out.println("HTTPS ERROR CODE8::: "+httpsConnection.getResponseCode());
			//500
			Properties._HTTP_ERROR_CODE = 500;
		    throw new IOException("Internal Server Error");
		} else if (httpsConnection.getResponseCode() == HttpConnection.HTTP_BAD_GATEWAY) {
			System.out.println("HTTPS ERROR CODE9::: "+httpsConnection.getResponseCode());
			//502
			Properties._HTTP_ERROR_CODE = 502;
		    throw new IOException("Bad Gateway");
		} 
		
		//close all connections***
		httpsConnection.close();	   
		is.close();
		//************************
     }
}
