package com.mobileforce.bango.utility.network;

import com.mobileforce.bango.utility.Properties;


public class UserAgent {
	public static String getUserAgent(){
		String result = "";
		
		//System.out.println("OS>>> "+Properties._BB_OS);
		//System.out.println("Platform>>> "+DeviceInfo.getPlatformVersion());
		//System.out.println("USER AGENT>>> "+System.getProperty("browser.useragent"));
		
//		//BB dng OS 5 kebawah
//		if(Properties._BB_OS.startsWith("4") || Properties._BB_OS.startsWith("5")){
//			result = "BlackBerry" + Properties._BB_TYPE + "/" + Properties._BB_OS + " Profile/" + System.getProperty("microedition.profiles") + " Configuration/" + System.getProperty("microedition.configuration") + " VendorID/" + Branding.getVendorId();
//		}
//		else{
//			//OS 6 & 7
//			//result = "Mozilla/5.0 (BlackBerry; U; BlackBerry " + Properties._BB_TYPE + "; en-US) AppleWebKit/534.1+ (KHTML, like Gecko) Version/" + Properties._BB_OS + " Mobile Safari/534.1+";
//			result = System.getProperty("browser.useragent");
//		}
		
		result = Properties._APP_NAME + "/" + Properties._APP_VERSION + " " + "BlackBerry" + "/" + Properties._BB_OS;
		
		System.out.println("User-Agent>>> "+result);
		return result;
	}
}
